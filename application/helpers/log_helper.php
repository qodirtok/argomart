<?php
function is_login() //memvalidasi akses login
{
    $log = get_instance();
    if (!$log->session->userdata('username')) {
        redirect('auth');
    } else {
        $id_lvl = $log->session->userdata('id_lvl');
        $akses = $log->uri->segment('1');

        $query = $log->db->get_where('tb_user', ['id_lvl' => $id_lvl])->row_array();
        $menuId = $query['id_lvl'];

        $level = $log->db->get_where('tb_lvl', [
            'id_lvl' => $menuId,
            'id_lvl' => $akses
        ])->row_array();

        if ($level['id_lvl'] == 2) {
            redirect('auth/blocked');
        }
    }
}

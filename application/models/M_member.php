<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_member extends CI_Model
{

	private $_table = "tb_member"; //Nama tabel

	public $Kode_member;
	public $Nama;
	public $Nohp;
	public $Alamat;

	public function rules() // digunakan untuk validatornya 
	{
		# code...

		return [
			[
				'field' => 'Kode_member',
				'label' => 'Kode_member',
				'rules' => 'required'
			],
			[
				'field' => 'Nama',
				'label' => 'Nama',
				'rules' => 'required'
			],
			[
				'field' => 'Nohp',
				'label' => 'Nohp',
				'rules' => 'numeric'
			],
			[
				'field' => 'Alamat',
				'label' => 'Alamat',
				'rules' => 'required'
			]
		];
	}

	public function createKode() // digunakan untuk membuat kode member
	{
		# code...
		$this->db->SELECT('RIGHT(tb_member.Kode_member,4) as kode', FALSE);
		$this->db->order_by('Kode_member', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('tb_member');
		if ($query->num_rows() <> 0) {
			// jika kodesudah ada
			$data = $query->row();
			$kode = intval($data->kode) + 1;
		} else {
			//jika kode belum ada
			$kode = 1;
		}

		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 ini nanti digunakan untuk mengatur banyaknya digit 
		$kodejadi = "MDDMRKT-KDM-" . $kodemax;

		return $kodejadi;
	}

	public function getAll() //mengambil semua data
	{
		# code...
		return $this->db->get($this->_table)->result();
	}

	public function getById($id) //mengambil data berdasarkan id
	{
		# code...
		return $this->db->get_where($this->_table, ["id_member" => $id])->row();
	}

	public function save() //tambah data member
	{
		# code...
		$_POST = $this->input->post();
		$this->Kode_member = $_POST['Kode_member'];
		$this->Nama = $_POST['Nama'];
		$this->Nohp = $_POST['Nohp'];
		$this->Alamat = $_POST['Alamat'];
		$this->db->insert($this->_table, $this);
	}

	public function M_update() // mengupdate data dari database
	{
		# code...
		$_POST = $this->input->post();
		// $this->id_member = $_POST['id_member'];
		$this->Kode_member = $_POST['Kode_member'];
		$this->Nama = $_POST['Nama'];
		$this->Nohp = $_POST['Nohp'];
		$this->Alamat = $_POST['Alamat'];

		$this->db->update($this->_table, $this, array('id_member' => $_POST['id_member']));
	}

	public function delete($id)
	{
		return $this->db->delete($this->_table, array('id_member' => $id));
	}
}

/* End of file M_member.php */

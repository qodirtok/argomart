<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_returjual extends CI_Model
{
    private $_table     = 'tb_penjualan';
    private $_tb        = 'tb_retur_penjualan';
    private $_tbjurnal  = 'tb_jurnal';

    public function rules() // digunakan untuk validatornya 
    {
        # code...

        return [

            [
                'field' => 'jumlah',
                'label' => 'jumlah',
                'rules' => 'numeric'
            ]
        ];
    }


    function getAll()
    {
        $m = date('m');
        $this->db->JOIN('tb_member', 'tb_member.id_member=tb_penjualan.id_member');
        $this->db->where('tb_penjualan.id_member !=', 13);
        $this->db->where('MONTH(tgl_penjualan)', $m);
        // $this->db->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur=tb_penjualan.no_faktur');
        return $this->db->get($this->_table)->result();
    }


    function getRetur()
    {
        $this->db->select('*,sum(jumlah) as jml')
            ->from('tb_retur_penjualan')
            ->group_by('no_faktur');
        return $this->db->get()->result();
    }

    function detail($id)
    {
        $this->db->JOIN('tb_member', 'tb_member.id_member=tb_penjualan.id_member');
        $this->db->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur=tb_penjualan.no_faktur');
        return $this->db->get_where($this->_table, ["no_faktur" => $id])->row();
    }

    function detaill($id)
    {
        $this->db->SELECT('*')
            ->FROM('tb_penjualan')
            ->JOIN('tb_member', 'tb_member.id_member=tb_penjualan.id_member')
            ->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur=tb_penjualan.no_faktur')
            ->WHERE('tb_penjualan.no_faktur', $id);

        $hsl = $this->db->get()->result();
        return $hsl;
    }
    public function save() //tambah data member
    {
        # code...

        $metode =  $_POST['metode'];
        $retur = [
            'no_faktur'    => $_POST['nofak'],
            'Kode_barang'  => $_POST['kode'],
            'nama'         => $_POST['nama'],
            'jumlah'       => $_POST['jumlah'],
            'harga'        => $_POST['harga'],
            'tanggal'      => date('Y-m-d'),
            'sub_total'    => ($_POST['jumlah'] * $_POST['harga']),
            'id_member'    => $_POST['id_member'],
            'id_user'      => $_POST['id_user']
        ];
        $tukbard = [
            'tanggal' => date('Y-m-d'),
            'bukti' => 'RJ' . $_POST['nofak'],
            'kode_akun' => '4201',
            'ket' => 'Retur Penjualan ' . $_POST['nama'],
            'kredit' => 0,
            'debet' => $_POST['harbel'],
            'id_user' => $_POST['id_user']
        ];

        $tukbark = [
            'tanggal' => date('Y-m-d'),
            'bukti' => 'RJ' . $_POST['nofak'],
            'kode_akun' => '1141',
            'ket' => 'Retur Penjualan ' . $_POST['nama'],
            'kredit' => $_POST['harbel'],
            'debet' => 0,
            'id_user' => $_POST['id_user']
        ];
        $tunaid = [
            'tanggal' => date('Y-m-d'),
            'bukti' => 'RJ' . $_POST['nofak'],
            'kode_akun' => '4201',
            'ket' => 'Retur Penjualan ' . $_POST['nama'],
            'kredit' => 0,
            'debet' => $_POST['harga'],
            'id_user' => $_POST['id_user']
        ];

        $tunaik = [
            'tanggal' => date('Y-m-d'),
            'bukti' => 'RJ' . $_POST['nofak'],
            'kode_akun' => '1111',
            'ket' => 'Retur Penjualan ' . $_POST['nama'],
            'kredit' => $_POST['harga'],
            'debet' => 0,
            'id_user' => $_POST['id_user']
        ];
        $piutid = [
            'tanggal' => date('Y-m-d'),
            'bukti' => 'RJ' . $_POST['nofak'],
            'kode_akun' => '4201',
            'ket' => 'Retur Penjualan ' . $_POST['nama'],
            'kredit' => 0,
            'debet' => $_POST['harga'],
            'id_user' => $_POST['id_user']
        ];

        $piutk = [
            'tanggal' => date('Y-m-d'),
            'bukti' => 'RJ' . $_POST['nofak'],
            'kode_akun' => '1131',
            'ket' => 'Retur Penjualan ' . $_POST['nama'],
            'kredit' => $_POST['harga'],
            'debet' => 0,
            'id_user' => $_POST['id_user']
        ];

        $uhutang = [
            'sisa_bayar' => $_POST['old_hutang'] - $_POST['harga']
        ];
        $ubarang = [
            'stok' => $_POST['stok'] - $_POST['jumlah']
        ];

        if ($metode == 1) { //tukar barang
            $this->db->insert($this->_tbjurnal, $tukbard);
            $this->db->insert($this->_tbjurnal, $tukbark);
            $this->db->update('tb_barang', $ubarang, ['Kode_barang' => $_POST['kode']]);
        } else if ($metode == 2) { //tunai
            $this->db->insert($this->_tbjurnal, $tunaid);
            $this->db->insert($this->_tbjurnal, $tunaik);
        } else { //kurangi piutang
            $this->db->insert($this->_tbjurnal, $piutid);
            $this->db->insert($this->_tbjurnal, $piutk);
            $this->db->update('tb_hutang', $uhutang, ['no_faktur' => $_POST['nofak']]);
            $this->db->update('tb_hutang_tmp', $uhutang, ['no_faktur' => $_POST['nofak']]);
        }

        $this->db->insert($this->_tb, $retur);
    }
}

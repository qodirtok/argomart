<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_lporstok extends CI_Model
{
    public function rules() // digunakan untuk validatornya 
    {
        return [

            [
                'field' => 'awal',
                'label' => 'awal',
                'rules' => 'required'
            ],
            [
                'field' => 'akhir',
                'label' => 'akhir',
                'rules' => 'required'
            ]
        ];
    }

    public function stok($awal, $akhir)
    {
        $this->db->select('*, SUM(tb_detail_pembelian.jumlah) AS masuk');
        $this->db->from('tb_pembelian');
        $this->db->join('tb_detail_pembelian', 'tb_detail_pembelian.id_pembelian=tb_pembelian.id_pembelian');
        $this->db->JOIN('tb_barang', 'tb_barang.Kode_barang=tb_detail_pembelian.Kode_barang');
        $this->db->JOIN('tb_satuan', ' tb_satuan.id_satuan=tb_barang.id_satuan');
        $this->db->where('tb_pembelian.tgl_beli >=', $awal);
        $this->db->where('tb_pembelian.tgl_beli <=', $akhir);
        // $this->db->where('tb_detail_penjualan.tgl_jual >=', $awal);
        // $this->db->where('tb_detail_penjualan.tgl_jual <=', $akhir);
        $this->db->group_by('tb_detail_pembelian.Kode_barang');
        return $stok = $this->db->get()->result();
    }
}

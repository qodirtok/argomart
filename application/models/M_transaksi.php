<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transaksi extends CI_Model
{

	private $_table = "tb_penjualan"; //Nama tabel penjualan
	private $_table2 = "tb_barang";
	private $_tabledetailjul = " tb_detail_penjualan";
	private $_tbjurnal = "tb_jurnal";
	private $_tbhutang = "tb_hutang_tmp";
	private $tbpembayaran = "tb_pembayaran";

	// public $tgl;
	public $no_faktur;
	public $Kode_barang;
	public $jumlah;
	public $satuan;
	public $harga;
	public $diskon;

	public function rules() // digunakan untuk validatornya 
	{
		# code...

		return [
			[
				'field' => 'no_faktur',
				'label' => 'no_faktur',
				'rules' => 'required'
			],
			[
				'field' => 'kode_brg',
				'label' => 'kode_brg',
				'rules' => 'required'
			],
			[
				'field' => 'qty',
				'label' => 'Qty',
				'rules' => 'required'
			],
			[
				'field' => 'satuan',
				'label' => 'Satuan',
				'rules' => 'required'
			],
			[
				'field' => 'hrg_jual',
				'label' => 'Hrg_jual',
				'rules' => 'required'
			],
			[
				'field' => 'diskon',
				'label' => 'Diskon',
				'rules' => 'required'
			],
			[
				'field' => 'stok',
				'label' => 'Stok',
				'rules' => 'required'
			]
		];
	}

	public function pembayaran($idpem)
	{
		$datas = $this->db->query("SELECT * FROM tb_pembayaran WHERE id_pembayaran='$idpem'");
		if ($datas->num_rows() > 0) {
			foreach ($datas->result() as $data) {
				$hasil = array(
					'id_pembayaran' => $data->id_pembayaran,
					'nama_pembayaran' => $data->nama_pembayaran,
					// 'Nohp' => $data->Nohp,
					// 'harga' => $data->harga,
					// 'satuan' => $data->satuan,
				);
			}
		}
		return $hasil;
	}

	public function pengiriman($kirim)
	{
		$datas = $this->db->query("SELECT * FROM tb_pengiriman WHERE id_pengiriman='$kirim'");
		if ($datas->num_rows() > 0) {
			foreach ($datas->result() as $data) {
				$hasil = array(
					'id_pengiriman' => $data->id_pengiriman,
					'nama_pengiriman' => $data->nama_pengiriman,
					// 'Nohp' => $data->Nohp,
					// 'harga' => $data->harga,
					// 'satuan' => $data->satuan,
				);
			}
		}
		return $hasil;
	}

	public function getAllpenjualan()
	{
		$this->db->JOIN('tb_barang', 'tb_barang.id_barang=tb_penjualan.id_barang');
		$detail = $this->db->get($this->_tabel)->result();
		return $detail;
	}


	public function selectByCart($kobar)
	{
		# code...
		$this->db->SELECT('jumlah')
			->FROM('tb_penjualan_tmp')
			->WHERE('Kode_barang', $kobar);
	}

	public function cartadd()
	{
		$_POST = $this->input->post();
		$this->no_faktur = $_POST['no_faktur'];
		$this->Kode_barang = $_POST['kode_brg'];
		$this->harga = str_replace(",", "", $_POST['hrg_jual']) - $_POST['diskon'];
		$this->jumlah = $_POST['qty'];
		$this->satuan = $_POST['satuan'];
		$this->diskon = $_POST['diskon'];
		// echo var_dump($this);
		// die;
		$this->db->insert($this->_tabel1, $this);
	}

	public function getByKodeDiskon($kobar)
	{
		# code...
		$this->db->SELECT('*')
			->FROM('tb_barang')
			->JOIN('tb_satuan', 'tb_satuan.id_satuan=tb_barang.id_satuan')
			->JOIN('tb_diskon', 'tb_diskon.id_barang = tb_barang.id_barang', 'RIGHT')
			->WHERE('Kode_barang', $kobar);

		$query = $this->db->get();
		return $query;
	}

	public function cartUpdate($kobar)
	{
		$_POST = $this->input->post();
		$data = $_POST['qty'];

		$up = array(
			'jumlah' => $data
		);

		$this->db->WHERE('Kode_barang', $kobar)
			->UPDATE($this->_tabel1, $up);
	}

	public function getdatatr($kobar)
	{
		return $this->db->get_where($this->_tabel1, ["Kode_barang" => $kobar])->result_array();
	}


	public function krngstok($kobar)
	{
		$_POST = $this->input->post();
		$krng = ($_POST['stok'] - $_POST['qty']);

		$data = array(
			'stok' => $krng
		);

		$this->db->WHERE('Kode_barang', $kobar)
			->UPDATE($this->_table2, $data);
	}

	public function getAlldetailId($id)
	{
		return $this->db->get_where($this->_tabel1, ["id_pnjualan_tmp" => $id])->row();
	}

	public function delete($id)
	{
		return $this->db->delete($this->_tabel1, array('id_pnjualan_tmp' => $id));
	}

	function simpan_penjualan($nofak, $tgl, $total, $jml_uang, $kembalian, $idmember, $lunas, $kirim)
	{

		date_default_timezone_set('Asia/Jakarta');
		$jam = date("H:i:s");
		$idadmin = $this->session->userdata('id_user');
		$status = 'Proses';
		$keterangan = 'Grosir';
		if ($kirim == 2) {
			$this->db->query("INSERT INTO tb_pengirim (no_faktur,tgl_kirim,id_member,status) VALUES ('$nofak','$tgl', '$idmember','$status')");
		}
		$this->db->query("INSERT INTO tb_penjualan (no_faktur,tgl_penjualan,jam_penjualan,jmlh_uang,total_penjualan,kembalian,keterangan_jual,id_member,id_user) VALUES ('$nofak','$tgl','$jam','$jml_uang','$total','$kembalian','$keterangan','$idmember','$idadmin')");
		foreach ($this->cart->contents() as $item) {
			$qty = $item['qty'];
			$harjul = $item['harjul'];
			$harjulgro = $item['hrgjualgrosir'];
			if ($qty >= 3) {
				$hrgjul = $harjulgro;
				$price = (((100 - $item['disc']) / 100) * str_replace(",", "", $hrgjul));
				$total = $price * $qty;
			} else {
				$hrgjul = $harjul;
				$total = $harjul * $qty;
			}
			$data = array(
				'no_faktur' 			=>	$nofak,
				'tgl_jual'				=> 	$tgl,
				'Kode_barang'			=>	$item['id'],
				'nama'			 		=>	$item['name'],
				'satuan'			    =>	$item['satuan'],
				'hrg_pokok'				=>	$item['harpok'],
				'harga'					=>	$hrgjul,
				'jumlah'			    =>	$qty,
				'diskon'				=>	$item['disc'],
				'sub_total'				=>	$total
			);
			$kas = [
				'kode_akun'  => '1111',
				'debet'  	 => $total,
				'kredit'  	 => 0,
				'bukti'		 => 'JL' . $nofak,
				'tanggal'	 => date('Y-m-d'),
				'ket'		 => 'Penjualan Dengan No.Faktur : ' . $nofak,
				'id_user'	 => $idadmin
			];

			$hutang = [
				'id_member'  => $idmember,
				'dibayar'    => 0,
				'sisa_bayar' => $total,
				'no_faktur'  => $nofak
			];

			$piutang = [
				'kode_akun'  => '1131',
				'debet'  	 => $total,
				'kredit'  	 => 0,
				'bukti'		 => 'PU' . $nofak,
				'tanggal'	 => date('Y-m-d'),
				'ket'		 => 'Piutang Penjualan No.Faktur : ' . $nofak,
				'id_user'	 => $idadmin
			];
			$penjualan = [
				'kode_akun'  => '4101',
				'debet'  	 => 0,
				'kredit'  	 => $total,
				'bukti'		 => 'JL' . $nofak,
				'tanggal'	 => date('Y-m-d'),
				'ket'		 => 'Penjualan Dengan No.Faktur : ' . $nofak,
				'id_user'	 => $idadmin
			];
			$hpp = [
				'kode_akun'  => '5101',
				'debet'		 =>	$item['harpok'] * $item['qty'],
				'kredit'  	 => 0,
				'bukti'		 => 'JL' . $nofak,
				'tanggal'	 => date('Y-m-d'),
				'ket'		 => 'Penjualan Dengan No.Faktur : ' . $nofak,
				'id_user'	 => $idadmin
			];
			$persedian = [
				'kode_akun'  => '1141',
				'debet'  	 => 0,
				'kredit'     =>	$item['harpok'] * $item['qty'],
				'bukti'		 => 'JL' . $nofak,
				'tanggal'    => date('Y-m-d'),
				'ket'		 => 'Penjualan Dengan No.Faktur : ' . $nofak,
				'id_user'	 => $idadmin
			];
			if ($lunas == 1) {
				$this->db->insert($this->_tbjurnal, $kas);
			} else {
				$this->db->insert($this->_tbjurnal, $piutang);
				$this->db->insert($this->_tbhutang, $hutang);
			}
			$this->db->insert($this->_tbjurnal, $penjualan);
			$this->db->insert($this->_tbjurnal, $hpp);
			$this->db->insert($this->_tbjurnal, $persedian);
			$this->db->insert($this->_tabledetailjul, $data);
			$this->db->query("update tb_barang set stok=stok-'$item[qty]' where Kode_barang='$item[id]'");
		}
		return true;
	}

	function update($id, $value, $modul)
	{

		$this->db->where(array("id_pnjualan_tmp" => $id));
		$this->db->update("tb_penjualan_tmp", array($modul => $value));
	}

	function createKode()
	{
		$q = $this->db->query("SELECT MAX(RIGHT(no_faktur,4)) AS kd_max FROM tb_penjualan WHERE DATE(tgl_penjualan)=CURDATE()");
		$kd = "";
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $k) {
				$tmp = ((int) $k->kd_max) + 1;
				$kd = sprintf("%04s", $tmp);
			}
		} else {
			$kd = "0001";
		}
		return date('dmy') . $kd;
	}


	function hapuscart()
	{
		return $this->db->empty_table($this->_tabel1);
	}

	function strukcetak($nofak)
	{
		$this->db->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur=tb_penjualan.no_faktur');
		return $cetak = $this->db->get_where($this->_table, ["tb_penjualan.no_faktur" => $nofak])->result();
	}

	function get_member($idmember)
	{
		$hsl = $this->db->query("SELECT * FROM tb_member WHERE id_member='$idmember'");
		if ($hsl->num_rows() > 0) {
			foreach ($hsl->result() as $data) {
				$hasil = array(
					'id_member' => $data->id_member,
					'Alamat' => $data->Alamat,
					'Nohp' => $data->Nohp,
					// 'harga' => $data->harga,
					// 'satuan' => $data->satuan,
				);
			}
		}
		return $hasil;
	}

	function pengirim($nofak)
	{
		$this->db->JOIN('tb_member', 'tb_member.id_member=tb_pengirim.id_member');
		return $this->db->get_where('tb_pengirim', ["tb_pengirim.no_faktur" => $nofak])->result();
	}

	function get_subkategori($id)
	{
		$hasil = $this->db->query("SELECT * FROM tb_member WHERE id_kategori='$id'");
		return $hasil->result();
	}
}

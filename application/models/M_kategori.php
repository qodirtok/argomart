<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_kategori extends CI_Model
{

	private $_table = "tb_katagori"; //Nama tabel
	
	public $nma_katagori;
	
	public function rules() // digunakan untuk validatornya 
	{
		# code...

		return [

			[
				'field' => 'Nama',
				'label' => 'Kategori',
				'rules' => 'required'
			]
		];
	}

	

	public function getAll() //mengambil semua data
	{
		# code...
		return $this->db->get($this->_table)->result();
	}

	public function getById($id) //mengambil data berdasarkan id
	{
		# code...
		return $this->db->get_where($this->_table, ["id_katagori" => $id])->row();
	}

	public function save() //tambah data member
	{
		# code...
		$_POST = $this->input->post();		
		$this->nma_katagori  = $_POST['Nama'];		
		$this->db->insert($this->_table, $this);
	}

	public function M_update() // mengupdate data dari database
	{
		# code...
		$_POST = $this->input->post();
		$this->id_katagori   = $_POST['id_katagori'];		
		$this->nma_katagori  = $_POST['Nama'];
		$this->db->update($this->_table, $this, array('id_katagori' => $_POST['id_katagori']));
	}

	public function delete($id)
	{
		return $this->db->delete($this->_table, array('id_katagori' => $id));
	}
}

/* End of file M_member.php */

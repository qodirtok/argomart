<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_satuan extends CI_Model
{

	private $_table = 'tb_satuan';

	public $nma_satuan;

	public function rules()
	{
		# code...
		return [
			[
				'field' => 'nma_satuan',
				'label' => 'nma_satuan',
				'rules' => 'required'
			]
		];
	}

	public function getAll()
	{
		# code...
		return $this->db->get($this->_table)->result();
	}

	public function getById($id)
	{
		# code...
		$this->db->SELECT('*')
			->FROM($this->_table)
			->WHERE('id_satuan', $id);

		$query = $this->db->get();
		return $query->result();
	}

	public function save()
	{
		# code...
		$_POST = $this->input->post();

		$this->nma_satuan = $_POST['nma_satuan'];

		$this->db->INSERT($this->_table, $this);
	}

	public function update()
	{
		$_POST = $this->input->post();

		$data = array(
			'nma_satuan' => $_POST['nma_satuan']
		);

		$this->db->WHERE('id_satuan', $_POST['id_satuan'])
			->UPDATE($this->_table, $data);
	}

	public function delete($id)
	{
		# code...
		$this->db->WHERE('id_satuan', $id)
			->DELETE($this->_table);
	}
}

/* End of file M_satuan.php */

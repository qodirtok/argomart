<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_labarugi extends CI_Model
{




    public function rules() // digunakan untuk validatornya 
    {
        # code...

        return [

            [
                'field' => 'awal',
                'label' => 'awal',
                'rules' => 'required'
            ],
            [
                'field' => 'akhir',
                'label' => 'akhir',
                'rules' => 'required'
            ]
        ];
    }

    function getPenjualan($awal, $akhir) //ambil data penjualan
    {

        $this->db->select('*,SUM(debet) AS D,SUM(kredit) AS K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal >=', $awal);
        $this->db->where('tanggal <=', $akhir);
        $this->db->where('kelompok  ', 'LR');
        $this->db->where('tb_akun.kode_akun', '4101');
        return $this->db->get()->result();
    }
    function getHapok($awal, $akhir) //ambil data haraga pokok penjualan
    {
        $this->db->select('*,SUM(debet) as D,SUM(kredit)as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal >=', $awal);
        $this->db->where('tanggal <=', $akhir);
        $this->db->where('kelompok  ', 'LR');
        $this->db->where('tb_akun.kode_akun', '5101');
        return $this->db->get()->result();
    }
    function getRePo($awal, $akhir) //ambil data Retur & Potongan Penjualan
    {
        $this->db->select('*,SUM(debet) as D,SUM(kredit)as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal >=', $awal);
        $this->db->where('tanggal <=', $akhir);
        $this->db->like('tb_jurnal.kode_akun', '42');
        $this->db->group_by('tb_jurnal.kode_akun');
        return $this->db->get()->result();
    }
    function getBiaya($awal, $akhir) //ambil data biaya
    {
        $this->db->select('*,SUM(debet) as D,SUM(kredit) as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal >=', $awal);
        $this->db->where('tanggal <=', $akhir);
        $this->db->like('tb_jurnal.kode_akun', '52');
        $this->db->group_by('tb_jurnal.kode_akun');
        return $this->db->get()->result();
    }
    function getPendLain($awal, $akhir) //ambil data biaya
    {
        $this->db->select('*,SUM(debet) as D,SUM(kredit) as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal >=', $awal);
        $this->db->where('tanggal <=', $akhir);
        $this->db->where('tb_jurnal.kode_akun', '6101');

        return $this->db->get()->result();
    }
}

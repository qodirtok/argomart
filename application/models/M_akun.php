<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_akun extends CI_Model
{
    public function GetallAkun()
    {
        $this->db->FROM('tb_user');
        $this->db->JOIN('tb_lvl', 'tb_lvl.id_lvl = tb_user.id_lvl');
        $this->db->where('tb_user.id_lvl!=', 5);
        $this->db->order_by('tb_user.nama', 'ASC');
        return $this->db->get()->result();
    }

    public function GetallAkunId($id)
    {
        return $this->db->get_where('tb_user', ["id_user" => $id])->row();
    }

    public function hpsAkun($id)
    {
        return $this->db->delete('tb_user', array('id_user' => $id));
    }

    public function ubah($id)
    {
        $config['max_size'] = 2048;
        $config['allowed_types'] = "png|jpg|jpeg|gif";
        $config['remove_spaces'] = TRUE;
        $config['overwrite'] = TRUE;
        $config['upload_path'] = './asset/img';

        $this->load->library('upload');
        $this->upload->initialize($config);

        //ambil data image
        if (!$this->upload->do_upload('image')) {
            $pict = "default.jpg";
        } else {
            $data_image = $this->upload->data('file_name');
            $pict = $data_image;
        }

        // echo json_encode($pict);
        // var_dump($this->upload->do_upload('image'));
        // die;

        $data = [
            'nama' => $this->input->post('nama'),
            'jk' => $this->input->post('jk'),
            'nohp' => $this->input->post('nohp'),
            'alamat' => $this->input->post('alamat'),
            'username' => $this->input->post('username'),
            'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'image' => $pict,
            'id_lvl' => $this->input->post('hk')
        ];
        $this->db->WHERE('id_user', $id);
        $this->db->update('tb_user', $data);
    }
}

/* End of file M_akun.php */

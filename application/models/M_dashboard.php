<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_dashboard extends CI_Model
{
    function getBarang($id)
    {
        $tgl = date('Y-m-d');
        $this->db->select('*,sum(jumlah) as jml')
            ->from('tb_detail_penjualan')
            ->join('tb_penjualan', 'tb_detail_penjualan.no_faktur=tb_penjualan.no_faktur')
            ->where('tgl_penjualan', $tgl)
            ->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getPenjualan($id)
    {
        $tgl = date('Y-m-d');
        $this->db->select('*,sum(total_penjualan) as total')
            ->from('tb_penjualan')
            ->where('tgl_penjualan', $tgl)
            ->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getKK($id)
    {
        $tgl = date('Y-m-d');
        $this->db->select('*,sum(jumlah) as jml')
            ->from('tb_kaskeluar')
            ->where('tanggal', $tgl)
            ->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getKM($id)
    {
        $tgl = date('Y-m-d');
        $this->db->select('*,sum(jumlah) as jml')
            ->from('tb_kasmasuk')
            ->where('tanggal', $tgl)
            ->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getPembelian($id)
    {
        $tgl = date('Y-m-d');
        $this->db->select('*,sum(total) as harga')
            ->from('tb_detail_pembelian')
            ->join('tb_pembelian', 'tb_detail_pembelian.id_pembelian=tb_pembelian.id_p')
            ->where('tgl_beli', $tgl)
            ->where('id_user', $id);
        return $this->db->get()->result();
    }

    function getJual($id) //ambil data penjualan
    {
        $tgl = date('Y-m-d');
        $this->db->select('*,SUM(debet) AS D,SUM(kredit) AS K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal', $tgl);
        $this->db->where('tb_akun.kode_akun', '4101');
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getHapok($id) //ambil data haraga pokok penjualan
    {
        $tgl = date('Y-m-d');
        $this->db->select('*,SUM(debet) as D,SUM(kredit)as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal', $tgl);
        $this->db->where('tb_akun.kode_akun', '5101');
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }

    function barangTersedia()
    {
        $this->db->select('*, COUNT(Kode_barang) AS jumlah');
        $this->db->from('tb_barang');
        $this->db->where('stok>=', '50');
        return $this->db->get()->result();
    }
    function bt()
    {
        $this->db->from('tb_barang');
        $this->db->join('tb_satuan', 'tb_satuan.id_satuan=tb_barang.id_satuan');
        $this->db->join('tb_katagori', 'tb_katagori.id_katagori=tb_barang.id_katagori');
        $this->db->where('tb_barang.stok>=', '50');
        return $this->db->get()->result();
    }
    function bhh()
    {
        $this->db->select('*, COUNT(Kode_barang) AS jumlah');
        $this->db->from('tb_barang');
        $this->db->where('stok>=', 10);
        $this->db->where('stok<=', 50);
        return $this->db->get()->result();
    }
    function bhhabis()
    {
        $this->db->from('tb_barang');
        $this->db->join('tb_satuan', 'tb_satuan.id_satuan=tb_barang.id_satuan');
        $this->db->join('tb_katagori', 'tb_katagori.id_katagori=tb_barang.id_katagori');
        $this->db->where('stok>=', 10);
        $this->db->where('stok<=', 50);
        return $this->db->get()->result();
    }
    function bhs()
    {
        $this->db->select('*, COUNT(Kode_barang) AS jumlah');
        $this->db->from('tb_barang');
        $this->db->where('stok<=', 10);
        return $jmlh = $this->db->get()->result();
    }
    function bhabis()
    {
        $this->db->from('tb_barang');
        $this->db->join('tb_satuan', 'tb_satuan.id_satuan=tb_barang.id_satuan');
        $this->db->join('tb_katagori', 'tb_katagori.id_katagori=tb_barang.id_katagori');
        $this->db->where('stok<=', 10);
        return $jmlh = $this->db->get()->result();
    }

    // grafik
    function grafikpenjualan()
    {
        // $Y = date('Y');
        // $this->db->select('*');
        // $this->db->select('MONTH(tb_penjualan.tgl_penjualan) as bulan,sum(tb_penjualan.total_penjualan) as Pendapatan , (sum(tb_detail_penjualan.harga - tb_detail_penjualan.hrg_pokok)*sum(tb_detail_penjualan.jumlah)) as Keuntungan');
        // $this->db->from('tb_penjualan');
        // $this->db->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur = tb_penjualan.no_faktur');
        // $this->db->where('YEAR(tgl_penjualan)', $Y);
        // $this->db->group_by('MONTH(tgl_penjualan)');

        // return $result = $this->db->get()->result();
        $Y = date('Y');
        $this->db->select('*');
        $this->db->select('MONTH(tb_penjualan.tgl_penjualan) as bulan,sum(tb_detail_penjualan.sub_total) as Pendapatan , sum((tb_detail_penjualan.harga - tb_detail_penjualan.hrg_pokok) * tb_detail_penjualan.jumlah) as Keuntungan');
        $this->db->from('tb_penjualan');
        $this->db->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur = tb_penjualan.no_faktur');
        $this->db->where('YEAR(tgl_penjualan)', $Y);
        $this->db->group_by('MONTH(tgl_penjualan)');

        return $result = $this->db->get()->result();
    }
}

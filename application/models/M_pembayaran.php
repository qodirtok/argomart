<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pembayaran extends CI_Model
{
    public function rules() // digunakan untuk validatornya 
    {
        # code...

        return [

            [
                'field' => 'awal',
                'label' => 'awal',
                'rules' => 'required'
            ],
            [
                'field' => 'akhir',
                'label' => 'akhir',
                'rules' => 'required'
            ]
        ];
    }


    function getPemCash($awal, $akhir)
    {
        $this->db->select('*')
            ->from('tb_jurnal')
            ->join('tb_pembelian', 'tb_jurnal.bukti=tb_pembelian.id_pembelian')
            ->join('tb_supplier', 'tb_pembelian.id_supplier=tb_supplier.id_supplier')
            ->where('tanggal >=', $awal)
            ->where('tanggal <=', $akhir)
            ->where('kode_akun =', 1111)
            ->where('debet =', 0);
        return $this->db->get()->result();
    }
    function getPemKredit($awal, $akhir)
    {
        $this->db->select('*')
            ->from('tb_jurnal')
            ->where('tanggal >=', $awal)
            ->where('tanggal <=', $akhir)
            ->where('kode_akun =', 2101)
            ->where('kredit =', 0);
        return $this->db->get()->result();
    }


    function getPembelian($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_pembelian');
        $this->db->join('tb_detail_pembelian', 'tb_pembelian.id_pembelian=tb_detail_pembelian.id_pembelian');
        $this->db->join('tb_barang', 'tb_barang.Kode_barang=tb_detail_pembelian.Kode_barang');
        $this->db->join('tb_supplier', 'tb_supplier.id_supplier = tb_pembelian.id_supplier');
        $this->db->where('tgl_beli', $tanggal);
        $this->db->where('tb_pembelian.id_supplier', $id);
        return $this->db->get()->result();
    }

    function alldata()
    {
        $this->db->select('*');
        $this->db->from('tb_pembelian');
        $this->db->join('tb_detail_pembelian', 'tb_pembelian.id_pembelian=tb_detail_pembelian.id_pembelian');
        $this->db->join('tb_barang', 'tb_barang.Kode_barang=tb_detail_pembelian.Kode_barang');
        $this->db->join('tb_supplier', 'tb_supplier.id_supplier = tb_pembelian.id_supplier');
        return $this->db->get()->result();
    }
}

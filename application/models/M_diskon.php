<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_diskon extends CI_Model
{

	private $_table = 'tb_diskon';

	public $id_barang = 'id_barang';
	public $diskon = 'diskon';
	public $berakhir = 'berakhir';

	public function rules()
	{
		# code...
		return [
			[
				'field' => 'id_barang',
				'label' => 'id_barang',
				'rules' => 'required'
			],
			[
				'field' => 'diskon',
				'label' => 'diskon',
				'rules' => 'required'
			],
			[
				'field' => 'berakhir',
				'label' => 'berakhir',
				'rules' => 'required'
			]
		];
	}

	function deleteOldDiskon()
	{
		$this->db->query("DELETE FROM `tb_diskon` WHERE `berakhir` <= NOW() ");
	}

	public function getAll()
	{
		# code...
		$this->db->SELECT('*')
			->FROM('tb_diskon')
			->JOIN('tb_barang', 'tb_barang.id_barang = tb_diskon.id_barang');

		$query = $this->db->get();

		return $query->result();
	}

	public function getAllLanding()
	{
		# code...
		$this->db->SELECT('*')
			->FROM('tb_diskon')
			->JOIN('tb_barang', 'tb_barang.id_barang = tb_diskon.id_barang', 'LEFT');

		$query = $this->db->get();

		return $query->result();
	}

	public function getByKode($kobar)
	{
		# code...
		$this->db->SELECT('*')
			->FROM('tb_diskon')
			->JOIN('tb_barang', 'tb_barang.id_barang = tb_diskon.id_barang', 'LEFT')
			->WHERE('Kode_barang', $kobar);

		$query = $this->db->get();

		return $query;
	}

	public function save()
	{
		# code...
		$_POST = $this->input->post();
		
		$this->id_barang = $_POST['id_barang'];
		$this->diskon = $_POST['diskon'];
		$this->berakhir = $_POST['berakhir'];

		$this->db->insert($this->_table, $this);
	}

	public function delete($id)
	{
		# code...
		return $this->db->delete($this->_table, array('id' => $id));
		
	}
}

/* End of file M_diskon.php */

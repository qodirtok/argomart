<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kasir extends CI_Model
{




    public function rules() // digunakan untuk validatornya 
    {
        # code...

        return [

            [
                'field' => 'awal',
                'label' => 'awal',
                'rules' => 'required'
            ],
            [
                'field' => 'akhir',
                'label' => 'akhir',
                'rules' => 'required'
            ]
        ];
    }



    function getModal($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_modal');
        $this->db->where('tanggal', $tanggal);
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getKM($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_kasmasuk');
        $this->db->where('tanggal', $tanggal);
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getKK($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_kaskeluar');
        $this->db->where('tanggal', $tanggal);
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getPU($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_jurnal');
        $this->db->where('tanggal', $tanggal);
        $this->db->where('kode_akun', '2101');
        $this->db->like('bukti', 'UT');
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getRJ($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_jurnal');
        $this->db->where('tanggal', $tanggal);
        $this->db->where('kode_akun', '4201');
        $this->db->like('bukti', 'RJ');
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getRB($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_jurnal');
        $this->db->where('tanggal', $tanggal);
        $this->db->where('kode_akun', '1111');
        $this->db->like('bukti', 'RB');
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getBP($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_jurnal');
        $this->db->where('tanggal', $tanggal);
        $this->db->where('kode_akun', '2101');
        $this->db->like('bukti', 'BU');
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getPenjualan($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_penjualan');
        $this->db->join('tb_detail_penjualan', 'tb_penjualan.no_faktur=tb_detail_penjualan.no_faktur');
        $this->db->where('tgl_penjualan', $tanggal);
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getPembelian($id, $tanggal)
    {
        $this->db->select('*');
        $this->db->from('tb_pembelian');
        $this->db->join('tb_detail_pembelian', 'tb_pembelian.id_pembelian=tb_detail_pembelian.id_pembelian');
        $this->db->join('tb_barang', 'tb_barang.Kode_barang=tb_detail_pembelian.Kode_barang');
        $this->db->where('tgl_beli', $tanggal);
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_profile extends CI_Model
{
    function GetAll($id)
    {
        return $this->db->get_where('tb_user', ['id_user' => $id])->row_array();
    }

    function ubah($id)
    {
        $config['max_size'] = 2048;
        $config['allowed_types'] = "png|jpg|jpeg|gif";
        $config['remove_spaces'] = TRUE;
        $config['overwrite'] = TRUE;
        $config['upload_path'] = './asset/img';
        $this->load->library('upload');
        $this->upload->initialize($config);
        //ambil data image
        if (!$this->upload->do_upload('image')) {
            $pict = "default.jpg";
        } else {
            $data_image = $this->upload->data('file_name');
            $pict = $data_image;
        }
        $data = [
            'nama' => $this->input->post('nama_akun'),
            'jk' => $this->input->post('jk'),
            'nohp' => $this->input->post('Nohp'),
            'alamat' => $this->input->post('alamat'),
            'image' => $pict
        ];
        $this->db->WHERE('id_user', $id);
        $this->db->update('tb_user', $data);
    }
}

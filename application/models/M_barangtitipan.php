<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_barangtitipan extends CI_Model
{

    private $_table = 'tb_barangtitip';

    public $Kode_barangtitip;
    public $nama_barang;
    public $hrg_perbiji;
    public $total_jual;
    public $stok_barangtitip;
   
    public function rules()
    {
        # code...
        return [
            [
                'field' => 'Kode_barangtitip',
                'label' => 'Kode barang',
                'rules' => 'required'
            ],
            [
                'field' => 'nama_barang',
                'label' => 'Nama Barang',
                'rules' => 'required'
            ],
            [
                'field' => 'harga_perbiji',
                'label' => 'Harga Perbiji',
                'rules' => 'required'
            ],
            [
                'field' => 'total_jual',
                'label' => 'Total',
                'rules' => 'required'
            ],
            [
                'field' => 'stok',
                'label' => 'Stok',
                'rules' => 'required'
            ]
        ];
    }

    public function getAll()
    {
        $this->db->SELECT('*')
            ->FROM('tb_barangtitip');

        $query = $this->db->get();

        return $query->result();
    }

    public function getById($id)
    {
        # code...
        return $this->db->get_where($this->_table, ["id_barangtitip" => $id])->row();
    }

    function getBykode($kobar)
    {
        $this->db->SELECT('*')
            ->FROM('tb_barangtitip')
            ->WHERE('Kode_barangtitip', $kobar);
        $query = $this->db->get();

        return $query;
        return $this->db->get_where($this->_table, ["Kode_barangtitip" => $kobar])->row();
    }

    function get_barang($kobar)
    {
        $this->db->SELECT('*')
            ->FROM('tb_barang')
            ->WHERE('Kode_barangtitip', $kobar);

        $hsl = $this->db->get();

        // $hsl = $this->db->query("SELECT * FROM tb_barang a JOIN tb_satuan b ON a.id_satuan = b.id_satuan where a.Kode_barang='$kobar'");
        return $hsl;
    }

    public function save()
    {
        # code...
        $_POST = $this->input->post();


        $this->Kode_barangtitip = htmlspecialchars($_POST['Kode_barangtitip']);
        $this->nama_barang = htmlspecialchars($_POST['nama_barang']);
        $this->hrg_perbiji = str_replace(".", "", $_POST['harga_perbiji']);
        $this->total_jual = str_replace(".", "", $_POST['total_jual']);
        $this->stok_barangtitip = htmlspecialchars($_POST['stok']);
      
        // var_dump($this);
        // die;
        $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $_POST = $this->input->post();
        # code...
        $this->Kode_barangtitip = htmlspecialchars($_POST['Kode_barangtitip']);
        $this->nama_barang = htmlspecialchars($_POST['nama_barang']);
        $this->hrg_perbiji = str_replace(".", "", $_POST['harga_perbiji']);
        $this->total_jual = str_replace(".", "", $_POST['total_jual']);
        $this->stok_barangtitip = htmlspecialchars($_POST['stok']);

        $this->db->WHERE('Kode_barangtitip', $_POST['Kode_barangtitip'])
            ->UPDATE($this->_table, $this);
    }

    public function tambahStock()
    {
        $_POST = $this->input->post();

        $tambah = (htmlspecialchars($_POST['old_stok']) + htmlspecialchars($_POST['stok']));

        $data = array(
            'stok' => $tambah
        );

        $this->db->WHERE('id_barang', $_POST['id_barang'])
            ->UPDATE('tb_barang', $data);
    }

    public function kurangStok()
    {
        # code...
        $_POST = $this->input->post();

        $kurang = (htmlspecialchars($_POST['old_stok']) - htmlspecialchars($_POST['stok']));

        $data = array(
            'stok' => $kurang
        );

        $this->db->WHERE('id_barang', $_POST['id_barang'])
            ->UPDATE('tb_barang', $data);
    }

    private function _deleteImage($id)
    {
        $image = $this->getById($id);
        if ($image->image != "default.jpg") {
            $filename = explode(".", $image->image)[0];
            return array_map('unlink', glob(FCPATH . "asset/img/barang/$filename.*"));
        }
    }

    public function delete($id)
    {
        # code...
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array('id_barangtitip' => $id));
    }

      public function kode(){
		  $this->db->select('RIGHT(tb_barangtitip.Kode_barangtitip,2) as kode_barang', FALSE);
		  $this->db->order_by('Kode_barangtitip','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get($this->_table);  //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
			   //cek kode jika telah tersedia    
			   $data = $query->row();      
			   $kode = intval($data->kode_barang) + 1; 
		  }
		  else{      
			   $kode = 1;  //cek jika kode belum terdapat pada table
		  }
			  $tgl=date('dmY'); 
			  $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
			  $kodetampil = "BRTITIP"."5".$tgl.$batas;  //format kode
			  return $kodetampil;  
		 }
}

/* End of file M_barang.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_barang extends CI_Model
{

	private $_table = 'tb_barang';

	public $Kode_barang;
	public $nama;
	public $stok;
	public $hrg_jual;
	public $hrg_beli;
	public $id_satuan;
	public $id_katagori;
	// public $image = "default.jpg";

	public function rules()
	{
		# code...
		return [
			[
				'field' => 'Kode_barang',
				'label' => 'Kode_barang',
				'rules' => 'required'
			],
			[
				'field' => 'nama',
				'label' => 'nama',
				'rules' => 'required'
			],
			[
				'field' => 'stok',
				'label' => 'stok',
				'rules' => 'required'
			],
			[
				'field' => 'hrg_beli',
				'label' => 'hrg_beli',
				'rules' => 'required'
			],
			[
				'field' => 'id_satuan',
				'label' => 'id_satuan',
				'rules' => 'required'
			],
			[
				'field' => 'id_katagori',
				'label' => 'id_katagori',
				'rules' => 'required'
			],
			[
				'field' => 'persenan',
				'label' => 'persenan',
				'rules' => 'required'
			]
			// [
			// 	'field' => 'persenangrosir',
			// 	'label' => 'persenangrosir',
			// 	'rules' => 'required'
			// ]
		];
	}


	public function createKode() // digunakan untuk membuat kode member
	{
		# code...
		$this->db->SELECT('RIGHT(tb_barang.kode_barang,4) as kode', FALSE);
		$this->db->order_by('kode_barang', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('tb_barang');
		if ($query->num_rows() <> 0) {
			// jika kodesudah ada
			$data = $query->row();
			$kode = intval($data->kode) + 1;
		} else {
			//jika kode belum ada
			$kode = 1;
		}

		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kodejadi = "ARGMRT-TTP-" . $kodemax;

		return $kodejadi;
	}
	public function getAll()
	{
		$this->db->SELECT('*')
			->FROM('tb_barang')
			->JOIN('tb_satuan', 'tb_satuan.id_satuan = tb_barang.id_satuan')
			->JOIN('tb_katagori', 'tb_katagori.id_katagori = tb_barang.id_katagori');

		$query = $this->db->get();

		return $query->result();
	}

	public function getById($id)
	{
		# code...
		return $this->db->get_where($this->_table, ["id_barang" => $id])->row();
	}

	function getBykode($kobar)
	{
		$this->db->SELECT('*')
			->FROM('tb_barang')
			->JOIN('tb_satuan', 'tb_satuan.id_satuan=tb_barang.id_satuan')
			->LIKE('nama', $kobar)
			->OR_WHERE('Kode_barang', $kobar);
		$query = $this->db->get();

		return $query;
		// return $this->db->get_where($this->_table, ["Kode_barang" => $kobar])->row();
	}

	function get_barang($kobar)
	{
		$this->db->SELECT('*')
			->FROM('tb_barang')
			->JOIN('tb_satuan', 'tb_satuan.id_satuan=tb_barang.id_satuan')
			->LIKE('tb_barang.nama', $kobar)
			->OR_WHERE('tb_barang.Kode_barang', $kobar);

		$hsl = $this->db->get();

		// $hsl = $this->db->query("SELECT * FROM tb_barang a JOIN tb_satuan b ON a.id_satuan = b.id_satuan where a.Kode_barang='$kobar'");
		return $hsl;
	}

	public function save()
	{
		# code...
		$_POST = $this->input->post();

		$config['max_size'] = 2048;
		$config['allowed_types'] = "png|jpg|jpeg|gif";
		$config['remove_spaces'] = TRUE;
		$config['file_name'] = $_POST['Kode_barang'];
		$config['overwrite'] = TRUE;
		$config['upload_path'] = './asset/img/barang';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload('image')) {
			$pict = "default.jpg";
		} else {
			$data_image = $this->upload->data('file_name');
			$pict = $data_image;
		}


		$beli = str_replace(".", "", $_POST['hrg_beli']);
		$hrgjual = $beli + (($_POST['persenan'] / 100) * $beli);
		$hrggrosir = $beli + (($_POST['persenangrosir'] / 100) * $beli);

		$this->Kode_barang = htmlspecialchars($_POST['Kode_barang']);
		$this->nama = htmlspecialchars($_POST['nama']);
		$this->stok = htmlspecialchars($_POST['stok']);
		$this->image = htmlspecialchars($pict);
		$this->hrg_beli = htmlspecialchars($beli);
		$this->persenan = $_POST['persenan'];
		$this->persenangrosir = $_POST['persenangrosir'];
		$this->hrg_jual = $hrgjual;
		$this->hrg_jual_grosir = $hrggrosir;
		$this->id_satuan = htmlspecialchars($_POST['id_satuan']);
		$this->id_katagori = htmlspecialchars($_POST['id_katagori']);
		// var_dump($this);
		// die;
		$this->db->insert($this->_table, $this);
	}

	public function update()
	{
		$_POST = $this->input->post();
		# code...
		$config['max_size'] = 2048;
		$config['allowed_types'] = "png|jpg|jpeg|gif";
		$config['remove_spaces'] = TRUE;
		$config['file_name'] = $_POST['Kode_barang'];
		$config['overwrite'] = TRUE;
		$config['upload_path'] = './asset/img/barang';

		// $this->load->library('upload');
		$this->upload->initialize($config);

		//ambil data image
		if (!$this->upload->do_upload('image')) {
			$pict = "default.jpg";
		} else {
			$data_image = $this->upload->data('file_name');
			$pict = $data_image;
		}

		$beli = str_replace(".", "", $_POST['hrg_beli']);
		$hrgjual = $beli + (($_POST['persenan'] / 100) * $beli);
		$hrggrosir = $beli + (($_POST['persenangrosir'] / 100) * $beli);

		$this->id_barang = htmlspecialchars($_POST['id_barang']);
		$this->Kode_barang = htmlspecialchars($_POST['Kode_barang']);
		$this->nama = htmlspecialchars($_POST['nama']);
		$this->stok = htmlspecialchars($_POST['stok']);
		$this->image = $pict;
		$this->hrg_beli = htmlspecialchars(str_replace(".", "", $_POST['hrg_beli']));
		$this->persenan = $_POST['persenan'];
		$this->persenangrosir = $_POST['persenangrosir'];
		$this->hrg_jual = htmlspecialchars($hrgjual);
		$this->hrg_jual_grosir = $hrggrosir;
		$this->id_satuan = htmlspecialchars($_POST['id_satuan']);
		$this->id_katagori = htmlspecialchars($_POST['id_katagori']);

		$this->db->WHERE('id_barang', $_POST['id_barang'])
			->UPDATE($this->_table, $this);
	}

	public function tambahStock()
	{
		$_POST = $this->input->post();

		$tambah = (htmlspecialchars($_POST['old_stok']) + htmlspecialchars($_POST['stok']));

		$data = array(
			'stok' => $tambah
		);

		$this->db->WHERE('id_barang', $_POST['id_barang'])
			->UPDATE('tb_barang', $data);
	}

	public function kurangStok()
	{
		# code...
		$_POST = $this->input->post();

		$kurang = (htmlspecialchars($_POST['old_stok']) - htmlspecialchars($_POST['stok']));

		$data = array(
			'stok' => $kurang
		);

		$this->db->WHERE('id_barang', $_POST['id_barang'])
			->UPDATE('tb_barang', $data);
	}

	private function _deleteImage($id)
	{
		$image = $this->getById($id);
		if ($image->image != "default.jpg") {
			$filename = explode(".", $image->image)[0];
			return array_map('unlink', glob(FCPATH . "asset/img/barang/$filename.*"));
		}
	}

	public function delete($id)
	{
		# code...
		$this->_deleteImage($id);
		return $this->db->delete($this->_table, array('id_barang' => $id));
	}
}

/* End of file M_barang.php */

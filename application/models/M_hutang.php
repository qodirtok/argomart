<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_hutang extends CI_Model
{


	private $_table   = "tb_hutang";
	private $_tbjurnal = "tb_jurnal";
	private $_tmpbayar = "tb_tmpbayar";

	public $id_member;
	public $dibayar;
	public $sisa_bayar;

	public function rules()
	{
		return [
			[
				'field' => 'id_member',
				'label' => 'Kode_member',
				'rules' => 'required'
			],
			[
				'field' => 'dibayar',
				'label' => 'dibayar',
				'rules' => 'required'
			],
			[
				'field' => 'sisa_bayar',
				'label' => 'sisa_bayar',
				'rules' => 'numeric'
			]
		];
	}

	public function getAll()
	{
		$this->db->SELECT('*')
			->FROM('tb_hutang')
			->JOIN('tb_supplier', 'tb_supplier.id_supplier = tb_hutang.id_member')
			->GROUP_BY('id_supplier');
		$query = $this->db->get()->result();
		return $query;
	}

	public function getMember()
	{
		$this->db->SELECT('*')
			->FROM('tb_supplier');

		$query = $this->db->get()->result();
		return $query;
	}

	public function getById($id)
	{
		$this->db->SELECT('*')
			->FROM($this->_table)
			->JOIN('tb_supplier', 'tb_supplier.id_supplier = tb_hutang.id_member')
			->WHERE('id_hutang', $id);

		$query = $this->db->get()->result();

		return $query;
	}

	public function gethtngById($id)
	{
		$this->db->SELECT('*')
			->FROM($this->_tmpbayar)
			->WHERE('id_hutang', $id);

		$query = $this->db->get()->result();

		return $query;
	}
	public function getByKode($id)
	{
		$this->db->SELECT('*')
			->FROM($this->_table)
			->JOIN('tb_supplier', 'tb_supplier.id_member = tb_hutang.id_member')
			->WHERE('id_member', $id);

		$query = $this->db->get()->result();

		return $query;
	}

	public function save()
	{
		$_POST = $this->input->post();

		$this->id_member = $_POST['id_member'];
		$this->sisa_bayar = $_POST['sisa_bayar'];
		$this->dibayar = $_POST['dibayar'];
		$member = $_POST['id_member'];
		// $this->status = 'B';

		foreach ($this->db->get_where('tb_supplier', ['id_member' => $member])->result() as $d) {
			$nama = $d->Nama;
		}


		$tgl = date('Y-m-d');
		$tb = date('ymd');
		$kredit = [
			'tanggal' => $tgl,
			'bukti' => 'PU' . $tb,
			'kode_akun' => '1111',
			'ket' => 'Utang ' . $nama,
			'debet' => 0,
			'kredit' => $_POST['sisa_bayar'],
			'id_user' => $_POST['id_user']
		];

		$debet = [
			'tanggal' => $tgl,
			'bukti' => 'PU' . $tb,
			'kode_akun' => '2101',
			'ket' => 'Utang ' . $nama,
			'debet' => $_POST['sisa_bayar'],
			'kredit' => 0,
			'id_user' => $_POST['id_user']
		];

		$this->db->insert($this->_tbjurnal, $debet);
		$this->db->insert($this->_tbjurnal, $kredit);


		$this->db->insert('tb_hutang_tmp', $this);
		// $this->db->insert($this->_table, $this);
	}
	public function saveUpdate()
	{
		# code...
		$this->id_member = $_POST['id_member'];
		$this->sisa_bayar = $_POST['sisa_bayar'];
		$this->dibayar = $_POST['dibayar'];

		$this->db->insert('tb_hutang_tmp', $this);
		// $this->db->update($this->_table, $this, array('id_hutang' => $id));
	}


	public function bayar($id)
	{
		$_POST = $this->input->post();

		$bayar = $_POST['old_utang'] - $_POST['nominal'];
		$dibayar = $_POST['old_bayar'] + $_POST['nominal'];

		if ($bayar = 0 || $bayar <= 0) {
			# code...
			$this->sisa_bayar = 0;
			$this->dibayar = 0;
			$this->id_member = $_POST['id_member'];
		} else {
			$bayar = $_POST['old_utang'] - $_POST['nominal'];
			$dibayar = $_POST['old_bayar'] + $_POST['nominal'];
			$this->dibayar = $dibayar;
			$this->sisa_bayar = $bayar;
			$this->id_member = $_POST['id_member'];
		}

		$tmp = [
			'id_hutang'			=> $_POST['id_hutang'],
			'id_member'			=> $_POST['id_member'],
			'nominal'			=> $_POST['nominal'],
			'date'				=> date('Y-m-d')
		];
		$tgl = date('Y-m-d');
		$tb = date('ymd');
		$kredit = [
			'tanggal' => $tgl,
			'bukti' => 'BU' . $tb,
			'kode_akun' => '1111',
			'ket' => 'Pembayaran Utang ' . $_POST['nama'],
			'debet' => 0,
			'kredit' => $_POST['nominal'],
			'id_user' => $_POST['id_user']
		];

		$debet = [
			'tanggal' => $tgl,
			'bukti' => 'BU' . $tb,
			'kode_akun' => '2101',
			'ket' => 'Pembayaran Utang ' . $_POST['nama'],
			'debet' => $_POST['nominal'],
			'kredit' => 0,
			'id_user' => $_POST['id_user']
		];

		$this->db->INSERT($this->_tmpbayar, $tmp);
		$this->db->insert($this->_tbjurnal, $debet);
		$this->db->insert($this->_tbjurnal, $kredit);

		$this->db->update($this->_table, $this, array('id_hutang' => $id));
	}

	function cetak($id)
	{
		$this->db->SELECT('*')
			->FROM('tb_tmpbayar')
			->JOIN('tb_supplier', 'tb_supplier.id_supplier = tb_tmpbayar.id_member')
			->JOIN('tb_hutang', 'tb_hutang.id_hutang=tb_tmpbayar.id_hutang')
			->WHERE('tb_tmpbayar.id_hutang', $id);

		return $this->db->get()->result();
	}
}

/* End of file M_hutang.php */

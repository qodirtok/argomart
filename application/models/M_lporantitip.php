<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_lporantitip extends CI_Model
{

    public function rules() // digunakan untuk validatornya 
    {
        return [
            [
                'field' => 'awal',
                'label' => 'awal',
                'rules' => 'required'
            ],
            [
                'field' => 'akhir',
                'label' => 'akhir',
                'rules' => 'required'
            ]
        ];
    }

    public function getallpenjualan($awal, $akhir)
    {
        // $this->db->select('*,SUM(IF(tanggal >= ' . $awal . '  <=  ' . $akhir . ', debet, 0)) AS D,SUM(IF(tanggal >=   ' . $awal . '  <= ' . $akhir . '  , kredit, 0)) AS K');
        $this->db->from('tb_penjualan');
        $this->db->join('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur=tb_penjualan.no_faktur');
        $this->db->join('tb_barang', 'tb_barang.Kode_barang = tb_detail_penjualan.Kode_barang');
        $this->db->like('tb_barang.Kode_barang', 'ARGMRT-TTP-');
        $this->db->where('tgl_penjualan >=', $awal);
        $this->db->where('tgl_penjualan <=', $akhir);
        return $this->db->get()->result();
    }

    public function getalltitip()
    {
        $this->db->from('tb_barang')
            ->Join('tb_satuan', 'tb_satuan.id_satuan = tb_barang.id_satuan')
            ->like('Kode_barang', 'ARGMRT-TTP-');
        return $this->db->get()->result();
    }

    public function getallpembelian($awal, $akhir)
    {
        // $this->db->select('*,SUM(IF(tanggal >= ' . $awal . '  <=  ' . $akhir . ', debet, 0)) AS D,SUM(IF(tanggal >=   ' . $awal . '  <= ' . $akhir . '  , kredit, 0)) AS K');
        $this->db->from('tb_pembelian');
        $this->db->join('tb_detail_pembelian', 'tb_detail_pembelian.id_pembelian=tb_pembelian.id_pembelian');
        $this->db->join('tb_barang', 'tb_detail_pembelian.Kode_barang=tb_barang.Kode_barang');
        $this->db->where('tgl_beli >=', $awal);
        $this->db->where('tgl_beli <=', $akhir);
        // $this->db->where('kelompok  ', 'LR');
        // $this->db->where('tb_akun.kode_akun', '4101');
        return $this->db->get()->result();
    }
}

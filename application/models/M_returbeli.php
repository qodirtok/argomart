<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_returbeli extends CI_Model
{
    private $_table = 'tb_pembelian';
    private $_tb    = 'tb_retur_pembelian';
    private $_tbjurnal = 'tb_jurnal';

    public function rules() // digunakan untuk validatornya 
    {
        # code...

        return [

            [
                'field' => 'jumlah',
                'label' => 'jumlah',
                'rules' => 'numeric'
            ]
        ];
    }


    function getAll()
    {
        $m = date('m');
        $this->db->JOIN('tb_supplier', 'tb_supplier.id_supplier=tb_pembelian.id_supplier');
        $this->db->where('MONTH(tgl_beli)', $m);
        $this->db->group_by('tb_supplier.id_supplier');

        // $this->db->JOIN('tb_detail_pembelian', 'tb_detail_pembelian.id_pembelian=tb_pengirim.id_pembelian');
        return $this->db->get($this->_table)->result();
    }

    function getLaporan($awal, $akhir)
    {
        $this->db->select('*')
            ->from('tb_retur_pembelian')
            ->join('tb_supplier', 'tb_retur_pembelian.id_supplier=tb_supplier.id_supplier')
            ->where('tanggal >=', $awal)
            ->where('tanggal <=', $akhir);
        return $this->db->get()->result();
    }

    function getRetur()
    {
        $this->db->select('*,sum(jumlah) as jml')
            ->from('tb_retur_pembelian')
            ->group_by('id_pembelian');
        return $this->db->get()->result();
    }

    function detail($id)
    {
        $this->db->JOIN('tb_supplier', 'tb_supplier.id_supplier=tb_pembelian.id_supplier');
        $this->db->JOIN('tb_detail_pembelian', 'tb_detail_pembelian.id_pembelian=tb_pembelian.id_pembelian');
        return $this->db->get_where($this->_table, ["id_pembelian" => $id])->row();
    }

    function detaill($id)
    {
        $this->db->SELECT('*')
            ->FROM('tb_pembelian')
            ->JOIN('tb_supplier', 'tb_supplier.id_supplier=tb_pembelian.id_supplier')
            ->JOIN('tb_detail_pembelian', 'tb_detail_pembelian.id_pembelian=tb_pembelian.id_pembelian')
            ->JOIN('tb_barang', 'tb_detail_pembelian.Kode_barang=tb_barang.Kode_barang')
            ->JOIN('tb_satuan', 'tb_satuan.id_satuan=tb_barang.id_satuan')
            ->WHERE('tb_supplier.id_supplier', $id);

        $hsl = $this->db->get()->result();
        return $hsl;
    }

    public function save() //tambah data member
    {
        # code...

        $nf = substr($_POST['nofak'], 2);
        $cek = $_POST['cek'];
        $retur = [
            'no_faktur'    => $_POST['nofak'],
            'Kode_barang'  => $_POST['kode'],
            'nama'         => $_POST['nama'],
            'jumlah'       => $_POST['jumlah'],
            'harga'        => $_POST['harga'],
            'tanggal'      => date('Y-m-d'),
            'sub_total'    => ($_POST['jumlah'] * $_POST['harga']),
            'id_supplier'  => $_POST['id_supplier'],
            'id_user'      => $_POST['id_user']
        ];

        $tunaid = [
            'tanggal' => date('Y-m-d'),
            'bukti' => 'RB' . $nf,
            'kode_akun' => '1111',
            'ket' => 'Retur pembelian ' . $_POST['nama'],
            'kredit' => 0,
            'debet' => $_POST['harga'],
            'id_user' => $_POST['id_user']
        ];
        $hutangd = [
            'tanggal' => date('Y-m-d'),
            'bukti' => 'RB' . $nf,
            'kode_akun' => '2101',
            'ket' => 'Retur pembelian ' . $_POST['nama'],
            'kredit' => 0,
            'debet' => $_POST['harga'],
            'id_user' => $_POST['id_user']
        ];

        $tunaik = [
            'tanggal' => date('Y-m-d'),
            'bukti' => 'RB' . $nf,
            'kode_akun' => '1141',
            'ket' => 'Retur pembelian ' . $_POST['nama'],
            'kredit' => $_POST['harga'],
            'debet' => 0,
            'id_user' => $_POST['id_user']
        ];

        if ($cek == 'Lunas') {
            $this->db->insert($this->_tbjurnal, $tunaid);
        } else {
            $this->db->insert($this->_tbjurnal, $hutangd);
            $this->db->query("update tb_hutang set 	sisa_bayar=sisa_bayar-'$_POST[harga]' where id_member='$_POST[id_supplier]'");
        }
        $this->db->insert($this->_tbjurnal, $tunaik);
        $this->db->query("update tb_barang set 	stok=stok-'$_POST[jumlah]' where Kode_barang='$_POST[kode]'");
        $this->db->insert($this->_tb, $retur);
    }
}

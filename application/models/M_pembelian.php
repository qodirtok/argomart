<?php
class M_pembelian extends CI_Model
{

    function simpan_pembelian($nofnofakturak, $tglfak, $suplier, $beli_kode, $tunai, $kembali)
    {
        date_default_timezone_set('Asia/Jakarta');
        $hutang  = str_replace("-", "", $kembali);
        $tunay  = str_replace(",", "", $tunai);
        $jam = date("H:i:s");
        $id_user = $this->session->userdata('id_user');
        $this->db->query("INSERT INTO tb_pembelian (id_pembelian,no_faktur,id_supplier,tgl_beli,jam_pembelian,id_user) VALUES ('$beli_kode','$nofnofakturak','$suplier','$tglfak','$jam','$id_user')");
        foreach ($this->cart->contents() as $item) {
            $data = array(
                // 'd_beli_nofak'         =>    $nofnofakturak,
                'Kode_barang'           =>    $item['id'],
                'harga_beli'            =>    $item['price'],
                'harga_jual'            =>    $item['harjulecer'],
                'jumlah'                =>    $item['qty'],
                'persenan'              =>    $item['persenan'],
                'total'                 =>    $item['subtotal'],
                'id_pembelian'          =>    $beli_kode
            );


            $persedian = [
                'kode_akun' => '1141',
                'kredit'    => 0,
                'debet'     => $item['subtotal'],
                'bukti'     => $beli_kode,
                'tanggal'   => date('Y-m-d'),
                'ket'       => 'Pembelian Dengan No.Faktur : ' . $beli_kode,
                'id_user'   => $id_user
            ];
            $kas = [
                'kode_akun' => '1111',
                'debet'     => 0,
                'kredit'    => $item['price'] * $item['qty'],
                'bukti'     => $beli_kode,
                'tanggal'   => date('Y-m-d'),
                'ket'       => 'Pembelian Dengan No.Faktur : ' . $beli_kode,
                'id_user'   => $id_user
            ];
            $cash = [
                'kode_akun' => '1111',
                'debet'     => 0,
                'kredit'    => $tunay,
                'bukti'     => $beli_kode,
                'tanggal'   => date('Y-m-d'),
                'ket'       => 'Pembelian Dengan No.Faktur : ' . $beli_kode,
                'id_user'   => $id_user
            ];
            $utang = [
                'kode_akun' => '2101',
                'kredit'    => $hutang,
                'debet'     => 0,
                'bukti'     => 'UT-' . $beli_kode,
                'tanggal'   => date('Y-m-d'),
                'ket'       => 'Hutang Pembelian Dengan No.Faktur : ' . $beli_kode,
                'id_user'   => $id_user
            ];
            $ht = [
                'id_member'  => $suplier,
                'dibayar'    => 0,
                'sisa_bayar' => $hutang,
                'no_faktur'  => $nofnofakturak
            ];

            if ($kembali >= 0) {
                $this->db->insert('tb_jurnal', $kas);
            }
            $this->db->insert('tb_jurnal', $persedian);

            $this->db->insert('tb_detail_pembelian', $data);
            $this->db->query("update tb_barang set 	stok=stok+'$item[qty]',hrg_beli='$item[price]',hrg_jual='$item[harjulecer]',persenan='$item[persenan]' where Kode_barang='$item[id]'");
        }
        if ($kembali < 0) {
            $this->db->insert('tb_jurnal', $cash);
            $this->db->insert('tb_jurnal', $utang);
            $this->db->insert('tb_hutang_tmp', $ht);
        }
        return true;
    }
    function get_kobel()
    {
        $q = $this->db->query("SELECT MAX(RIGHT(id_pembelian,6)) AS kd_max FROM tb_pembelian WHERE DATE(tgl_beli)=CURDATE()");
        $kd = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $k) {
                $tmp = ((int) $k->kd_max) + 1;
                $kd = sprintf("%06s", $tmp);
            }
        } else {
            $kd = "000001";
        }
        return "BL" . date('dmy') . $kd;
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_neraca extends CI_Model
{
    //
    // function getAkun() //ambil akun
    // {
    //     return $this->db->get('tb_akun')->result();
    // }
    function getNrcSaldo($awal, $akhir) //ambil saldo berjalan
    {
        $this->db->select('*,SUM(debet) as D, SUM(kredit) as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_jurnal.kode_akun=tb_akun.kode_akun');
        $this->db->where('tanggal >=', $awal);
        $this->db->where('tanggal <=', $akhir);
        $this->db->group_by('tb_jurnal.kode_akun');
        return $this->db->get()->result();
    }
    function getAktiva($tanggal) //ambil saldo aktiva neraca
    {
        $this->db->select('*,SUM(debet) as D, SUM(kredit) as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_jurnal.kode_akun=tb_akun.kode_akun');
        $this->db->where('tb_akun.kode_akun >=', 1111);
        $this->db->where('tb_akun.kode_akun <=', 1323);
        $this->db->where('tanggal <=', $tanggal);
        $this->db->where('kelompok', 'NRC');
        $this->db->group_by('tb_jurnal.kode_akun');
        return $this->db->get()->result();
    }
    function getPasiva($tanggal) //ambil saldo aktiva neraca
    {
        $this->db->select('*,SUM(debet) as D, SUM(kredit) as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_jurnal.kode_akun=tb_akun.kode_akun');
        $this->db->where('tb_akun.kode_akun >=', 2101);
        $this->db->where('tb_akun.kode_akun <=', 3002);
        $this->db->where('tanggal <=', $tanggal);
        $this->db->where('kelompok', 'NRC');
        $this->db->group_by('tb_jurnal.kode_akun');
        return $this->db->get()->result();
    }

    //  start laba rugi

    function getPenjualan($tanggal) //ambil data penjualan
    {

        $this->db->select('*,SUM(debet) AS D,SUM(kredit) AS K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal <=', $tanggal);
        $this->db->where('kelompok  ', 'LR');
        $this->db->where('tb_akun.kode_akun', '4101');
        return $this->db->get()->result();
    }
    function getHapok($tanggal) //ambil data haraga pokok penjualan
    {
        $this->db->select('*,SUM(debet) as D,SUM(kredit)as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal <=', $tanggal);
        $this->db->where('kelompok  ', 'LR');
        $this->db->where('tb_akun.kode_akun', '5101');
        return $this->db->get()->result();
    }
    function getRePo($tanggal) //ambil data Retur & Potongan Penjualan
    {
        $this->db->select('*,SUM(debet) as D,SUM(kredit)as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal <=', $tanggal);
        $this->db->like('tb_jurnal.kode_akun', '42');
        $this->db->group_by('tb_jurnal.kode_akun');
        return $this->db->get()->result();
    }
    function getBiaya($tanggal) //ambil data biaya
    {
        $this->db->select('*,SUM(debet) as D,SUM(kredit) as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal <=', $tanggal);
        $this->db->like('tb_jurnal.kode_akun', '52');
        $this->db->group_by('tb_jurnal.kode_akun');
        return $this->db->get()->result();
    }
    function getPendLain($tanggal) //ambil data biaya
    {
        $this->db->select('*,SUM(debet) as D,SUM(kredit) as K');
        $this->db->from('tb_jurnal');
        $this->db->join('tb_akun', 'tb_akun.kode_akun=tb_jurnal.kode_akun');
        $this->db->where('tanggal <=', $tanggal);
        $this->db->where('tb_jurnal.kode_akun', '6101');

        return $this->db->get()->result();
    }

    // end laba rugi
}

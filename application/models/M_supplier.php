<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_supplier extends CI_Model
{

	private $_table = "tb_supplier"; //Nama tabel

	public $kode_supplier;
	public $nma_supplier;
	public $alamat;
	public $notlpn;
	public $cperson;

	public function rules() // digunakan untuk validatornya 
	{
		# code...

		return [
			[
				'field' => 'Kode_supplier',
				'label' => 'Kode_supplier',
				'rules' => 'required'
			],
			[
				'field' => 'Nama',
				'label' => 'Nama',
				'rules' => 'required'
			],
			[
				'field' => 'Nohp',
				'label' => 'Nohp',
				'rules' => 'numeric'
			],
			[
				'field' => 'Cp',
				'label' => 'Cp',
				'rules' => 'required'
			],
			[
				'field' => 'Alamat',
				'label' => 'Alamat',
				'rules' => 'required'
			]
		];
	}

	public function createKode() // digunakan untuk membuat kode member
	{
		# code...
		$this->db->SELECT('RIGHT(tb_supplier.kode_supplier,4) as kode', FALSE);
		$this->db->order_by('kode_supplier', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('tb_supplier');
		if ($query->num_rows() <> 0) {
			// jika kodesudah ada
			$data = $query->row();
			$kode = intval($data->kode) + 1;
		} else {
			//jika kode belum ada
			$kode = 1;
		}

		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kodejadi = "ARGMART-SPL-" . $kodemax;

		return $kodejadi;
	}

	public function getAll() //mengambil semua data
	{
		# code...
		return $this->db->get($this->_table)->result();
	}

	public function SelectAll() //mengambil semua data
	{
		# code...
		return $this->db->get($this->_table)->result_array();
	}

	public function getById($id) //mengambil data berdasarkan id
	{
		# code...
		return $this->db->get_where($this->_table, ["id_supplier" => $id])->row();
	}

	public function save() //tambah data member
	{
		# code...
		$_POST = $this->input->post();
		$this->kode_supplier = $_POST['Kode_supplier'];
		$this->nma_supplier  = $_POST['Nama'];
		$this->alamat 		 = $_POST['Alamat'];
		$this->notlpn        = $_POST['Nohp'];
		$this->cperson       = $_POST['Cp'];
		$this->db->insert($this->_table, $this);
	}

	public function M_update() // mengupdate data dari database
	{
		# code...
		$_POST = $this->input->post();
		$this->id_supplier   = $_POST['id_supplier'];
		$this->kode_supplier = $_POST['Kode_supplier'];
		$this->nma_supplier  = $_POST['Nama'];
		$this->alamat 		 = $_POST['Alamat'];
		$this->notlpn        = $_POST['Nohp'];
		$this->cperson       = $_POST['Cp'];

		$this->db->update($this->_table, $this, array('id_supplier' => $_POST['id_supplier']));
	}

	public function delete($id)
	{
		return $this->db->delete($this->_table, array('id_supplier' => $id));
	}
}

/* End of file M_member.php */

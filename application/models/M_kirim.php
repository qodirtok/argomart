<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kirim extends CI_Model
{
    private $_table = 'tb_pengirim';


    function getAll()
    {
        $this->db->JOIN('tb_member', 'tb_member.id_member=tb_pengirim.id_member');
        // $this->db->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur=tb_pengirim.no_faktur');
        return $this->db->get($this->_table)->result();
    }

    function getAllharian()
    {
        $tglnow = date('Y-m-d');
        $this->db->JOIN('tb_member', 'tb_member.id_member=tb_pengirim.id_member');
        // $this->db->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur=tb_pengirim.no_faktur');
        return $this->db->get_where($this->_table,  array('tb_pengirim.tgl_kirim' => $tglnow))->result();
    }

    function getbulan($tglawal, $tglakhir)
    {
        $this->db->JOIN('tb_member', 'tb_member.id_member=tb_pengirim.id_member');
        $this->db->WHERE('tb_pengirim.tgl_kirim >=', $tglawal);
        $this->db->WHERE('tb_pengirim.tgl_kirim <=', $tglakhir);
        return $this->db->get($this->_table)->result();
    }

    function detail($id)
    {
        $this->db->JOIN('tb_member', 'tb_member.id_member=tb_pengirim.id_member');
        $this->db->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur=tb_pengirim.no_faktur');
        return $this->db->get_where($this->_table, ["no_faktur" => $id])->row();
    }

    function detaill($id)
    {
        $this->db->SELECT('*')
            ->FROM('tb_pengirim')
            ->JOIN('tb_member', 'tb_member.id_member=tb_pengirim.id_member')
            ->JOIN('tb_detail_penjualan', 'tb_detail_penjualan.no_faktur=tb_pengirim.no_faktur')
            ->WHERE('tb_pengirim.no_faktur', $id);

        $hsl = $this->db->get()->result();
        return $hsl;
    }

    function update($id)
    {
        $status = 'Terkirim';
        $data = array(
            'status' => $status
        );
        $this->db->WHERE('no_faktur', $id);
        $this->db->UPDATE($this->_table, $data);
    }
}

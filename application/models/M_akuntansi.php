<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_akuntansi extends CI_Model
{
    private $_table = "tb_akun";
    private $_tbk   = "tb_kaskeluar";
    private $_tbm   = "tb_kasmasuk";
    private $_tbj   = "tb_jurnal";


    public function rules() // digunakan untuk validatornya 
    {
        # code...

        return [

            [
                'field' => 'jumlah',
                'label' => 'jumlah',
                'rules' => 'required'
            ]
        ];
    }
    public function rulesbb() // digunakan untuk validatornya 
    {
        # code...

        return [

            [
                'field' => 'awal',
                'label' => 'awal',
                'rules' => 'required'
            ],
            [
                'field' => 'akhir',
                'label' => 'akhir',
                'rules' => 'required'
            ]
        ];
    }


    function getAkun()
    {
        return $this->db->get($this->_table)->result();
        //  return $this->db->get_where($this->_table, ["Kode_barang" => $kobar])->row();
    }

    function totalkredit($id)
    {
        $m = date("m");
        $this->db->select('SUM(kredit)as total');
        $this->db->from('tb_jurnal');
        $this->db->where('MONTH(tanggal)', $m);
        $this->db->where('id_user', $id);
        $this->db->like('bukti', 'JU');
        return $this->db->get()->result();
    }
    function totaldebet($id)
    {
        $m = date("m");
        $this->db->select('SUM(debet)as total');
        $this->db->from('tb_jurnal');
        $this->db->where('MONTH(tanggal)', $m);
        $this->db->where('id_user', $id);
        $this->db->like('bukti', 'JU');
        return $this->db->get()->result();
    }
    function getIsi($awal, $akhir) //tampilkanbuku besar
    {
        $this->db->select('*');
        $this->db->from('tb_jurnal');
        $this->db->JOIN('tb_akun', 'tb_jurnal.kode_akun=tb_akun.kode_akun');
        $this->db->where('tanggal >=', $awal);
        $this->db->where('tanggal <=', $akhir);
        return $this->db->get()->result();
    }

    function filterIsi($awal, $akhir) //tampilkanbuku besar
    {
        $this->db->select('DISTINCT(kode_akun)');
        $this->db->from('tb_jurnal');
        $this->db->where('tanggal >=', $awal);
        $this->db->where('tanggal <=', $akhir);
        return $this->db->get()->result();
    }
    function getAll($id) //mengambil semua data kas keluar
    {
        # code...
        $m = date("m");
        $this->db->select('*');
        $this->db->from('tb_kaskeluar');
        $this->db->JOIN('tb_akun', 'tb_kaskeluar.kode_akun=tb_akun.kode_akun');
        $this->db->where('MONTH(tanggal)', $m);
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }
    function getKM($id) //mengambil semua data kas masuk
    {
        # code...
        $m = date("m");
        $this->db->select('*');
        $this->db->from('tb_kasmasuk');
        $this->db->JOIN('tb_akun', 'tb_kasmasuk.kode_akun=tb_akun.kode_akun');
        $this->db->where('MONTH(tanggal)', $m);
        $this->db->where('id_user', $id);
        return $this->db->get()->result();
    }

    public function filterJurnal($id) //mengambil semua data
    {
        # code...
        // SELECT * FROM tb_jurnal j JOIN tb_akun a ON j.akundebet=a.kode_akun JOIN tb_akun b ON j.akunkredit=b.kode_akun;
        $m = date("m");
        $this->db->select('DISTINCT(bukti)');
        $this->db->from('tb_jurnal');
        $this->db->where('MONTH(tanggal)', $m);
        $this->db->where('id_user', $id);
        $this->db->like('bukti', 'JU');
        return $this->db->get()->result();
    }

    public function getJurnal($id) //mengambil semua data
    {
        # code...
        // SELECT * FROM tb_jurnal j JOIN tb_akun a ON j.akundebet=a.kode_akun JOIN tb_akun b ON j.akunkredit=b.kode_akun;
        $m = date("m");
        $this->db->select('*');
        $this->db->from('tb_jurnal');
        $this->db->JOIN('tb_akun', 'tb_jurnal.kode_akun=tb_akun.kode_akun');
        $this->db->where('MONTH(tanggal)', $m);
        $this->db->where('id_user', $id);
        $this->db->like('bukti', 'JU');
        return $this->db->get()->result();
    }

    public function getById($id) //mengambil data berdasarkan id
    {
        # code...
        return $this->db->get_where($this->_tbk, ["kdbukti" => $id])->row();
    }
    public function getByKm($id) //mengambil data berdasarkan id
    {
        # code...
        return $this->db->get_where($this->_tbm, ["kdbukti" => $id])->row();
    }
    public function getByJurnal($id) //mengambil data berdasarkan id
    {
        # code...
        return $this->db->get_where($this->_tbj, ["idjurnal" => $id])->row();
    }
    function createKode($id) //kode kk
    {
        $q = $this->db->query("SELECT MAX(RIGHT(kdbukti,2)) AS kd_max FROM tb_kaskeluar WHERE DATE(tanggal)=CURDATE() and id_user=$id ");
        $kd = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $k) {
                $tmp = ((int) $k->kd_max) + 1;
                $kd = sprintf("%02s", $tmp);
            }
        } else {
            $kd = "01";
        }
        return 'KK' . date('ymd') . '-' . $kd;
    }
    function createKm($id) //kode km
    {
        $q = $this->db->query("SELECT MAX(RIGHT(kdbukti,2)) AS kd_max FROM tb_kasmasuk WHERE DATE(tanggal)=CURDATE() and id_user=$id ");
        $kd = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $k) {
                $tmp = ((int) $k->kd_max) + 1;
                $kd = sprintf("%02s", $tmp);
            }
        } else {
            $kd = "01";
        }
        return 'KM' . date('ymd') . '-' . $kd;
    }

    function createKj($id) //kode jurnal
    {
        $q = $this->db->query("SELECT MAX(RIGHT(bukti,2)) AS kd_max FROM tb_jurnal WHERE DATE(tanggal)=CURDATE() and id_user=$id and bukti like '%JU%' ");
        $kd = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $k) {
                $tmp = ((int) $k->kd_max) + 1;
                $kd = sprintf("%02s", $tmp);
            }
        } else {
            $kd = "01";
        }
        return 'JU' . date('ymd') . '-' . $kd;
    }
    public function save() //tambah data member
    {
        # code...
        $tgl = date("Y-m-d");
        $jumlah = str_replace(".", "", $_POST['jumlah']);
        $kk = [
            'tanggal' => $tgl,
            'jumlah' => $jumlah,
            'kdbukti' => $_POST['bukti'],
            'kode_akun' => $_POST['akun'],
            'ket' => $_POST['ket'],
            'id_user' => $_POST['id_user']
        ];
        $kredit = [
            'tanggal' => $tgl,
            'bukti' => $_POST['bukti'],
            'kode_akun' => '1111',
            'ket' => $_POST['ket'],
            'kredit' => $jumlah,
            'id_user' => $_POST['id_user']
        ];
        $debet = [
            'tanggal' => $tgl,
            'bukti' => $_POST['bukti'],
            'kode_akun' => $_POST['akun'],
            'ket' => $_POST['ket'],
            'debet' => $jumlah,
            'id_user' => $_POST['id_user']
        ];

        $this->db->insert($this->_tbk, $kk);
        $this->db->insert($this->_tbj, $debet);
        $this->db->insert($this->_tbj, $kredit);
    }
    public function savekm() //tambah data member
    {
        # code...
        $tgl = date("Y-m-d");
        $jumlah = str_replace(".", "", $_POST['jumlah']);
        $km = [
            'tanggal' => $tgl,
            'jumlah' => $jumlah,
            'kdbukti' => $_POST['bukti'],
            'kode_akun' => $_POST['akun'],
            'ket' => $_POST['ket'],
            'id_user' => $_POST['id_user']
        ];
        $debet = [
            'tanggal' => $tgl,
            'bukti' => $_POST['bukti'],
            'kode_akun' => '1111',
            'ket' => $_POST['ket'],
            'debet' => $jumlah,
            'id_user' => $_POST['id_user']
        ];
        $kredit = [
            'tanggal' => $tgl,
            'bukti' => $_POST['bukti'],
            'kode_akun' => $_POST['akun'],
            'ket' => $_POST['ket'],
            'kredit' => $jumlah,
            'id_user' => $_POST['id_user']
        ];

        $this->db->insert($this->_tbm, $km);
        $this->db->insert($this->_tbj, $debet);
        $this->db->insert($this->_tbj, $kredit);
    }
    public function savejurnal() //tambah data member
    {
        # code...
        $tgl = date("Y-m-d");
        $jumlah = str_replace(".", "", $_POST['jumlah']);

        $debet = [
            'tanggal' => $tgl,
            'bukti' => $_POST['bukti'],
            'kode_akun' => $_POST['debet'],
            'ket' => $_POST['ket'],
            'kredit' => 0,
            'debet' => $jumlah,
            'id_user' => $_POST['id_user']
        ];

        $kredit = [
            'tanggal' => $tgl,
            'bukti' => $_POST['bukti'],
            'kode_akun' => $_POST['kredit'],
            'ket' => $_POST['ket'],
            'kredit' => $jumlah,
            'debet' => 0,
            'id_user' => $_POST['id_user']
        ];


        $this->db->insert($this->_tbj, $debet);
        $this->db->insert($this->_tbj, $kredit);
    }


    public function M_update() // mengupdate data dari kk
    {
        # code...
        $_POST = $this->input->post();
        // $this->id_modal = $_POST['id_modal'];
        $jumlah = str_replace(".", "", $_POST['jumlah']);
        $kk = [
            'ket' => $_POST['ket'],
            'kode_akun' => $_POST['akun'],
            'jumlah' => $jumlah
        ];

        $kredit = [
            'ket' => $_POST['ket'],
            'kredit' => $jumlah
        ];
        $debet = [
            'ket' => $_POST['ket'],
            'kode_akun' => $_POST['akun'],
            'debet' => $jumlah
        ];
        $this->db->update($this->_tbk, $kk, array('kdbukti' => $_POST['bukti']));
        $this->db->update($this->_tbj, $kredit, array('bukti' => $_POST['bukti'], 'kode_akun' => '1111'));
        $this->db->update($this->_tbj, $debet, array('bukti' => $_POST['bukti'], 'kode_akun' => $_POST['ak']));
    }
    public function M_updatekm() // mengupdate data dari database
    {
        # code...
        $_POST = $this->input->post();
        // $this->id_modal = $_POST['id_modal'];
        $jumlah = str_replace(".", "", $_POST['jumlah']);
        $km = [
            'ket' => $_POST['ket'],
            'kode_akun' => $_POST['akun'],
            'jumlah' => $jumlah
        ];
        $debet = [
            'ket' => $_POST['ket'],
            'debet' => $jumlah
        ];
        $kredit = [
            'ket' => $_POST['ket'],
            'kode_akun' => $_POST['akun'],
            'kredit' => $jumlah
        ];

        $this->db->update($this->_tbm, $km, array('kdbukti' => $_POST['bukti']));
        $this->db->update($this->_tbj, $debet, array('bukti' => $_POST['bukti'], 'kode_akun' => '1111'));
        $this->db->update($this->_tbj, $kredit, array('bukti' => $_POST['bukti'], 'kode_akun' => $_POST['ak']));
    }

    public function M_updatejurnal() // mengupdate data dari database
    {
        # code...
        $_POST = $this->input->post();
        // $this->id_modal = $_POST['id_modal'];
        $jumlah = str_replace(".", "", $_POST['jumlah']);
        $jenis = $_POST['jenis'];
        $debet = [
            'debet'      => $jumlah
        ];
        $kredit = [
            'kredit'      => $jumlah
        ];

        if ($jenis == 'D') {
            $this->db->update($this->_tbj, $debet, array('idjurnal' => $_POST['bukti']));
        } else {
            $this->db->update($this->_tbj, $kredit, array('idjurnal' => $_POST['bukti']));
        }
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_modal extends CI_Model
{

    private $_table = "tb_modal"; //tanggal tabel


    public $tanggal;
    public $modal;


    public function rules() // digunakan untuk validatornya 
    {
        # code...

        return [

            [
                'field' => 'modal',
                'label' => 'modal',
                'rules' => 'required'
            ]
        ];
    }


    public function getAll($id) //mengambil semua data
    {
        # code...
        $tgl = date("Y-m-d");
        return $this->db->get_where($this->_table, ["tanggal" => $tgl, "id_user" => $id])->result();
    }

    public function getById($id) //mengambil data berdasarkan id
    {
        # code...
        return $this->db->get_where($this->_table, ["id_modal" => $id])->row();
    }

    public function save() //tambah data member
    {
        # code...
        $tgl = date("Y-m-d");
        $mdlkasir = str_replace(".", "", $_POST['modal']);

        $this->tanggal = $tgl;
        $this->modal = $mdlkasir;
        $this->id_user = $_POST['id_user'];

        $this->db->insert($this->_table, $this);
    }

    public function M_update() // mengupdate data dari database
    {
        # code...
        $_POST = $this->input->post();
        // $this->id_modal = $_POST['id_modal'];
        $mdlkasir = str_replace(".", "", $_POST['modal']);
        $this->id_modal = $_POST['id_modal'];
        $this->tanggal = $_POST['tanggal'];
        $this->modal = $mdlkasir;

        $this->db->update($this->_table, $this, array('id_modal' => $_POST['id_modal']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array('id_modal' => $id));
    }
}

/* End of file M_member.php */

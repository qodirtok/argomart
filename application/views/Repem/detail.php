 <!-- Begin Page Content -->
 <div class="container-fluid">

     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <a href="<?= base_url('returpembelian'); ?>" class="btn badge">
                 <h6 class="m-0 font-weight-bold text-primary"> <i class="fas fa-arrow-left"></i>&nbsp;<?= $judul; ?></h6>
             </a>
         </div>
         <div class="card-body">
             <div class="table-responsive">
                 <table style="text-align:center;" class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>No Faktur</th>
                             <th>Nama Barang</th>
                             <th>Stok <br>
                                 Barang Saat Ini</th>
                             <th>Jumlah Pembelian <br> Satuan</th>
                             <th>Harga</th>
                             <th>Sub Total</th>
                             <th>Status Pembayaran</th>
                             <th>Aksi</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php foreach ($detail as $row => $value) : ?>
                             <?php $number = 1; ?>
                             <tr>
                                 <td>
                                     <?= ($row + 1) ?>
                                 </td>
                                 <td>
                                     <?= $value->id_pembelian ?>
                                 </td>
                                 <td>
                                     <?= $value->nama ?>
                                 </td>
                                 <td>
                                     <?= $value->stok ?>
                                 </td>
                                 <td>
                                     <?= $value->jumlah ?> / <?= $value->nma_satuan ?>
                                 </td>
                                 <td>
                                     Rp. <?= number_format($value->hrg_beli); ?>
                                 </td>
                                 <td>
                                     Rp. <?= number_format($value->total); ?>
                                 </td>
                                 <td>
                                     <?php

                                        $this->db->select('*')
                                            ->from('tb_jurnal')
                                            ->like('bukti', $value->id_pembelian);
                                        $cek = $this->db->get()->result();
                                        foreach ($cek as $data) {
                                            if ($data->kode_akun == '2101') {
                                                $pemb = 'Hutang';
                                            } else if ($data->kode_akun == '1111') {
                                                $pemb = 'Lunas';
                                            }
                                        }
                                        ?>
                                     <?= $pemb ?>

                                 </td>
                                 <td>
                                     <a onclick="editModal<?= ($row + 1) ?>('<?= base_url('returpembelian/detaill/') . $value->id_pembelian; ?>')" href="#!" class="btn badge btn-outline-primary">Proses</a>

                                     <div class="modal fade" id="editModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                         <div class="modal-dialog" role="document">
                                             <div class="modal-content">
                                                 <div class="modal-header">
                                                     <h5 class="modal-title" id="exampleModalLabel">Proses Retur Penjualan</h5>
                                                     <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                         <span aria-hidden="true">×</span>
                                                     </button>
                                                 </div>
                                                 <div class="modal-body">
                                                     <form action="<?= base_url('returpembelian/store/') ?>" method="post" enctype="multipart/form-data">
                                                         <input type="hidden" name="nofak" value="<?= $value->id_pembelian ?>">
                                                         <input type="hidden" name="kode" value="<?= $value->Kode_barang ?>">
                                                         <input type="hidden" name="harga" value="<?= $value->hrg_beli ?>">
                                                         <input type="hidden" name="stok" value="<?= $value->stok ?>">
                                                         <input type="hidden" name="cek" value="<?= $pemb ?>">
                                                         <input type="hidden" name="id_supplier" value="<?= $value->id_supplier ?>">
                                                         <input type="hidden" name="id_user" value="<?= $user['id_user']; ?>">

                                                         <div class="form-group row">
                                                             <div class="col-lg-4 mb-3 mb-sm-0">
                                                                 <label for="nama">Nama Barang</label>
                                                             </div>
                                                             <div class="col-lg-8">
                                                                 <input readonly class="form-control <?= form_error('nama') ? 'is-invalid' : '' ?>" type="text" name="nama" value="<?= $value->nama ?>" required />
                                                                 <div class="invalid-feedback">
                                                                     <?= form_error('nama') ?>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="form-group row">
                                                             <div class="col-lg-4 mb-3 mb-sm-0">
                                                                 <label for="jumlah">Jumlah Barang</label>
                                                             </div>
                                                             <div class="col-lg-8">
                                                                 <input class="form-control <?= form_error('jumlah') ? 'is-invalid' : '' ?>" type="number" name="jumlah" min="0" max="<?= $value->stok; ?>" value="<?= $value->jumlah ?>" required />
                                                                 <div class="invalid-feedback">
                                                                     <?= form_error('jumlah') ?>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <br>
                                                         <div class="form-group">
                                                             <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                                         </div>
                                                     </form>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                     <script>
                                         function editModal<?= ($row + 1) ?>(url) {
                                             $('#editModal<?= ($row + 1) ?>').modal();
                                         }
                                     </script>
                                 </td>
                             </tr>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
                 <!-- <hr>
                 <a href="<?= base_url('kirimbrg'); ?>" onclick="window.open('<?= base_url('kirimbrg/cetak/') . $value->id_pembelian; ?>');" class="btn btn-outline-success float-right">Cetak</a> -->
             </div>
         </div>
     </div>
 </div> <!-- /.container-fluid -->

 </div>
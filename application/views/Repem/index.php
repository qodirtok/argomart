 <!-- Begin Page Content -->
 <div class="container-fluid">
     <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
     <?php if ($this->session->flashdata('success')) : ?>
         <div class="alert alert-success" role="alert">
             <?php echo $this->session->flashdata('success'); ?>
         </div>
     <?php endif; ?>
     <?php if ($this->session->flashdata('failed')) : ?>
         <div class="alert alert-danger" role="alert">
             <?php echo $this->session->flashdata('failed'); ?>
         </div>
     <?php endif; ?>
     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <h6 class="m-0 font-weight-bold text-primary">Retur Titipan</h6>
         </div>
         <div class="card-body">
             <div class="table-responsive">
                 <table style="text-align:center;" class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                         <tr>
                             <th>No</th>
                             <!-- <th>No Faktur</th> -->
                             <th>Nama</th>
                             <th>No. Handphone</th>
                             <th>Alamat</th>
                             <th>Aksi</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php foreach ($kirim as $row => $value) : ?>
                             <?php $number = 1; ?>
                             <tr>
                                 <td>
                                     <?= ($row + 1) ?>
                                 </td>
                                 <!-- <td>
                                     <?= $value->id_pembelian ?>
                                 </td> -->
                                 <td>
                                     <?= $value->nma_supplier ?>
                                 </td>
                                 <td>
                                     <?= $value->notlpn ?>
                                 </td>
                                 <td>
                                     <?= $value->alamat ?>
                                 </td>
                                 <td>
                                     <a href="<?= base_url('returpembelian/detail/') . $value->id_supplier; ?>" class="btn badge btn-outline-primary">Detail</a>
                                 </td>
                             </tr>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div> <!-- /.container-fluid -->

 </div>
<!-- Begin Page Content -->
<div class="container-fluid">


	<div class="card bg-info text-white shadow" style="">
		<div class="card-body" style="">
			<center>
				<marquee behavior="" direction="">
					--- Agromart ---
				</marquee>
			</center>
			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
		</div>
	</div>
	<hr>


	<div class="card shadow mb-4 col-sm-6 mx-auto">
		<div class="card-header mb-4 ">
			<h6 class="m-0 font-weight-bold text-primary"> Form input Barang Baru</h6>
		</div>
		<div class="card-body mb-4">
			<?php if ($this->session->flashdata('success')) : ?>
				<?php echo $this->session->flashdata('success'); ?>
			<?php endif; ?>
			<?php if ($this->session->flashdata('failed')) : ?>
				<?php echo $this->session->flashdata('failed'); ?>
			<?php endif; ?>
			<form action="<?= base_url('barang'), "/store"; ?>" method="post" enctype="multipart/form-data">
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="Kode_barang">Kode Barang</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control <?= form_error('Kode_barang') ? 'is-invalid' : '' ?>" name="Kode_barang" placeholder="Barcode barang" autofocus value="<?= $kodebarang ?>" required>
						<small class="text-danger pl-2"> * Kode barang di atas untuk barang titipan</small>
						<div class="invalid-feedback">
							<?= form_error('Kode_barang') ?>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="nama">Nama barang</label>
					</div>
					<div class="col-lg-9">
						<input class="form-control <?= form_error('nama') ? 'is-invalid' : '' ?>" type="text" name="nama" placeholder="Nama Barang" required />
						<div class="invalid-feedback">
							<?= form_error('nama') ?>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="hrg_beli">Harga Beli</label>
					</div>
					<div class="col-lg-9">
						<input class=" form-control <?= form_error('hrg_beli') ? 'is-invalid' : '' ?>" type="text" id="rupiah" name="hrg_beli" placeholder="Harga Beli" required />
						<div class="invalid-feedback">
							<?= form_error('hrg_beli') ?>
						</div>
					</div>
				</div>
				<!-- <div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="hrg_jual_grosir">Harga Jual Grosir<br>(persen %)</label>
					</div>
					<div class="col-lg-9">
						<input class="form-control <?= form_error('persenangrosir') ? 'is-invalid' : '' ?>" type="text" name="persenangrosir" placeholder="Harga Jual Grosir" />
						<div class="invalid-feedback">
							<?= form_error('persenangrosir') ?>
						</div>
					</div>
				</div> -->
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="hrg_jual">Harga Jual</label>
					</div>
					<div class="col-lg-9">
						<input class="form-control <?= form_error('persenan') ? 'is-invalid' : '' ?>" type="text" id="ecer" name="persenan" placeholder="Harga Jual" required />
						<div class="invalid-feedback">
							<?= form_error('persenan') ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="stok" value="0">
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="hrg_beli">Kategori</label>
					</div>
					<!-- <label for="sel1">Select list:</label> -->
					<div class="col-lg-9">
						<select id="sel1" name="id_katagori" class="form-control selectpicker show-tick" data-live-search="true" title="Pilih Kategori" data-width="100%" required>
							<?php foreach ($kategori as $row) : ?>
								<option value="<?= $row->id_katagori ?>"><?= $row->nma_katagori ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="hrg_beli">Satuan</label>
					</div>
					<!-- <label for="sel1">Select list:</label> -->
					<div class="col-lg-9">
						<select id="sel2" name="id_satuan" class="form-control selectpicker show-tick" data-live-search="true" title="Pilih Satuan" data-width="100%" required>
							<?php foreach ($satuan as $row2) : ?>
								<option value="<?= $row2->id_satuan ?>"><?= $row2->nma_satuan ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="customFile">Upload</span>
					</div>
					<div class="custom-file">
						<input type="file" name="image" class="custom-file-input" id="customFile">
						<label class="custom-file-label" for="customFile">Choose file</label>
					</div>
				</div>
				<br>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">Simpan</button>
					<a href="<?= base_url('barang') ?>" class="btn btn-success btn-block">Batal</a>
				</div>
			</form>
		</div>
	</div>

</div>
<!-- /.container-fluid -->

</div>
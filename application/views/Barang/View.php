 <!-- Begin Page Content -->
 <div class="container-fluid">
 	<!-- <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div> -->

 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
 	<div class="card bg-info text-white shadow" style="">
 		<div class="card-body" style="">
 			<center>
 				<marquee behavior="" direction="">
 					--- halaman ini menampilkan list data barang ---
 				</marquee>
 			</center>
 			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
 		</div>
 	</div>
 	<hr>
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Data Barang</h6>
 		</div>

 		<div class="card-body">
 			<div>
 				<?php if ($this->session->flashdata('success')) : ?>
 					<?php echo $this->session->flashdata('success'); ?>
 				<?php endif; ?>
 				<?php if ($this->session->flashdata('failed')) : ?>
 					<?php echo $this->session->flashdata('failed'); ?>
 				<?php endif; ?>
 			</div>
 			<div>
 				<a href="<?= base_url('barang'), "/add"; ?>" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Tambah Barang</a>
 			</div>
 			<hr>
 			<div class="table-responsive">
 				<table style="text-align:center;font-size:14px;" class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr>
 							<th>No</th>
 							<th>Photo</th>
 							<th>Kategori Barang</th>
 							<th>Kode barang
 								<hr>
 								Nama barang</th>
 							<th>Harga Beli
 							</th>
 							<!-- <th>
 								Harga Jual (Grosir)
 							</th> -->
 							<th>
 								Harga Jual
 							</th>
 							<th>Stok / Satuan</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php foreach ($barang as $row => $value) : ?>
 							<?php $number = 1; ?>
 							<tr>
 								<td>
 									<?= ($row + 1) ?>
 								</td>
 								<td>
 									<img src="<?= base_url('asset/img/barang/'); ?><?= $value->image ?>" height="75px" alt="">
 								</td>
 								<td>
 									<?= $value->nma_katagori ?>
 								</td>
 								<td>
 									<?= $value->Kode_barang ?>
 									<hr>
 									<?= $value->nama ?>
 								</td>
 								<td><?= "Rp." . number_format($value->hrg_beli) ?>

 								</td>

 								<td>
 									<?= "Rp." . number_format($value->hrg_jual) ?>
 								</td>
 								<td>
 									<?php if ($value->stok >= 1) { ?>
 										<?= $value->stok . " / " . $value->nma_satuan ?> <?php } else { ?>
 										<label class="badge badge-danger">Habis</label>
 									<?php } ?>
 								</td>
 								<td>
 									<!-- <a href="<?= site_url('barang') ?>/show/<?= $value->id_barang ?>" class="badge badge-warning "><span class="fa fa-eye"> detail</span></a> -->
 									<!-- <a href="<?= site_url('barang') ?>/edit/<?= $value->id_barang ?>" class="badge badge-info"><span class="fa fa-edit"> edit barang</span></a> -->
 									<a onclick="editBarang<?= ($row + 1) ?>('<?= site_url('barang') ?>/update/<?= $value->id_barang ?>')" href="#!" class="badge badge-info tombol-edit"><span class="fa fa-edit"> Edit barang</span></a>
 									<div class="modal fade" id="editbarang<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 										<div class="modal-dialog" role="document">
 											<div class="modal-content">
 												<div class="modal-header">
 													<h5 class="modal-title" id="exampleModalLabel">Edit Barang</h5>
 													<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 														<span aria-hidden="true">×</span>
 													</button>
 												</div>
 												<div class="modal-body">
 													<form action="<?= site_url('barang/update/' . $value->id_barang) ?>" method="post" enctype="multipart/form-data">
 														<input type="hidden" name="id_barang" value="<?= $value->id_barang ?>">
 														<input type="hidden" name="stok" value="<?= $value->stok ?>">
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="Kode_barang">Kode Barang</label>
 															</div>
 															<div class="col-lg-9">
 																<input type="text" class="form-control <?= form_error('Kode_barang') ? 'is-invalid' : '' ?>" name="Kode_barang" placeholder="Barcode barang" value="<?= $value->Kode_barang ?>" readonly>
 																<div class="invalid-feedback">
 																	<?= form_error('Kode_barang') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="nama">Nama barang</label>
 															</div>
 															<div class="col-lg-9">
 																<input class="form-control <?= form_error('nama') ? 'is-invalid' : '' ?>" type="text" name="nama" placeholder="Nama Barang" value="<?= $value->nama ?>" />
 																<div class="invalid-feedback">
 																	<?= form_error('nama') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="hrg_beli">Harga Beli</label>
 															</div>
 															<div class="col-lg-9">
 																<input class="form-control <?= form_error('hrg_beli') ? 'is-invalid' : '' ?>" type="text" id="rupiah" name="hrg_beli" placeholder="Harga Beli" value="<?= number_format($value->hrg_beli, '0', ',', '.'); ?>" />
 																<div class="invalid-feedback">
 																	<?= form_error('hrg_beli') ?>
 																</div>
 															</div>
 														</div>
 														<!-- <div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="hrg_jual_grosir">Harga Jual</label>
 															</div>
 															<div class="col-lg-9">
 																<input class="form-control <?= form_error('hrg_jual_grosir') ? 'is-invalid' : '' ?>" type="text" id="uang" name="persenangrosir" placeholder="Harga Jual (Grosir)" value="<?= $value->persenangrosir ?>" />
 																<div class="invalid-feedback">
 																	<?= form_error('hrg_jual_grosir') ?>
 																</div>
 															</div>
 														</div> -->
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="hrg_jual">Harga Jual</label>
 															</div>
 															<div class="col-lg-9">
 																<input class="form-control <?= form_error('persenan') ? 'is-invalid' : '' ?>" type="text" id="ecer" name="persenan" placeholder="Harga Jual" value="<?= number_format($value->hrg_jual, '0', ',', '.'); ?>" />
 																<div class="invalid-feedback">
 																	<?= form_error('persenan') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="sel1">Kategori</label>
 															</div>
 															<div class="col-lg-9">
 																<select class="form-control" id="sel1" name="id_katagori">
 																	<option value="<?= $value->id_katagori ?>">
 																		<?php foreach ($kategori as $key) :
																					if ($value->id_katagori == $key->id_katagori) {
																						echo $key->nma_katagori;
																					}
																				endforeach; ?>
 																	</option>
 																	<?php foreach ($kategori as $row5) : ?>
 																		<option value="<?= $row5->id_katagori ?>"><?= $row5->nma_katagori ?></option>
 																	<?php endforeach; ?>
 																</select>
 															</div>
 														</div>

 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="sel1">Kategori</label>
 															</div>
 															<div class="col-lg-9">
 																<select class="form-control" id="sel2" name="id_satuan">
 																	<option value="<?= $value->id_satuan ?>">
 																		<?php foreach ($satuan as $key2) :
																					if ($value->id_satuan == $key2->id_satuan) {
																						echo $key2->nma_satuan;
																					}
																				endforeach; ?>
 																	</option>
 																	<?php foreach ($satuan as $row2) : ?>
 																		<option value="<?= $row2->id_satuan ?>"><?= $row2->nma_satuan ?></option>
 																	<?php endforeach; ?>
 																</select>
 															</div>
 														</div>

 														<div class="input-group">
 															<div class="input-group-prepend">
 																<span class="input-group-text" id="customFile">Upload</span>
 															</div>
 															<div class="custom-file">
 																<input type="file" name="image" class="custom-file-input" id="customFile">
 																<label class="custom-file-label" for="customFile">Choose file</label>
 															</div>
 														</div>
 														<br>
 														<div class="form-group ">
 															<button type="submit" class="btn btn-block btn-primary btn-user">Simpan</button>
 														</div>
 													</form>
 												</div>
 											</div>
 										</div>
 									</div>

 									<script>
 										function editBarang<?= ($row + 1) ?>(url) {
 											$('#editbarang<?= ($row + 1) ?>').modal();
 										}
 									</script>


 									<a onclick="deleteConfirm<?= ($row + 1) ?>('<?= site_url('barang/destroy/' . $value->id_barang) ?>')" href="#!" class="badge badge-danger"><span class="fa fa-trash"> Hapus</span></a>

 									<!-- Logout Delete Confirmation-->
 									<div class="modal fade" id="deleteModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 										<div class="modal-dialog" role="document">
 											<div class="modal-content">
 												<div class="modal-header">
 													<h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ?</h5>
 													<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 														<span aria-hidden="true">×</span>
 													</button>
 												</div>
 												<div class="modal-body">
 													<p>Hapus pada barang <?= $value->nama ?> ?</p>
 													Data yang dihapus tidak akan bisa dikembalikan.
 												</div>
 												<div class="modal-footer">
 													<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
 													<a id="btn-delete<?= ($row + 1) ?>" class="btn btn-danger" href="#">Delete</a>
 												</div>
 											</div>
 										</div>
 									</div>
 									<script>
 										function deleteConfirm<?= ($row + 1) ?>(url) {
 											$('#btn-delete<?= ($row + 1) ?>').attr('href', url);
 											$('#deleteModal<?= ($row + 1) ?>').modal();
 										}
 									</script>
 									<!-- <hr>
 									<a onclick="tambahStok<?= ($row + 1) ?>('<?= site_url('barang/updateStok' . $value->id_barang) ?>')" href="#!" class="badge badge-success tombol-tambahstok"><span class="fa fa-plus"> Tambah stok</span></a> -->

 									<!-- Logout Delete Confirmation-->
 									<div class="modal fade" id="stokModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 										<div class="modal-dialog" role="document">
 											<div class="modal-content">
 												<div class="modal-header">
 													<h5 class="modal-title" id="exampleModalLabel">tambahStok</h5>
 													<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 														<span aria-hidden="true">×</span>
 													</button>
 												</div>
 												<div class="modal-body">
 													<p>
 														Stok awal : <?= $value->stok ?>
 													</p>
 													<form action="<?= site_url('barang/updateStok/' . $value->id_barang) ?>" method="POST">
 														<div class="form-group col-lg-8 mx-auto">
 															<input type="hidden" name="id_barang" value="<?= $value->id_barang ?>">
 															<input type="hidden" name="old_stok" value="<?= $value->stok ?>">
 															<input class="form-control <?= form_error('stok') ? 'is-invalid' : '' ?>" type="text" name="stok" placeholder="Tambah stok barang" />
 															<div class="invalid-feedback">
 																<?= form_error('stok') ?>
 															</div>
 														</div>
 														<div class="form-group col-lg-6 mx-auto">
 															<button type="submit" class="btn btn-primary btn-block">Submit</button>
 														</div>
 													</form>
 												</div>
 											</div>
 										</div>
 									</div>


 									<script>
 										function tambahStok<?= ($row + 1) ?>(url) {
 											$('#stokModal<?= ($row + 1) ?>').modal();
 										}
 									</script>
 								</td>
 							</tr> <?php endforeach; ?>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>
 </div> <!-- /.container-fluid -->

 </div>
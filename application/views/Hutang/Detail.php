<div class="container">

	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">
				<div class="col-lg-12">
					<div class="p-5">
						<div>
							<?= $this->session->flashdata('message'); ?>
						</div>
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">Detail hutang</h1>
						</div>
						<form class="user">
							<div class="table-responsive">
								<table class="table table-striped table-bordered nowrap">
									<?php foreach ($detail as $rows => $value) : ?>
										<tr>
											<th width="30%">Kode Supplier</th>
											<th width="1%">:</th>
											<td>
												<p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?= $value->kode_supplier ?></p>
											</td>
										</tr>
										<tr>
											<th width="30%">Nama Supplier</th>
											<th width="1%">:</th>
											<td>
												<p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?= $value->nma_supplier ?></p>
											</td>
										</tr>
										<tr>
											<th width="30%">No hp</th>
											<th width="1%">:</th>
											<td>
												<p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?= $value->notlpn ?></p>
											</td>
										</tr>
										<tr>
											<th width="30%">Alamat</th>
											<th width="1%">:</th>
											<td>
												<p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?= $value->alamat ?></p>
											</td>
										</tr>
										<tr>
											<th width="30%">Status</th>
											<th width="1%">:</th>
											<td>
												<p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?php if ($value->sisa_bayar <= 0) { ?>
														<span class="badge badge-success">Lunas</span>
													<?php } else { ?>
														<span class="badge badge-danger">Belum Lunas</span>
													<?php } ?></p>
											</td>
										</tr>
										<tr>
											<th width="30%">Sisa hutang</th>
											<th width="1%">:</th>
											<td>
												<p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?php if ($value->sisa_bayar >= 0) {
																																			echo 'Rp.' . number_format($value->sisa_bayar);
																																		} else {
																																			echo 'Rp.' . number_format(0);
																																		} ?></p>
											</td>
										</tr>
										<tr>
											<th width="30%">Bayar</th>
											<th width="1%">:</th>
											<td>
												<p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?php echo 'Rp.' . number_format($value->dibayar) ?></p>
												<a onclick="bayar('<?= base_url('user/hutang-bayar/') . $value->id_hutang ?>')" href="#!" class="btn btn-google btn-user btn-block">
													<i class="fas fa-pencil-alt"></i>&nbsp; bayar
												</a>
											</td>
										</tr>

								</table>
							</div>

							<div class="modal fade" id="editHutang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Edit Nominal</h5>
											<button class="close" type="button" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">×</span>
											</button>
										</div>
										<div class="modal-body">
											<form action="<?php echo base_url('user/hutang-tambahUtang/') . $value->id_hutang; ?>" method="post" enctype="multipart/form-data">
												<input type="hidden" name="id_member" value="<?php echo $value->id_supplier ?>">
												<input type="text" name="old_utang" value="<?php echo $value->sisa_bayar ?>">

												<div class="form-group row">
													<div class="col-lg-4 mb-3 mb-sm-0">
														<label for="Alamat">Nominal</label>
													</div>
													<div class="col-lg-8">
														<input id="sisa_bayar" class="nominal form-control <?= form_error('sisa_bayar') ? 'is-invalid' : '' ?>" type="text" name="sisa_bayar" placeholder="" />
														<div class="invalid-feedback">
															<?= form_error('sisa_bayar') ?>
														</div>
													</div>
												</div>
												<br>
												<div class="form-group">
													<button type="submit" class="btn btn-primary btn-block">Submit</button>
												</div>
											</form>
										</div>
									</div>

								</div>
							</div>

							<div class="modal fade" id="bayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Bayar</h5>
											<button class="close" type="button" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">×</span>
											</button>
										</div>
										<div class="modal-body">
											<form action="<?= base_url('user/hutang-bayar/') . $value->id_hutang; ?>" method="post" enctype="multipart/form-data">
												<input type="hidden" name="id_member" value="<?php echo $value->id_member ?>">
												<input type="hidden" name="id_hutang" value="<?php echo $this->uri->segment(3) ?>">
												<input type="hidden" name="id_user" value="<?php echo $user['id_user']; ?>">
												<input type="hidden" name="nama" value="<?php echo $value->nma_supplier ?>">
												<input type="hidden" name="old_utang" value="<?php echo $value->sisa_bayar ?>">
												<input type="hidden" name="old_bayar" value="<?php echo $value->dibayar ?>">
												<div class="form-group row">
													<div class="col-lg-4 mb-3 mb-sm-0">
														<label for="Alamat">Nominal</label>
													</div>
													<div class="col-lg-8">
														<input id="nominal" class="nominal form-control <?= form_error('nominal') ? 'is-invalid' : '' ?>" type="text" name="nominal" placeholder="" />
														<div class="invalid-feedback">
															<?= form_error('nominal') ?>
														</div>
													</div>
												</div>
												<br>
												<div class="form-group">
													<button type="submit" class="btn btn-primary btn-block">Simpan</button>
												</div>
											</form>
										</div>
									</div>

								</div>
							</div>
							<script>
								function editHutang(url) {
									$('#editHutang').modal();
								};

								function bayar(url) {
									$('#bayar').modal();
								};
							</script>
						<?php endforeach; ?>
						</form>
						<hr>
						<div class="card shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary">Data histori Hutang</h6>
							</div>
							<div class="card-body">
								<div>
								</div>
								<!-- <hr> -->
								<div class="table-responsive">
									<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
										<thead>
											<th>#</th>
											<th>Bayar (Rp)</th>
											<th>Tanggal</th>
											<th>Aksi</th>
										</thead>
										<tbody>
											<?php foreach ($tmp as $key => $t) : ?>
												<tr>
													<td><?= ($key + 1); ?></td>
													<td><?= "Rp." . number_format($t->nominal, 0, ',', '.'); ?></td>
													<td><?= $t->date; ?></td>
													<td><a href="<?= base_url('Hutang/Cetak/' . $t->id_hutang); ?>" class="btn btn-outline-primary"><i class="fas fa-print"></i> Print</a></td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

</div>
 <!-- Begin Page Content -->
 <div class="container-fluid">
 	<!-- <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div> -->
 	<?php if ($this->session->flashdata('success')) : ?>
 		<div class="alert alert-success" role="alert">
 			<?php echo $this->session->flashdata('success'); ?>
 		</div>
 	<?php endif; ?>
 	<?php if ($this->session->flashdata('failed')) : ?>
 		<div class="alert alert-danger" role="alert">
 			<?php echo $this->session->flashdata('failed'); ?>
 		</div>
 	<?php endif; ?>
 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
 	<div class="card bg-info text-white shadow" style="">
 		<div class="card-body" style="">
 			<center>
 				<marquee behavior="" direction="">
 					--- halaman ini menampilkan list data Hutang ---
 				</marquee>
 			</center>
 			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
 		</div>
 	</div>
 	<hr>
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Data Hutang</h6>
 		</div>
 		<div class="card-body">
 			<div>
 				<!-- <a onclick="tambahMember('<?= base_url('user/hutang-add') ?>')" href="#!" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Tambah data</a> -->

 				<div class="modal fade" id="tambahMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 					<div class="modal-dialog" role="document">
 						<div class="modal-content">
 							<div class="modal-header">
 								<h5 class="modal-title" id="exampleModalLabel">Input Hutang</h5>
 								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 									<span aria-hidden="true">×</span>
 								</button>
 							</div>
 							<div class="modal-body">
 								<form action="<?= base_url('user/hutang-add'); ?>" method="post" enctype="multipart/form-data">
 									<input type="hidden" name="dibayar" value="0">
 									<input type="hidden" name="id_user" value="<?= $user['id_user']; ?>">
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="kode member">Kode Member</label>
 										</div>
 										<div class="col-lg-8">
 											<select id="sel1" name="id_member" class="form-control selectpicker show-tick" data-live-search="true" title="Kode member" data-width="100%" required>
 												<?php foreach ($member as $row2 => $val) : ?>
 													<option value="<?= $val->id_member ?>"><?php echo $val->nma_supplier ?> - <?= $val->kode_supplier ?></option>
 												<?php endforeach; ?>
 											</select>
 										</div>
 									</div>
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="Alamat">Nominal</label>
 										</div>
 										<div class="col-lg-8">
 											<input id="nominal" class="nominal form-control <?= form_error('nominal') ? 'is-invalid' : '' ?>" type="text" name="sisa_bayar" placeholder="" />
 											<div class="invalid-feedback">
 												<?= form_error('nominal') ?>
 											</div>
 										</div>
 									</div>
 									<br>
 									<div class="form-group">
 										<button type="submit" class="btn btn-primary btn-block">Submit</button>
 									</div>
 								</form>
 							</div>
 						</div>

 					</div>
 				</div>
 				<script>
 					function tambahMember(url) {
 						$('#tambahMember').modal();
 					}
 				</script>
 			</div>
 			<!-- <hr> -->
 			<div class="table-responsive">
 				<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr>
 							<th>No</th>
 							<th>Kode Supplier</th>
 							<th>Nama Supplier</th>
 							<th>Nomor Handphone</th>
 							<th>Status</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php foreach ($hutang as $row => $value) : ?>
 							<?php $number = 1; ?>
 							<tr>
 								<td>
 									<?= ($row + 1) ?>
 								</td>
 								<td>
 									<?= $value->kode_supplier ?>
 								</td>
 								<td>
 									<?= $value->nma_supplier ?>
 								</td>
 								<td>
 									<?= $value->notlpn ?>
 								</td>
 								<td>
 									<?php if ($value->sisa_bayar <= 0) { ?>
 										<span class="badge badge-success">Lunas</span>
 									<?php } else { ?>
 										<span class="badge badge-danger">Belum Lunas</span>
 									<?php } ?>
 								</td>
 								<td>
 									<a href="<?php echo base_url('user/hutang-detail/') . $value->id_hutang ?>" class="btn badge btn-outline-primary">Detail</a>
 								</td>
 							</tr>
 						<?php endforeach; ?>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>
 	<!-- /.container-fluid -->
 </div>
 </div>

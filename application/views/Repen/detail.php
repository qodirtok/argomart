 <!-- Begin Page Content -->
 <div class="container-fluid">

     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <a href="<?= base_url('returpenjualan'); ?>" class="btn badge">
                 <h6 class="m-0 font-weight-bold text-primary"> <i class="fas fa-arrow-left"></i>&nbsp;<?= $judul; ?></h6>
             </a>
         </div>
         <div class="card-body">
             <div class="table-responsive">
                 <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                         <tr class="text-center">
                             <th>No</th>
                             <th>No Faktur</th>
                             <th>Nama Barang</th>
                             <th>Jumlah / Satuan</th>
                             <th>Harga</th>
                             <th>Sub Total</th>
                             <th>Aksi</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php foreach ($detail as $row => $value) : ?>
                             <?php $number = 1; ?>
                             <tr>
                                 <td class="text-center">
                                     <?= ($row + 1) ?>
                                 </td>
                                 <td>
                                     <?= $value->no_faktur ?>
                                 </td>
                                 <td>
                                     <?= $value->nama ?>
                                 </td>
                                 <td>
                                     <?= $value->jumlah ?> / <?= $value->satuan ?>
                                 </td>
                                 <td>
                                     Rp. <?= number_format($value->harga); ?>
                                 </td>
                                 <td>
                                     Rp. <?= number_format($value->sub_total); ?>
                                 </td>
                                 <td>
                                     <a onclick="editModal<?= ($row + 1) ?>('<?= base_url('returpenjualan/detaill/') . $value->no_faktur; ?>')" href="#!" class="btn badge btn-outline-primary">Proses</a>

                                     <div class="modal fade" id="editModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                         <div class="modal-dialog" role="document">
                                             <div class="modal-content">
                                                 <div class="modal-header">
                                                     <h5 class="modal-title" id="exampleModalLabel">Proses Retur Penjualan</h5>
                                                     <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                         <span aria-hidden="true">×</span>
                                                     </button>
                                                 </div>
                                                 <div class="modal-body">
                                                     <form action="<?= base_url('returpenjualan/store/') ?>" method="post" enctype="multipart/form-data">
                                                         <?php foreach ($this->db->get_where('tb_hutang', ['no_faktur' => $value->no_faktur])->result() as $ht) ?>
                                                         <?php foreach ($this->db->get_where('tb_barang', ['Kode_barang' => $value->Kode_barang])->result() as $br) ?>
                                                         <input type="hidden" name="nofak" value="<?= $value->no_faktur ?>">
                                                         <?php if (isset($ht)) { ?>
                                                             <input type="hidden" name="old_hutang" value="<?= $ht->sisa_bayar ?>">
                                                         <?php } ?>
                                                         <input type="hidden" name="stok" value="<?= $br->stok ?>">
                                                         <input type="hidden" name="kode" value="<?= $value->Kode_barang ?>">
                                                         <input type="hidden" name="harga" value="<?= $value->harga ?>">
                                                         <input type="hidden" name="id_user" value="<?= $user['id_user']; ?>">
                                                         <input type="hidden" name="id_member" value="<?= $value->id_member ?>">
                                                         <input type="hidden" name="harbel" value="<?= $value->hrg_pokok ?>">
                                                         <div class="form-group row">
                                                             <div class="col-lg-4 mb-3 mb-sm-0">
                                                                 <label for="metode">Metode Retur</label>
                                                             </div>
                                                             <div class="col-lg-8">
                                                                 <select name="metode" id="metode" class="form-control" required>
                                                                     <option value="">Pilih Metode</option>
                                                                     <option value="1">Ganti Dengan Barang</option>
                                                                     <option value="2">Ganti Dengan Tunai</option>
                                                                     <option value="3">Mengurangi Piutang</option>
                                                                 </select>
                                                                 <div class="invalid-feedback">
                                                                     <?= form_error('metode') ?>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="form-group row">
                                                             <div class="col-lg-4 mb-3 mb-sm-0">
                                                                 <label for="nama">Nama Barang</label>
                                                             </div>
                                                             <div class="col-lg-8">
                                                                 <input readonly class="form-control <?= form_error('nama') ? 'is-invalid' : '' ?>" type="text" name="nama" value="<?= $value->nama ?>" required />
                                                                 <div class="invalid-feedback">
                                                                     <?= form_error('nama') ?>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="form-group row">
                                                             <div class="col-lg-4 mb-3 mb-sm-0">
                                                                 <label for="jumlah">Jumlah Barang</label>
                                                             </div>
                                                             <div class="col-lg-8">
                                                                 <input class="form-control <?= form_error('jumlah') ? 'is-invalid' : '' ?>" type="number" name="jumlah" min="0" max="<?= $value->jumlah; ?>" value="<?= $value->jumlah ?>" required />
                                                                 <div class="invalid-feedback">
                                                                     <?= form_error('jumlah') ?>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <br>
                                                         <div class="form-group">
                                                             <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                                         </div>
                                                     </form>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                     <script>
                                         function editModal<?= ($row + 1) ?>(url) {
                                             $('#editModal<?= ($row + 1) ?>').modal();
                                         }
                                     </script>
                                 </td>
                             </tr>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
                 <!-- <hr>
                 <a href="<?= base_url('kirimbrg'); ?>" onclick="window.open('<?= base_url('kirimbrg/cetak/') . $value->no_faktur; ?>');" class="btn btn-outline-success float-right">Cetak</a> -->
             </div>
         </div>
     </div>
 </div> <!-- /.container-fluid -->

 </div>
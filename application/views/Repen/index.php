 <!-- Begin Page Content -->
 <div class="container-fluid">
     <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
     <?php if ($this->session->flashdata('success')) : ?>
         <div class="alert alert-success" role="alert">
             <?php echo $this->session->flashdata('success'); ?>
         </div>
     <?php endif; ?>
     <?php if ($this->session->flashdata('failed')) : ?>
         <div class="alert alert-danger" role="alert">
             <?php echo $this->session->flashdata('failed'); ?>
         </div>
     <?php endif; ?>
     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <h6 class="m-0 font-weight-bold text-primary">Retur Penjualan</h6>
         </div>
         <div class="card-body">
             <div class="table-responsive">
                 <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                         <tr class="text-center">
                             <th>No</th>
                             <th>No Faktur</th>
                             <th>Tgl. Penjualan</th>
                             <th>Nama</th>
                             <th>Tgl / Jml Retur</th>
                             <th>Aksi</th>
                         </tr>
                     </thead>
                     <tbody>

                         <?php foreach ($repen as $row => $value) : ?>
                             <?php $number = 1; ?>
                             <tr>
                                 <td class="text-center">
                                     <?= ($row + 1) ?>
                                 </td>
                                 <td>
                                     <?= $value->no_faktur ?>
                                 </td>
                                 <td>
                                     <?= $value->tgl_penjualan ?>
                                 </td>
                                 <td>
                                     <?= $value->Nama ?>
                                 </td>
                                 <td class="text-center">
                                     <?php foreach ($retur as $r => $v)
                                                if ($value->no_faktur == $v->no_faktur) {
                                                    $tgl = $v->tanggal;
                                                    $jml = $v->jml;
                                                    echo $rt = $tgl . ' / ' . $jml;
                                                }
                                            ?>

                                 </td>
                                 <td class="text-center">
                                     <a href="<?= base_url('returpenjualan/detail/') . $value->no_faktur; ?>" class="btn badge btn-outline-primary">Detail</a>
                                 </td>
                             </tr>
                         <?php endforeach; ?>

                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div> <!-- /.container-fluid -->

 </div>
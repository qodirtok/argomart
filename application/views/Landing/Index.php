<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- <title><?= $title ?></title> -->

	<!-- Custom fonts for this template-->
	<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?= base_url('asset/'); ?>css/sb-admin-2.min.css" rel="stylesheet">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		.mySlides {
			display: none;
		}
	</style>
	<title>MiddosMarket</title>
</head>

<body class="bg-gradient-primary">
	<div class="container">
		<h1 class="w3-center">
			<span class="alert alert-info">Diskon Barang</span>
		</h1>
		<br>
		<br>
		<br>
		<div class="card o-hidden border-0 shadow-lg my-5">
			<div class="card-body p-0">
				<div class="row">
					<div class="col-lg-12">
						<div class="p-5">
							<marquee behavior="" direction="up">
								<div class="" style="">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>No</th>
													<th>Gambar</th>
													<th>Nama barang</th>
													<th>Diskon (%)</th>
													<th>Harga</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($barang as $key => $value) : ?>
													<tr>
														<td><?php echo ($key + 1) ?></td>
														<td style="width: 50%;"><img class="mySlides" src="<?= base_url('asset/img/barang/'); ?><?= $value->image ?>" style="width:10%"></td>
														<td><?php echo $value->nama ?></td>
														<td><?php echo $value->diskon . "%" ?></td>
														<td><?php $dolardiskon = (((100 - $value->diskon) / 100) * $value->hrg_jual);
																echo number_format($dolardiskon); ?></td>
													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</marquee>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="<?= base_url('asset/'); ?>vendor/jquery/jquery.min.js"></script>
	<script src="<?= base_url('asset/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="<?= base_url('asset/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="<?= base_url('asset/'); ?>js/sb-admin-2.min.js"></script>

	<!-- Page level plugins -->
	<script src="<?= base_url('asset/'); ?>vendor/chart.js/Chart.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="<?= base_url('asset/'); ?>js/demo/chart-area-demo.js"></script>
	<script src="<?= base_url('asset/'); ?>js/demo/chart-pie-demo.js"></script>

	<script>
		var myIndex = 0;
		carousel();

		function carousel() {
			var i;
			var x = document.getElementsByClassName("mySlides");
			for (i = 0; i < x.length; i++) {
				x[i].style.display = "none";
			}
			myIndex++;
			if (myIndex > x.length) {
				myIndex = 1
			}
			x[myIndex - 1].style.display = "block";
			setTimeout(carousel, 2000); // Change image every 2 seconds
		}
	</script>

	<script type="text/javascript">
		setTimeout(function() {
			location.reload();
		}, 100000);
	</script>
</body>

</html>

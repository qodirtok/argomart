 <!-- Begin Page Content -->
 <div class="container-fluid">

 	<!-- Page Heading -->
 	<h1 class="h3 mb-2 text-gray-800">.::Data Akun::.</h1>
 	<hr>
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Data Akun</h6>
 		</div>
 		<div class="card-body">
 			<div>
 				<?= $this->session->flashdata('message'); ?>
 			</div>
 			<div>
 				<a href="<?= base_url('akun'), "/tambah"; ?>" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Tambah Akun</a>
 			</div>
 			<hr>
 			<div class="table-responsive">
 				<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr>
 							<th>No</th>
 							<th>Nama</th>
 							<th>Jenis Kelamain</th>
 							<th>No. Handphone/Telp</th>
 							<th>Alamat</th>
 							<th>Username</th>
 							<th>Foto</th>
 							<th>Hak Akses</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php foreach ($tampil as $row => $value) : ?>
 							<?php $number = 1; ?>
 							<tr>
 								<td>
 									<?= ($row + 1) ?>
 								</td>
 								<td>
 									<?= $value->nama ?>
 								</td>
 								<td>
 									<?= $value->jk ?>
 								</td>
 								<td>
 									<?= $value->nohp ?>
 								</td>
 								<td>
 									<?= $value->alamat ?>
 								</td>
 								<td>
 									<?= $value->username ?>
 								</td>
 								<td>
 									<img src="<?= base_url('asset/img/'); ?><?= $value->image ?>" height="45px" alt="">
 								</td>
 								<td>
 									<?= $value->level ?>
 								</td>
 								<td>
 									<table>
 										<td>
 											<!-- <a href="<?= site_url('Akun') ?>/ubah/<?= $value->id_user ?>" class="badge badge-info tombol-edit" title="Edit Akun"><span class="fa fa-edit"></span> Edit Akun</a> -->
 											<a onclick="editakun<?= ($row + 1) ?>('<?= site_url('Akun') ?>/ubah/<?= $value->id_user ?>')" href="#!" class="badge badge-info tombol-edit" title="Edit Akun"><span class="fa fa-edit"> Edit Akun</span></a>
 											<div class="modal fade" id="editakun<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 												<div class="modal-dialog" role="document">
 													<div class="modal-content">
 														<div class="modal-header">
 															<h5 class="modal-title" id="exampleModalLabel">Edit akun</h5>
 															<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 																<span aria-hidden="true">×</span>
 															</button>
 														</div>
 														<div class="modal-body">
 															<form action="<?= site_url('akun/ubah/' . $value->id_user) ?>" method="post" enctype="multipart/form-data">
 																<input type="hidden" name="id_user" value="<?= $value->id_user ?>">
 																<div class="form-group row">
 																	<div class="col-sm-3 mb-3 mb-sm-0">
 																		<label for="nama">Nama</label>
 																	</div>
 																	<div class="col-lg-9">
 																		<input type="text" class="form-control <?= form_error('nama') ? 'is-invalid' : '' ?>" name="nama" placeholder="Nama" value="<?= $value->nama ?>">
 																		<div class="invalid-feedback">
																		 <?= form_error('nama', ' <small class="text-danger pl-3">', '</small>'); ?>
 																		</div>
 																	</div>
 																</div>
 																<div class="form-group row">
 																	<div class="col-sm-3 mb-3 mb-sm-0">
 																		<label for="jk">Jenis Kelamin</label>
 																	</div>
 																	<div class="col-lg-9">
 																		<input name="jk" type="radio" value="Laki - Laki" <?php if ($value->jk == 'Laki - Laki') { 
																																echo 'checked';
																															}  ?>> Laki - Laki
 																		<input type="radio" name="jk" value="Perempuan" <?php if ($value->jk == 'Perempuan') {
																																echo 'checked';
																															}  ?>> Perempuan
 																		<div class="invalid-feedback">
 																			<?= form_error('jk') ?>
 																		</div>
 																	</div>
 																</div>
 																<div class="form-group row">
 																	<div class="col-sm-3 mb-3 mb-sm-0">
 																		<label for="nohp">No. Handphone</label>
 																	</div>
 																	<div class="col-lg-9">
 																		<input class="form-control <?= form_error('nohp') ? 'is-invalid' : '' ?>" type="text" name="nohp" placeholder="No. Handphone" value="<?= $value->nohp ?>" />
 																		<div class="invalid-feedback">
 																			<?= form_error('nohp') ?>
 																		</div>
 																	</div>
 																</div>
 																<div class="form-group row">
 																	<div class="col-sm-3 mb-3 mb-sm-0">
 																		<label for="alamat">Alamat</label>
 																	</div>
 																	<div class="col-lg-9">
 																		<input class="form-control <?= form_error('alamat') ? 'is-invalid' : '' ?>" name="alamat" value="<?= $value->alamat ?>">
 																		<div class="invalid-feedback">
 																			<?= form_error('alamat') ?>
 																		</div>
 																	</div>
																 </div>

																 <div class="form-group row">
 																	<div class="col-sm-3 mb-3 mb-sm-0">
 																		<label for="username">Username</label>
 																	</div>
 																	<div class="col-lg-9">
 																		<input class="form-control <?= form_error('username') ? 'is-invalid' : '' ?>" name="username" value="<?= $value->username ?>"></input>
 																		<div class="invalid-feedback">
 																			<?= form_error('username') ?>
 																		</div>
 																	</div>
																 </div>

																 <div class="form-group row">
																	<div class="col-sm-3 mb-sm-0">
																		<label for="password">Password</label>	
																	</div>
																	<div class="col-sm-4 mb-2 mb-sm-0">
																		<input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Password" required>
																		<?= form_error('password1', ' <small class="text-danger pl-3">', '</small>'); ?>
																	</div>
																	<div class="col-sm-5">
																		<input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Re-Password" required>
																	</div>
																</div>
																 
																 <div class="form-group row">
																	 <div class="col-sm-3 mb-3 mb-sm-0">
																		 <label class="control-label">Hak Akses</label>
																	 </div>
																	 <div class="col-lg-9">
																		 <select class="form-control chosen-select-no-results" name="hk" required>
																			 <?php $pilih = $value->id_lvl; ?>
																			 <option value="">- Pilih -</option>
																			 <?php
																			 $this->db->order_by('level', 'DSC');
																			 foreach ($this->db->get('tb_lvl')->result() as $key => $hasil) : ?>
																			 <?php if($pilih == $hasil->id_lvl){ ?>
																				<option value="<?= $hasil->id_lvl; ?>" selected><?= $hasil->level; ?></option>
																			 <?php }else{ ?>
																				<option value="<?= $hasil->id_lvl; ?>"><?= $hasil->level; ?></option>
																			 <?php } ?>
																			 <?php endforeach; ?>
																		 </select>
																	 </div>
																 </div>
																 
																 <div class="input-group">
																	<div class="input-group-prepend">
																		<span class="input-group-text" id="customFile">Upload</span>
																	</div>
																	<div class="custom-file">
																		<input type="file" name="image" class="custom-file-input" id="customFile">
																		<label class="custom-file-label" for="customFile">Choose file</label>
																	</div>
																</div>

 																<br>
 																<div class="form-group ">
 																	<button type="submit" class="btn btn-block btn-primary btn-user">Submit</button>
 																</div>
 															</form>
 														</div>
 													</div>
 												</div>
 											</div>

 											<script>
 												function editakun<?= ($row + 1) ?> (url) {
 													$('#editakun<?= ($row + 1) ?>').modal();
 												}
 											</script>
 										</td>
 										<td>
 											<!-- <a href="<?= site_url('Akun') ?>/hapus/<?= $value->id_user ?>" class="badge badge-danger" title="Hapus"><span class="fa fa-trash"></span> Hapus</a> -->
 											<a onclick="deleteConfirm<?= ($row + 1) ?>('<?= site_url('akun/hapus/' . $value->id_user) ?>')" href="#!" class="badge badge-danger" title="Hapus"><span class="fa fa-trash"> Hapus</span></a>

 											<!-- Logout Delete Confirmation-->
 											<div class="modal fade" id="deleteModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 												<div class="modal-dialog" role="document">
 													<div class="modal-content">
 														<div class="modal-header">
 															<h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ?</h5>
 															<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 																<span aria-hidden="true">×</span>
 															</button>
 														</div>
 														<div class="modal-body">
 															<p>Hapus nama akun <?= $value->nama ?> ?</p>
 															Data yang dihapus tidak akan bisa dikembalikan.
 														</div>
 														<div class="modal-footer">
 															<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
 															<a id="btn-delete<?= ($row + 1) ?>" class="btn btn-danger" href="#">Delete</a>
 														</div>
 													</div>
 												</div>
 											</div>
 											<script>
 												function deleteConfirm<?= ($row + 1) ?> (url) {
 													$('#btn-delete<?= ($row + 1) ?>').attr('href', url);
 													$('#deleteModal<?= ($row + 1) ?>').modal();
 												}
 											</script>
 										</td>
 									</table>
 								</td>
 							</tr>
 						<?php endforeach; ?>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>

</div>
<!-- /.container-fluid -->

</div>

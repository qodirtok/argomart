<!-- Begin Page Content -->
<div class="container-fluid">

	<?php if ($this->session->flashdata('success')) : ?>
		<div class="alert alert-success" role="alert">
			<?php echo $this->session->flashdata('success'); ?>
		</div>
	<?php endif; ?>
	<?php if ($this->session->flashdata('failed')) : ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $this->session->flashdata('failed'); ?>
		</div>
	<?php endif; ?>

	<div class="card bg-info text-white shadow" style="">
		<div class="card-body" style="">
			<center>
				<marquee behavior="" direction="">
					--- Agromart ---
				</marquee>
			</center>
			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
		</div>
	</div>
	<hr>
	<div class="card shadow mb-4 col-sm-6 mx-auto">
		<div class="card-header mb-4 ">
			<h6 class="m-0 font-weight-bold text-primary"> Tambah Data Akun</h6>
		</div>
		<div class="card-body mb-4">
			<form action="<?= base_url('akun'), "/tambah"; ?>" method="post" enctype="multipart/form-data">
				<div class="form-group row">
					<div class="col-sm-3 mb-sm-0">
						<label for="nama">Nama</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="name" name="nama" placeholder="Nama Lengkap" value="<?= set_value('nama'); ?>" required>
						<?= form_error('nama', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="jk">Jenis Kelamin</label>
					</div>
					<div class="col-lg-9">
						<input type="radio" name="jk" value="Laki - Laki"> Laki - Laki &nbsp
						<input type="radio" name="jk" value="Perempuan"> perempuan <br>
						<?= form_error('jk', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-sm-0">
						<label for="nohp">No. Handphone</label>
					</div>
					<div class="col-lg-9">
						<input type="number" class="form-control form-control-user" id="nohp" name="nohp" maxlength="13" placeholder="No. Handphone/Telp" value="<?= set_value('nohp'); ?>" required>
						<?= form_error('nohp', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-sm-0">
						<label for="alamat">Alamat</label>
					</div>
					<div class="col-lg-9">
						<input class="form-control" name="alamat" id="alamat" value="<?= set_value('alamat'); ?>" required><br>
						<?= form_error('alamat', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-sm-0">
						<label for="username">Username</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="username" name="username" placeholder="Username" value="<?= set_value('username'); ?>" required>
						<?= form_error('username', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-sm-0">
						<label for="password">Password</label>
					</div>
					<div class="col-sm-4 mb-3 mb-sm-0">
						<input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Password" required>
						<?= form_error('password1', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
					<div class="col-sm-5">
						<input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Re-Password" required>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-sm-3 mb-sm-0">
						<label for="hk">Hak Akses</label>
					</div>
					<div class="col-lg-9">
						<select class="form-control chosen-select-no-results" name="hk" required>
							<option value="">- Pilih -</option>
							<?php
							if ($this->session->userdata('id_lvl') == 5) { } else {
								$this->db->where('id_lvl!=', 5);
							}
							$this->db->order_by('level', 'ASC');
							$HK = $this->db->get('tb_lvl')->result();
							foreach ($HK as $key => $value) : ?>
								<option value="<?= $value->id_lvl; ?>"><?= $value->level; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="customFile">Upload</span>
					</div>
					<div class="custom-file">
						<input type="file" name="image" class="custom-file-input" id="customFile">
						<label class="custom-file-label" for="customFile">Choose file</label>
					</div>
				</div>
				<br>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Tambah Akun</button>
					<a href="<?= base_url('akun') ?>" class="btn btn-success btn-block">Cancel</a>
				</div>
		</div>
		<!-- <br> -->
		<!-- <div class="form-group">
				<button type="submit" class="btn btn-primary btn-user"><i class="fa fa-plus"></i> Tambah Akun</button>
			</div> -->
		</form>
	</div>
</div>
</div>
<!-- /.container-fluid -->

<!-- </div> -->
 <!-- Begin Page Content -->
 <div class="container-fluid">

 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
 	<div class="card bg-info text-white shadow" style="">
 		<div class="card-body" style="">
 			<center>
 				<marquee behavior="" direction="">
 					--- halaman ini menampilkan list data kategori ---
 				</marquee>
 			</center>
 			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
 		</div>
 	</div>
 	<hr>
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Data Jenis Satuan</h6>
 		</div>
 		<div class="card-body">
 			<?php if ($this->session->flashdata('success')) : ?>
 			<?php echo $this->session->flashdata('success'); ?>
 			<?php endif; ?>
 			<?php if ($this->session->flashdata('failed')) : ?>
 			<?php echo $this->session->flashdata('failed'); ?>
 			<?php endif; ?>
 			<div>
 				<a onclick="tambahSatuan('<?= base_url('satuan/store'); ?>')" href="#!" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Input Satuan</a>

 				<div class="modal fade" id="tambahSatuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 					<div class="modal-dialog" role="document">
 						<div class="modal-content">
 							<div class="modal-header">
 								<h5 class="modal-title" id="exampleModalLabel">Input Satuan</h5>
 								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 									<span aria-hidden="true">×</span>
 								</button>
 							</div>
 							<div class="modal-body">
 								<form action="<?= base_url('satuan'), "/store"; ?>" method="post" enctype="multipart/form-data">

 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="nma_satuan">Nama Satuan</label>
 										</div>
 										<div class="col-lg-8">
 											<input class="form-control <?= form_error('nma_satuan') ? 'is-invalid' : '' ?>" type="text" name="nma_satuan" placeholder="" />
 											<div class="invalid-feedback">
 												<?= form_error('nma_satuan') ?>
 											</div>
 										</div>
 									</div>
 									<br>
 									<div class="form-group">
 										<button type="submit" class="btn btn-primary btn-block">Submit</button>
 									</div>
 								</form>
 							</div>
 						</div>
 					</div>
 				</div>
 				<script>
 					function tambahSatuan(url) {
 						$('#tambahSatuan').modal();
 					}
 				</script>
 			</div>
 			<hr>
 			<div class="table-responsive">
 				<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr>
 							<th>No</th>
 							<th>Nama Satuan</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php foreach ($satuan as $row => $value) : ?>
 						<?php $number = 1; ?>
 						<tr>
 							<td>
 								<?= ($row + 1) ?>
 							</td>
 							<td>
 								<?= $value->nma_satuan ?>
 							</td>
 							<td>
 								<a onclick="tambahSatuan<?= ($row + 1) ?>('<?= base_url('kategori/update/' . $value->id_satuan) ?>')" href="#!" class="badge badge-primary"><span><i class="fa fa-edit"></i></span> edit satuan</a>

 								<div class="modal fade" id="tambahSatuan<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 									<div class="modal-dialog" role="document">
 										<div class="modal-content">
 											<div class="modal-header">
 												<h5 class="modal-title" id="exampleModalLabel">Edit Satuan</h5>
 												<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 													<span aria-hidden="true">×</span>
 												</button>
 											</div>
 											<div class="modal-body">
 												<form action="<?= base_url('satuan/update/' . $value->id_satuan); ?>" method="post" enctype="multipart/form-data">
 													<input type="hidden" name="id_satuan" value="<?= $value->id_satuan ?>">
 													<div class="form-group row">
 														<div class="col-lg-4 mb-3 mb-sm-0">
 															<label for="Nama">Nama Satuan</label>
 														</div>
 														<div class="col-lg-8">
 															<input class="form-control <?= form_error('nma_satuan') ? 'is-invalid' : '' ?>" type="text" name="nma_satuan" value="<?= $value->nma_satuan ?>" required />
 															<div class="invalid-feedback">
 																<?= form_error('nma_satuan') ?>
 															</div>
 														</div>
 													</div>
 													<br>
 													<div class="form-group">
 														<button type="submit" class="btn btn-primary btn-block">Submit</button>
 													</div>
 												</form>
 											</div>
 										</div>
 									</div>
 								</div>
 								<script>
 									function tambahSatuan<?= ($row + 1) ?>(url) {
 										$('#tambahSatuan<?= ($row + 1) ?>').modal();
 									}
 								</script>

 								<a onclick="deleteConfirm<?= ($row + 1) ?>('<?= site_url('satuan/destroy/' . $value->id_satuan) ?>')" href="#!" class="badge badge-danger"><span class="fa fa-trash"> Hapus</span></a>

 								<!-- Logout Delete Confirmation-->
 								<div class="modal fade" id="deleteModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 									<div class="modal-dialog" role="document">
 										<div class="modal-content">
 											<div class="modal-header">
 												<h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ?</h5>
 												<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 													<span aria-hidden="true">×</span>
 												</button>
 											</div>
 											<div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
 											<div class="modal-footer">
 												<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
 												<a id="btn-delete<?= ($row + 1) ?>" class="btn btn-danger" href="#">Delete</a>
 											</div>
 										</div>
 									</div>
 								</div>
 								<script>
 									function deleteConfirm<?= ($row + 1) ?>(url) {
 										$('#btn-delete<?= ($row + 1) ?>').attr('href', url);
 										$('#deleteModal<?= ($row + 1) ?>').modal();
 									}
 								</script>
 							</td>
 						</tr>
 						<?php endforeach; ?>
 					</tbody>
 					<tbody>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>

 </div>
 <!-- /.container-fluid -->

 </div>

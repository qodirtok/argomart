 <!-- Begin Page Content -->
 <div class="container-fluid">
 	<!-- <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div> -->
 	<?php if ($this->session->flashdata('success')) : ?>
 	<div class="alert alert-success" role="alert">
 		<?php echo $this->session->flashdata('success'); ?>
 	</div>
 	<?php endif; ?>
 	<?php if ($this->session->flashdata('failed')) : ?>
 	<div class="alert alert-danger" role="alert">
 		<?php echo $this->session->flashdata('failed'); ?>
 	</div>
 	<?php endif; ?>
 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
 	<div class="card bg-info text-white shadow" style="">
 		<div class="card-body" style="">
 			<center>
 				<marquee behavior="" direction="">
 					--- halaman ini menampilkan list data Diskon ---
 				</marquee>
 			</center>
 			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
 		</div>
 	</div>
 	<hr>
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Data Diskon</h6>
 		</div>
 		<div class="card-body">
 			<div>
 				<a onclick="tambahMember('<?= base_url('diskon/store') ?>')" href="#!" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Input Data</a>

 				<div class="modal fade" id="tambahMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 					<div class="modal-dialog" role="document">
 						<div class="modal-content">
 							<div class="modal-header">
 								<h5 class="modal-title" id="exampleModalLabel">Input Data</h5>
 								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 									<span aria-hidden="true">×</span>
 								</button>
 							</div>
 							<div class="modal-body">
 								<form action="<?= base_url('diskon/store'); ?>" method="post" enctype="multipart/form-data">

 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="kode member">Kode Barang</label>
 										</div>
 										<div class="col-lg-8">
 											<select id="sel1" name="id_barang" class="form-control selectpicker show-tick" data-live-search="true" title="Kode Barang" data-width="100%" required>
 												<?php foreach ($kdbarang as $row2 => $val) : ?>
 												<option value="<?= $val->id_barang ?>"><?php echo $val->Kode_barang ?> - <?php echo $val->nama ?></option>
 												<?php endforeach; ?>
 											</select>
 										</div>
 									</div>
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="kode member">Diskon (%)</label>
 										</div>
 										<div class="col-lg-8">
 											<input type="number" class="input-sm form-control" name="diskon" placeholder="" />
 										</div>
 									</div>
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="Alamat">Tanggal Berakhir</label>
 										</div>
 										<div class="col-lg-8">
 											<div class="input-daterange input-group" id="datepicker">
 												<!-- <input type="hidden" class="input-sm form-control" name="awal" placeholder="Tanggal Awal" />
 												<div class="input-group-prepend">
 													<span class="input-group-text">s.d</span>
 												</div> -->
 												<input type="date" class="input-sm form-control" name="berakhir" placeholder="Tanggal Akhir" />
 											</div>
 										</div>
 									</div>
 									<br>
 									<div class="form-group">
 										<button type="submit" class="btn btn-primary btn-block">Submit</button>
 									</div>
 								</form>
 							</div>
 						</div>

 					</div>
 				</div>
 				<script>
 					function tambahMember(url) {
 						$('#tambahMember').modal();
 					}
 				</script>
 			</div>
 			<hr>
 			<div class="table-responsive">
 				<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr>
 							<th>No</th>
 							<th>Kode Barang</th>
 							<th>Nama Barang</th>
 							<th>Diskon</th>
 							<th>Harga</th>
 							<th>tanggal berakhir</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php foreach ($listDiskon as $row => $value) : ?>
 						<?php $number = 1; ?>
 						<tr>
 							<td>
 								<?= ($row + 1) ?>
 							</td>
 							<td>
 								<?= $value->Kode_barang ?>
 							</td>
 							<td>
 								<?= $value->nama ?>
 							</td>
 							<td>
 								<?= $value->diskon . '%' ?>
 							</td>
 							<td>
 								<?php $dolardiskon = (((100 - $value->diskon) / 100) * $value->hrg_jual);
										echo number_format($dolardiskon); ?>
 							</td>
 							<td>
 								<?= $value->berakhir ?>
 							</td>
 							<td>
 								<a onclick="deleteConfirm<?= ($row + 1) ?>('<?= site_url('diskon/destroy/' . $value->id) ?>')" href="#!" class="badge badge-danger btn-sm tombol-hapus"><span class="fa fa-trash"> Hapus</span></a>

 								<!-- Logout Delete Confirmation-->
 								<div class="modal fade" id="deleteModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 									<div class="modal-dialog" role="document">
 										<div class="modal-content">
 											<div class="modal-header">
 												<h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ?</h5>
 												<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 													<span aria-hidden="true">×</span>
 												</button>
 											</div>
 											<div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
 											<div class="modal-footer">
 												<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
 												<a id="btn-delete<?= ($row + 1) ?>" class="btn btn-danger" href="#">Delete</a>
 											</div>
 										</div>
 									</div>
 								</div>
 								<script>
 									function deleteConfirm<?= ($row + 1) ?>(url) {
 										$('#btn-delete<?= ($row + 1) ?>').attr('href', url);
 										$('#deleteModal<?= ($row + 1) ?>').modal();
 									}
 								</script>
 							</td>
 						</tr>
 						<?php endforeach; ?>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>
 	<!-- /.container-fluid -->
 </div>
 </div>
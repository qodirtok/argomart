 <!-- Begin Page Content -->
 <div class="container-fluid">
 	<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
 	<?php if ($this->session->flashdata('success')) : ?>
 	<div class="alert alert-success" role="alert">
 		<?php echo $this->session->flashdata('success'); ?>
 	</div>
 	<?php endif; ?>
 	<?php if ($this->session->flashdata('failed')) : ?>
 	<div class="alert alert-danger" role="alert">
 		<?php echo $this->session->flashdata('failed'); ?>
 	</div>
 	<?php endif; ?>
 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
 	<div class="card bg-info text-white shadow" style="">
 		<div class="card-body" style="">
 			<center>
 				<marquee behavior="" direction="">
 					--- halaman ini menampilkan data modal kasir hari ini ---
 				</marquee>
 			</center>
 			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
 		</div>
 	</div>
 	<hr>
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Data Modal Kasir</h6>
 		</div>
 		<div class="card-body">
 			<?php if ($modal) { ?>
 			<?php } else { ?>
 			<div>
 				<!-- <a href="<?= base_url('modal'), "/add"; ?>" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Tambah Modal</a> -->
 				<a onclick="tambahModal('<?= base_url('modal/store'); ?>')" href="#!" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Tambah Modal</a>

 				<div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 					<div class="modal-dialog" role="document">
 						<div class="modal-content">
 							<div class="modal-header">
 								<h5 class="modal-title" id="exampleModalLabel">Input Modal</h5>
 								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 									<span aria-hidden="true">×</span>
 								</button>
 							</div>
 							<div class="modal-body">
 								<form action="<?= base_url('modal'), "/store"; ?>" method="post" enctype="multipart/form-data">

 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="modal">Modal Kasir</label>
 										</div>
 										<div class="col-lg-8">
 											<input type="hidden" name="id_user" value="<?= $user['id_user']; ?>">
 											<input class="form-control <?= form_error('modal') ? 'is-invalid' : '' ?>" type="text" id="rupiah" name="modal" placeholder="" />
 											<div class="invalid-feedback">
 												<?= form_error('modal') ?>
 											</div>
 										</div>
 									</div>
 									<br>
 									<div class="form-group">
 										<button type="submit" class="btn btn-primary btn-block">Submit</button>
 									</div>
 								</form>
 							</div>
 						</div>
 					</div>
 				</div>
 				<script>
 					function tambahModal(url) {
 						$('#tambahModal').modal();
 					}
 				</script>
 			</div>
 			<hr>
 			<?php } ?>

 			<div class="table-responsive table-hover">
 				<table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr>
 							<th>No</th>
 							<th>Tanggal</th>
 							<th>Modal Hari ini(Rp)</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php foreach ($modal as $row => $value) : ?>
 						<?php $number = 1;
								$tgl = $value->tanggal;
								$date = explode('-', $tgl);
								$tanggal = $date[2] . '-' . $date[1] . '-' . $date[0];
								$modal = $value->modal;
								?>
 						<tr>
 							<td>
 								<?= ($row + 1) ?>
 							</td>
 							<td>
 								<?= $tanggal ?>
 							</td>
 							<td>
 								<?= 'Rp. ' . number_format($modal, 0, ",", ".") ?>
 							</td>
 							<td>
 								<a onclick="editModal<?= ($row + 1) ?>('<?= base_url('modal/update' . $value->id_modal) ?>')" href="#!" class="badge badge-primary"><span><i class="fa fa-plus"></i></span> edit Modal</a>

 								<div class="modal fade" id="editModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 									<div class="modal-dialog" role="document">
 										<div class="modal-content">
 											<div class="modal-header">
 												<h5 class="modal-title" id="exampleModalLabel">Edit Modal</h5>
 												<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 													<span aria-hidden="true">×</span>
 												</button>
 											</div>
 											<div class="modal-body">
 												<form action="<?= base_url('modal/update/' . $value->id_modal) ?>" method="post" enctype="multipart/form-data">
 													<input type="hidden" name="id_modal" value="<?= $value->id_modal ?>">
 													<input type="hidden" name="tanggal" value="<?= $value->tanggal ?>">
 													<div class="form-group row">
 														<div class="col-lg-4 mb-3 mb-sm-0">
 															<label for="modal">Modal Kasir</label>
 														</div>
 														<div class="col-lg-8">
 															<input class="form-control <?= form_error('modal') ? 'is-invalid' : '' ?>" type="text" id="rupiah" name="modal" value="<?= $value->modal ?>" />
 															<div class="invalid-feedback">
 																<?= form_error('modal') ?>
 															</div>
 														</div>
 													</div>
 													<br>
 													<div class="form-group">
 														<button type="submit" class="btn btn-primary btn-block">Submit</button>
 													</div>
 												</form>
 											</div>
 										</div>
 									</div>
 								</div>
 								<script>
 									function editModal<?= ($row + 1) ?>(url) {
 										$('#editModal<?= ($row + 1) ?>').modal();
 									}
 								</script>
 							</td>
 						</tr>
 						<?php endforeach; ?>
 					</tbody>
 					<!-- <tfoot>
 							<tr>
 								<th>No</th>
 								<th>Nama Modal</th>
 								<th>Aksi</th>
 							</tr>
 						</tfoot> -->
 					<tbody>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>

 </div>
 <!-- /.container-fluid -->

 </div>
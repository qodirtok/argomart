<!-- Begin Page Content -->
<div class="container-fluid">


    <div class="card shadow mb-4 col-sm-6 mx-auto">
        <div class=" card-header mb-4 ">
            <h6 class="m-0 font-weight-bold text-primary">Ubah Profile</h6>
        </div>
        <div class="card-body mb-4">
            <?php echo form_open_multipart('Profile/ubahprofile/' . $tampil['id_user']); ?>
            <input type="hidden" id="id_user" name="id_user" value="<?= $tampil['id_user'] ?>">
            <div class="form-group row">
                <div class="col-sm-3 mb-3 mb-sm-0">
                    <label for="nama_akun">Nama Akun</label>
                </div>
                <div class="col-lg-9">
                    <input type="text" class="form-control form-control-user" id="nama_akun" name="nama_akun" placeholder="<?= $tampil['nama'] ?>" value="<?= $tampil['nama']; ?>">
                    <?= form_error('nama_akun', ' <small class="text-danger pl-3">', '</small>'); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3 mb-3 mb-sm-0">
                    <label for="jk">Jenis Kelamin</label>
                </div>
                <div class="col-lg-9">
                    <input name="jk" type="radio" value="Laki - Laki" <?php if ($tampil['jk'] == 'Laki - Laki') {
                                                                            echo 'checked';
                                                                        }  ?>> Laki - Laki
                    <input type="radio" name="jk" value="Perempuan" <?php if ($tampil['jk'] == 'Perempuan') {
                                                                        echo 'checked';
                                                                    }  ?>> Perempuan
                    <div class="invalid-feedback">
                        <?= form_error('jk') ?>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3 mb-3 mb-sm-0">
                    <label for="Nohp">No. Handphone</label>
                </div>
                <div class="col-lg-9">
                    <input type="text" class="form-control form-control-user" id="Nohp" name="Nohp" placeholder="<?= $tampil['nohp']; ?>" value="<?= $tampil['nohp']; ?>">
                    <?= form_error('Nohp', ' <small class="text-danger pl-3">', '</small>'); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3 mb-3 mb-sm-0">
                    <label for="alamat">Alamat</label>
                </div>
                <div class="col-lg-9">
                    <input type="text" class="form-control form-control-user" id="alamat" name="alamat" placeholder="<?= $tampil['alamat']; ?>" value="<?= $tampil['alamat']; ?>">
                    <?= form_error('alamat', ' <small class="text-danger pl-3">', '</small>'); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">Foto</div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <img src="<?= base_url('asset/img/') . $tampil['image']; ?>" class="img-thumbnail">
                        </div>
                        <div class="col-sm-9">
                            <input type="file" class="custom-file-input" id="image" name="image">
                            <label class="custom-file-label" for="image">Choose File</label>

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-save"></i> Simpan</button>
                <a href="<?= base_url('profile') ?>" class="btn btn-success btn-block"><i class="fas fa-arrow-left"></i> Kembali</a>
            </div>
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
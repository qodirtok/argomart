<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block">
                    <table>
                        <tr>
                            <td>
                                <img src="<?= base_url(); ?>asset/img/<?= $user['image']; ?>" width="490" height="600" alt="">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-7">
                    <div class="p-5">
                        <div>
                            <?= $this->session->flashdata('message'); ?>
                        </div>
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Profile</h1>
                        </div>
                        <form class="user">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered nowrap">
                                    <tr>
                                        <th width="30%">Nama akun</th>
                                        <th width="1%">:</th>
                                        <td>
                                            <p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?= $user['nama']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Jenis Kelamin</th>
                                        <th width="1%">:</th>
                                        <td>
                                            <p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?= $user['jk']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="30%">No. Handphone</th>
                                        <th width="1%">:</th>
                                        <td>
                                            <p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?= $user['nohp']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Alamat</th>
                                        <th width="1%">:</th>
                                        <td>
                                            <p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?= $user['alamat']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Status</th>
                                        <th width="1%">:</th>
                                        <td>
                                            <p style="width: 300px; font-size: 15px; text-align: justify; font-weight: bold;"><?= $user['level']; ?></p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <hr>
                            <a href="<?= base_url('Profile/ubahprofile'); ?>/<?= $user['id_user']; ?>" class="btn btn-google btn-user btn-block">
                                <i class="fas fa-pencil-alt"></i>&nbsp; Ubah Profile
                            </a>
                            <a href="<?= base_url('Profile/gantipassword'); ?>/<?= $user['id_user']; ?>" class="btn btn-primary btn-user btn-block">
                                <i class="fas fa-key"></i>&nbsp; Ganti Password
                            </a>
                        </form>
                        <hr>
                        <div class="text-center">

                        </div>
                        <div class="text-center">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Begin Page Content -->
<div class="container-fluid">


    <div class="card shadow mb-4 col-sm-6 mx-auto">
        <div class=" card-header mb-4 ">
            <h6 class="m-0 font-weight-bold text-primary">Ubah Profile</h6>
        </div>
        <div class="card-body mb-4">
            <div>
                <?= $this->session->flashdata('message'); ?>
            </div>
            <?php echo form_open_multipart('Profile/gantipassword/' . $tampil['id_user']); ?>
            <input type="hidden" id="id_user" name="id_user" value="<?= $tampil['id_user'] ?>">
            <div class="form-group row">
                <div class="col-sm-3 mb-3 mb-sm-0">
                    <label for="passwordlama">Password Lama</label>
                </div>
                <div class="col-lg-9">
                    <input type="password" class="form-control form-control-user" id="passwordlama" name="passwordlama" placeholder="Password Lama">
                    <?= form_error('passwordlama', ' <small class="text-danger pl-3">', '</small>'); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3 mb-3 mb-sm-0">
                    <label for="passwordbaru">Password Baru</label>
                </div>
                <div class="col-lg-9">
                    <input type="password" class="form-control form-control-user" id="passwordbaru" name="passwordbaru" placeholder="Password Baru">
                    <?= form_error('passwordbaru', ' <small class="text-danger pl-3">', '</small>'); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3 mb-3 mb-sm-0">
                    <label for="ulangpassword">Ulang Password</label>
                </div>
                <div class="col-lg-9">
                    <input type="password" class="form-control form-control-user" id="ulangpassword" name="ulangpassword" placeholder="Ulang Password">
                    <?= form_error('ulangpassword', ' <small class="text-danger pl-3">', '</small>'); ?>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-key"></i> Ganti Password</button>
                <a href="<?= base_url('profile') ?>" class="btn btn-success btn-block"><i class="fas fa-arrow-left"></i> Kembali</a>
            </div>
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
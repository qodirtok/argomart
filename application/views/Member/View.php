 <!-- Begin Page Content -->
 <div class="container-fluid">
 	<!-- <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div> -->
 	<?php if ($this->session->flashdata('success')) : ?>
 		<div class="alert alert-success" role="alert">
 			<?php echo $this->session->flashdata('success'); ?>
 		</div>
 	<?php endif; ?>
 	<?php if ($this->session->flashdata('failed')) : ?>
 		<div class="alert alert-danger" role="alert">
 			<?php echo $this->session->flashdata('failed'); ?>
 		</div>
 	<?php endif; ?>
 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
 	<div class="card bg-info text-white shadow" style="">
 		<div class="card-body" style="">
 			<center>
 				<marquee behavior="" direction="">
 					--- halaman ini menampilkan list data member ---
 				</marquee>
 			</center>
 			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
 		</div>
 	</div>
 	<hr>
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Data Member</h6>
 		</div>
 		<div class="card-body">
 			<div>
 				<a onclick="tambahMember('<?= base_url('member/store') ?>')" href="#!" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Tambah Member</a>

 				<div class="modal fade" id="tambahMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 					<div class="modal-dialog" role="document">
 						<div class="modal-content">
 							<div class="modal-header">
 								<h5 class="modal-title" id="exampleModalLabel">Tambah Member</h5>
 								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 									<span aria-hidden="true">×</span>
 								</button>
 							</div>
 							<div class="modal-body">
 								<form action="<?= base_url('member/store'); ?>" method="post" enctype="multipart/form-data">
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="kode member">Kode Member</label>
 										</div>
 										<div class="col-lg-8">
 											<input class="form-control <?= form_error('Kode_member') ? 'is-invalid' : '' ?>" type="text" name="Kode_member" value="<?= $kodemember ?>" placeholder="<?= $kodemember; ?>" readonly />
 											<div class="invalid-feedback">
 												<?= form_error('Kode_member') ?>
 											</div>
 										</div>
 									</div>
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="Nama">Full Nama</label>
 										</div>
 										<div class="col-lg-8">
 											<input class="form-control <?= form_error('Nama') ? 'is-invalid' : '' ?>" type="text" name="Nama" required />
 											<div class="invalid-feedback">
 												<?= form_error('Nama') ?>
 											</div>
 										</div>
 									</div>
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="Nohp">Nomor Hp</label>
 										</div>
 										<div class="col-lg-8">
 											<input class="form-control <?= form_error('Nohp') ? 'is-invalid' : '' ?>" type="number" name="Nohp" placeholder="" required />
 											<div class="invalid-feedback">
 												<?= form_error('Nohp') ?>
 											</div>
 										</div>
 									</div>
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="Alamat">Alamat</label>
 										</div>
 										<div class="col-lg-8">
 											<input class="form-control <?= form_error('Alamat') ? 'is-invalid' : '' ?>" type="text" name="Alamat" placeholder="" required />
 											<div class="invalid-feedback">
 												<?= form_error('Alamat') ?>
 											</div>
 										</div>
 									</div>
 									<br>
 									<div class="form-group">
 										<button type="submit" class="btn btn-primary btn-block">Submit</button>
 									</div>
 								</form>
 							</div>
 						</div>

 					</div>
 				</div>
 				<script>
 					function tambahMember(url) {
 						$('#tambahMember').modal();
 					}
 				</script>
 			</div>
 			<hr>
 			<div class="table-responsive">
 				<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr>
 							<th>No</th>
 							<th>Kode Member</th>
 							<th>Nama</th>
 							<th>Nomor Handphone</th>
 							<th>Alamat</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php foreach ($member as $row => $value) : ?>
 							<?php $number = 1; ?>
 							<tr>
 								<td>
 									<?= ($row + 1) ?>
 								</td>
 								<td>
 									<?= $value->Kode_member ?>
 								</td>
 								<td>
 									<?= $value->Nama ?>
 								</td>
 								<td>
 									<?= $value->Nohp ?>
 								</td>
 								<td>
 									<?= $value->Alamat ?>
 								</td>
 								<td>
 									<a onclick="editMember<?= ($row + 1) ?>('<?= site_url('member/update' . $value->id_member) ?>')" href="#!" class="badge badge-primary"><span class="fa fa-edit"> Edit Member</span></a>

 									<div class="modal fade" id="editMember<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 										<div class="modal-dialog" role="document">
 											<div class="modal-content">
 												<div class="modal-header">
 													<h5 class="modal-title" id="exampleModalLabel">Detail Member</h5>
 													<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 														<span aria-hidden="true">×</span>
 													</button>
 												</div>
 												<div class="modal-body">
 													<form action="<?= base_url('member/update/' . $value->id_member); ?>" method="post" enctype="multipart/form-data">
 														<input type="hidden" name="id_member" value="<?= $value->id_member ?>">
 														<div class="form-group row">
 															<div class="col-lg-4 mb-3 mb-sm-0">
 																<label for="kode member">Kode Member</label>
 															</div>
 															<div class="col-lg-8">
 																<input class="form-control <?= form_error('Kode_member') ? 'is-invalid' : '' ?>" type="text" name="Kode_member" value="<?= $value->Kode_member ?>" placeholder="<?= $kodemember; ?>" readonly />
 																<div class="invalid-feedback">
 																	<?= form_error('Kode_member') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-lg-4 mb-3 mb-sm-0">
 																<label for="Nama">Full Nama</label>
 															</div>
 															<div class="col-lg-8">
 																<input class="form-control <?= form_error('Nama') ? 'is-invalid' : '' ?>" type="text" value="<?= $value->Nama ?>" name="Nama" required />
 																<div class="invalid-feedback">
 																	<?= form_error('Nama') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-lg-4 mb-3 mb-sm-0">
 																<label for="Nohp">Nomor Hp</label>
 															</div>
 															<div class="col-lg-8">
 																<input class="form-control <?= form_error('Nohp') ? 'is-invalid' : '' ?>" type="text" value="<?= $value->Nohp ?>" name="Nohp" placeholder="" required />
 																<div class="invalid-feedback">
 																	<?= form_error('Nohp') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-lg-4 mb-3 mb-sm-0">
 																<label for="Alamat">Alamat</label>
 															</div>
 															<div class="col-lg-8">
 																<input class="form-control <?= form_error('Alamat') ? 'is-invalid' : '' ?>" type="text" value="<?= $value->Alamat ?>" name="Alamat" placeholder="" required />
 																<div class="invalid-feedback">
 																	<?= form_error('Alamat') ?>
 																</div>
 															</div>
 														</div>
 														<br>
 														<div class="form-group">
 															<button type="submit" class="btn btn-primary btn-block">Submit</button>
 														</div>
 													</form>
 												</div>
 											</div>

 										</div>
 									</div>
 									<script>
 										function editMember<?= ($row + 1) ?>(url) {
 											$('#editMember<?= ($row + 1) ?>').modal();
 										}
 									</script>
 									<?php if ($this->session->userdata('id_lvl') == 1 || $this->session->userdata('id_lvl') == 5) : ?>
 										<a onclick="deleteConfirm<?= ($row + 1) ?>('<?= site_url('member/destroy/' . $value->id_member) ?>')" href="#!" class="badge badge-danger btn-sm tombol-hapus"><span class="fa fa-trash"> Hapus</span></a>

 										<!-- Logout Delete Confirmation-->
 										<div class="modal fade" id="deleteModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 											<div class="modal-dialog" role="document">
 												<div class="modal-content">
 													<div class="modal-header">
 														<h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ?</h5>
 														<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 															<span aria-hidden="true">×</span>
 														</button>
 													</div>
 													<div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
 													<div class="modal-footer">
 														<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
 														<a id="btn-delete<?= ($row + 1) ?>" class="btn btn-danger" href="#">Delete</a>
 													</div>
 												</div>
 											</div>
 										</div>
 										<script>
 											function deleteConfirm<?= ($row + 1) ?>(url) {
 												$('#btn-delete<?= ($row + 1) ?>').attr('href', url);
 												$('#deleteModal<?= ($row + 1) ?>').modal();
 											}
 										</script>
 									<?php endif; ?>
 								</td>
 							</tr>
 						<?php endforeach; ?>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>
 	<!-- /.container-fluid -->
 </div>
 </div>

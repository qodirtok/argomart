 <!-- Begin Page Content -->
 <div class="container-fluid">

     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <a href="<?= base_url('kirimbrg'); ?>" class="btn badge">
                 <h6 class="m-0 font-weight-bold text-primary"> <i class="fas fa-arrow-left"></i>&nbsp; Kirim Barang</h6>
             </a>
         </div>
         <div class="card-body">

             <div class="table-responsive">
                 <table style="text-align:center;" class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>No Faktur</th>
                             <th>Tanggal</th>
                             <th>Nama</th>
                             <th>No. Handphone</th>
                             <th>Alamat</th>
                             <th>Status</th>
                             <th>Aksi</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php foreach ($bln as $row => $value) : ?>
                         <?php $number = 1; ?>
                         <tr>
                             <td>
                                 <?= ($row + 1) ?>
                             </td>
                             <td>
                                 <?= $value->no_faktur ?>
                             </td>
                             <td>
                                 <?= date('d-m-Y', strtotime($value->tgl_kirim)); ?>
                             </td>
                             <td>
                                 <?= $value->Nama ?>
                             </td>
                             <td>
                                 <?= $value->Nohp ?>
                             </td>
                             <td>
                                 <?= $value->Alamat ?>
                             </td>
                             <td>
                                 <?php if ($value->status == "Proses") { ?>
                                 <label class="badge badge-warning"><?= $value->status ?></label>
                                 <?php } else { ?>
                                 <label class="badge badge-success"><?= $value->status ?></label>
                                 <?php } ?>
                             </td>
                             <td>
                                 <a href="<?= base_url('kirimbrg/detail/') . $value->no_faktur; ?>" class="btn badge btn-outline-primary">Detail</a>
                                 <a href="#" class="btn badge btn-outline-success">Terkirim</a>
                             </td>
                         </tr>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div> <!-- /.container-fluid -->

 </div>
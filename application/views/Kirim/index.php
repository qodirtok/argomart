 <!-- Begin Page Content -->
 <div class="container-fluid">

     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <h6 class="m-0 font-weight-bold text-primary">Kirim Barang &nbsp;&nbsp;&nbsp;&nbsp; <?= date('d-m-Y'); ?></h6>
         </div>
         <div class="card-body">
             <form action="<?= base_url('kirimbrg/tglnow'); ?>" method="post" enctype="multipart/form-data">
                 <div class="form-group row">
                     <div class="col-sm-1 mb-1 mb-sm-0">
                         <label for="tgl1">Tanggal Awal : </label>
                     </div>
                     <div class="col-lg-3">
                         <input type="date" class="form-control <?= form_error('tgl1') ? 'is-invalid' : '' ?>" name="tgl1" placeholder="Tanggal Awal..." required>
                         <div class="invalid-feedback">
                             <?= form_error('tgl1') ?>
                         </div>
                     </div>
                 </div>
                 <div class="form-group row">
                     <div class="col-sm-1 mb-1 mb-sm-0">
                         <label for="tgl2">Tanggal Akhir : </label>
                     </div>
                     <div class="col-lg-3">
                         <input type="date" class="form-control <?= form_error('tgl2') ? 'is-invalid' : '' ?>" name="tgl2" placeholder="Tanggal Awal..." required>
                         <div class="invalid-feedback">
                             <?= form_error('tgl2') ?>
                         </div>
                     </div>
                     <button type="submit" class="btn btn-primary">Cari..</button>
                 </div>
             </form>
             <hr>
             <div class="table-responsive">
                 <table style="text-align:center;" class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>No Faktur</th>
                             <th>Tanggal</th>
                             <th>Nama</th>
                             <th>No. Handphone</th>
                             <th>Alamat</th>
                             <th>Status</th>
                             <th>Aksi</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php foreach ($kirimnow as $row => $value) : ?>
                         <?php $number = 1; ?>
                         <tr>
                             <td>
                                 <?= ($row + 1) ?>
                             </td>
                             <td>
                                 <?= $value->no_faktur ?>
                             </td>
                             <td>
                                 <?= date('d-m-Y', strtotime($value->tgl_kirim)); ?>
                             </td>
                             <td>
                                 <?= $value->Nama ?>
                             </td>
                             <td>
                                 <?= $value->Nohp ?>
                             </td>
                             <td>
                                 <?= $value->Alamat ?>
                             </td>
                             <td>
                                 <?php if ($value->status == "Proses") { ?>
                                 <label class="badge badge-warning"><?= $value->status ?></label>
                                 <?php } else { ?>
                                 <label class="badge badge-success"><?= $value->status ?></label>
                                 <?php } ?>
                             </td>
                             <td>
                                 <a href="<?= base_url('kirimbrg/detail/') . $value->no_faktur; ?>" class="btn badge btn-outline-primary">Detail</a>
                                 <a href="<?= base_url('kirimbrg/proses/') . $value->no_faktur; ?>" class="btn badge btn-outline-success">Terkirim</a>
                             </td>
                         </tr>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div> <!-- /.container-fluid -->

 </div>
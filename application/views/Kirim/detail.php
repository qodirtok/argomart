 <!-- Begin Page Content -->
 <div class="container-fluid">

     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <a href="<?= base_url('kirimbrg'); ?>" class="btn badge">
                 <h6 class="m-0 font-weight-bold text-primary"> <i class="fas fa-arrow-left"></i>&nbsp;<?= $judul; ?></h6>
             </a>
         </div>
         <div class="card-body">
             <div class="table-responsive">
                 <table style="text-align:center;" class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                         <tr>
                             <th>No</th>
                             <th>No Faktur</th>
                             <th>Nama Barang</th>
                             <th>Jumlah / Satuan</th>
                             <th>Harga</th>
                             <th>Diskon</th>
                             <th>Sub Total</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php foreach ($detail as $row => $value) : ?>
                             <?php $number = 1; ?>
                             <tr>
                                 <td>
                                     <?= ($row + 1) ?>
                                 </td>
                                 <td>
                                     <?= $value->no_faktur ?>
                                 </td>
                                 <td>
                                     <?= $value->nama ?>
                                 </td>
                                 <td>
                                     <?= $value->jumlah ?> / <?= $value->satuan ?>
                                 </td>
                                 <td>
                                     Rp. <?= number_format($value->harga); ?>
                                 </td>
                                 <td>
                                     <?= $value->diskon ?>
                                 </td>
                                 <td>
                                     Rp. <?= number_format($value->sub_total); ?>
                                 </td>
                             </tr>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
                 <hr>
                 <a href="<?= base_url('kirimbrg'); ?>" onclick="window.open('<?= base_url('kirimbrg/cetak/') . $value->no_faktur; ?>');" class="btn btn-outline-success float-right">Cetak</a>
             </div>
         </div>
     </div>
 </div> <!-- /.container-fluid -->

 </div>
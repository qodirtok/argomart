 <!-- Begin Page Content -->
 <div class="container-fluid">
     <!-- <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
 	<?php if ($this->session->flashdata('msg')) : ?>																																																																																																																																	</div>
 	<?php endif; ?>
 	<?php if ($this->session->flashdata('failed')) : ?>																																																																																																																													</div>
 	<?php endif; ?> -->
     <!-- Page Heading -->
     <!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-shopping-cart"></i> Pembelian</h6>
         </div>
         <div class="card-body">
             <center><?= $this->session->flashdata('msg'); ?></center>
             <!-- <h3 class="page-header">
                 <a href="#" data-toggle="modal" data-target="#largeModal" class="pull-right"><small>Cari Produk!</small></a>
             </h3> -->
             <!-- ============ MODAL ADD =============== -->
             <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                 <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                         <div class="modal-header">
                             <h5 class="modal-title" id="exampleModalLabel">Detail Barang</h5>
                             <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                             </button>
                         </div>
                         <div class="modal-body" style="overflow:scroll;height:500px;">

                             <table class="table table-bordered table-condensed" style="font-size:12px;" id="mydata">
                                 <thead>
                                     <tr>
                                         <th style="text-align:center;width:40px;">No</th>
                                         <th style="width:120px;">Kode Barang</th>
                                         <th style="width:240px;">Nama Barang</th>
                                         <th>Satuan</th>
                                         <th style="width:100px;">Harga Jual (Grosir)</th>
                                         <th>Stok</th>
                                         <th style="width:100px;text-align:center;">Aksi</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     <?php
                                        $no = 0;
                                        foreach ($barang as $a) :
                                            $no++;
                                            $id = $a->Kode_barang;
                                            $nm = $a->nama;
                                            $satuan = $a->id_satuan;
                                            $nma_satuan = $a->nma_satuan;
                                            $harpok = $a->hrg_beli;
                                            $harjul = $a->hrg_jual_grosir;
                                            $stok = $a->stok;
                                            $kat_id = $a->id_katagori;
                                            ?>
                                         <tr>
                                             <td style="text-align:center;"><?php echo $no; ?></td>
                                             <td><?php echo $id; ?></td>
                                             <td><?php echo $nm; ?></td>
                                             <td style="text-align:center;"><?php echo $nma_satuan; ?></td>
                                             <td style="text-align:right;"><?php echo 'Rp ' . number_format($harjul); ?></td>
                                             <td style="text-align:center;"><?php echo $stok; ?></td>
                                             <td style="text-align:center;">
                                                 <form action="<?php echo base_url() . 'Pembelian/add_to_cart' ?>" method="post">
                                                     <input type="hidden" name="kode_brg" value="<?php echo $id ?>">
                                                     <input type="hidden" name="nabar" value="<?php echo $nm; ?>">
                                                     <input type="hidden" name="satuan" value="<?php echo $nma_satuan; ?>">
                                                     <input type="hidden" name="stok" value="<?php echo $stok; ?>">
                                                     <input type="hidden" name="hrg_jual" value="<?php echo number_format($harjul); ?>">
                                                     <input type="hidden" name="diskon" value="0">
                                                     <input type="hidden" name="qty" value="1" required>
                                                     <button type="submit" class="btn btn-outline-info btn-sm" title="Pilih"><span class="fa fa-edit"></span> Pilih</button>
                                                 </form>
                                             </td>
                                         </tr>
                                     <?php endforeach; ?>
                                 </tbody>
                             </table>

                         </div>

                         <div class="modal-footer">
                             <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>

                         </div>
                     </div>
                 </div>
             </div>
             <!-- End Modal -->
             <div class="panel panel-primary alert-info">
                 <div class="panel-body" style="height: 240px;">
                     <form action="<?= base_url('pembelian/add_to_cart'); ?>" method="post">
                         <br>
                         <table>
                             <tr>
                                 <th style="width:100px;padding-bottom:5px;">
                                     <label for="nofak">No Faktur</label>
                                 </th>
                                 <th style="width:300px;padding-bottom:5px;">
                                     <input class="form-control" type="text" name="nofaktur" value="<?= $this->session->userdata('nofaktur'); ?>" required />
                                     <div class="invalid-feedback">
                                     </div>
                                 </th>
                                 <th style="width:200px;padding-bottom:5px;">
                                 </th>
                                 <th style="width:80px;padding-bottom:5px;">Suplier</th>
                                 <td style="width:350px;">
                                     <select name="suplier" class="form-control selectpicker show-tick" data-live-search="true" title="Pilih Suplier" data-width="100%" required>
                                         <?php foreach ($sup as $i) {
                                                $id_sup = $i['id_supplier'];
                                                $nm_sup = $i['nma_supplier'];
                                                $notelp_sup = $i['notlpn'];
                                                $sess_id = $this->session->userdata('suplier');
                                                if ($sess_id == $id_sup)
                                                    echo "<option value='$id_sup' selected>$nm_sup - $notelp_sup</option>";
                                                else
                                                    echo "<option value='$id_sup'>$nm_sup - $notelp_sup</option>";
                                            } ?>
                                     </select>
                                 </td>
                             </tr>
                             <tr>
                                 <th style="width:100px;padding-bottom:5px;">
                                     <label for="tgl">Tanggal</label>
                                 </th>
                                 <th style="width:100px;padding-bottom:5px;">
                                     <div class="input-daterange input-group" id="datepicker">
                                         <input type="text" class="input-sm form-control" name="tgl" placeholder="Tanggal" value="<?= $this->session->userdata('tglfak'); ?>" required />
                                     </div>
                                 </th>
                             </tr>
                             <tr>
                                 <th style="width:100px;padding-bottom:5px;">
                                     <label for="kdbarang">Kode Barang/Nama</label>
                                 </th>
                                 <th style="width:300px;padding-bottom:5px;">
                                     <input type="text" class="form-control input_sm" name="kode_brg" id="kode_brg" autofocus />
                                 </th>
                             </tr>
                             <tr>
                                 <div id="detail_penjualan" style="position:absolute;right:120px;margin-top: 130px;">
                                 </div>
                             </tr>
                         </table>
                     </form>
                     <br>
                     <!-- <div class="card-body"> -->
                 </div>
             </div>
         </div>



         <div class="card-body">
             <form id="form-id" action="<?= base_url() . 'pembelian/simpan_pembelian' ?>" method="post">
                 <input type="hidden" id="id_member" name="id_member" value="13">
                 <input type="hidden" name="idpembayaran" value="1">
                 <input type="hidden" name="idpengiriman" value="1">
                 <hr>
                 <div class="table-responsive">
                     <table class="table table-condensed table-hover">
                         <tr>
                             <td>
                                 <h5>Total</h5>
                             </td>
                             <td align="right">
                                 <h5>Rp</h5>
                             </td>
                             <td align="right">
                                 <div class="col-lg-5">
                                     <b><input type="text" name="total2" value="<?php echo number_format($this->cart->total()); ?>" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly></b>
                                     <input type="hidden" id="total" name="total" value="<?= $this->cart->total(); ?>" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly>
                                 </div>
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <h5>Tunai</h5>
                             </td>
                             <td align="right">
                                 <h5>Rp</h5>
                             </td>
                             <td align="right">
                                 <div class="col-lg-5">
                                     <input type="text" id="jml_uang" name="jml_uang" class="jml_uang form-control input-sm" style="text-align:right;margin-bottom:5px;" required>
                                     <input type="hidden" id="jml_uang2" name="jml_uang2" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" required>
                                 </div>
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <h5>Kembalian</h5>
                             </td>
                             <td align="right">
                                 <h5>Rp</h3>
                             </td>
                             <td align="right">
                                 <div class="col-lg-5">
                                     <input type="text" id="kembalian" name="kembalian" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly>
                                 </div>
                             </td>
                         </tr>
                     </table>
                 </div>
                 <br>
                 <div class="table-responsive">
                     <table>
                         <tr>
                             <button type="submit" class="btn btn-outline-primary"> Simpan </button>
                             <!-- <td align="right"><a href="<?= base_url('transaksi'); ?>" class="btn btn-outline-primary" onclick="document.getElementById('form-id').submit();"> Simpan </a></td> -->
                             <!-- <td align="right"><button class="btn btn-outline-primary float-right">Selesai</button></td> window.location.reload(); -->
                             <!-- <td align="right"><a href="<?= base_url('Transaksi/hapuscart') ?>" class="btn btn-outline-danger float-right">Clear</a></td> -->
                         </tr>
                     </table>
                 </div>
             </form>

             <hr>
             <?= $this->session->flashdata('message'); ?>
             <div class="table-responsive">
                 <table class="table table-bordered table-condensed table-hover" width='100%'>
                     <thead>
                         <tr>
                             <th style="text-align:center;">Kode<br>Barang</th>
                             <th style="text-align:center;">Nama<br>Barang</th>
                             <th style="text-align:center;">Satuan</th>
                             <th style="text-align:center;">Harga Beli</th>
                            <!-- <th style="text-align:center;">Harga Jual(%)</th> -->
                             <th style="text-align:center;">Harga Jual(Rp.)</th>
                             <th style="text-align:center;">Jumlah<br>Beli</th>
                             <th style="text-align:center;">Total</th>
                             <th></th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i = 1; ?>
                         <?php foreach ($this->cart->contents() as $items) : ?>
                             <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
                             <tr>
                                 <td><?= $items['id']; ?></td>
                                 <td><?= $items['name']; ?></td>
                                 <td style="text-align:center;"><?= $items['satuan']; ?></td>
                                 <td style="text-align:right;"><?php echo number_format($items['price']); ?></td>
                                 <!-- <td style="text-align:right;"><?php echo number_format($items['persenan']); ?></td>-->
                                 <td style="text-align:right;"><?php echo number_format($items['harjulecer']); ?></td>
                                 <td style="text-align:center;"><?php echo number_format($items['qty']); ?></td>
                                 <td style="text-align:right;"><?php echo number_format($items['subtotal']); ?></td>
                                 <td style="text-align:center;"><a href="<?= site_url() . 'pembelian/remove/' . $items['rowid']; ?>" class="btn badge btn-outline-danger"><span class="fas fa-fw fa-times"></span> Batal</a></td>
                             </tr>

                             <?php $i++; ?>
                         <?php endforeach; ?>
                         <tr>
                             <td colspan='6' align='center'><b>Total</b></td>
                             <td align='right' colspan="1"><b><?php echo number_format($this->cart->total()); ?></b></td>
                             <td></td>
                         </tr>
                     </tbody>

                 </table>
                 <br>
                 <div class="table-responsive">
                     <table>
                         <tr>
                             <!-- <td align="right"><a href="<?= base_url('pembelian/simpan_pembelian'); ?>" class="btn btn-outline-primary"> Simpan </a></td> -->

                         </tr>
                     </table>
                 </div>
             </div>
         </div>

         <br>
     </div>
 </div>
 </div>

 <!-- /.container-fluid -->
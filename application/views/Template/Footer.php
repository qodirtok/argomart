<!-- Footer -->
<footer class="sticky-footer bg-white">
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span>Copyright &copy; Agromart 2019</span>
		</div>
	</div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Apakah Anda Yakin Ingin Keluar?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Pilih "Keluar" Jika Ingin Keluar</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
				<a class="btn btn-primary" href="<?= base_url('user/logout'); ?>">Keluar</a>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('asset/'); ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('asset/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('asset/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('asset/'); ?>js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="<?= base_url('asset/'); ?>vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('asset/'); ?>vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="<?= base_url('asset/'); ?>js/demo/datatables-demo.js"></script>

<!-- SweetAlert -->
<script src="<?= base_url('asset/'); ?>sweetalert2/dist/sweetalert2.all.min.js"></script>
<script src="<?= base_url('asset/'); ?>sweetalert2/dist/MyScript.js"></script>
<!-- format price -->
<script src="<?php echo base_url('asset/') ?>js/jquery.price_format.min.js"></script>
<script src="<?php echo base_url('asset/') ?>js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url('asset/') ?>js/datepicker.min.js"></script>
<script src="<?php echo base_url() . 'asset/select/js/bootstrap-select.min.js' ?>"></script>

<script src="<?php echo base_url() . 'asset/chartt/js/morris.min.js' ?>"></script>
<script src="<?php echo base_url() . 'asset/chartt/js/raphael-min.js' ?>"></script>
<script src="<?= base_url() . 'asset/myscript.js' ?>"></script>

<!-- start scrip print recta -->
<script src="<?php echo base_url('asset/') ?>js/recta.js"></script>

  <script type="text/javascript">
    var printer

    $('#print').click(() => {
      printer = new Recta(8755764224, 1811)
      printer.open().then(() => {
        printer.align('center')
          .text('Hello World !!')
          .bold(true)
          .text('This is bold text')
          .bold(false)
          .underline(true)
          .text('This is underline text')
          .underline(false)
          .barcode('UPC-A', '123456789012')
          .cut()
          .print()

      }).catch((e) => {
        // Show Error if get an Error
        $('#error').text(e)
      })
    })
  </script>	

<!-- end scrip print recta -->

<!-- Page level custom scripts -->

<!-- <script>
	const flashData = $('.flash-data').data('flashdata');
	if (flashData) {
		Swal.fire({
			title: 'Selamat datang',
			type: 'success',
			text: 'Sudahkan Anda Menginputkan Modal Hari Ini ? jika belum silahkan inputkan modal terlebih dahulu !'
		});
	}
</script> -->

<!-- <script>
	$(window).load(function() {
		$('#myModal').modal('show');
	});
</script> -->

<script>
	$(document).ready(function() {
		areaChart();
		$(window).resize(function() {
			window.areaChart.redraw();
		});
	});

	function areaChart() {
		window.areaChart = Morris.Area({
			element: 'graph',
			data: <?= $report; ?>,
			xkey: 'bulan',
			ykeys: ['Pendapatan', 'Keuntungan'],
			labels: ['Pendapatan', 'Keuntungan'],
			lineColors: ['#1e88e5', '#ff3321'],
			lineWidth: '3px',
			resize: true,
			redraw: true
		});
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kirim').change(function() {
			var id = $(this).val();
			$.ajax({
				url: "<?php echo base_url(); ?>transaksi/get_subkategori",
				method: "POST",
				data: {
					id: id
				},
				async: false,
				dataType: 'json',
				success: function(data) {
					var html = '';
					var i;
					for (i = 0; i < data.length; i++) {
						html += '<option>' + data[i].Nama + '</option>';
					}
					$('.subkategori').html(html);
				}
			});
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#mydata').DataTable();
	});
</script>

<script>
	$(function() {
		$('#jml_uang').on("input", function() {
			var total = $('#total').val();
			var jumuang = $('#jml_uang').val();
			var hsl = jumuang.replace(/[^\d]/g, "");
			$('#jml_uang2').val(hsl);
			$('#kembalian').val(hsl - total);
		})

	});
</script>

<script>
	$(function() {
		$('.jml_uang').priceFormat({
			prefix: '',
			//centsSeparator: '',
			centsLimit: 0,
			thousandsSeparator: ','
		});
		$('#jml_uang2').priceFormat({
			prefix: '',
			//centsSeparator: '',
			centsLimit: 0,
			thousandsSeparator: ''
		});
		$('#kembalian').priceFormat({
			prefix: '',
			//centsSeparator: '',
			centsLimit: 0,
			thousandsSeparator: ','
		});
		$('.harjul').priceFormat({
			prefix: '',
			//centsSeparator: '',
			centsLimit: 0,
			thousandsSeparator: ','
		});
	});
</script>

<script>
	// Add the following code if you want the name of the file appear on select
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {

		$("#kode_brg").focus();
		$("#kode_brg").on("input", function() {
			var kobar = {
				kode_brg: $(this).val()
			};
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() . 'transaksi/get_barang'; ?>",
				data: kobar,
				success: function(msg) {
					$('#detail_barang').html(msg);
				}
			});
		});
		$("#kode_brg").keypress(function(e) {
			if (e.which == 13) {
				$("#jumlah").focus();
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {

		$("#kode_brg").focus();
		$("#kode_brg").on("input", function() {
			var kobar = {
				kode_brg: $(this).val()
			};
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() . 'penjualecer/get_barang'; ?>",
				data: kobar,
				success: function(msg) {
					$('#detail_barang_ecer').html(msg);
				}
			});
		});
		$("#kode_brg").keypress(function(e) {
			if (e.which == 13) {
				$("#jumlah").focus();
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		//Ajax kabupaten/kota insert
		$("#kode_brg").focus();
		$("#kode_brg").on("input", function() {
			var kobar = {
				kode_brg: $(this).val()
			};
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() . 'pembelian/get_barang'; ?>",
				data: kobar,
				success: function(msg) {
					$('#detail_penjualan').html(msg);
				}
			});
		});
		$("#kode_brg").keypress(function(e) {
			if (e.which == 13) {
				$("#jumlah").focus();
			}
		});
	});
</script>

<script type="text/javascript">
	$('#btnadd').on('click', function() {
		var lastRow = $('#table-row tbody tr:first').html();
		//alert(lastRow);
		$('#table-row tbody').append('<tr>' + lastRow + '</tr>');
		$('#table-row tbody tr:first input').val('');
		$("#kode").focus();
	});
	// Delete row on click in the table
	$('#table-row').on('click', 'tr a', function(e) {
		var lenRow = $('#table-row tbody tr').length;
		e.preventDefault();
		if (lenRow == 1 || lenRow <= 1) {
			alert("Can't remove all row!");
		} else {
			$(this).parents('tr').remove();
		}
	});
</script>

<script type="text/javascript">
	$(function() {

		$(document).on("click", "td", function() {
			$(this).find("span[class~='caption']").hide();
			$(this).find("input[class~='editor']").fadeIn().focus();
		});

		$(document).on("keydown", ".editor", function(e) {
			if (e.keyCode == 13) {
				var target = $(e.target);
				var value = target.val();
				var id = target.attr("data-id");
				var data = {
					id: id,
					value: value
				};
				if (target.is(".field-jumlah")) {
					data.modul = "jumlah";
				}

				$.ajax({
					type: "POST",
					url: "<?php echo base_url('transaksi/updatejmlh'); ?>",
					data: data,
					success: function(a) {
						target.hide();
						target.siblings("span[class~='caption']").html(value).fadeIn();
					}

				})

			}

		});

	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kode').on('input', function() {

			var kode = $(this).val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('kaskeluar/getakun') ?>",
				dataType: "JSON",
				data: {
					kode: kode
				},
				cache: false,
				success: function(data) {
					$.each(data, function(kode_akun, nama_akun) {
						$('[name="nama[]"]').val(data.nama_akun);
						// $('[name="harga"]').val(data.harga);
						// $('[name="satuan"]').val(data.satuan);

					});

				}
			});
			return false;
		});
	});
</script>



<script type="text/javascript">
	$(document).ready(function() {
		$(".btn1").click(function() {
			$("p").hide();
		});

		$(".btn2").click(function() {

			$("p").show();

		});

	});
</script>

<script>
	function myFunction() {
		var x = document.getElementById("myDIV");
		if (x.style.display === "block") {
			x.style.display = "none";
		} else {
			x.style.display = "block";
		}
	}
</script>


<script type="text/javascript">
	$(document).ready(function() {
		$('#idmember').on('change', function() {

			var idmember = $(this).val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('Transaksi/get_memberr') ?>",
				dataType: "JSON",
				data: {
					idmember: idmember
				},
				cache: false,
				success: function(data) {
					$.each(data, function(id_member, Alamat, Nohp) {
						$('[name="id_member"]').val(data.id_member);
						$('[name="alamat"]').val(data.Alamat);
						$('[name="nohp"]').val(data.Nohp);
					});

				}
			});
			return false;
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#idpem').on('change', function() {

			var idpem = $(this).val();
			$.ajax({
				type: "POST",
				url: "<?= base_url('transaksi/pembayaran') ?>",
				dataType: "JSON",
				data: {
					idpem: idpem
				},
				cache: false,
				success: function(data) {
					$.each(data, function(id_pembayaran, nama_pembayaran) {
						$('[name="idpembayaran"]').val(data.id_pembayaran);
						$('[name="nama_pembayaran"]').val(data.nama_pembayaran);
					});

				}
			});
			return false;
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kirim').on('change', function() {

			var kirim = $(this).val();
			$.ajax({
				type: "POST",
				url: "<?= base_url('transaksi/pengiriman') ?>",
				dataType: "JSON",
				data: {
					kirim: kirim
				},
				cache: false,
				success: function(data) {
					$.each(data, function(id_pengiriman, nama_pengiriman) {
						$('[name="idpengiriman"]').val(data.id_pengiriman);
						$('[name="nama_pengiriman"]').val(data.nama_pengiriman);
					});

				}
			});
			return false;
		});
	});
</script>


<script type="text/javascript">
	$(function() {
		$('#datetimepicker').datetimepicker({
			format: 'DD MMMM YYYY HH:mm',
		});

		$('#datepicker').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#datepicker2').datetimepicker({
			format: 'YYYY-MM-DD',
		});

		$('#timepicker').datetimepicker({
			format: 'HH:mm'
		});
	});
</script>

<script type="text/javascript">
	$(function() {
		$('.harpok').priceFormat({
			prefix: '',
			//centsSeparator: '',
			centsLimit: 0,
			thousandsSeparator: ','
		});
		$('.harjul').priceFormat({
			prefix: '',
			//centsSeparator: '',
			centsLimit: 0,
			thousandsSeparator: ','
		});
	});
</script>

<script>
	$('.input-daterange').datepicker({
		format: "yyyy-mm-dd",
		todayBtn: "linked",
		language: "id"
	});
</script>

<script type="text/javascript">
	var rupiah = document.getElementById('rupiah');
	rupiah.addEventListener('keyup', function(e) {
		// tambahkan 'Rp.' pada saat form di ketik
		// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
		rupiah.value = formatRupiah(this.value, '');
	});

	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
	}
</script>

<script type="text/javascript">
	var uang = document.getElementById('uang');
	uang.addEventListener('keyup', function(e) {
		// tambahkan 'Rp.' pada saat form di ketik
		// gunakan fungsi formatuang() untuk mengubah angka yang di ketik menjadi format angka
		uang.value = formatRupiah(this.value, '');
	});

	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			uang = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			uang += separator + ribuan.join('.');
		}

		uang = split[1] != undefined ? uang + ',' + split[1] : uang;
		return prefix == undefined ? uang : (uang ? '' + uang : '');
	}
</script>

<script type="text/javascript">
	var ecer = document.getElementById('ecer');
	ecer.addEventListener('keyup', function(e) {
		// tambahkan 'Rp.' pada saat form di ketik
		// gunakan fungsi formatecer() untuk mengubah angka yang di ketik menjadi format angka
		ecer.value = formatRupiah(this.value, '');
	});

	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			ecer = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			ecer += separator + ribuan.join('.');
		}

		ecer = split[1] != undefined ? ecer + ',' + split[1] : ecer;
		return prefix == undefined ? ecer : (ecer ? '' + ecer : '');
	}
</script>




</body>

</html>
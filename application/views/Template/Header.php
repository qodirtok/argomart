<!DOCTYPE html>
<?php
$id_level = $this->session->userdata('id_lvl');

$menu     = $this->uri->segment(1);
$sub_menu = $this->uri->segment(2);
$sub_menu3 = $this->uri->segment(3);

?>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?= $title; ?></title>
	<link rel="shortcut icon" href="<?= base_url(); ?>asset/img/keranjangnew.png">
	<!-- Custom fonts for this template-->
	<!-- <link href="<?= base_url('asset/'); ?>fontawesome/css/all.min.css" rel="stylesheet" type="text/css"> -->
	<link href="<?= base_url('asset/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<!-- <link href="<?= base_url('asset/'); ?>css/font-awesome.css" rel="stylesheet" type="text/css"> -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?= base_url('asset/'); ?>css/sb-admin-2.min.css" rel="stylesheet">
	<link href="<?= base_url('asset/'); ?>css/style.css" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url() . 'asset/chartt/css/morris.css' ?>">

	<!-- Custom styles for this page -->
	<link href="<?= base_url('asset/'); ?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
	<link href="<?php echo base_url('asset/'); ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('asset/'); ?>css/datepicker.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() . 'asset/select/css/bootstrap-select.min.css' ?>" rel="stylesheet">

	<style rel="stylesheet">
		#myDIV {
			width: 100%;
			padding: 3px 0;
			text-align: center;
			background-color: #d7f1f5;
			margin-top: 20px;
			display: none;
		}
	</style>
	<!-- Custom style
	<link rel="stylesheet" href="<?= base_url('asset/'); ?>css/my_css.css"> -->
</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

			<!-- Sidebar - Brand -->
			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('User'); ?>">
				<div class="sidebar-brand-icon">
					<!-- <i class="fas fa-laugh-wink"></i> -->
					<img src="<?= base_url(); ?>asset/img/keranjangnew.png" height="50px">
				</div>
				<div class="sidebar-brand-text mx-3">Agromart</div>
			</a>


			<!-- Divider -->
			<hr class="sidebar-divider my-0">

			<!-- Nav Item - Dashboard -->
			<li class="nav-item <?php if ($menu == "User") {
									echo 'active';
								} ?>">
				<!-- <li class="nav-item"> -->
				<a class="nav-link" href="<?= base_url('User'); ?>">
					<i class="fas fa-fw fa-tachometer-alt"></i>
					<span>Dashboard</span></a>
			</li>

			<!-- Divider -->

			<?php if ($id_level == '1' || $id_level == '2' || $id_level == '5') : ?>
				<hr class="sidebar-divider">
				<div class="sidebar-heading">
					Modal
				</div>
				<li class="nav-item <?php if ($menu == "Modal") {
										echo 'active';
									} ?>">
					<!-- <li class="nav-item"> -->
					<a class="nav-link" href="<?= base_url('Modal'); ?>">
						<i class="fas fa-fw fa-money-check"></i>
						<span>Modal Kasir</span></a>
				</li>
			<?php endif; ?>

			<?php if ($id_level == '1' || $id_level == '2' || $id_level == '5') : ?>
				<hr class="sidebar-divider">
				<!-- Heading -->
				<div class="sidebar-heading">
					Master
				</div>
				<!-- Nav Item - Pages Collapse Menu -->
				<li class="nav-item <?php if ($menu == "member" || $menu == "barang" || $menu == "kategori" || $menu == "supplier" || $menu == "satuan") {
										echo "active";
									} ?>">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
						<i class="far fa-file-alt"></i>
						<span>Master</span>
					</a>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
						<div class="bg-white py-2 collapse-inner rounded">
							<h6 class="collapse-header">Master</h6>
							<?php if ($id_level == '1' || $id_level == '5') : ?>
								<a class="collapse-item <?php if ($sub_menu == "barang" || $menu == "barang") {
															echo "active";
														} ?>" href="<?= base_url('barang'); ?>">Barang</a>
								<a class="collapse-item <?php if ($sub_menu == "kategori" || $menu == "kategori") {
															echo "active";
														} ?>" href="<?= base_url('kategori') ?>">Katagori Barang</a>
								<a class="collapse-item <?php if ($sub_menu == "supplier" || $menu == "supplier") {
															echo "active";
														} ?>" href="<?= base_url('supplier'); ?>">Supplier</a>
								<a class="collapse-item <?php if ($sub_menu == "satuan" || $menu == "satuan") {
															echo "active";
														} ?>" href="<?= base_url('satuan') ?>">Satuan</a>
							<?php endif; ?>
						</div>
					</div>
				</li>
			<?php endif; ?>

			<?php if ($id_level == '1' || $id_level == '3'  || $id_level == '5') : ?>
				<hr class="sidebar-divider">
				<!-- Heading -->
				<div class="sidebar-heading">
					Gudang
				</div>
				<!-- Nav Item - Pages Collapse Menu -->
				<li class="nav-item <?php if ($menu == "pembelian") {
										echo 'active';
									} ?>">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#gudang" aria-expanded="true" aria-controls="gudang">
						<i class="fas fa-boxes"></i>
						<span>Gudang</span>
					</a>
					<div id="gudang" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
						<div class="bg-white py-2 collapse-inner rounded">
							<h6 class="collapse-header">Gudang</h6>
							<a class="collapse-item <?php if ($menu  == "pembelian") {
														echo "active";
													} ?>" href="<?= base_url('pembelian'); ?>">Pembelian</a>
						</div>
					</div>

				<li class="nav-item <?php if ($menu == "hutang") {
										echo "active";
									} ?>">
					<a class="nav-link" href="<?php echo base_url('hutang') ?>">
						<i class="far fa-file-alt"></i>
						<span>Hutang</span></a>
				</li>
				</li>
			<?php endif; ?>

			<?php if ($id_level == '1' || $id_level == '2' || $id_level == '5') : ?>
				<hr class="sidebar-divider">
				<div class="sidebar-heading">
					Transaksi
				</div>
				<li class="nav-item <?php if ($menu == "transaksi" || $menu == "Penjualecer") {
										echo 'active';
									} ?>">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
						<i class="fas fa-fw fa-shopping-cart"></i>
						<span>Transaksi</span>
					</a>
					<div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
						<div class="bg-white py-2 collapse-inner rounded">
							<h6 class="collapse-header">Transaksi</h6>
							<!-- <a class="collapse-item <?php if ($menu == "transaksi") {
																echo "active";
															} ?>" href="<?= base_url('transaksi'); ?>">Penjualan (Grosir)</a> -->
							<a class="collapse-item <?php if ($menu == "Penjualecer") {
														echo "active";
													} ?>" href="<?= base_url('Penjualecer'); ?>">Penjualan</a>


						</div>
					</div>
				</li>

				<!-- <li class="nav-item <?php if ($menu == "hutang") {
												echo "active";
											} ?>">
					<a class="nav-link" href="<?php echo base_url('hutang') ?>">
						<i class="far fa-file-alt"></i>
						<span>Hutang</span></a>
				</li> -->

				<li class="nav-item <?php if ($menu == "diskon") {
										echo "active";
									} ?>">
					<a class="nav-link" href="<?php echo base_url('diskon') ?>">
						<i class="fas fa-fw fa-tags"></i>
						<span>Diskon</span></a>
				</li>
			<?php endif; ?>

			<?php if ($id_level == '1' || $id_level == '5') : ?>
				<hr class="sidebar-divider">
				<div class="sidebar-heading">
					Keuangan
				</div>
				<li class="nav-item <?php if ($sub_menu == "kaskeluar") {
										echo 'active';
									} else if ($sub_menu == "kasmasuk") {
										echo 'active';
									} else if ($sub_menu == "jurnalumum") {
										echo 'active';
									} ?>">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTri" aria-expanded="true" aria-controls="collapseTwo">
						<i class="fas fa-hand-holding-usd"></i>
						<span>Keuangan</span>
					</a>
					<div id="collapseTri" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
						<div class="bg-white py-2 collapse-inner rounded">
							<h6 class="collapse-header">Keuangan</h6>
							<a class="collapse-item <?php if ($sub_menu == "kaskeluar") {
														echo "active";
													} ?>" href="<?= base_url('User/kaskeluar'); ?>">Kas Keluar</a>
							<a class="collapse-item <?php if ($sub_menu == "kasmasuk") {
														echo "active";
													} ?>" href="<?= base_url('kasmasuk'); ?>">kas Masuk</a>
							<a class="collapse-item <?php if ($sub_menu == "jurnalumum") {
														echo "active";
													} ?>" href="<?= base_url('User/jurnalumum'); ?>">Jurnal Umum</a>
						</div>
					</div>
				</li>
			<?php endif; ?>



			<?php if ($id_level == '1' || $id_level == '2' || $id_level == '5') : ?>
				<!-- <hr class="sidebar-divider"> -->
				<li class="nav-item <?php if ($sub_menu == "Kirimbrg") {
										echo 'active';
									} ?>">
					<!-- <li class="nav-item"> -->
					<!-- <a class="nav-link" href="<?= base_url('User/Kirimbrg'); ?>">
						<i class="fas fa-fw fa-truck-loading"></i>
						<span>Kirim Barang</span></a> -->
				</li>
			<?php endif; ?>

			<?php if ($id_level == '1' || $id_level == '2' || $id_level == '5') : ?>
				<hr class="sidebar-divider">
				<div class="sidebar-heading">
					Retur
				</div>
				<li class="nav-item <?php if ($menu == "returpembelian") {
										echo 'active';
									} else if ($menu == "returpenjualan") {
										echo 'active';
									} ?>">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities2" aria-expanded="true" aria-controls="collapseUtilities">
						<i class="fas fa-sync-alt"></i>
						<span>Retur</span>
					</a>
					<div id="collapseUtilities2" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
						<div class="bg-white py-2 collapse-inner rounded">
							<h6 class="collapse-header">Retur</h6>
							<a class="collapse-item <?php if ($menu == "returpembelian") {
														echo "active";
													} ?>" href="<?= base_url('returpembelian'); ?>">Retur Titipan</a>
							<!-- <a class="collapse-item <?php if ($menu == "returpenjualan") {
																echo "active";
															} ?>" href="<?= base_url('returpenjualan'); ?>">Retur Penjualan</a> -->
						</div>
					</div>
				</li>
			<?php endif; ?>


			<?php if ($id_level == '1' || $id_level == '5') : ?>
				<hr class="sidebar-divider">
				<div class="sidebar-heading">
					Akun
				</div>
				<li class="nav-item <?php if ($menu == "akun") {
										echo 'active';
									} ?>">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
						<i class="fas fa-fw fa-users"></i>
						<span>Akun</span>
					</a>
					<div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
						<div class="bg-white py-2 collapse-inner rounded">
							<h6 class="collapse-header">Akun:</h6>
							<a class="collapse-item <?php if ($menu == "akun") {
														echo 'active';
													} ?>" href="<?= base_url('akun'); ?>">Data Akun</a>
							<!-- <a class="collapse-item" href="utilities-border.html">Hak Akses</a> -->
						</div>
					</div>
				</li>
			<?php endif; ?>

			<?php if ($id_level == '1' || $id_level == '2' || $id_level == '5') : ?>
				<hr class="sidebar-divider">
				<div class="sidebar-heading">
					Laporan
				</div>
				<li class="nav-item <?php if ($menu == "lporanjul" || $menu == "lporstok" || $menu == "laporankasir" || $menu == "bukubesar" || $menu == "labarugi" || $menu == "aruskas" || $menu == "neracasaldo" || $menu == "neraca" || $menu == "lporstok" || $menu == "Lporanpembayaran") {
										echo 'active';
									} ?>">
					<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities3" aria-expanded="true" aria-controls="collapseUtilities">
						<i class="fas fa-fw fa-file-pdf"></i>
						<span>Laporan</span>
					</a>
					<div id="collapseUtilities3" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
						<div class="bg-white py-2 collapse-inner rounded">
							<h6 class="collapse-header">Laporan Transaksi</h6>
							<a class="collapse-item <?php if ($this->uri->segment(1) == "laporankasir") {
														echo "active";
													} ?>" href="<?= base_url('laporankasir'); ?>">Laporan Kasir</a>
							<a class="collapse-item <?php if ($this->uri->segment(1) == "lporanjul") {
														echo "active";
													} ?>" href="<?= base_url('lporanjul'); ?>">Laporan Penjualan</a>
							<a class="collapse-item <?php if ($this->uri->segment(1) == "lporanbel") {
														echo "active";
													} ?>" href="<?= base_url('lporanbel'); ?>">Laporan Pembelian</a>
							<a class="collapse-item <?php if ($this->uri->segment(1) == "lporantitip") {
														echo "active";
													} ?>" href="<?= base_url('lporantitip'); ?>">Laporan Barang Titip</a>
							<a class="collapse-item <?php if ($this->uri->segment(1) == "lporstok") {
														echo "active";
													} ?>" href="<?= base_url('lporstok'); ?>">Laporan Stok</a>
							<a class="collapse-item <?php if ($this->uri->segment(1) == "Lporanpembayaran") {
														echo "active";
													} ?>" href="<?= base_url('lporanpembayaran'); ?>">Laporan Pembayaran</a>

							<h6 class="collapse-header">Laporan Retur</h6>
							<a class="collapse-item <?php if ($this->uri->segment(1) == "laporanreturbeli") {
														echo "active";
													} ?>" href="<?= base_url('laporanreturbeli'); ?>">Retur Pembelian</a>

							<?php if ($id_level == '1' || $id_level == '5') : ?>
								<h6 class="collapse-header">Laporan Retur</h6>
								<a class="collapse-item <?php if ($this->uri->segment(1) == "laporanreturbeli") {
															echo "active";
														} ?>" href="<?= base_url('laporanreturbeli'); ?>">Retur Pembelian</a>
								<h6 class="collapse-header">Laporan Keuangan</h6>
								<a class="collapse-item <?php if ($this->uri->segment(1) == "bukubesar") {
															echo "active";
														} ?>" href="<?= base_url('bukubesar'); ?>">Buku Besar</a>
								<a class="collapse-item <?php if ($this->uri->segment(1) == "labarugi") {
															echo "active";
														} ?>" href="<?= base_url('labarugi'); ?>">Laba Rugi</a>
								<a class="collapse-item <?php if ($this->uri->segment(1) == "neracasaldo") {
															echo "active";
														} ?>" href="<?= base_url('neracasaldo'); ?>">Neraca Saldo</a>
								<a class="collapse-item <?php if ($this->uri->segment(1) == "neraca") {
															echo "active";
														} ?>" href="<?= base_url('neraca'); ?>">Neraca</a>
							<?php endif; ?>
						</div>
					</div>
				</li>
			<?php endif; ?>


			<!-- Divider -->
			<hr class="sidebar-divider">
			<li class="nav-item <?php if ($this->uri->segment(1) == "tentang") {
									echo "active";
								} ?>">
				<a class="nav-link" href="<?= base_url('tentang'); ?>">
					<i class="fas fa-fw fa-info-circle"></i>
					<span>Tentang</span>
				</a>
			</li>

			<hr class="sidebar-divider">
			<li class="nav-item">
				<a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
					<i class="fas fa-fw fa-sign-out-alt"></i>
					<span>Keluar</span>
				</a>
			</li>

			<!-- Divider -->
			<hr class="sidebar-divider d-none d-md-block">
			<!-- Sidebar Toggler (Sidebar) -->
			<div class="text-center d-none d-md-inline">
				<button class="rounded-circle border-0" id="sidebarToggle"></button>
			</div>

		</ul>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

					<!-- Sidebar Toggle (Topbar) -->
					<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
						<i class="fa fa-bars"></i>
					</button>

					<!-- Topbar Navbar -->
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link text-gray-600" role="button" href="<?php echo base_url('landing') ?>">Display Diskon</a>
						</li>

						<div class="topbar-divider d-none d-sm-block"></div>

						<!-- Nav Item - User Information -->
						<li class="nav-item dropdown no-arrow">
							<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $user['nama']; ?></span>
								<img class="img-profile rounded-circle" src="<?= base_url(); ?>asset/img/<?= $user['image']; ?>">
							</a>
							<!-- Dropdown - User Information -->
							<div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
								<a class="dropdown-item" href="<?= base_url('Profile'); ?>">
									<i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
									Profile
								</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
									<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
									Keluar
								</a>
							</div>
						</li>

					</ul>

				</nav>
				<!-- End of Topbar -->
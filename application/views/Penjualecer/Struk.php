<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="refresh" content="2; url=<?= base_url('Penjualecer');?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title ?></title>
    <style>
        .table {
            font-size: 11px;
            margin-left: -9px;
            margin-right: -24px;
            margin-top: -1px;
            text-align: left;
        }
    </style>
    <textarea id="printing-css" style="display:none;">.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
<script type="text/javascript">
 window.focus();
 window.print();
//}
</script>
</head>
<!-- onload="window.print()" -->

<body>
    <div style="font-weight: bold;font-size: 15px;text-align: center;">Agromart</div>
    <div style="font-size: 11px;text-align: center;">Jl. S. Supriadi No.48</div>
    <div style="font-size: 11px;text-align: center;">Bandungrejosari, Kec. Sukun</div>
    <div style="font-size: 11px;text-align: center;">Kota Malang,Jawa Timur</div>

    <div style="font-size: 11px;margin-top: 25px;margin-left: -5px;">No. Faktur : <?= $nofak; ?></div>
    <div style="font-size: 11px;margin-top:-7px;margin-left: -5px;margin-right: -20px;">---------------------------------------------------</div>
    <div style="font-size: 11px;margin-top: -5px;margin-left: -5px;"><?= date("d-m-Y H:i"); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= $idadmin ?> </div>
    <div style="font-size: 11px;margin-top:-7px;margin-left: -5px;margin-right: -20px;">---------------------------------------------------</div>

    <?php foreach ($pengirim as $row => $kirim) : ?>

        <table border="0" style="font-size: 11px;margin-top: -7px;margin-left: -8px;">
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td><?= $kirim->Nama; ?></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td><?= $kirim->Alamat; ?></td>
            </tr>
            <tr>
                <td>No. Handphone</td>
                <td>:</td>
                <td><?= $kirim->Nohp; ?></td>
            </tr>
        </table>

        <div style="font-size: 11px;margin-top:-7px;margin-left: -5px;margin-right: -20px;">---------------------------------------------------</div>
    <?php endforeach; ?>

    <!-- cellpadding="15" celspacing="0" -->
    <table class="table" border="0">
        <?php foreach ($cetak as $cetakk) : ?>
            <tr>
                <td width="80px" style="font-weight:100;"><?= $cetakk->nama; ?></td>
                <td width="20px" style="font-weight:100;"><?= $cetakk->jumlah; ?></td>
                <td width="30px" style="font-weight:100;"><?= number_format($cetakk->harga, 0, ",", "."); ?></td>
                <td width="30px" style="font-weight:100;text-align: right;"><?= number_format($cetakk->sub_total, 0, ",", ".") ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <div style="font-size: 11px;margin-top:-5px;margin-left: -5px;margin-right: -20px;">---------------------------------------------------</div>
    <!-- style="font-size: 12px;margin-top: -7px;margin-left: -25px;" -->
    <table border="0" class="table">
        <tr>
            <td>Total</td>
            <td width="70px">:</td>
            <td>Rp.</td>
            <td style="text-align: right;"><?= number_format($cetakk->total_penjualan, 0, ",", ".") ?></td>
        </tr>
        <tr>
            <td>Tunai</td>
            <td width="70px">:</td>
            <td>Rp.</td>
            <td style="text-align: right;"><?= number_format($cetakk->jmlh_uang, 0, ",", ".") ?> </td>
        </tr>
        <tr>
            <td>Kembalian</td>
            <td width="70px">:</td>
            <td>Rp.</td>
            <td style="text-align: right;"><?= number_format($cetakk->kembalian, 0, ",", "."); ?></td>
        </tr>
    </table>
    <div style="font-size: 11px;margin-top:-7px;margin-left: -5px;margin-right: -20px;">---------------------------------------------------</div>
    <div style="font-size: 11px;text-align: center;">---Terima Kasih---</div>
    <div style="font-size: 11px;text-align: center;">---Shilakan Datang Kembali---</div>
</body>

</html>
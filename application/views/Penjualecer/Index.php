 <!-- Begin Page Content -->
 <div class="container-fluid">
 	<!-- <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
 	<?php if ($this->session->flashdata('msg')) : ?>																																																																																																																																	</div>
 	<?php endif; ?>
 	<?php if ($this->session->flashdata('failed')) : ?>																																																																																																																																									</div>
 	<?php endif; ?> -->
 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-shopping-cart"></i> Penjualan</h6>
 		</div>
 		<div class="card-body">
 			<center><?= $this->session->flashdata('msg'); ?></center>
 			<h3 class="page-header">
 				<a href="#" data-toggle="modal" data-target="#largeModal" class="pull-right"><small>Cari Barang!</small></a>
 			</h3>

 			<!-- ============ MODAL ADD =============== -->
 			<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
 				<div class="modal-dialog modal-lg">
 					<div class="modal-content">
 						<div class="modal-header">
 							<h5 class="modal-title" id="exampleModalLabel">Detail Barang</h5>
 							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 								<span aria-hidden="true">×</span>
 							</button>
 						</div>
 						<div class="modal-body" style="overflow:scroll;height:500px;">

 							<table class="table table-bordered table-condensed" style="font-size:12px;" id="mydata">
 								<thead>
 									<tr>
 										<th style="text-align:center;width:40px;">No</th>
 										<th style="width:120px;">Kode Barang</th>
 										<th style="width:240px;">Nama Barang</th>
 										<th>Satuan</th>
 										<th style="width:100px;">Harga Jual (Eceran)</th>
 										<th>Stok</th>
 										<th style="width:100px;text-align:center;">Aksi</th>
 									</tr>
 								</thead>
 								<tbody>
 									<?php
										$no = 0;
										foreach ($barang as $a) :
											$no++;
											$id = $a->Kode_barang;
											$nm = $a->nama;
											$satuan = $a->id_satuan;
											$nma_satuan = $a->nma_satuan;
											$harpok = $a->hrg_beli;
											$harjul = $a->hrg_jual;
											$stok = $a->stok;
											$kat_id = $a->id_katagori;
											?>
 										<tr>
 											<td style="text-align:center;"><?= $no; ?></td>
 											<td><?= $id; ?></td>
 											<td><?= $nm; ?></td>
 											<td style="text-align:center;"><?= $nma_satuan; ?></td>
 											<td style="text-align:right;"><?= 'Rp ' . number_format($harjul); ?></td>
 											<td style="text-align:center;"><?= $stok; ?></td>
 											<td style="text-align:center;">
 												<form action="<?= base_url() . 'penjualecer/add_to_cart' ?>" method="post">
 													<input type="hidden" name="kode_brg" value="<?= $id ?>">
 													<input type="hidden" name="nabar" value="<?= $nm; ?>">
 													<input type="hidden" name="satuan" value="<?= $nma_satuan; ?>">
 													<input type="hidden" name="stok" value="<?= $stok; ?>">
 													<input type="hidden" name="hrg_jual" value="<?= number_format($harjul); ?>">
 													<input type="hidden" name="diskon" value="0">
 													<input type="hidden" name="qty"  value="1" required>
 													<button type="submit" class="btn btn-outline-info btn-sm" title="Pilih"><span class="fa fa-edit"></span> Pilih</button>
 												</form>
 											</td>
 										</tr>
 									<?php endforeach; ?>
 								</tbody>
 							</table>

 						</div>

 						<div class="modal-footer">
 							<button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>

 						</div>
 					</div>
 				</div>
 			</div>

 			<!-- ==================TRANSAKSI=============================== -->
 			<div class="panel panel-primary alert-info">

 				<div class="panel-body">
 					<form action="<?= base_url('penjualecer'), "/add_to_cart"; ?>" method="post">
 						<br>
 						<table>
 							<tr>
 								<th style="width:100px;padding-bottom:5px;padding-left: 10px;">
 									<label for="tgl">Tanggal</label>
 								</th>
 								<th style="width:300px;padding-bottom:5px;padding-left: 30px;">
 									<input class="form-control" type="text" name="tgl" value="<?= $tgl; ?>" readonly />
 								</th>
 								<!-- <th style="width:100px;padding-bottom:5px;padding-left: 20px;">
 									<label for="kirim">Kirim Barang</label>
 								</th> -->
 								<!-- <th style="width:300px;padding-bottom:5px;padding-left: 30px;">
 									<select name="idmember" class="form-control selectpicker show-tick" data-live-search="true" title="Pilih Nama Member" data-width="100%" required>
 										<option value="13" selected>Pelanggan Umum</option> -->
 								<!-- <?php foreach ($this->db->get('tb_member')->result() as $i) {
											$id_mem = $i->id_member;
											$nm_mem = $i->Nama;
											$sess_id = $this->session->userdata('idmember');
											if ($sess_id == $id_mem)
												echo "<option value='$id_mem' selected>$nm_mem</option>";
											else
												echo "<option value='$id_mem'>$nm_mem</option>";
										} ?> -->
 								<!-- 
 									</select>
 								</th> -->
 							</tr>
 							<tr>
 								<th style="width:100px;padding-bottom:5px;padding-left: 10px;">
 									<label for="nofak">No Faktur</label>
 								</th>
 								<th style="width:300px;padding-bottom:5px;padding-left: 30px;">
 									<input class=" form-control" type="text" name="no_faktur" value="<?= $nofaktur; ?>" readonly />
 									<div class="invalid-feedback">
 									</div>
 								</th>
 							</tr>
 							<tr>
 								<th style="width:100px;padding-bottom:5px;padding-left: 10px;">
 									<label for="kdbarang">Kode Barang /<br>Nama</label>
 								</th>
 								<th style="width:300px;padding-bottom:5px;padding-left: 30px;">
 									<input type="text" class="form-control input_sm" name="kode_brg" id="kode_brg" autofocus />
 								</th>
 								<th>
 									<div id="detail_barang_ecer" style="position:absolute;right:45px;">
 									</div>
 								</th>
 							</tr>
 						</table>
 						<!-- <div class="col-lg-2" style="left:10px;">
 								<label for="pembayaran">Bayar Lunas ?</label>
 							</div>
 							<div class="col-lg-2">
 								<div class="custom-control custom-switch">
 									<input type="checkbox" class="custom-control-input" checked id="customSwitch1">
 									<label class="custom-control-label" for="customSwitch1"></label>
 								</div>
							 </div> -->
 					</form>
 				</div>

 				<br><br><br><br>
 			</div>
 			<!-- </div> -->
 			<!-- <a class="btn btn-success" onclick="myFunction()" style="color:black;">Pengiriman</a> -->
 			<!-- <div class="card-body"> -->
 			<form id="form-id" action="<?php echo base_url() . 'penjualecer/simpan_penjualan' ?>" method="post">


 				<hr>
 				<div class="table-responsive">
 					<table class="table table-condensed table-hover">
 						<tr>
 							<td>
 								<h5>Total</h5>
 							</td>
 							<td align="right">
 								<h5>Rp</h5>
 							</td>
 							<td align="right">
 								<div class="col-lg-5">
 									<b><input type="text" name="total2" value="<?= number_format($this->cart->total()); ?>" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly></b>
 									<input type="hidden" id="total" name="total" value="<?= $this->cart->total(); ?>" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly>
 								</div>
 							</td>
 						</tr>
 						<tr>
 							<td>
 								<h5>Tunai</h5>
 							</td>
 							<td align="right">
 								<h5>Rp</h5>
 							</td>
 							<td align="right">
 								<div class="col-lg-5">
 									<input type="text" id="jml_uang" name="jml_uang" class="jml_uang form-control input-sm" style="text-align:right;margin-bottom:5px;" required>
 									<input type="hidden" id="jml_uang2" name="jml_uang2" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" required>
 								</div>
 							</td>
 						</tr>
 						<tr>
 							<td>
 								<h5>Kembalian</h5>
 							</td>
 							<td align="right">
 								<h5>Rp</h3>
 							</td>
 							<td align="right">
 								<div class="col-lg-5">
 									<input type="text" id="kembalian" name="kembalian" class="form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly>
 								</div>
 							</td>
 						</tr>
 					</table>
 				</div>

 				<br>
 				<div class="table-responsive">
 					<table>
 						<tr>
 							<!-- <td align="right"><a href="<?= base_url('penjualecer'); ?>" class="btn btn-outline-primary" onclick="document.getElementById('form-id').submit();"> Simpan </a></td> -->
 							<td align="right"><button class="btn btn-outline-primary float-right">Selesai</button></td>
 							<!-- <td align="right"><a href="<?= base_url('penjualecer/hapuscart') ?>" class="btn btn-outline-danger float-right">Clear</a></td> -->
 						</tr>
 					</table>
 				</div>
 			</form>
 			<hr>
 		</div>


 		<div class="card-body">
 			<?= $this->session->flashdata('message'); ?>
 			<div class="table-responsive">
 				<table class="table table-bordered table-condensed table-hover" width='100%'>
 					<thead>
 						<tr>
 							<th style="text-align:center;">Kode<br>Barang</th>
 							<th style="text-align:center;">Nama<br>Barang</th>
 							<th style="text-align:center;">Jumlah<br>(item)</th>
 							<th style="text-align:center;">Satuan</th>
 							<th style="text-align:center;">Diskon(Rp)</th>
 							<th style="text-align:center;">Harga</th>
 							<th style="text-align:center;">Total</th>
 							<th></th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php $i = 1; ?>
 						<?php foreach ($this->cart->contents() as $items) : ?>
 							<?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
 							<?php
									$qty = $items['qty'];
									$harjul = $items['harjul'];
									$harjulgro = $items['hrgjualgrosir'];
									?>
 							<tr>
 								<td><?= $items['id']; ?></td>
 								<td><?= $items['name']; ?></td>
 								<td style="text-align:center;"><?php echo number_format($qty); ?></td>
 								<td style="text-align:center;"><?= $items['satuan']; ?></td>
 								<td style="text-align:right;"><?php echo $items['disc'] . '%'; ?></td>
 								<td style="text-align:right;"><?php echo number_format($harjul); ?></td>
 								<td style="text-align:right;"><?php echo number_format($items['subtotal']); ?></td>

 								<td style="text-align:center;"><a href="<?= site_url() . 'penjualecer/remove/' . $items['rowid']; ?>" class="btn badge btn-outline-danger"><span class="fas fa-fw fa-times"></span> Batal</a></td>
 							</tr>

 							<?php $i++; ?>
 						<?php endforeach; ?>
 						<tr>
 							<td colspan='2' align='center'><b>Total</b></td>
 							<td align='center'><b><?php echo number_format($this->cart->total_items()); ?> item</b></td>
 							<td align='right' colspan="4"><b><?php echo number_format($this->cart->total()); ?></b></td>
 							<td></td>
 						</tr>
 					</tbody>

 				</table>

 			</div>
 		</div>

 		<br>


 		</form>
 	</div>
 </div>

 <!-- /.container-fluid -->
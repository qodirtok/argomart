<div class="container-fluid">

	<!-- Page Heading -->
	<?php if ($this->session->flashdata('message')) : ?>
		<div class="flash-data" data-flashdata="<?= $this->session->flashdata('message'); ?>"></div>
	<?php endif; ?>
	<div>
		<!-- <h1 class="h3 mb-4 text-gray-800">Ini tampilan User </h1> -->
		<div class="row">

			<!-- Earnings (Monthly) Card Example -->
			<div class="col-xl-3 col-md-6 mb-4">
				<div class="card border-left-primary shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Barang Terjual (Hari Ini)</div>
								<?php foreach ($barang as $data => $bt) {
									$barter = $bt->jml;
								}
								if ($barter == 0) {
									$barangterjual = 0;
								} else {
									$barangterjual = $barter;
								}
								?>
								<div class="h5 mb-0 font-weight-bold text-gray-800"><?= $barangterjual; ?></div>
							</div>
							<div class="col-auto text text-primary">
								<i class="fas fa-shopping-cart fa-2x text-300"></i>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Earnings (Monthly) Card Example -->
			<div class="col-xl-3 col-md-6 mb-4">
				<div class="card border-left-success shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Pengeluaran (Hari Ini)</div>
								<?php
								foreach ($beli as $data => $bl) {
									$pengeluaran = $bl->harga;
								}
								foreach ($kk as $data => $ka) {
									$pengeluaran = $pengeluaran + $ka->jml;
								}
								$pg = number_format($pengeluaran, 0, ',', '.');
								?>
								<div class="h5 mb-0 font-weight-bold text-gray-800">Rp. <?= $pg; ?></div>
							</div>
							<div class="col-auto text text-success">
								<i class="fas fa-dollar-sign fa-2x text-300"></i>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Earnings (Monthly) Card Example -->
			<div class="col-xl-3 col-md-6 mb-4">
				<div class="card border-left-info shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<div class="text-xs font-weight-bold text-info text-uppercase mb-1">Keuntungan (Hari Ini)</div>
								<?php
								foreach ($pj as $data) {
									$keuntungan = $data->K - $data->D;
								}
								foreach ($hp as $data) {
									$keuntungan = $keuntungan - ($data->D - $data->K);
								}
								$ktgn = number_format($keuntungan, 0, ',', '.');
								?>
								<div class="h5 mb-0 font-weight-bold text-gray-800">Rp. <?= $ktgn; ?></div>
							</div>
							<div class="col-auto text text-info">
								<i class="fas fa-clipboard-list fa-2x text-300"></i>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Pending Requests Card Example -->
			<div class="col-xl-3 col-md-6 mb-4">
				<div class="card border-left-warning shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pendapatan (Hari Ini)</div>
								<?php
								foreach ($jual as $data) {
									$pendapatan = $data->K - $data->D;
								}
								foreach ($km as $data => $ks) {
									$pendapatan = $pendapatan + $ks->jml;
								}
								$pn = number_format($pendapatan, 0, ',', '.');
								?>
								<div class="h5 mb-0 font-weight-bold text-gray-800">Rp. <?= $pn; ?></div>
							</div>
							<div class="col-auto text text-warning">
								<i class="fas fa-comments-dollar fa-2x text-300"></i>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Stok barang tersedia -->
			<div class="col-xl-4 col-md-6 mb-4">
				<div class="card border-left-success shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<a href="<?= base_url('user/barangtersedia'); ?>">
									<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Barang Tersedia
									</div>
								</a>
								<?php
								foreach ($tersedia as $data => $sedia) {
									$tersedia = $sedia->jumlah;
								}
								?>
								<div class="h5 mb-0 font-weight-bold text-gray-800"><?= $tersedia; ?></div>
							</div>
							<div class="col-auto text text-success">
								<i class="fas fa-boxes fa-2x text-300"></i>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xl-4 col-md-6 mb-4">
				<div class="card border-left-warning shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<a href="<?= base_url('user/hampirhabis'); ?>">
									<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Jumlah Barang Hampir Habis
									</div>
								</a>
								<?php
								foreach ($bhh as $data => $value) {
									$hh = $value->jumlah;
								}
								?>
								<div class="h5 mb-0 font-weight-bold text-gray-800"><?= $hh; ?></div>
							</div>
							<div class="col-auto text text-warning">
								<!-- <i class="fas fa-exclamation-triangle"></i> -->
								<i class="fas fa-exclamation-triangle fa-2x text-300"></i>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xl-4 col-md-6 mb-4">
				<div class="card border-left-danger shadow h-100 py-2">
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<a href="<?= base_url('user/habis'); ?>">
									<div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Jumlah Barang Habis
									</div>
								</a>
								<?php
								foreach ($bhs as $data => $values) {
									$bhss = $values->jumlah;
								}
								?>
								<div class="h5 mb-0 font-weight-bold text-gray-800"><?= $bhss; ?></div>
							</div>
							<div class="col-auto text text-danger">
								<!-- <i class="fas fa-exclamation-circle"></i> -->
								<i class="fas fa-exclamation-circle fa-2x text-300"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end stok -->

			<!-- Area Chart -->
			<div class="col-xl-12 col-lg-7">

				<div class="card shadow mb-4">
					<div class="card-header py-3">
						<h6 class="m-0 font-weight-bold text-primary">Grafik Pendapatan & Keuntungan Penjualan (Perbulan)</h6>
					</div>
					<div class="card-body">
						<div class="col-md-15">
							<div id="graph"></div>
							<!-- <canvas id="myAreaChart"></canvas> -->
							<!-- <canvas id="canvasc"></canvas> -->
						</div>
					</div>
				</div>
			</div>
			<!-- Content Row -->
		</div>
	</div>
</div>

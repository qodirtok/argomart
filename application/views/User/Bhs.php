 <!-- Begin Page Content -->
 <div class="container-fluid">

     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <a href="<?= base_url('User'); ?>" class="btn badge">
                 <h6 class="m-0 font-weight-bold text-primary"> <i class="fas fa-arrow-left"></i>&nbsp;Jumlah Barang Habis</h6>
             </a>
         </div>
         <div class="card-body">
             <div class="text text-danger mb-2">* Bila Stok Tinggal Sedikit Mohon Melaporkan Kebagian Gudang</div>
             <div class="table-responsive">
                 <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                         <tr class="text-center">
                             <th>No</th>
                             <th>Kode Barang</th>
                             <th>Nama Barang</th>
                             <th>Stok / Satuan</th>
                             <th>Harga Beli</th>
                             <th>Harga Jual
                                 <hr>
                                 Harga Grosir
                             </th>
                             <th>Katagori</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php foreach ($bhs as $row => $value) : ?>
                             <?php $number = 1; ?>
                             <tr>
                                 <td class="text-center">
                                     <?= ($row + 1) ?>
                                 </td>
                                 <td>
                                     <?= $value->Kode_barang ?>
                                 </td>
                                 <td>
                                     <?= $value->nama ?>
                                 </td>
                                 <td class="text-center">
                                     <?= $value->stok ?> / <?= $value->nma_satuan ?>
                                 </td>
                                 <td class="text-center">
                                     Rp. <?= number_format($value->hrg_beli); ?>
                                 </td>
                                 <td class="text-center">
                                     Rp. <?= number_format($value->hrg_jual); ?>
                                     <hr>
                                     Rp. <?= number_format($value->hrg_jual_grosir); ?>
                                 </td>
                                 <td class="text-center">
                                     <?= $value->nma_katagori ?>
                                 </td>

                             </tr>
                         <?php endforeach; ?>
                     </tbody>
                 </table>
                 <!-- <hr>
                 <a href="<?= base_url('kirimbrg'); ?>" onclick="window.open('<?= base_url('kirimbrg/cetak/') . $value->no_faktur; ?>');" class="btn btn-outline-success float-right">Cetak</a> -->
             </div>
         </div>
     </div>
 </div> <!-- /.container-fluid -->

 </div>
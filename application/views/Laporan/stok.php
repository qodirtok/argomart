<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="card bg-info text-white shadow" style="">
        <div class="card-body" style="">
            <center>
                <marquee behavior="" direction="">
                    --- MIDDOS MARKET ---
                </marquee>
            </center>
            <!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
        </div>
    </div>
    <hr>

    <div class="card shadow mb-4 col-md-6 mx-auto">
        <div class=" card-header mb-4 ">
            <h6 class="m-0 font-weight-bold text-primary">Laporan Stok</h6>
        </div>
        <div class="card-body mb-4 m-auto">
            <form action="<?= base_url('lporstok'), "/stok"; ?>" target="_blank" method="post">
                <div class="input-daterange input-group" id="datepicker">
                    <input type="text" class="input-sm form-control" name="awal" placeholder="Tanggal Awal" required />
                    <div class="input-group-prepend">
                        <span class="input-group-text">s.d</span>
                    </div>
                    <input type="text" class="input-sm form-control" name="akhir" placeholder="Tanggal Akhir" required />
                </div>

                <div class="form-group mt-4">
                    <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-print"></i> Cetak</button>
                </div>
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
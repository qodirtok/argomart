<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="card bg-info text-white shadow" style="">
        <div class="card-body" style="">
            <center>
                <marquee behavior="" direction="">
                    --- MIDDOS MARKET ---
                </marquee>
            </center>
            <!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
        </div>
    </div>
    <hr>

    <div class="card shadow mb-4 col-md-5 mx-auto">
        <div class=" card-header mb-4 ">
            <h6 class="m-0 font-weight-bold text-primary"> Laporan Kasir</h6>
        </div>
        <div class="card-body mb-4 m-auto">
            <form action="<?= base_url('laporankasir'), "/cetak"; ?>" target="_blank" method="post">
                <?php
                if ($this->session->userdata('id_lvl') == '1') {
                    ?>
                    <div class="input-group">
                        <select class="form-control selectpicker show-tick" data-live-search="true" title="Pilih User" name="user" id="user" required>
                            <?php
                                $this->db->where('id_user!=', 9);
                                $us = $this->db->get('tb_user')->result();
                                foreach ($us as $data) {                          ?>
                                <option value="<?= $data->id_user; ?>"><?= $data->nama; ?></option>
                            <?php } ?>

                        </select>
                    </div>
                    <br>
                <?php } ?>
                <div class="input-daterange input-group" id="datepicker">
                    <input type="text" class="input-sm form-control" name="tanggal" placeholder="Tanggal" required />
                </div>

                <div class="form-group mt-4">
                    <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-print"></i> Cetak</button>
                </div>
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<div class="container-fluid">

	<!-- Page Heading -->
	<?= $this->session->flashdata('message'); ?>
	<!-- <h1 class="h3 mb-4 text-gray-800">Ini tampilan User </h1> -->
	<div class="row">
		<div class="col-xl-4 mx-auto ">
			<div class="card shadow mb-4">
				<div class="card-header py-3">
					<h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-info-circle"></i> Tentang</h6>
				</div>
				<div class="card-body">
					<center>
						<table>
							<tr>
								<th>Lisensi</th>
								<th>:</th>
								<td>Agromart</td>
							</tr>
							<tr>
								<th>Version</th>
								<th>:</th>
								<td>1.0 Beta</td>
							</tr>

							<tr>
								<th>Copyright</th>
								<th>:</th>
								<td>Insan Media</td>
							</tr>
						</table>
					</center>
 			</div>
			</div>
		</div>
		<!-- Content Row -->
	</div>
</div>
</div>
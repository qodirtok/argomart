<!-- Begin Page Content -->
<div class="container-fluid">


	<div class="card bg-info text-white shadow" style="">
		<div class="card-body" style="">
			<center>
				<marquee behavior="" direction="">
					--- MIDDOS MARKET ---
				</marquee>
			</center>
			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
		</div>
	</div>
	<hr>


	<div class="card shadow mb-4 col-sm-6 mx-auto">
		<div class="card-header mb-4 ">
			<h6 class="m-0 font-weight-bold text-primary"> Form input Barang Titipan</h6>
		</div>
		<div class="card-body mb-4">
			<?php if ($this->session->flashdata('success')) : ?>
				<?php echo $this->session->flashdata('success'); ?>
			<?php endif; ?>
			<?php if ($this->session->flashdata('failed')) : ?>
				<?php echo $this->session->flashdata('failed'); ?>
			<?php endif; ?>
			<form action="<?= base_url('Barangtitipan'), "/store"; ?>" method="post" enctype="multipart/form-data">
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="Kode_barang">Kode Barang</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control <?= form_error('Kode_barang') ? 'is-invalid' : '' ?>" name="Kode_barangtitip" value="<?= $kode; ?>" placeholder="Kode Barang" readonly/>
						<div class="invalid-feedback">
							<?= form_error('Kode_barang') ?>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="nama">Nama barang</label>
					</div>
					<div class="col-lg-9">
						<input class="form-control <?= form_error('nama') ? 'is-invalid' : '' ?>" type="text" name="nama_barang" placeholder="Nama Barang" />
						<div class="invalid-feedback">
							<?= form_error('nama') ?>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="hrg_perbiji">Harga Perbiji</label>
					</div>
					<div class="col-lg-9">
						<input class=" form-control <?= form_error('harga_perbiji') ? 'is-invalid' : '' ?>" type="text" id="rupiah" name="harga_perbiji" placeholder="Harga Perbiji" />
						<div class="invalid-feedback">
							<?= form_error('harga_perbiji') ?>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="Total">Total</label>
					</div>
					<div class="col-lg-9">
						<input class="form-control <?= form_error('total_jual') ? 'is-invalid' : '' ?>" type="text" id="uang" name="total_jual" placeholder="Total" />
						<div class="invalid-feedback">
							<?= form_error('total_jual') ?>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="stok">Stok</label>
					</div>
					<div class="col-lg-9">
						<input class="form-control <?= form_error('stok') ? 'is-invalid' : '' ?>" type="number" name="stok" placeholder="Stok" />
						<div class="invalid-feedback">
							<?= form_error('stok') ?>
						</div>
					</div>
				</div>

				<br>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">Submit</button>
					<a href="<?= base_url('Barangtitipan') ?>" class="btn btn-success btn-block">Cancel</a>
				</div>
			</form>
		</div>
	</div>

</div>
<!-- /.container-fluid -->

</div>

 <!-- Begin Page Content -->
 <div class="container-fluid">
 	<!-- <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div> -->

 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
 	<div class="card bg-info text-white shadow" style="">
 		<div class="card-body" style="">
 			<center>
 				<marquee behavior="" direction="">
 					--- halaman ini menampilkan list data barang titipan ---
 				</marquee>
 			</center>
 			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
 		</div>
 	</div>
 	<hr>
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Data Barang titipan</h6>
 		</div>

 		<div class="card-body">
 			<div>
 				<?php if ($this->session->flashdata('success')) : ?>
 					<?php echo $this->session->flashdata('success'); ?>
 				<?php endif; ?>
 				<?php if ($this->session->flashdata('failed')) : ?>
 					<?php echo $this->session->flashdata('failed'); ?>
 				<?php endif; ?>
 			</div>
 			<div>
 				<a href="<?= base_url('Barangtitipan'), "/add"; ?>" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Input Barang Titip</a>
 			</div>
 			<hr>
 			<div class="table-responsive">
 				<table style="text-align:center;font-size:14px;" class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr>
 							<th>No</th>
 							<th>Kode Barang</th>
 							<th>Nama Barang</th>
 							<th>Harga perbiji</th>
 							<th>Total Jual</th>
 							<th>stok</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php foreach ($barang as $row => $value) : ?>
 							<?php $number = 1; ?>
 							<tr>
 								<td>
 									<?= ($row + 1) ?>
 								</td>
 								<td>
 									<?= $value->Kode_barangtitip ?>
 								</td>
 								<td><?= $value->nama_barang ?></td>
 								<td><?= "Rp." . number_format($value->hrg_perbiji) ?></td>
 								<td>
 								<?= "Rp." . number_format($value->total_jual) ?>
 								</td>
 								<td>
 									<?= $value->stok_barangtitip ?>
 								</td>
 								<td>
 									<!-- <a href="<?= site_url('barang') ?>/show/<?= $value->id_barang ?>" class="badge badge-warning "><span class="fa fa-eye"> detail</span></a> -->
 									<!-- <a href="<?= site_url('barang') ?>/edit/<?= $value->id_barang ?>" class="badge badge-info"><span class="fa fa-edit"> edit barang</span></a> -->
 									<a onclick="editBarang<?= ($row + 1) ?>('<?= site_url('Barangtitipan') ?>/update/<?= $value->id_barangtitip ?>')" href="#!" class="badge badge-info tombol-edit"><span class="fa fa-edit"> Edit barang</span></a>
 									<div class="modal fade" id="editbarang<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 										<div class="modal-dialog" role="document">
 											<div class="modal-content">
 												<div class="modal-header">
 													<h5 class="modal-title" id="exampleModalLabel">Edit Barang</h5>
 													<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 														<span aria-hidden="true">×</span>
 													</button>
 												</div>
 												<div class="modal-body">
 													<form action="<?= site_url('Barangtitipan/update/' . $value->id_barangtitip) ?>" method="post" enctype="multipart/form-data">
 														<input type="hidden" name="id_barangtitip" value="<?= $value->id_barangtitip ?>">
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="Kode_barang">Kode Barang</label>
 															</div>
 															<div class="col-lg-9">
 																<input type="text" class="form-control <?= form_error('Kode_barang') ? 'is-invalid' : '' ?>" name="Kode_barangtitip" placeholder="Kode Barang" value="<?= $value->Kode_barangtitip ?>" readonly>
 																<div class="invalid-feedback">
 																	<?= form_error('Kode_barang') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="nama">Nama barang</label>
 															</div>
 															<div class="col-lg-9">
 																<input class="form-control <?= form_error('nama') ? 'is-invalid' : '' ?>" type="text" name="nama_barang" placeholder="Nama Barang" value="<?= $value->nama_barang ?>" />
 																<div class="invalid-feedback">
 																	<?= form_error('nama') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="hrg_beli">Harga Per/Biji</label>
 															</div>
 															<div class="col-lg-9">
 																<input class="form-control <?= form_error('hrg_beli') ? 'is-invalid' : '' ?>" type="text" id="rupiah" name="harga_perbiji" placeholder="Harga Per/Biji" value="<?= $value->hrg_perbiji ?>" />
 																<div class="invalid-feedback">
 																	<?= form_error('hrg_beli') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="total">Total</label>
 															</div>
 															<div class="col-lg-9">
 																<input class="form-control <?= form_error('total') ? 'is-invalid' : '' ?>" type="text" id="uang" name="total_jual" placeholder="Total" value="<?= $value->total_jual ?>" />
 																<div class="invalid-feedback">
 																	<?= form_error('total') ?>
 																</div>
 															</div>
 														</div>
 														<div class="form-group row">
 															<div class="col-sm-3 mb-3 mb-sm-0">
 																<label for="stok_barangtitip">Stok</label>
 															</div>
 															<div class="col-lg-9">
 																<input class="form-control <?= form_error('stok_barangtitip') ? 'is-invalid' : '' ?>" type="text" id="ecer" name="stok" placeholder="Stok Barang" value="<?= $value->stok_barangtitip ?>" />
 																<div class="invalid-feedback">
 																	<?= form_error('stok_barangtitip') ?>
 																</div>
 															</div>
 														</div>
 														<br>
 														<div class="form-group ">
 															<button type="submit" class="btn btn-block btn-primary btn-user">Submit</button>
 														</div>
 													</form>
 												</div>
 											</div>
 										</div>
 									</div>

 									<script>
 										function editBarang<?= ($row + 1) ?>(url) {
 											$('#editbarang<?= ($row + 1) ?>').modal();
 										}
 									</script>


 									<a onclick="deleteConfirm<?= ($row + 1) ?>('<?= site_url('Barangtitipan/destroy/' . $value->id_barangtitip) ?>')" href="#!" class="badge badge-danger"><span class="fa fa-trash"> Hapus</span></a>

 									<!-- Logout Delete Confirmation-->
 									<div class="modal fade" id="deleteModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 										<div class="modal-dialog" role="document">
 											<div class="modal-content">
 												<div class="modal-header">
 													<h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ?</h5>
 													<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 														<span aria-hidden="true">×</span>
 													</button>
 												</div>
 												<div class="modal-body">
 													<p>Hapus pada barang <?= $value->nama_barang ?> ?</p>
 													Data yang dihapus tidak akan bisa dikembalikan.
 												</div>
 												<div class="modal-footer">
 													<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
 													<a id="btn-delete<?= ($row + 1) ?>" class="btn btn-danger" href="#">Delete</a>
 												</div>
 											</div>
 										</div>
 									</div>
 									<script>
 										function deleteConfirm<?= ($row + 1) ?>(url) {
 											$('#btn-delete<?= ($row + 1) ?>').attr('href', url);
 											$('#deleteModal<?= ($row + 1) ?>').modal();
 										}
 									</script>
 								</td>
 							</tr> <?php endforeach; ?>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>
 </div> <!-- /.container-fluid -->

 </div>
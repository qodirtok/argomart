 <!-- Begin Page Content -->
 <div class="container-fluid">
 	<!-- <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div> -->
 	<!-- <?php
			if ($this->session->flashdata('flash')) {
				# code...
				$this->session->flashdata('flash');
			}
			?> -->
 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 	<hr> -->
 	<div class="card bg-info text-white shadow" style="">
 		<div class="card-body" style="">
 			<center>
 				<marquee behavior="" direction="">
 					--- halaman ini menampilkan list data supplier ---
 				</marquee>
 			</center>
 			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
 		</div>
 	</div>
 	<hr>
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Data Supplier</h6>
 		</div>
 		<div class="card-body">
 			<?= $this->session->flashdata('msg'); ?>
 			<div>
 				<a href="<?= base_url('supplier'), "/add"; ?>" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Tambah Supplier</a>
 			</div>
 			<hr>
 			<div class="table-responsive">
 				<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr>
 							<th>No</th>
 							<th>Kode Supplier</th>
 							<th>Nama Supplier</th>
 							<th>Nomor Handphone</th>
 							<th>Alamat</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php foreach ($supplier as $row => $value) : ?>
 						<?php $number = 1; ?>
 						<tr>
 							<td>
 								<?= ($row + 1) ?>
 							</td>
 							<td>
 								<?= $value->kode_supplier ?>
 							</td>
 							<td>
 								<?= $value->nma_supplier ?>
 							</td>
 							<td>
 								<?= $value->notlpn ?>
 							</td>
 							<td>
 								<?= $value->alamat ?>
 							</td>
 							<td>
 								<a onclick="editsup<?= ($row + 1) ?>('<?= site_url('supplier') ?>/update/<?= $value->id_supplier ?>')" href="#!" class="badge badge-info tombol-edit"><span class="fa fa-edit"> Edit barang</span></a>
 								<div class="modal fade" id="editsup<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 									<div class="modal-dialog" role="document">
 										<div class="modal-content">
 											<div class="modal-header">
 												<h5 class="modal-title" id="exampleModalLabel">Edit Supplier</h5>
 												<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 													<span aria-hidden="true">×</span>
 												</button>
 											</div>
 											<div class="modal-body">
 												<form action="<?= site_url('supplier/update/' . $value->id_supplier) ?>" method="post">
 													<input type="hidden" name="id_supplier" value="<?= $value->id_supplier ?>">
 													<div class="form-group row">
 														<div class="col-sm-3 mb-3 mb-sm-0">
 															<label for="Kode_supplier">Kode Supplier</label>
 														</div>
 														<div class="col-lg-9">
 															<input type="text" class="form-control form-control-user" id="Kode_supplier" name="Kode_supplier" placeholder="<?= $value->kode_supplier ?>" value="<?= $value->kode_supplier; ?>" readonly>
 															<?= form_error('Kode_supplier', ' <small class="text-danger pl-3">', '</small>'); ?>
 														</div>
 													</div>
 													<div class="form-group row">
 														<div class="col-sm-3 mb-3 mb-sm-0">
 															<label for="Nama">Nama</label>
 														</div>
 														<div class="col-lg-9">
 															<input type="text" class="form-control form-control-user" id="Nama" name="Nama" placeholder="<?= $value->nma_supplier ?>" value="<?= $value->nma_supplier ?>">
 															<?= form_error('Nama', ' <small class="text-danger pl-3">', '</small>'); ?>
 														</div>
 													</div>
 													<div class="form-group row">
 														<div class="col-sm-3 mb-3 mb-sm-0">
 															<label for="Nohp">No. Handphone</label>
 														</div>
 														<div class="col-lg-9">
 															<input type="text" class="form-control form-control-user" id="Nohp" name="Nohp" placeholder="<?= $value->notlpn ?>" value="<?= $value->notlpn ?>">
 															<?= form_error('Nohp', ' <small class="text-danger pl-3">', '</small>'); ?>
 														</div>
 													</div>
 													<div class="form-group row">
 														<div class="col-sm-3 mb-3 mb-sm-0">
 															<label for="Cp">Contact Person</label>
 														</div>
 														<div class="col-lg-9">
 															<input type="text" class="form-control form-control-user" id="Cp" name="Cp" placeholder="<?= $value->cperson ?>" value="<?= $value->cperson ?>">
 															<?= form_error('Cp', ' <small class="text-danger pl-3">', '</small>'); ?>
 														</div>
 													</div>
 													<div class="form-group row">
 														<div class="col-sm-3 mb-3 mb-sm-0">
 															<label for="alamat">Alamat</label>
 														</div>
 														<div class="col-lg-9">
 															<textarea class="form-control form-control-user" id="Alamat" name="Alamat" placeholder="<?= $value->alamat ?>" value="<?= $value->alamat ?>"><?= $value->alamat ?></textarea><?= form_error('Alamat', ' <small class="text-danger pl-3">', '</small>'); ?>
 														</div>
 													</div>
 													<br>
 													<div class="form-group ">
 														<button type="submit" class="btn btn-block btn-primary">Submit</button>
 													</div>
 												</form>
 											</div>
 										</div>
 									</div>
 								</div>
 								<script>
 									function editsup<?= ($row + 1) ?>(url) {
 										$('#editsup<?= ($row + 1) ?>').modal();
 									}
 								</script>
 								<a onclick="deleteConfirm<?= ($row + 1) ?>('<?= site_url('supplier/destroy/' . $value->id_supplier) ?>')" href="#!" class="badge badge-danger"><span class="fa fa-trash"> Hapus</span></a>

 								<!-- Logout Delete Confirmation-->
 								<div class="modal fade" id="deleteModal<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 									<div class="modal-dialog" role="document">
 										<div class="modal-content">
 											<div class="modal-header">
 												<h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ?</h5>
 												<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 													<span aria-hidden="true">×</span>
 												</button>
 											</div>
 											<div class="modal-body">
 												<p>Hapus pada Nama Supplier <?= $value->nma_supplier ?> ?</p>
 												Data yang dihapus tidak akan bisa dikembalikan.
 											</div>
 											<div class="modal-footer">
 												<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
 												<a id="btn-delete<?= ($row + 1) ?>" class="btn btn-danger" href="#">Delete</a>
 											</div>
 										</div>
 									</div>
 								</div>
 								<script>
 									function deleteConfirm<?= ($row + 1) ?>(url) {
 										$('#btn-delete<?= ($row + 1) ?>').attr('href', url);
 										$('#deleteModal<?= ($row + 1) ?>').modal();
 									}
 								</script>
 							</td>
 						</tr>
 						<?php endforeach; ?>
 					</tbody>

 					<tbody>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>

 </div>
 <!-- /.container-fluid -->

 </div>
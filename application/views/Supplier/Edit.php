<!-- Begin Page Content -->
<div class="container-fluid">

	<div class="card bg-info text-white shadow" style="">
		<div class="card-body" style="">
			<center>
				<marquee behavior="" direction="">
					--- MIDDOS MARKET ---
				</marquee>
			</center>
			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
		</div>
	</div>
	<hr>

	<div class="card shadow mb-4">
		<div class=" card-header mb-4 ">
			<h6 class="m-0 font-weight-bold text-primary"> Form pendaftaran Supplier Baru</h6>
		</div>
		<div class="card-body mb-4">
			<form action="<?= base_url('supplier'); ?>/update/<?= $getById->id_supplier; ?>" method="post">
				<input type="hidden" id=id_supplier"" name="id_supplier" value="<?= $getById->id_supplier ?>">
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="Kode_supplier">Kode Supplier</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="Kode_supplier" name="Kode_supplier" placeholder="<?= $getById->kode_supplier ?>" value="<?= $getById->kode_supplier; ?>" readonly>
						<?= form_error('Kode_supplier', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="Nama">Nama</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="Nama" name="Nama" placeholder="<?= $getById->nma_supplier ?>" value="<?= $getById->nma_supplier ?>">
						<?= form_error('Nama', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="Nohp">No. Handphone</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="Nohp" name="Nohp" placeholder="<?= $getById->notlpn ?>" value="<?= $getById->notlpn ?>">
						<?= form_error('Nohp', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="Cp">Contact Person</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="Cp" name="Cp" placeholder="<?= $getById->cperson ?>" value="<?= $getById->cperson ?>">
						<?= form_error('Cp', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="alamat">Alamat</label>
					</div>
					<div class="col-lg-9">
						<textarea class="form-control form-control-user" id="Alamat" name="Alamat" placeholder="<?= $getById->alamat ?>" value="<?= $getById->alamat ?>"><?= $getById->alamat ?></textarea><?= form_error('Alamat', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group col-lg-9">
					<button type="submit" class="btn btn-primary btn-user">Submit</button>
				</div>
			</form>
		</div>
	</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- Begin Page Content -->
<div class="container-fluid">

	<div class="card bg-info text-white shadow" style="">
		<div class="card-body" style="">
			<center>
				<marquee behavior="" direction="">
					--- Agromart ---
				</marquee>
			</center>
			<!-- <div class="text-white-50 small" style="">#1cc88a</div> -->
		</div>
	</div>
	<hr>

	<div class="card shadow mb-4 col-sm-6 mx-auto">
		<div class=" card-header mb-4 ">
			<h6 class="m-0 font-weight-bold text-primary"> Form pendaftaran Supplier Baru</h6>
		</div>
		<div class="card-body mb-4">
			<form action="<?= base_url('supplier'), "/store"; ?>" method="post">
				<?= $this->session->flashdata('msg'); ?>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="Kode_supplier">Kode Supplier</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="Kode_supplier" name="Kode_supplier" placeholder="<?= $kodesupplier; ?>" value="<?= $kodesupplier ?>" readonly>
						<?= form_error('Kode_supplier', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="nama">Nama Lengkap</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="Nama" name="Nama" placeholder="Full Name" value="<?= set_value('Nama'); ?>">
						<?= form_error('Nama', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="Nohp">No. Handphone</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="Nohp" name="Nohp" placeholder="No Handphone" value="<?= set_value('Nohp'); ?>">
						<?= form_error('Nohp', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="cp">Contact Person</label>
					</div>
					<div class="col-lg-9">
						<input type="text" class="form-control form-control-user" id="Cp" name="Cp" placeholder="Contact Person" value="<?= set_value('Cp'); ?>">
						<?= form_error('Cp', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<label for="alamat">Alamat</label>
					</div>
					<div class="col-lg-9">
						<textarea class="form-control form-control-user" id="Alamat" name="Alamat" placeholder="Alamat" value="<?= set_value('Alamat'); ?>"></textarea><?= form_error('Alamat', ' <small class="text-danger pl-3">', '</small>'); ?>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block"><i class="fas fa-save"></i> Simpan</button>
					<a href="<?= base_url('supplier') ?>" class="btn btn-success btn-block"><i class="fas fa-arrow-left"></i> Kembali</a>
				</div>
			</form>
		</div>
	</div>

</div>
<!-- /.container-fluid -->

</div>
 <!-- Begin Page Content -->

 <div class="container-fluid">
 	<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
 	<?php if ($this->session->flashdata('success')) : ?>
 	<div class="alert alert-success" role="alert">
 		<?php echo $this->session->flashdata('success'); ?>
 	</div>
 	<?php endif; ?>
 	<?php if ($this->session->flashdata('failed')) : ?>
 	<div class="alert alert-danger" role="alert">
 		<?php echo $this->session->flashdata('failed'); ?>
 	</div>
 	<?php endif; ?>
 	<!-- Page Heading -->
 	<!-- <h1 class="h3 mb-2 text-gray-800"></h1>
 		<hr> -->
 	<!-- DataTales Example -->
 	<div class="card shadow mb-4">
 		<div class="card-header py-3">
 			<h6 class="m-0 font-weight-bold text-primary">Kas Keluar</h6>
 		</div>
 		<div class="card-body">
 			<center><?= $this->session->flashdata('msg'); ?></center>
 			<?= $this->session->flashdata('message'); ?>
 			<div class="table-responsive">
 				<a onclick="tambahData('<?= base_url('kaskeluar/store'); ?>')" href="#!" class="btn btn-primary"><span><i class="fa fa-plus"></i></span> Tambah Data</a>

 				<div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 					<div class="modal-dialog" role="document">
 						<div class="modal-content">
 							<div class="modal-header">
 								<h5 class="modal-title" id="exampleModalLabel">Kas Keluar</h5>
 								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 									<span aria-hidden="true">×</span>
 								</button>
 							</div>
 							<div class="modal-body">
 								<form action="<?= base_url('kaskeluar'), "/store"; ?>" method="post" enctype="multipart/form-data">

 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="akun">Kode Akun</label>
 										</div>
 										<div class="col-lg-8">
 											<input type="hidden" name="id_user" value="<?= $user['id_user']; ?>">
 											<input type="hidden" name="bukti" value="<?= $bukti; ?>">
 											<select class="form-control selectpicker show-tick" data-live-search="true" title="Pilih Akun" name="akun" id="akun" required>
 												<?php foreach ($akun as $a) { ?>
 												<option value="<?= $a->kode_akun; ?> "><?= $a->kode_akun . ' - ' . $a->nama_akun; ?></option>
 												<?php } ?>


 											</select>
 											<div class="invalid-feedback">
 												<?= form_error('akun') ?>
 											</div>
 										</div>
 									</div>
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="jumlah">Jumlah (Rp)</label>
 										</div>
 										<div class="col-lg-8">
 											<input class="form-control <?= form_error('jumlah') ? 'is-invalid' : '' ?>" type="text" id="rupiah" name="jumlah" placeholder="" />
 											<div class="invalid-feedback">
 												<?= form_error('jumlah') ?>
 											</div>
 										</div>
 									</div>
 									<div class="form-group row">
 										<div class="col-lg-4 mb-3 mb-sm-0">
 											<label for="ket">Keterangan</label>
 										</div>
 										<div class="col-lg-8">
 											<textarea class="form-control <?= form_error('ket') ? 'is-invalid' : '' ?>" type="text" id="ket" name="ket" placeholder=""></textarea>

 											<div class="invalid-feedback">
 												<?= form_error('ket') ?>
 											</div>
 										</div>
 									</div>
 									<br>
 									<div class="form-group">
 										<button type="submit" class="btn btn-primary btn-block">Submit</button>
 									</div>
 								</form>
 							</div>
 						</div>
 					</div>
 				</div>
 				<script>
 					function tambahData(url) {
 						$('#tambahData').modal();
 					}
 				</script>
 				<hr>

 				<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
 					<thead>
 						<tr class="text-center">
 							<th>Kode<br>Bukti</th>
 							<th>Kode<br>Akun</th>
 							<th>Nama Akun</th>
 							<th>Keterangan</th>
 							<th>Jumlah (Rp)</th>
 							<th>Aksi</th>
 						</tr>
 					</thead>
 					<tbody id="table-body">
 						<?php foreach ($view as $row => $value) : ?>

 						<tr>
 							<td><?= $value->kdbukti; ?></td>
 							<td><?= $value->kode_akun; ?></td>
 							<td><?= strtoupper($value->nama_akun); ?></td>
 							<td><?= $value->ket; ?></td>
 							<td><?= 'Rp. ' . number_format($value->jumlah, 0, ",", "."); ?></td>
 							<td>
 								<a onclick="editKK<?= ($row + 1) ?>('<?= base_url('kaskeluar/update' . $value->kdbukti) ?>')" href="#!" class="badge badge-info"><span><i class="fa fa-edit"></i></span> Edit</a>

 								<div class="modal fade" id="editKK<?= ($row + 1) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 									<div class="modal-dialog" role="document">
 										<div class="modal-content">
 											<div class="modal-header">
 												<h5 class="modal-title" id="exampleModalLabel">Edit Kas Keluar</h5>
 												<button class="close" type="button" data-dismiss="modal" aria-label="Close">
 													<span aria-hidden="true">×</span>
 												</button>
 											</div>
 											<div class="modal-body">
 												<form action="<?= base_url('kaskeluar/update/' . $value->kdbukti) ?>" method="post" enctype="multipart/form-data">
 													<input type="hidden" name="bukti" value="<?= $value->kdbukti ?>">
 													<input type="hidden" name="tanggal" value="<?= $value->tanggal ?>">
 													<input type="hidden" name="ak" value="<?= $value->kode_akun ?>">
 													<div class="form-group row">
 														<div class="col-lg-4 mb-3 mb-sm-0">
 															<label for="akun">Kode Akun</label>
 														</div>
 														<div class="col-lg-8">
 															<select class="form-control selectpicker show-tick" data-live-search="true" title="Pilih Akun" name="akun" id="akun" required>

 																<?php foreach ($akun as $a) { ?>

 																<option <?php if ($value->kode_akun == $a->kode_akun) {
																						echo 'selected';
																					} ?> value="<?= $a->kode_akun; ?> "><?= $a->kode_akun . ' - ' . strtoupper($a->nama_akun); ?></option>
 																<?php } ?>


 															</select>
 															<div class="invalid-feedback">
 																<?= form_error('akun') ?>
 															</div>
 														</div>
 													</div>
 													<div class="form-group row">
 														<div class="col-lg-4 mb-3 mb-sm-0">
 															<label for="jumlah">Jumlah (Rp)</label>
 														</div>
 														<div class="col-lg-8">
 															<input class="form-control <?= form_error('jumlah') ? 'is-invalid' : '' ?>" type="text" id="rupiah" name="jumlah" placeholder="" value="<?= $value->jumlah; ?>">
 															<div class="invalid-feedback">
 																<?= form_error('jumlah') ?>
 															</div>
 														</div>
 													</div>
 													<div class="form-group row">
 														<div class="col-lg-4 mb-3 mb-sm-0">
 															<label for="ket">Keterangan</label>
 														</div>
 														<div class="col-lg-8">
 															<textarea class="form-control <?= form_error('ket') ? 'is-invalid' : '' ?>" type="text" id="ket" name="ket" placeholder=""><?= $value->ket; ?></textarea>
 															<div class="invalid-feedback">
 																<?= form_error('ket') ?>
 															</div>
 														</div>
 													</div>
 													<br>
 													<div class="form-group">
 														<button type="submit" class="btn btn-primary btn-block">Submit</button>
 													</div>
 												</form>
 											</div>
 										</div>
 									</div>
 								</div>
 								<script>
 									function editKK<?= ($row + 1) ?>(url) {
 										$('#editKK<?= ($row + 1) ?>').modal();
 									}
 								</script>
 							</td>
 						</tr>
 						<?php endforeach ?>
 					</tbody>

 				</table>

 			</div>
 		</div>
 	</div>

 </div>
 </div>

 <!-- /.container-fluid -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class pembelian extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_barang');
        $this->load->model('M_supplier');
        $this->load->model('M_pembelian');
        $this->load->library('form_validation');
        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Pembelian";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['sup'] = $this->M_supplier->SelectAll();
            $data['barang'] = $this->M_barang->getAll();
            $data['tgl'] = date('d-m-Y');
            $this->load->view('Template/Header', $data);
            $this->load->view('Pembelian/Index', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function get_barang()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $kobar = $this->input->post('kode_brg');
            $x['brg'] = $this->M_barang->getBykode($kobar);
            $this->load->view('Pembelian/Detail_penjualan', $x);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function add_to_cart()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $nofaktur = $this->input->post('nofaktur');
            $tgl = $this->input->post('tgl');
            $suplier = $this->input->post('suplier');
            $this->session->set_userdata('nofaktur', $nofaktur);
            $this->session->set_userdata('tglfak', $tgl);
            $this->session->set_userdata('suplier', $suplier);
            $kobar = $this->input->post('kode_brg');
            $produk = $this->M_barang->get_barang($kobar);
            $i = $produk->row_array();
            $data = array(
                // 'id'       => $i['Kode_barang'],
                'id'       => $this->input->post('kodebarang'),
                'name'     => $this->input->post('nabar'),
                'satuan'   => $this->input->post('satuan'),
                'price'    => $this->input->post('harbeli'),
                //'persenangrosir' => $this->input->post('persenangrosir'),
                'persenan' => $this->input->post('persenan'),
                'harjul'   => $this->input->post('harbeli') + (($this->input->post('persenangrosir') / 100) * $this->input->post('harbeli')),
                'harjulecer'   => $this->input->post('hrg_jual'),
                'qty'      => $this->input->post('jumlah')
            );

            $this->cart->insert($data);
            redirect('pembelian');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function remove()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $row_id = $this->uri->segment(3);
            $this->cart->update(array(
                'rowid'      => $row_id,
                'qty'     => 0
            ));
            redirect('pembelian');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function simpan_pembelian()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $nofaktur = $this->session->userdata('nofaktur');
            $tglfak   = $this->session->userdata('tglfak');
            $suplier  = $this->session->userdata('suplier');
            $tunai   = str_replace(",", "", $this->input->post('jml_uang'));
            $kembali = str_replace(",", "", $this->input->post('kembalian'));
            if (!empty($nofaktur) && !empty($tglfak) && !empty($suplier)) {
                $beli_kode = $this->M_pembelian->get_kobel();
                $order_proses = $this->M_pembelian->simpan_pembelian($nofaktur, $tglfak, $suplier, $beli_kode, $tunai, $kembali);
                if ($order_proses) {
                    $this->cart->destroy();
                    $this->session->unset_userdata('nofaktur');
                    $this->session->unset_userdata('tglfak');
                    $this->session->unset_userdata('suplier');
                    echo $this->session->set_flashdata('msg', '<div class="sufee-alert alert with-close alert-primary alert-dismissible fade show">
                    <span class="badge badge-pill badge-primary">Success</span>
                    Pembelian Berhasil di Simpan ke Database.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>');
                    redirect('pembelian');
                } else {
                    redirect('pembelian');
                }
            } else {
                echo $this->session->set_flashdata('msg', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger">Gagal</span>
                Pembelian Gagal di Simpan, Mohon Periksa Kembali Semua Inputan Anda!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>');
                redirect('pembelian');
            }
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }
}

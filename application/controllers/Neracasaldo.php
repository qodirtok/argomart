<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Neracasaldo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_neraca');
        $this->load->library('form_validation');

        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Neraca Saldo";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('Template/Header', $data);
            $this->load->view('Laporan/Nrcsaldo', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }
    function periode($date)
    {
        $str = explode('-', $date);
        $bulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Maret',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];
        return ' ' . $bulan[$str[1]] . ' ' . $str[0];
    }

    public function cetak() //untuk mencetak buku besar
    {
        if (isset($_POST['awal']) and isset($_POST['awal'])) {
            date_default_timezone_set('Asia/Jakarta');
            $awal   = $_POST['awal'];
            $akhir  = $_POST['akhir'];
            $nrcsaldo = $this->M_neraca->getNrcSaldo($awal, $akhir);
            $tgla = explode('-', $awal);
            $da = $tgla[2];
            $tglb = explode('-', $akhir);
            $db = $tglb[2];
            $periode = date($akhir);
            $bln = $this->periode($periode);

            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AddPage();
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 15);
            $pdf->Cell(0, 0.75, 'NERACA SALDO', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 11);
            $pdf->Cell(0, 0.75, 'Agromart', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Periode ' . $db . $bln, 0, 0, 'C');
            $pdf->Line(11, 35, 200, 35);


            $pdf->Ln(10);
            $pdf->Cell(1);
            $pdf->Ln(5);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(20, 8, 'KODE', 1, 0, 'C');
            $pdf->Cell(80, 8, 'NAMA AKUN', 1, 0, 'C');
            $pdf->Cell(45, 8, 'DEBET', 1, 0, 'C');
            $pdf->Cell(45, 8, 'KREDIT', 1, 0, 'C');

            $nd = 1;
            $nk = 1;

            foreach ($nrcsaldo as $data => $nrcs) {
                $pdf->Ln(8);
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(20, 8, $nrcs->kode_akun, 1, 0, 'C');
                $pdf->Cell(80, 8, $nrcs->nama_akun, 1, 0, 'L');

                if ($nrcs->jenis == "DEBET") {
                    if ($nd == 1) {
                        $nd++;
                        $debet  = $nrcs->D;
                        $sdb  = $nrcs->D - $nrcs->K;
                        $tdb  = $sdb;
                        $sd  = number_format($sdb, 0, ",", ".");
                        $pdf->Cell(45, 8, $sd . '  ', 1, 0, 'R');
                    } else {
                        $debet  = $debet + $nrcs->D;
                        $sdb  = $nrcs->D - $nrcs->K;
                        $tdb  = $tdb + $sdb;
                        $sd  = number_format($sdb, 0, ",", ".");
                        $pdf->Cell(45, 8, $sd . '  ', 1, 0, 'R');
                    }
                } else {
                    $pdf->Cell(45, 8,  '0  ', 1, 0, 'R');
                }
                if ($nrcs->jenis == "KREDIT") {
                    if ($nk == 1) {
                        $nk++;
                        $skr  = $nrcs->K - $nrcs->D;
                        $tkr  = $skr;
                        $sk  = number_format($skr, 0, ",", ".");
                        $pdf->Cell(45, 8, $sk . '  ', 1, 0, 'R');
                    } else {
                        $skr  = $nrcs->K - $nrcs->D;
                        $tkr  = $tkr + $skr;
                        $sk  = number_format($skr, 0, ",", ".");
                        $pdf->Cell(45, 8, $sk . '  ', 1, 0, 'R');
                    }
                } else {
                    $pdf->Cell(45, 8,  '0  ', 1, 0, 'R');
                }
            }

            $d  = number_format($tdb, 0, ",", ".");
            $k  = number_format($tkr, 0, ",", ".");


            $pdf->Ln(8);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(100, 8, 'TOTAL', 1, 0, 'C');
            $pdf->Cell(45, 8, $d . '  ', 1, 0, 'R');
            $pdf->Cell(45, 8, $k . '  ', 1, 0, 'R');

            if ($tdb != $tkr) {
                $pdf->ln(15);
                if ($tdb > $tkr) {
                    $pdf->Cell(0, 0.75, 'Tidak Balance Selisih :' . ($tdb - $tkr), 0, 0, 'C');
                } else {
                    $pdf->Cell(0, 0.75, 'Tidak Balance Selisih :' . ($tkr - $tdb), 0, 0, 'C');
                }
            }

            $pdf->Output("Neraca Saldo.pdf", "I");
        } else {
            $data['title'] = "Access Denied";
            $this->load->view('404_accessdenied', $data);
        }
    }
}

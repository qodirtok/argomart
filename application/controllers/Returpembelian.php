<?php
defined('BASEPATH') or exit('No direct script access allowed');

class returpembelian extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_returbeli');
        $this->load->model('M_transaksi');
        $this->load->library('form_validation');
        is_login();
    }

    function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['kirim'] = $this->M_returbeli->getAll();
            $data['title'] = 'Agromart - Retur Titipan';

            $this->load->view('template/header', $data);
            $this->load->view('Repem/index.php', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function laporan()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['kirim'] = $this->M_returbeli->getAll();
            $data['title'] = 'Agromart - Retur Titipan';

            $this->load->view('template/header', $data);
            $this->load->view('Laporan/returbeli.php', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function detail($id = null)
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['title'] = 'Agromart - Detail Retur Titipan';
            $data['judul'] = 'Detail Retur Titipan';
            $data['detail'] = $this->M_returbeli->detaill($id);

            $this->session->set_userdata('id', $id);
            $this->load->view('template/header', $data);
            $this->load->view('Repem/detail', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function store() //untuk mengirim data inputan user ke database dan hanya mengatur validasi saja selebihnya ada di model
    {
        # code...
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $repen = $this->M_returbeli;
            $validation = $this->form_validation;
            $validation->set_rules($repen->rules());

            if ($validation->run()) {
                $repen->save();
                $this->session->set_flashdata('success', 'Berhasil disimpan');
            } else {
                $this->session->set_flashdata('failed', 'gagal disimpan');
            }
            $id = $this->session->userdata('id');
            redirect('/returpembelian/detail/' . $id);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function periode($date)
    {
        $str = explode('-', $date);
        $bulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Maret',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];
        return $str[2] . ' ' . $bulan[$str[1]] . ' ' . $str[0];
    }

    public function cetak() //untuk mencetak buku besar
    {

        if (isset($_POST['awal']) and isset($_POST['awal'])) {
            date_default_timezone_set('Asia/Jakarta');
            $awal  = $_POST['awal'];
            $akhir = $_POST['akhir'];

            $retur      = $this->M_returbeli->getLaporan($awal, $akhir);

            $tgl1      = $this->periode($awal);
            $tgl2      = $this->periode($akhir);

            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AddPage();
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 15);
            $pdf->Cell(0, 0.75, 'LAPORAN PEMBAYARAN', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 11);
            $pdf->Cell(0, 0.75, 'Agromart', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 0, 0, 'C');
            $pdf->Line(11, 30, 200, 30);


            $pdf->Ln(10);
            $pdf->Cell(1);
            if ($awal == $akhir) {
                $pdf->Cell(23, 8, 'Tanggal : ' . $tgl1, 0, 0, 'L');
            } else {
                $pdf->Cell(23, 8, 'Tanggal : ' . $tgl1 . ' s.d ' . $tgl2, 0, 0, 'L');
            }
            $pdf->Ln(8);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(15, 8, 'No', 1, 0, 'C');
            $pdf->Cell(20, 8, 'Tanggal', 1, 0, 'C');
            $pdf->Cell(88, 8, 'Keterangan', 1, 0, 'C');
            $pdf->Cell(20, 8, 'Supplier', 1, 0, 'C');
            $pdf->Cell(46, 8, 'Nominal', 1, 0, 'C');
            $total = 0;
            $no = 1;
            foreach ($retur as $data => $d) {
                $total += $d->sub_total;
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(15, 8, $no++, 'LR', 0, 'C');
                $pdf->Cell(20, 8, $d->tanggal, 'LR', 0, 'C');
                $pdf->Cell(88, 8, 'Retur Pembelian Bukti ' . $d->nama . ' ' . $d->jumlah . ' x @ ' . $d->harga, 'LR', 0, 'L');
                $pdf->Cell(20, 8, strtoupper($d->nma_supplier), 'LR', 0, 'L');
                $pdf->Cell(46, 8, number_format($d->sub_total, 0, ',', '.') . ' ', 'LR', 0, 'R');
            }


            $pdf->Ln();
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(143, 8, 'TOTAL', '1', 0, 'C');
            $pdf->Cell(46, 8, number_format($total, 0, ',', '.') . ' ', '1', 0, 'R');

            $pdf->Output("Laporan Pembayaran.pdf", "I");
        } else {
            $data['title'] = "Access Denied";
            $this->load->view('404_accessdenied', $data);
        }
    }
}

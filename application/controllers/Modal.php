<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Modal extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_modal');
        $this->load->library('form_validation');
        is_login();
    }

    //menapilkan semua list data modal
    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $data['user']['id_user'];
            $data['modal'] = $this->M_modal->getAll($id);
            $data['title'] = 'Agromart - Modal Kasir';
            $this->load->view('Template/Header', $data);
            $this->load->view('Modal/View', $data);
            $this->load->view('Template/Footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function Add() //untuk menampilkan halaman form tambah modal
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            # code...
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['title'] = 'Agromart - Tambah modal';
            $this->load->view('Template/Header', $data);
            $this->load->view('Modal/Tambah.php', $data);
            $this->load->view('Template/Footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function store() //untuk mengirim data inputan user ke database dan hanya mengatur validasi saja selebihnya ada di model
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            # code...
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $modal = $this->M_modal;
            $validation = $this->form_validation;
            $validation->set_rules($modal->rules());

            if ($validation->run()) {
                $modal->save();
                $this->session->set_flashdata('success', 'Berhasil disimpan');
            } else {
                $this->session->set_flashdata('failed', 'gagal disimpan');
            }
            redirect('/Modal');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function show($id = null) //untuk menuju ke halaman view Detail berdasarkan modal yang dipilih
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            # code...
            if (!isset($id)) {
                show_404();
            } else {
                if ($this->M_modal->getById($id)) {
                    $data['kodemodal'] = $this->M_modal->createKode();
                    $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
                    $data['title'] = 'Agromart - view Data modal';
                    $this->load->view('Template/Header', $data);
                    $this->load->view('Modal/Detail.php', $data);
                    $this->load->view('Template/Footer');
                }
            }
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function edit($id = null)
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            # code...
            if (!isset($id)) {
                show_404();
            } else {
                if ($this->M_modal->getById($id)) {
                    $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
                    $data['title'] = 'Agromart - Edit Data modal';
                    $data['kodemodal'] = $this->M_modal->createKode();
                    $data['getById'] = $this->M_modal->getById($id);
                    $this->load->view('Template/Header', $data);
                    $this->load->view('Modal/Edit.php', $data);
                    $this->load->view('Template/Footer');
                }
            }
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function update($id = null)
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            # code...
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();

            if (!isset($id)) redirect('Modal');

            $modal = $this->M_modal;
            $validation = $this->form_validation;
            $validation->set_rules($modal->rules());

            if ($validation->run()) {
                $modal->M_update();
                $this->session->set_flashdata('success', 'Berhasil disimpan');
            } else {
                $this->session->set_flashdata('failed', 'gagal disimpan');
            }

            $data["tb_modal"] = $modal->getById($id);
            if (!$data["tb_modal"]) show_404();


            redirect('/Modal');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function destroy($id = null)
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            # code...
            if (!isset($id)) show_404();

            if ($this->M_modal->delete($id)) {
                # code...
                redirect('/Modal');
            }
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }
}

/* End of file modal.php */

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_diskon');
		$this->M_diskon->deleteOldDiskon();
		
	}
	

	public function index()
	{
		$data['barang'] = $this->M_diskon->getAllLanding();
		$this->load->view('Landing/Index',$data);
		
	}

}

/* End of file Landing.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporankasir extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_kasir');
        $this->load->library('form_validation');

        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Laporan Kasir";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('Template/Header', $data);
            $this->load->view('Laporan/Kasir', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function cetak() //untuk mencetak buku besar
    {

        if (isset($_POST['tanggal'])) {
            date_default_timezone_set('Asia/Jakarta');
            $tgl  = $_POST['tanggal'];
            if ($this->session->userdata('id_lvl') == '1') {
                $user  = $_POST['user'];
                $data['user'] = $this->db->get_where('tb_user', ['id_user' => $user])->row_array();
                $modal        = $this->M_kasir->getModal($user, $tgl);
                $km           = $this->M_kasir->getKM($user, $tgl);
                $kk           = $this->M_kasir->getKK($user, $tgl);
                $pu           = $this->M_kasir->getPU($user, $tgl);
                $rb           = $this->M_kasir->getRB($user, $tgl);
                $bp           = $this->M_kasir->getBP($user, $tgl);
                $penjualan    = $this->M_kasir->getPenjualan($user, $tgl);
                $pembelian    = $this->M_kasir->getPembelian($user, $tgl);
            } else {
                $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
                $modal        = $this->M_kasir->getModal($data['user']['id_user'], $tgl);
                $km           = $this->M_kasir->getKM($data['user']['id_user'], $tgl);
                $kk           = $this->M_kasir->getKK($data['user']['id_user'], $tgl);
                $rb           = $this->M_kasir->getRB($data['user']['id_user'], $tgl);
                $pu           = $this->M_kasir->getPU($data['user']['id_user'], $tgl);
                $bp           = $this->M_kasir->getBP($data['user']['id_user'], $tgl);
                $penjualan    = $this->M_kasir->getPenjualan($data['user']['id_user'], $tgl);
                $pembelian    = $this->M_kasir->getPembelian($data['user']['id_user'], $tgl);
            }

            if (!isset($total)) {
                $total = 0;
            }
            if (!isset($saldo)) {
                $saldo = 0;
            }
            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AddPage();
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 15);
            $pdf->Cell(0, 0.75, 'LAPORAN KASIR', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 11);
            $pdf->Cell(0, 0.75, 'Agromart', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Jl Bunga Desember', 0, 0, 'C');
            $pdf->Line(11, 30, 200, 30);


            $pdf->Ln(10);
            $pdf->Cell(1);
            $pdf->Cell(23, 8, 'KASIR : ' . strtoupper($data['user']['nama']), 0, 0, 'L');
            $pdf->Ln(8);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(23, 8, 'TANGGAL', 1, 0, 'C');
            $pdf->Cell(132, 8, 'KETERANGAN', 1, 0, 'C');
            $pdf->Cell(35, 8, 'SALDO', 1, 0, 'C');



            // $tgl = explode('-', $date)3
            // $y = $tgl[0];
            // $m = $tgl[1];
            // $dt = $tgl[2];
            // $dmy = $dt . '-' . $m . '-' . $y;

            foreach ($modal as $data) {

                $saldo = $data->modal;

                $total  = number_format($saldo, 0, ",", ".");
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(23, 8, $data->tanggal, 'LR', 0, 'C');
                $pdf->Cell(132, 8, 'Modal Kasir ', 'LR', 0, 'L');
                $pdf->Cell(35, 8,  $total . ' ', 'LR', 0, 'R');
            }
            foreach ($km as $data) {

                $saldo = $saldo + $data->jumlah;
                $total  = number_format($saldo, 0, ",", ".");
                $jumlah  = number_format($data->jumlah, 0, ",", ".");
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(23, 8, $data->tanggal, 'LR', 0, 'C');
                $pdf->Cell(132, 8, $data->ket . ' @ Rp. ' . $jumlah, 'LR', 0, 'L');
                $pdf->Cell(35, 8,  $total . ' ', 'LR', 0, 'R');
            }

            foreach ($kk as $data) {

                $saldo = $saldo - $data->jumlah;

                $total  = number_format($saldo, 0, ",", ".");
                $jumlah  = number_format($data->jumlah, 0, ",", ".");
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(23, 8, $data->tanggal, 'LR', 0, 'C');
                $pdf->SetFont('Arial', 'I', 10);
                $pdf->Cell(132, 8,  $data->ket . ' @ Rp. (' . $jumlah . ')', 'LR', 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(35, 8,  $total . ' ', 'LR', 0, 'R');
            }
            foreach ($rb as $data) {

                $saldo = $saldo + $data->debet;
                $total  = number_format($saldo, 0, ",", ".");
                $jumlah  = number_format($data->debet, 0, ",", ".");
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(23, 8, $data->tanggal, 'LR', 0, 'C');
                $pdf->Cell(132, 8, $data->ket . ' @ Rp. ' . $jumlah, 'LR', 0, 'L');
                $pdf->Cell(35, 8,  $total . ' ', 'LR', 0, 'R');
            }
            foreach ($penjualan as $data) {

                $saldo = $saldo + $data->sub_total;

                $total  = number_format($saldo, 0, ",", ".");
                $harga  = number_format($data->harga, 0, ",", ".");
                $jumlah  = number_format($data->sub_total, 0, ",", ".");
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(23, 8, $data->tgl_penjualan, 'LR', 0, 'C');
                $pdf->Cell(132, 8, 'Penjualan ' . $data->nama . ' ' . $data->jumlah . ' x ' . $harga . ' Rp. ' . $jumlah, 'LR', 0, 'L');
                $pdf->Cell(35, 8,  $total . ' ', 'LR', 0, 'R');
            }


            foreach ($pembelian as $data) {

                $saldo = $saldo - $data->total;

                $total  = number_format($saldo, 0, ",", ".");
                $harga  = number_format($data->harga_beli, 0, ",", ".");
                $jumlah  = number_format($data->total, 0, ",", ".");
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(23, 8, $data->tgl_beli, 'LR', 0, 'C');
                $pdf->SetFont('Arial', 'I', 10);
                $pdf->Cell(132, 8, 'Pembelian ' . $data->nama . ' ' . $data->jumlah . ' x ' . $harga . ' Rp. (' . $jumlah . ')', 'LR', 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(35, 8,  $total . ' ', 'LR', 0, 'R');
            }

            foreach ($pu as $data) {

                $saldo = $saldo + $data->kredit;

                $total  = number_format($saldo, 0, ",", ".");
                $jumlah  = number_format($data->kredit, 0, ",", ".");
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(23, 8, $data->tanggal, 'LR', 0, 'C');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(132, 8,  $data->ket . ' Rp. ' . $jumlah, 'LR', 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(35, 8,  $total . ' ', 'LR', 0, 'R');
            }
            foreach ($bp as $data) {

                $saldo = $saldo - $data->debet;

                $total  = number_format($saldo, 0, ",", ".");
                $jumlah  = number_format($data->debet, 0, ",", ".");
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(23, 8, $data->tanggal, 'LR', 0, 'C');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(132, 8,  $data->ket . ' Rp. ' . $jumlah, 'LR', 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(35, 8,  $total . ' ', 'LR', 0, 'R');
            }


            $pdf->Ln();
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(155, 8, 'TOTAL', '1', 0, 'C');
            $pdf->Cell(35, 8, $total . ' ', '1', 0, 'R');

            $pdf->Output("Laporan Kasir.pdf", "I");
        } else {
            $data['title'] = "Access Denied";
            $this->load->view('404_accessdenied', $data);
        }
    }
}

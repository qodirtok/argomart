<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_member');
        $this->load->model('M_barang');
        $this->load->model('M_kategori');
        $this->load->model('M_supplier');
        $this->load->model('M_satuan');
        $this->load->model('M_transaksi');
        $this->load->model('M_hutang');
        $this->load->model('M_akuntansi');
        $this->load->model('M_modal');
        $this->load->model('M_kirim');
        $this->load->model('M_returjual');
        $this->load->model('M_akun');
        $this->load->model('M_dashboard');
        // $this->load->model('M_returbeli');
        $this->load->library('form_validation');
        is_login();
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
        $data['title'] = 'Agromart - Dashboard';
        $data['barang'] = $this->M_dashboard->getBarang($data['user']['id_user']);
        $data['kk'] = $this->M_dashboard->getKK($data['user']['id_user']);
        $data['km'] = $this->M_dashboard->getKM($data['user']['id_user']);
        $data['beli'] = $this->M_dashboard->getPembelian($data['user']['id_user']);
        $data['jual'] = $this->M_dashboard->getJual($data['user']['id_user']);
        $data['pj'] = $this->M_dashboard->getJual($data['user']['id_user']);
        $data['hp'] = $this->M_dashboard->getHapok($data['user']['id_user']);
        $data['tersedia'] = $this->M_dashboard->barangTersedia();
        $data['bhh'] = $this->M_dashboard->bhh();
        $data['bhs'] = $this->M_dashboard->bhs();
        $report = $this->M_dashboard->grafikpenjualan();
        $data['report'] = json_encode($report);
        $this->load->view('Template/Header', $data);
        $this->load->view('User/Index.php', $data);
        $this->load->view('Template/Footer');
    }

    function barangtersedia()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['title'] = 'Agromart - Jumlah Barang Tersedia';
            $data['tersedia'] = $this->M_dashboard->bt();
            $this->load->view('Template/Header', $data);
            $this->load->view('User/Bt', $data);
            $this->load->view('Template/Footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function hampirhabis()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['title'] = 'Agromart - Jumlah Barang Tersedia';
            $data['bhh'] = $this->M_dashboard->bhhabis();
            $this->load->view('Template/Header', $data);
            $this->load->view('User/Bhh', $data);
            $this->load->view('Template/Footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }
    function habis()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['title'] = 'Agromart - Jumlah Barang Tersedia';
            $data['bhs'] = $this->M_dashboard->bhabis();
            $this->load->view('Template/Header', $data);
            $this->load->view('User/Bhs', $data);
            $this->load->view('Template/Footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function member()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['member'] = $this->M_member->getAll();
            $data['kodemember'] = $this->M_member->createKode();
            $data['title'] = 'Agromart - Member';
            $this->load->view('template/header', $data);
            $this->load->view('Member/View', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function barang()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['barang'] = $this->M_barang->getAll();
            $data['kategori'] = $this->db->get('tb_katagori')->result();
            $data['satuan'] = $this->db->get('tb_satuan')->result();
            $data['title'] = 'Agromart - Barang';
            $this->load->view('template/header', $data);
            $this->load->view('Barang/View.php', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function kategori()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['kategori'] = $this->M_kategori->getAll();
            $data['title'] = 'Agromart - Kategori';
            $this->load->view('template/header', $data);
            $this->load->view('Kategori/View.php', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function supplier()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['supplier'] = $this->M_supplier->getAll();
            $data['title'] = 'Agromart - Suplier';
            $this->load->view('template/header', $data);
            $this->load->view('Supplier/View.php', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function satuan()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['satuan'] = $this->M_satuan->getAll();
            $data['title'] = 'Agromart - Satuan';
            $this->load->view('template/header', $data);
            $this->load->view('Satuan/View.php', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function pembelian()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Argomart - Pembelian";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['sup'] = $this->M_supplier->SelectAll();
            $data['tgl'] = date('d-m-Y');
            $this->load->view('template/header', $data);
            $this->load->view('pembelian/index', $data);
            $this->load->view('template/footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function transaksi()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Argomart - Penjualan (Grosir)";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['tgl'] = date('d-m-Y');
            $data['nofaktur'] = $this->M_transaksi->createKode();
            $data['barang'] = $this->M_barang->getAll();
            $this->load->view('template/header', $data);
            $this->load->view('transaksi/index', $data);
            $this->load->view('template/footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function hutang()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['title'] = 'Argomart - Hutang';
            $data['hutang'] = $this->M_hutang->getAll();
            $this->load->view('template/header', $data);
            $this->load->view('Hutang/View', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function kaskeluar()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Argomart - Kas Keluar";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $data['user']['id_user'];
            $data['bukti']  = $this->M_akuntansi->createKode($id);
            $data['akun']  = $this->M_akuntansi->getAkun();
            $data['view']  = $this->M_akuntansi->getAll($id);
            $this->load->view('template/header', $data);
            $this->load->view('kaskeluar/index', $data);
            $this->load->view('template/footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function jurnalumum()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Argomart - Jurnal Umum";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $data['user']['id_user'];
            $data['bukti']  = $this->M_akuntansi->createKj($id);
            $data['akun']  = $this->M_akuntansi->getAkun();
            $data['debet']  = $this->M_akuntansi->totaldebet($id);
            $data['kredit']  = $this->M_akuntansi->totalkredit($id);
            $data['view']  = $this->M_akuntansi->getJurnal($id);
            $this->load->view('template/header', $data);
            $this->load->view('jurnal/index', $data);
            $this->load->view('template/footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function modal()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $data['user']['id_user'];
            $data['modal'] = $this->M_modal->getAll($id);
            $data['title'] = 'Argomart - Modal Kasir';
            $this->load->view('template/header', $data);
            $this->load->view('Modal/View', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }


    function Kirimbrg()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['kirim'] = $this->M_kirim->getAll();
            $data['kirimnow'] = $this->M_kirim->getAllharian();
            $data['title'] = 'Argomart - Kirim Barang';
            $this->load->view('template/header', $data);
            $this->load->view('Kirim/index.php', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function returpenjualan()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['repen'] = $this->M_returjual->getAll();
            $data['retur'] = $this->M_returjual->getRetur();
            $data['title'] = 'Argomart - Retur Penjualan';

            $this->load->view('template/header', $data);
            $this->load->view('Repen/index.php', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function returpembelian()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['kirim'] = $this->M_returjual->getAll();
            $data['title'] = 'Argomart - Retur Pembelian';

            $this->load->view('template/header', $data);
            $this->load->view('Repem/index.php', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function akun()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['tampil'] = $this->M_akun->GetallAkun();
            $data['title'] = 'Argomart - Data Akun';
            $this->load->view('template/header', $data);
            $this->load->view('Akun/index', $data);
            $this->load->view('template/footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }







    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('id_lvl');
        $this->session->unset_userdata('id_user');
        $this->session->sess_destroy();

        $this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
        <span class="badge badge-pill badge-success">Berhasil</span>
        Anda Berhasil Logout.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>');
        redirect('auth');
    }
}

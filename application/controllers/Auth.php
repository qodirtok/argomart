<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_akun');
		$this->load->library('form_validation');
	}
	public function index()
	{
		if ($this->session->userdata('username')) {
			redirect('User');
		}
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Argomart - Login';
			$this->load->view('Template/Auth_header', $data);
			$this->load->view('Auth/Login');
			$this->load->view('Template/Auth_footer');
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$user = $this->db->get_where('tb_user', ['username' => $username])->row_array();
		$nama = $user['nama'];
		//jika user ada

		if ($user) {
			if (password_verify($password, $user['password'])) {
				$data = [
					'id_user' => $user['id_user'],
					'username' => $user['username'],
					'nama' => $user['nama'],
					'id_lvl' => $user['id_lvl']
				];
				if ($user['id_lvl'] == '1') { //Akses superadmin
					$this->session->set_userdata($data);
					$this->session->set_flashdata('message', 'welcome');
					redirect('User');
				} else if ($user['id_lvl'] == '2') { //akses Kasir
					$this->session->set_userdata($data);
					$this->session->set_flashdata('message', 'welcome');
					redirect('User');
				} else {
					$this->session->set_userdata($data);
					$this->session->set_flashdata('message', 'welcome');
					redirect('User');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
            <span class="badge badge-pill badge-danger">Gagal!</span>
            Password Salah!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>');
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
            <span class="badge badge-pill badge-danger">Gagal!</span>
            Username Tidak Ada!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>');
			redirect('auth');
		}
	}
	function error_not_found()
	{
		$data['title'] = 'Page Not Found';
		$this->load->view('404_content', $data);
	}

	function blocked()
	{
		echo 'access blocked';
	}
}

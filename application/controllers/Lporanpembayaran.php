<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lporanpembayaran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_pembayaran');
        $this->load->library('form_validation');

        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Laporan Pembayaran";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('Template/Header', $data);
            $this->load->view('Laporan/Pembayaran', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }
    function periode($date)
    {
        $str = explode('-', $date);
        $bulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Maret',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];
        return $str[2] . ' ' . $bulan[$str[1]] . ' ' . $str[0];
    }


    public function cetak() //untuk mencetak buku besar
    {

        if (isset($_POST['awal']) and isset($_POST['awal'])) {
            date_default_timezone_set('Asia/Jakarta');
            $awal  = $_POST['awal'];
            $akhir = $_POST['akhir'];

            $cash      = $this->M_pembayaran->getPemCash($awal, $akhir);
            $kredit    = $this->M_pembayaran->getPemKredit($awal, $akhir);

            $tgl1      = $this->periode($awal);
            $tgl2      = $this->periode($akhir);

            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AddPage();
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 15);
            $pdf->Cell(0, 0.75, 'LAPORAN PEMBAYARAN', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 11);
            $pdf->Cell(0, 0.75, 'Agromart', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 0, 0, 'C');
            $pdf->Line(11, 30, 200, 30);


            $pdf->Ln(10);
            $pdf->Cell(1);
            if ($awal == $akhir) {
                $pdf->Cell(23, 8, 'Tanggal : ' . $tgl1, 0, 0, 'L');
            } else {
                $pdf->Cell(23, 8, 'Tanggal : ' . $tgl1 . ' s.d ' . $tgl2, 0, 0, 'L');
            }
            $pdf->Ln(8);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(15, 8, 'No', 1, 0, 'C');
            $pdf->Cell(20, 8, 'Tanggal', 1, 0, 'C');
            $pdf->Cell(108, 8, 'Keterangan', 1, 0, 'C');
            $pdf->Cell(46, 8, 'Nominal', 1, 0, 'C');
            $total = 0;
            $no = 1;
            foreach ($cash as $data => $d) {
                $total += $d->kredit;
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(15, 8, $no++, 'LR', 0, 'C');
                $pdf->Cell(20, 8, $d->tanggal, 'LR', 0, 'C');
                $pdf->Cell(108, 8, 'Pembayaran Pembelian Bukti ' . $d->bukti . ' Ke Supplier ' . $d->nma_supplier, 'LR', 0, 'L');
                $pdf->Cell(46, 8, number_format($d->kredit, 0, ',', '.') . ' ', 'LR', 0, 'R');
            }

            foreach ($kredit as $data => $d) {
                $total += $d->debet;
                $pdf->Ln();
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(15, 8, $no++, 'LR', 0, 'C');
                $pdf->Cell(20, 8, $d->tanggal, 'LR', 0, 'C');
                $pdf->Cell(108, 8, $d->ket . ' Bukti ' . $d->bukti, 'LR', 0, 'L');
                $pdf->Cell(46, 8, number_format($d->debet, 0, ',', '.') . ' ', 'LR', 0, 'R');
            }

            $pdf->Ln();
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(143, 8, 'TOTAL', '1', 0, 'C');
            $pdf->Cell(46, 8, number_format($total, 0, ',', '.') . ' ', '1', 0, 'R');

            $pdf->Output("Laporan Pembayaran.pdf", "I");
        } else {
            $data['title'] = "Access Denied";
            $this->load->view('404_accessdenied', $data);
        }
    }
}

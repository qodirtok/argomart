<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Akun extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_akun');
        $this->load->library('form_validation');
        is_login();
    }
    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['tampil'] = $this->M_akun->GetallAkun();
            $data['title'] = 'Agromart - Data Akun';
            $this->load->view('Template/Header', $data);
            $this->load->view('Akun/Index', $data);
            $this->load->view('Template/Footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

	private function _uploadimg()
	{
		$config['upload_path']          = './asset/img/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['overwrite']            = true;
		$config['max_size']             = 10024; // 10MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {
			return $this->upload->data("file_name");
		} else {
			// return "default.jpg";
		}

		// echo '<pre>';
		// print_r($post);
		// echo '</pre>';
	}



	public function tambah()
	{
		$config['max_size'] = 2048;
		$config['allowed_types'] = "png|jpg|jpeg|gif";
		$config['remove_spaces'] = TRUE;
		$config['overwrite'] = TRUE;
		$config['upload_path'] = './asset/img';

		$this->load->library('upload');
		$this->upload->initialize($config);

		//ambil data image
		if (!$this->upload->do_upload('image')) {
			$pict = "default.jpg";
		} else {
			$data_image = $this->upload->data('file_name');
			$pict = $data_image;
		}

        $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
        $data['title'] = 'Agromart - Tambah Data Akun';
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
        $this->form_validation->set_rules('nohp', 'Jenis Kelamin', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        $this->form_validation->set_rules('hk', 'Hak Akses', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('Template/Header', $data);
            $this->load->view('Akun/Registrasion', $data);
            $this->load->view('Template/Footer');
        } else {
            $data = [
                'nama' => htmlspecialchars($this->input->post('nama')),
                'jk' => htmlspecialchars($this->input->post('jk')),
                'nohp' => htmlspecialchars($this->input->post('nohp')),
                'alamat' => htmlspecialchars($this->input->post('alamat')),
                'username' => htmlspecialchars($this->input->post('username')),
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'image' => $pict,
                'id_lvl' => htmlspecialchars($this->input->post('hk'))
            ];
            $this->db->insert('tb_user', $data);
            $this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
			<span class="badge badge-pill badge-success">Berhasil</span>
			Berhasil Membuat Akun.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
        </div>');
			redirect('Akun');
		}
	}

	public function ubah($id = null)
	{
		$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
		$data['tampil'] = $this->M_akun->GetallAkun();
		if (!isset($id)) redirect('akun');
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
		$this->form_validation->set_rules('nohp', 'Jenis Kelamin', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', [
			'matches' => 'Password dont match!',
			'min_length' => 'Password too short!'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
		$this->form_validation->set_rules('hk', 'Hak Akses', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('Template/Header', $data);
            $this->load->view('Akun/Index', $data);
            $this->load->view('Template/Footer');
        } else {
            $this->M_akun->ubah($id);
            $this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
			<span class="badge badge-pill badge-success">Berhasil</span>
			Berhasil disimpan.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
        </div>');
			redirect('Akun');
		}
	}


	public function hapus($id = Null)
	{
		$this->M_akun->hpsAkun($id);
		$this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
			<span class="badge badge-pill badge-success">Berhasil</span>
			Akun dihapus.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
        </div>');
		redirect('Akun');
	}
}

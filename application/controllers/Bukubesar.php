<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bukubesar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_akuntansi');
        $this->load->library('form_validation');

        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Buku Besar";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('Template/Header', $data);
            $this->load->view('Laporan/Bukubesar', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function cetak() //untuk mencetak buku besar
    {
        if (isset($_POST['awal']) and isset($_POST['awal'])) {
            date_default_timezone_set('Asia/Jakarta');
            $awal  = $_POST['awal'];
            $akhir = $_POST['akhir'];
            $isi = $this->M_akuntansi->getIsi($awal, $akhir);
            $filterIsi = $this->M_akuntansi->filterIsi($awal, $akhir);


            $akun  = $this->M_akuntansi->getAkun();


            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AddPage();
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 15);
            $pdf->Cell(0, 0.75, 'BUKU BESAR', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 10);
            $pdf->Cell(0, 0.75, 'Argomart', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 0, 0, 'C');
            $pdf->Line(11, 30, 200, 30);

            foreach ($akun as  $ak) {
                foreach ($filterIsi as  $filter) {
                    $no = 1;
                    if ($ak->kode_akun == $filter->kode_akun) {
                        $no = 1;
                        $pdf->Ln(10);
                        $pdf->Cell(1);
                        $pdf->Cell(20, 8, $ak->kode_akun . '-' . $ak->nama_akun, 0, 0, 'L');
                        $pdf->Ln(8);
                        $pdf->Cell(1);
                        $pdf->SetFont('Arial', 'B', 10);
                        $pdf->Cell(20, 8, 'TANGGAL', 1, 0, 'C');
                        $pdf->Cell(80, 8, 'KETERANGAN', 1, 0, 'C');
                        $pdf->Cell(30, 8, 'DEBET', 1, 0, 'C');
                        $pdf->Cell(30, 8, 'KREDIT', 1, 0, 'C');
                        $pdf->Cell(30, 8, 'SALDO', 1, 0, 'C');



                        foreach ($isi as $row => $filed) {
                            if ($ak->kode_akun == $filed->kode_akun) {
                                $d = number_format($filed->debet, 0, ",", ".");
                                $k = number_format($filed->kredit, 0, ",", ".");
                                $date = $filed->tanggal;
                                $tgl = explode('-', $date);
                                $y = $tgl[0];
                                $m = $tgl[1];
                                $dt = $tgl[2];
                                $dmy = $dt . '-' . $m . '-' . $y;
                                $pdf->Ln();
                                $pdf->Cell(1);
                                $pdf->SetFont('Arial', '', 10);
                                $pdf->Cell(20, 8, $dmy, 'LR', 0, 'C');
                                $pdf->Cell(80, 8, $filed->ket, 'LR', 0, 'L');
                                $pdf->Cell(30, 8,  $d . ' ', 'LR', 0, 'R');
                                $pdf->Cell(30, 8,  $k . ' ', 'LR', 0, 'R');

                                if ($filed->jenis == "DEBET") { //ketetia akun debet

                                    if ($no == 1) {
                                        $no++;
                                        $debet = $filed->debet;
                                        $kredit = $filed->kredit;
                                        if ($debet != 0) {
                                            $saldo = $filed->debet;
                                        } else {
                                            $saldo = -$filed->kredit;
                                        }
                                        $total = number_format($saldo, 0, ",", ".");
                                        $pdf->Cell(30, 8, $total . ' ', 'LR', 0, 'R');
                                    } else {
                                        if ($filed->debet != 0) {
                                            $debet  = $debet + $filed->debet;
                                            $saldo  = $saldo + $filed->debet;
                                            $total  = number_format($saldo, 0, ",", ".");
                                            $pdf->Cell(30, 8, $total . ' ', 'LR', 0, 'R');
                                        } else {
                                            $kredit = $kredit + $filed->kredit;
                                            $saldo  = $saldo - $filed->kredit;
                                            $total  = number_format($saldo, 0, ",", ".");
                                            $pdf->Cell(30, 8, $total . ' ', 'LR', 0, 'R');
                                        }
                                    }
                                } else { //ketika akun kredit

                                    if ($no == 1) {
                                        $no++;
                                        $debet = $filed->debet;
                                        $kredit = $filed->kredit;
                                        if ($kredit != 0) {
                                            $saldo = $filed->kredit;
                                        } else {
                                            $saldo = -$filed->debet;
                                        }
                                        $total = number_format($saldo, 0, ",", ".");
                                        $pdf->Cell(30, 8, $total . ' ', 'LR', 0, 'R');
                                    } else {
                                        if ($filed->kredit != 0) {
                                            $kredit  = $kredit + $filed->kredit;
                                            $saldo  = $saldo + $filed->kredit;
                                            $total  = number_format($saldo, 0, ",", ".");
                                            $pdf->Cell(30, 8, $total . ' ', 'LR', 0, 'R');
                                        } else {
                                            $debet = $debet + $filed->debet;
                                            $saldo  = $saldo - $filed->debet;
                                            $total  = number_format($saldo, 0, ",", ".");
                                            $pdf->Cell(30, 8, $total . ' ', 'LR', 0, 'R');
                                        }
                                    }
                                }
                            }
                        }
                        $totaldebet  = number_format($debet, 0, ",", ".");
                        $totalkredit  = number_format($kredit, 0, ",", ".");

                        $pdf->Ln();
                        $pdf->Cell(1);
                        $pdf->SetFont('Arial', '', 10);
                        $pdf->Cell(100, 8, 'TOTAL', '1', 0, 'C');
                        $pdf->Cell(30, 8, $totaldebet . ' ', '1', 0, 'R');
                        $pdf->Cell(30, 8, $totalkredit . ' ', '1', 0, 'R');
                        $pdf->Cell(30, 8, $total . ' ', '1', 0, 'R');
                    }
                }
            }

            $pdf->Output("Buku Besar.pdf", "I");
        } else {
            $data['title'] = "Access Denied";
            $this->load->view('404_accessdenied', $data);
        }
    }
}

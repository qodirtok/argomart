<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_supplier');
		$this->load->library('form_validation');
		is_login();
	}

	public function index()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$data['supplier'] = $this->M_supplier->getAll();
			$data['title'] = 'Agromart - Suplier';
			$this->load->view('Template/Header', $data);
			$this->load->view('Supplier/View.php', $data);
			$this->load->view('Template/Footer');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function Add() //untuk menampilkan halaman form tambah supplier
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			# code...
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$data['title'] = 'Agromart- Tambah Supplier';
			$data['kodesupplier'] = $this->M_supplier->createKode();
			$this->load->view('Template/Header', $data);
			$this->load->view('Supplier/Tambah.php', $data);
			$this->load->view('Template/Footer');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function store() //untuk mengirim data inputan user ke database dan hanya mengatur validasi saja selebihnya ada di model
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			# code...
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$supplier = $this->M_supplier;
			$validation = $this->form_validation;
			$validation->set_rules($supplier->rules());

			if ($validation->run()) {
				$supplier->save();
				$data['title'] = 'Agromart - Supplier';
				$this->session->set_flashdata('msg', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
				redirect('/supplier');
			} else {
				$this->session->set_flashdata('msg', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
				<span class="badge badge-pill badge-danger">Gagal</span>
				Gagal disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
				redirect('/supplier/add');
			}
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function show($id = null) //untuk menuju ke halaman view Detail berdasarkan supplier yang dipilih
	{
		# code...
		if (!isset($id)) {
			show_404();
		} else {
			if ($this->M_supplier->getById($id)) {
				$data['kodesupplier'] = $this->M_supplier->createKode();
				$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
				$data['title'] = 'Agromart - view Data Supplier';
				$this->load->view('Template/Header', $data);
				$this->load->view('Supplier/Detail.php', $data);
				$this->load->view('Template/Footer');
			}
		}
	}

	public function edit($id = null)
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			# code...
			if (!isset($id)) {
				show_404();
			} else {
				if ($this->M_supplier->getById($id)) {
					$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
					$data['title'] = 'Agromart - Edit Data Supplier';
					$data['kodesupplier'] = $this->M_supplier->createKode();
					$data['getById'] = $this->M_supplier->getById($id);
					// $this->load->view('template/header', $data);
					// $this->load->view('Supplier/view', $data);
					// $this->load->view('template/footer');
					redirect('supplier');
				}
			}
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function update($id = null)
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			# code...
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();

			if (!isset($id)) redirect('supplier');

			$Supplier = $this->M_supplier;
			$validation = $this->form_validation;
			$validation->set_rules($Supplier->rules());

			if ($validation->run()) {
				$Supplier->M_update();
				$this->session->set_flashdata('success', 'Berhasil disimpan');
			}

			$data["tb_supplier"] = $Supplier->getById($id);
			if (!$data["tb_supplier"]) show_404();

			redirect('supplier');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function destroy($id = null)
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			# code...
			if (!isset($id)) show_404();

			if ($this->M_supplier->delete($id)) {
				# code...
				redirect('/supplier');
			}
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}
}

/* End of file supplier.php */

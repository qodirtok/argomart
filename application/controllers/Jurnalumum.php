<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jurnalumum extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_akuntansi');
		$this->load->library('form_validation');
		is_login();
	}

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Jurnal Umum";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $data['user']['id_user'];
            $data['bukti']  = $this->M_akuntansi->createKj($id);
            $data['akun']  = $this->M_akuntansi->getAkun();
            $data['debet']  = $this->M_akuntansi->totaldebet($id);
            $data['kredit']  = $this->M_akuntansi->totalkredit($id);
            $data['view']  = $this->M_akuntansi->getJurnal($id);
            $this->load->view('Template/Header', $data);
            $this->load->view('Jurnal/Index', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

	public function store() //untuk mengirim data inputan user ke database dan hanya mengatur validasi saja selebihnya ada di model
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			# code...
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$jurnal = $this->M_akuntansi;
			$validation = $this->form_validation;
			$validation->set_rules($jurnal->rules());

			if ($validation->run()) {
				//$jurnal->savejurnal();
				$jurnal->savejurnal();

				$this->session->set_flashdata('success', 'Berhasil disimpan');
			} else {
				$this->session->set_flashdata('failed', 'gagal disimpan');
			}
			redirect('/jurnalumum');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function update($id = null)
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();

			if (!isset($id)) redirect('jurnalumum');

			$jurnal = $this->M_akuntansi;
			$validation = $this->form_validation;
			$validation->set_rules($jurnal->rules());

			if ($validation->run()) {
				$jurnal->M_updatejurnal();
				$this->session->set_flashdata('success', 'Berhasil disimpan');
			} else {
				$this->session->set_flashdata('failed', 'gagal disimpan');
			}

			$data["tb_jurnal"] = $jurnal->getByJurnal($id);
			if (!$data["tb_jurnal"]) show_404();


			redirect('/jurnalumum');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}
}

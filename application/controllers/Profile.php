<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_profile');
        $this->load->library('form_validation');
        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Profile";
            $this->db->JOIN('tb_lvl', 'tb_lvl.id_lvl=tb_user.id_lvl');
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('Template/Header', $data);
            $this->load->view('Profile/Index', $data);
            $this->load->view('Template/Footer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function ubahprofile($id = null)
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Ubah Profile";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['tampil'] = $this->M_profile->GetAll($id);
            if (!isset($id)) redirect('profile');
            $this->form_validation->set_rules('nama_akun', 'Nama', 'required|trim');
            $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
            $this->form_validation->set_rules('Nohp', 'Jenis Kelamin', 'required|trim');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');

            if ($this->form_validation->run() == false) {
                $this->load->view('Template/Header', $data);
                $this->load->view('Profile/Ubah', $data);
                $this->load->view('Template/Footer');
            } else {
                $this->M_profile->ubah($id);
                $this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
			<span class="badge badge-pill badge-success">Berhasil</span>
			Berhasil disimpan.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
        </div>');
                redirect('profile');
            }
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function gantipassword($id = null)
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Ganti Password";
            $this->db->JOIN('tb_lvl', 'tb_lvl.id_lvl=tb_user.id_lvl');
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['tampil'] = $this->M_profile->GetAll($id);

            $this->form_validation->set_rules('passwordlama', 'Password Lama', 'required|trim');
            $this->form_validation->set_rules('passwordbaru', 'Password Baru', 'required|trim|min_length[6]|matches[ulangpassword]');
            $this->form_validation->set_rules('ulangpassword', 'Konfirmasi Ulang Password', 'required|trim|min_length[6]|matches[passwordbaru]');
            if ($this->form_validation->run() == false) {
                $this->load->view('Template/Header', $data);
                $this->load->view('Profile/Gantipassword', $data);
                $this->load->view('Template/Footer');
            } else {
                $passwordlama = $this->input->post('passwordlama');
                $passwordbaru = $this->input->post('passwordbaru');
                if (!password_verify($passwordlama, $data['user']['password'])) {
                    $this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                <span class="badge badge-pill badge-danger">Gagal</span>
                Password Lama Salah.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>');
                    redirect('profile/gantipassword');
                } else {
                    if ($passwordlama == $passwordbaru) {
                        $this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                    <span class="badge badge-pill badge-danger">Gagal</span>
                    Password baru sama dengan Password Lama.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>');
                        redirect('profile/gantipassword');
                    } else {
                        $password_hash = password_hash($passwordbaru, PASSWORD_DEFAULT);

                        $this->db->set('password', $password_hash);
                        $this->db->where('id_user', $id);
                        $this->db->update('tb_user');
                        $this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                    <span class="badge badge-pill badge-success">Berhasil</span>
                    Password Baru Berhasil Di Buat.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>');
                        redirect('profile');
                    }
                }
            }
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }
}

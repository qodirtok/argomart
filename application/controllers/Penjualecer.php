<?php
defined('BASEPATH') or exit('No direct script access allowed');
//start kode direct print
require __DIR__ . '/../autoload.php';

use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\CapabilityProfile;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
//end kode direct print
class penjualecer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_penjualanecer');
        $this->load->model('M_barang');
        $this->load->model('M_diskon');
        $this->load->library('form_validation');
        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            // $this->cart->destroy();
            $data['title'] = "Agromart - Penjualan (Eceran)";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['tgl'] = date('d-m-Y');
            $data['nofaktur'] = $this->M_penjualanecer->createKode();
            $data['barang'] = $this->M_barang->getAll();
            $this->load->view('Template/Header', $data);
            $this->load->view('Penjualecer/Index', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function destroy($id = null)
    {
        $this->M_penjualanecer->delete($id);
        $this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
			<span class="badge badge-pill badge-success">Berhasil</span>
			Mengurangi Barang.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
        </div>');
        redirect('penjualecer');
    }

    // function get_memberr()
    // {
    //     $idmember = $this->input->post('idmember');
    //     $data = $this->M_penjualanecer->get_member($idmember);
    //     echo json_encode($data);
    // }

    function add_to_cart()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $kodebar = $this->input->post('kode_brg');
            $batas = $this->M_barang->get_barang($kodebar);
            $bts = $batas->row_array();
           if($bts['stok'] <= 0)
           {
                $this->session->set_flashdata('msg', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                    <span class="badge badge-pill badge-danger">Gagal</span>
                    Jumlah Stok Barang Sudah Habis.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>');
                    redirect('penjualecer');
           } else {
            $kobar = $this->input->post('kode_brg');
           
            
            $produk = $this->M_barang->get_barang($kobar);
            $i = $produk->row_array();

            $data = array(
                // 'id'       => $i['Kode_barang'],
                'id' => $this->input->post('kodebarang'),
                'name'     => $i['nama'],
                'satuan'   => $i['nma_satuan'],
                'harpok'   => $i['hrg_beli'],
                'price'    => (((100 - $this->input->post('diskon')) / 100) * $i['hrg_jual']),
                'disc'     => $this->input->post('diskon'),
                'qty'      => $this->input->post('qty'),
                'harjul'   => $i['hrg_jual'],
                'hrgjualgrosir' => $i['hrg_jual_grosir'],
            );
            $this->cart->insert($data);
            // if (!empty($this->cart->total_items())) {
            //     foreach ($this->cart->contents() as $items) {
            //         $id = $items['id'];
            //         $qtylama = $items['qty'];
            //         $rowid = $items['rowid'];
            //         $kobar = $this->input->post('kode_brg');
            //         $qty = $this->input->post('qty');
            //         if ($id == $kobar) {
            //             $up = array(
            //                 'rowid' => $rowid,
            //                 'qty' => $qtylama + $qty
            //             );
            //             $this->cart->update($up);
            //         } else {
            //             $this->cart->insert($data);
            //         }
            //     }
            // } else {
            //     $this->cart->insert($data);
            // }

            redirect('Penjualecer');
            }
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
      
    }

    function simpan_penjualan()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $total = $this->input->post('total');
            // $this->session->set_userdata('idmember', $idmember);
            $jml_uang = str_replace(",", "", $this->input->post('jml_uang'));
            $kembalian = $jml_uang - $total;
            if (!empty($total) && !empty($jml_uang)) {
                if ($jml_uang < $total) {
                    $this->session->set_flashdata('msg', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                    <span class="badge badge-pill badge-danger">Gagal</span>
                    Jumlah Uang yang anda masukan Kurang.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>');
                    redirect('penjualecer');
                } else {
                    $nofak = $this->M_penjualanecer->createKode();
                    // $idmember = $this->input->post('idmember');
                    $tgl = date('Y-m-d');
                    $this->session->set_userdata('nofak', $nofak);
                    $order_proses = $this->M_penjualanecer->simpan_penjualan($nofak, $tgl, $total, $jml_uang, $kembalian);
                    if ($order_proses) {
                        $this->cart->destroy();
                        // $this->session->unset_userdata('idmember', $idmember);
                        // $this->session->unset_userdata('tglfak');
                        // $this->session->unset_userdata('suplier');
                        // $this->load->view('admin/alert/alert_sukses');
                        $this->struk($nofak);
                        // redirect('penjualecer');
                    } else {
                        redirect('penjualecer');
                    }
                }
            } else {
                $this->session->set_flashdata('msg', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                    <span class="badge badge-pill badge-danger">Gagal</span>
                    Penjualan Gagal di Simpan, Mohon Periksa Kembali Semua Inputan Anda!.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>');
                redirect('penjualecer');
            }
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    public function cart()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $cart = $this->M_penjualanecer;
            $kobar = $this->input->post('kode_brg');
            $validation = $this->form_validation;
            $validation->set_rules($cart->rules());

            if ($validation->run()) {
                // !isset($kobar)
                // if (!isset($kobar)) {
                if (!isset($kobar)) {
                    $cart->selectByCart($kobar);
                    $cart->cartUpdate($kobar);
                } else {
                    $cart->cartadd();
                    $cart->krngstok($kobar);
                }
            } else {
                $this->session->set_flashdata('message', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
			<span class="badge badge-pill badge-danger">Gagal</span>
			Tambah Keranjang.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
        </div>');
            }
            redirect('penjualecer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function remove()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $row_id = $this->uri->segment(3);
            // $row_id = '6c14da109e294d1e8155be8aa4b1ce8e';
            $data = array(
                'rowid'      => $row_id,
                'qty'     => 0
            );
            $this->cart->update($data);
            // var_dump($row_id);
            // die;
            // $this->cart->update(array(
            //     'rowid'      => $row_id,
            //     'qty'     => 0
            // ));
            redirect('penjualecer');
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function get_barang()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $kobar = $this->input->post('kode_brg');
            // $kobar = '1234';
            $x['brg'] = $this->M_barang->getBykode($kobar);
            $x['diskn'] = $this->M_diskon->getByKode($kobar);
            // $x['byqty'] = $this->M_penjualanecer->selectByCart($kobar);
            $this->load->view('Penjualecer/Detail_barang_ecer', $x);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function updatejmlh()
    {
        $id = $this->input->post("id");
        $value = $this->input->post("value");
        $modul = $this->input->post("modul");
        $this->M_penjualanecer->update($id, $value, $modul);
        echo "{}";
    }

    function hapuscart()
    {
        $this->M_penjualanecer->hapuscart();
        redirect('penjualecer');
    }

    function struk($nofak)
    {
        date_default_timezone_set('Asia/Jakarta');
        $data['title'] = "Cetak Struk";
        $data['nofak'] = $nofak;
        $data['pengirim'] = $this->M_penjualanecer->pengirim($nofak);
        $data['cetak'] = $this->M_penjualanecer->strukcetak($nofak);
        $data['idadmin'] = $this->session->userdata('nama');
        // $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [76, 500]]);
        $this->load->view('Penjualecer/Struk',  $data);
        // $mpdf->AddPage("P");
        // $mpdf->WriteHTML($html);
        // $mpdf->Output('Struk', 'I');
    }

    function cetak($nofak)
    // function cetak()
    {
        $connector = new WindowsPrintConnector("xp-80");
        // $profile = CapabilityProfile::load("xp-80");
        // $connector = new WindowsPrintConnector("smb://192.168.43.1/xp-80");
        // $connector = new NetworkPrintConnector("192.168.1.9", 9100);
        $printer = new Printer($connector);
        $printer->initialize();
        date_default_timezone_set('Asia/Jakarta');
        $idadmin = $this->session->userdata('nama');
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setTextSize(1, 1);
        $printer->text("Agromart\n");
        $printer->text("Jl. S. Supriadi No.48\n");
        $printer->text("Bandungrejosari, Kec. Sukun,\n");
        $printer->text("Kota Malang,Jawa Timur\n");
        $printer->feed();
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("No. Faktur : $nofak\n");
        $printer->text("--------------------------------\n");
        $cetak = $this->M_penjualanecer->strukcetak($nofak);
        foreach ($cetak as $row) {
            $harga = number_format($row->harga, 0, ",", ".");
            $sub_total = number_format($row->sub_total, 0, ",", ".");
            $lm = strlen($row->nama);
            if ($lm == 4) {
                $nm = $row->nama . '     ';
            } else if ($lm == 5) {
                $nm = $row->nama . '    ';
            } else if ($lm == 6) {
                $nm = $row->nama . '   ';
            } else if ($lm == 7) {
                $nm = $row->nama . '  ';
            } else if ($lm == 8) {
                $nm = $row->nama . ' ';
            } else if ($lm == 9) {
                $nm = $row->nama . '';
            } else {
                $nm = $row->nama;
            }

            $lj = strlen($row->jumlah);
            if ($lj == 1) {
                $jml = '   ' . $row->jumlah;
            } else if ($lj == 2) {
                $jml = '  ' . $row->jumlah;
            } else if ($lj == 3) {
                $jml = '  ' . $row->jumlah;
            } else if ($lj == 4) {
                $jml = '' . $row->jumlah;
            }

            $lh = strlen($row->harga);
            if ($lh == 4) {
                $hg = '    ' . $harga;
            } else if ($lh == 5) {
                $hg = '   ' . $harga;
            } else if ($lh == 6) {
                $hg = '  ' . $harga;
            } else if ($lh == 7) {
                $hg = ' ' . $harga;
            }

            $lt = strlen($row->sub_total);
            if ($lt == 4) {
                $tot = '     ' . $sub_total;
            } else if ($lt == 5) {
                $tot = '    ' . $sub_total;
            } else if ($lt == 6) {
                $tot = '   ' . $sub_total;
            } else if ($lt == 7) {
                $tot = '  ' . $sub_total;
            }

            $printer->textRaw("$nm");
            $printer->textRaw("$jml");
            $printer->textRaw("$hg");
            $printer->textRaw("$tot\n");
        }

        $printer->text("--------------------------------\n");
        $totpen = number_format($row->total_penjualan, 0, ", ", ".");
        $tunai = number_format($row->jmlh_uang, 0, ",", ".");
        $kembali = number_format($row->kembalian, 0, ",", ".");
        $printer->text("Total     : Rp.  $totpen\n");
        $printer->text("Tunai     : Rp.  $tunai\n");
        $printer->text("Kembalian : Rp.  $kembali\n");
        $printer->text("--------------------------------\n");
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("----Terima Kasih----\n");
        $printer->text("----Silahkan Datang Kembali----\n\n\n");
        $printer->pulse();
        $printer->close();
    }
}

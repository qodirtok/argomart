<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Satuan extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_satuan');
		$this->load->library('form_validation');
		is_login();
	}

	public function index()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$data['satuan'] = $this->M_satuan->getAll();
			$data['title'] = 'Agromart - Satuan';
			$this->load->view('Template/Header', $data);
			$this->load->view('Satuan/View.php', $data);
			$this->load->view('Template/Footer');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function store()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			# code...
			$satuan = $this->M_satuan;
			$validation = $this->form_validation;
			$validation->set_rules($satuan->rules());

			if ($validation->run()) {
				$satuan->save();
				$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			} else {
				$this->session->set_flashdata('failed', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
					<span class="badge badge-pill badge-danger">Gagal</span>
					Gagal Disimpan.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					</div>');
			}

			redirect('satuan/');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function update($id = null)
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			if (!isset($id)) redirect('satuan/');

			$satuan = $this->M_satuan;
			$validation = $this->form_validation;
			$validation->set_rules($satuan->rules());

			if ($validation->run()) {
				$satuan->update();
				$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			} else {
				$this->session->set_flashdata('failed', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
					<span class="badge badge-pill badge-danger">Gagal</span>
					Gagal Disimpan.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					</div>');
			}

			$data["tb_satuan"] = $satuan->getById($id);
			if (!$data["tb_satuan"]) show_404();

			redirect('satuan/');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function destroy($id = null)
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			# code...
			if (!isset($id)) show_404();

			$this->M_satuan->delete($id);
			$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil Dihapus.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');

			redirect('satuan/');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}
}

/* End of file Satuan.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lporstok extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_lporstok');
        $this->load->library('form_validation');
        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '3' || $this->session->userdata('id_lvl') == '5') {
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $data['title'] = "Agromart - Laporan Stok";
            $this->load->view('Template/Header', $data);
            $this->load->view('Laporan/Stok', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

    function periode($date)
    {
        $str = explode('-', $date);
        $bulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Maret',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];
        return ' ' . $bulan[$str[0]] . ' ' . $str[1];
    }

    public function stok()
    {
        if (isset($_POST['awal']) and isset($_POST['awal'])) {
            date_default_timezone_set('Asia/Jakarta');
            $awal   = $_POST['awal'];
            $akhir  = $_POST['akhir'];
            $stok = $this->M_lporstok->stok($awal, $akhir);



            $tgla = explode('-', $awal);
            $da = $tgla[2];
            $tglb = explode('-', $akhir);
            $db = $tglb[2];
            $periode = date('m-Y');
            $bln = $this->periode($periode);

            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AddPage();
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 15);
            $pdf->Cell(0, 0.75, 'LAPORAN STOK', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 11);
            $pdf->Cell(0, 0.75, 'Middos Market', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Periode ' . $da . ' - ' . $db . $bln, 0, 0, 'C');
            $pdf->Line(11, 35, 200, 35);

            $pdf->Ln(15);
            // $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(20, 8, 'Tanggal', 1, 0, 'C');
            $pdf->Cell(123, 8, 'Nama Barang', 1, 0, 'C');
            $pdf->Cell(17, 8, 'Tersedia', 1, 0, 'C');
            $pdf->Cell(15, 8, 'Masuk', 1, 0, 'C');
            $pdf->Cell(15, 8, 'Keluar', 1, 0, 'C');
            // $pdf->Cell(20, 8, 'Total', 1, 0, 'C');   

            $stokk = 0;
            $stkmsk = 0;
            $stkkluar = 0;
            foreach ($stok as $data) {
                $pdf->Ln();
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(20, 8, $data->tgl_beli, 'LR', 0, 'C');
                $pdf->Cell(123, 8, $data->nama . ' / ' . $data->nma_satuan, 'LR', 0, 'L');
                $pdf->Cell(17, 8, $data->stok, 'LR', 0, 'C');
                $pdf->Cell(15, 8, $data->masuk, 'LR', 0, 'C');
                foreach ($this->db->query("Select SUM(jumlah) AS Kluar FROM tb_detail_penjualan WHERE Kode_barang = '$data->Kode_barang'")->result() as $data1) {
                    $kluar =  $data1->Kluar;
                }
                $pdf->Cell(15, 8, $kluar, 'LR', 0, 'C');
                $stokk += $data->stok;
                $stkmsk +=  $data->masuk;
                $stkkluar += $kluar;
            }

            $pdf->Ln();
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(143, 8, 'TOTAL', '1', 0, 'C');
            $pdf->Cell(17, 8, $stokk, '1', 0, 'C');
            $pdf->Cell(15, 8, $stkmsk, '1', 0, 'C');
            $pdf->Cell(15, 8, $stkkluar, '1', 0, 'C');


            $pdf->Output("Laporan Stok.pdf", "I");
        } else {
            $data['title'] = "Access Denied";
            $this->load->view('404_accessdenied', $data);
        }
    }
}

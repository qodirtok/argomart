<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Diskon extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_diskon');
		$this->load->model('M_barang');
		$this->load->library('form_validation');
		$this->M_diskon->deleteOldDiskon();

		is_login();
	}


	public function index()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$data['title'] = 'Agromart - Diskon';
			$data['listDiskon'] = $this->M_diskon->getAll();
			$data['kdbarang'] = $this->M_barang->getAll();
			$this->load->view('Template/Header', $data);
			$this->load->view('Diskon/View', $data);
			$this->load->view('Template/Footer');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}
	public function store()
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			$diskon = $this->M_diskon;
			$validation = $this->form_validation;
			$validation->set_rules($diskon->rules());

			if ($validation->run()) {
				# code...
				$diskon->save();
				$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			} else {
				# code...
				$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			}
			redirect('diskon');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function destroy($id)
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			# code...
			if (!isset($id)) show_404();

			if ($this->M_diskon->delete($id)) {
				# code...
				redirect('diskon');
			}
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}
}

/* End of file Diskon.php */

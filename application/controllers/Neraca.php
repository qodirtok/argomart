<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Neraca extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_neraca');
        $this->load->library('form_validation');

        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Neraca";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('Template/Header', $data);
            $this->load->view('Laporan/Neraca', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }
    function periode($date)
    {
        $str = explode('-', $date);
        $bulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Maret',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];
        return $str[2] . ' ' . $bulan[$str[1]] . ' ' . $str[0];
    }

    public function cetak() //untuk mencetak buku besar
    {
        if (isset($_POST['tanggal'])) {
            date_default_timezone_set('Asia/Jakarta');
            $tanggal  = $_POST['tanggal'];
            $penjualan = $this->M_neraca->getpenjualan($tanggal);
            $hapok     = $this->M_neraca->getHapok($tanggal);
            $repo      = $this->M_neraca->getRePo($tanggal);
            $biaya     = $this->M_neraca->getBiaya($tanggal);
            $pendlain     = $this->M_neraca->getPendLain($tanggal);
            $aktiva = $this->M_neraca->getAktiva($tanggal);
            $pasiva = $this->M_neraca->getPasiva($tanggal);

            $periode = $tanggal;
            $bln = $this->periode($periode);

            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AddPage();
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 15);
            $pdf->Cell(0, 0.75, 'NERACA', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 11);
            $pdf->Cell(0, 0.75, 'Agromart', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Periode ' .  $bln, 0, 0, 'C');
            $pdf->Line(11, 35, 200, 35);


            $pdf->Ln(10);
            $pdf->Ln(5);
            $pdf->Cell(15);
            $no = 1;

            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(95, 8, 'AKTIVA', 0, 0, 'L');
            $pdf->SetFont('Arial', '', 10);
            $tda = 0;
            foreach ($aktiva as $dta => $akv) {
                $pdf->Ln(8);
                $pdf->Cell(15);
                $pdf->Cell(10, 8, $akv->kode_akun . ' -', 0, 0, 'L');
                $pdf->Cell(100, 8, ' ' . $akv->nama_akun, 0, 0, 'L');
                if ($akv->jenis == "DEBET") {
                    if ($no == 1) {
                        $no++;
                        $sda  = $akv->D - $akv->K;
                        $tda = $sda;
                        $sd  = number_format($sda, 0, ",", ".");
                        $pdf->Cell(30, 8, $sd . ' ', 0, 0, 'R');
                    } else {
                        $sda  = $akv->D - $akv->K;
                        $tda = $tda + $sda;
                        $sd  = number_format($sda, 0, ",", ".");
                        $pdf->Cell(30, 8, $sd . ' ', 0, 0, 'R');
                    }
                } else {
                    if ($no == 1) {
                        $no++;
                        $sda  = $akv->K - $akv->D;
                        $tda = $tda + $sda;
                        $sd  = number_format($sda, 0, ",", ".");
                        $pdf->Cell(30, 8, $sd . ' ', 0, 0, 'R');
                    } else {
                        $sda  = $akv->K - $akv->D;
                        $tda = $tda + $sda;
                        $sd  = number_format($sda, 0, ",", ".");
                        $pdf->Cell(30, 8, $sd . ' ', 0, 0, 'R');
                    }
                }
            }


            $pdf->Ln(8);
            $pdf->Cell(15);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(110, 8, 'Total Aktiva', 0, 0, 'L');
            $tak  = number_format($tda, 0, ",", ".");
            $pdf->Cell(30, 8, $tak . ' ', 0, 0, 'R');

            $np = 1;
            $pdf->Ln(10);
            $pdf->Ln(5);
            $pdf->Cell(15);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(95, 8, 'PASIVA', 0, 0, 'L');
            $pdf->SetFont('Arial', '', 10);
            $tdp = 0;
            foreach ($pasiva as $d => $psv) {
                $pdf->Ln(8);
                $pdf->Cell(15);
                $pdf->Cell(10, 8, $psv->kode_akun . ' -', 0, 0, 'L');
                $pdf->Cell(100, 8, ' ' . $psv->nama_akun, 0, 0, 'L');
                if ($psv->jenis == "KREDIT") {
                    if ($np == 1) {
                        $np++;
                        $kpv = $psv->K;
                        $dpv  = $psv->D;
                        $sdp  = $psv->K - $psv->D;
                        $tdp  = $sdp;
                        $sd  = number_format($sdp, 0, ",", ".");
                        $pdf->Cell(30, 8, $sd . ' ', 0, 0, 'R');
                    } else {
                        $kpv = $kpv + $psv->K;
                        $dpv  = $dpv + $psv->D;
                        $sdp  = $psv->K - $psv->D;
                        $tdp  = $tdp + $sdp;
                        $sd  = number_format($sdp, 0, ",", ".");
                        $pdf->Cell(30, 8, $sd . ' ', 0, 0, 'R');
                    }
                } else {
                    if ($no == 1) {
                        $no++;
                        $dpv  = $psv->D;
                        $kpv = $psv->K;
                        $sdp  = $psv->D - $psv->K;
                        $tdp  = $sdp;
                        $sd  = number_format($sdp, 0, ",", ".");
                        $pdf->Cell(30, 8, $sd . ' ', 0, 0, 'R');
                    } else {
                        $dpv  = $dpv + $psv->D;
                        $kpv = $kpv + $psv->K;
                        $sdp  = $psv->D - $psv->K;
                        $tdp  = $tdp - $sdp;
                        $sd  = number_format($sdp, 0, ",", ".");
                        $pdf->Cell(30, 8, $sd . ' ', 0, 0, 'R');
                    }
                }
            }
            //start laba rugi
            foreach ($penjualan as $data) {
                $no = 1;
                $saldo = $data->K - $data->debet;
                $total = number_format($saldo, 0, ',', '.');
            }
            $retpot = 0;
            foreach ($repo as $data) {
                if ($no == 1) {
                    $no++;
                    $retpot = $data->D - $data->K;
                    $jumlah = $data->D - $data->K;
                } else {
                    $retpot = $retpot + $data->D - $data->K;
                    $jumlah = $data->D - $data->K;
                }
            }
            foreach ($hapok as $data) {
                $jumlah = $data->D - $data->K;
            }
            $labakotor = $saldo - ($jumlah + $retpot);
            foreach ($pendlain as $data) {
                $pend = $data->K - $data->D;
            }
            $beban = 0;
            foreach ($biaya as $data) {

                if ($no == 1) {
                    $no++;
                    $beban  = $data->D - $data->K;
                    $saldo = $data->D - $data->K;
                } else {

                    $beban = $beban + $data->D - $data->K;
                    $jumlah = $data->D - $data->K;
                }
            }
            $laba = ($labakotor + $pend) - $beban;
            $lb = number_format($laba, 0, ',', '.');
            $pdf->Ln(8);
            $pdf->Cell(15);
            $pdf->Cell(10, 8, '3003 -', 0, 0, 'L');
            $pdf->Cell(100, 8, ' Laba Periode Berjalan', 0, 0, 'L');
            $pdf->Cell(30, 8, $lb . ' ', 0, 0, 'R');

            //end laba rugi 
            $totalpasiva = $tdp + $laba;
            $pdf->Ln(8);
            $pdf->Cell(15);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(110, 8, 'Total Pasiva', 0, 0, 'L');
            $tap  = number_format($totalpasiva, 0, ",", ".");
            $pdf->Cell(30, 8, $tap . ' ', 0, 0, 'R');



            if ($tda != $totalpasiva) {
                $pdf->ln(15);
                if ($tda > $totalpasiva) {
                    $pdf->Cell(0, 0.75, 'Tidak Balance Selisih :' . ($tda - $totalpasiva), 0, 0, 'C');
                } else {
                    $pdf->Cell(0, 0.75, 'Tidak Balance Selisih :' . ($totalpasiva - $tda), 0, 0, 'C');
                }
            }


            $pdf->Output("Neraca.pdf", "I");
        } else {
            $data['title'] = "Access Denied";
            $this->load->view('404_accessdenied', $data);
        }
    }
}
// $k  = number_format($kredit, 0, ",", ".");

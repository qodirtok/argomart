<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kaskeluar extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_akuntansi');
		$this->load->library('form_validation');
		is_login();
	}

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Kas Keluar";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $data['user']['id_user'];
            $data['bukti']  = $this->M_akuntansi->createKode($id);
            $data['akun']  = $this->M_akuntansi->getAkun();
            $data['view']  = $this->M_akuntansi->getAll($id);
            $this->load->view('Template/Header', $data);
            $this->load->view('Kaskeluar/Index', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }

	public function store() //untuk mengirim data inputan user ke database dan hanya mengatur validasi saja selebihnya ada di model
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			# code...
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$kk = $this->M_akuntansi;
			$validation = $this->form_validation;
			$validation->set_rules($kk->rules());

			if ($validation->run()) {
				//$kk->savejurnal();
				$kk->save();

				$this->session->set_flashdata('success', 'Berhasil disimpan');
			} else {
				$this->session->set_flashdata('failed', 'gagal disimpan');
			}
			redirect('/kaskeluar');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function update($id = null)
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			# code...
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();

			if (!isset($id)) redirect('kaskeluar');

			$kk = $this->M_akuntansi;
			$validation = $this->form_validation;
			$validation->set_rules($kk->rules());

			if ($validation->run()) {
				$kk->M_update();
				$this->session->set_flashdata('success', 'Berhasil disimpan');
			} else {
				$this->session->set_flashdata('failed', 'gagal disimpan');
			}

			$data["tb_kaskeluar"] = $kk->getById($id);
			if (!$data["tb_kaskeluar"]) show_404();


			redirect('/kaskeluar');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hutang extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_hutang');
		$this->load->model('M_member');
		$this->load->library('form_validation');
		is_login();
	}

	public function index()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$data['title'] = 'Agromart - Hutang';
			$data['hutang'] = $this->M_hutang->getAll();
			$data['member'] = $this->M_member->getAll();
			$this->load->view('Template/Header', $data);
			$this->load->view('Hutang/View', $data);
			$this->load->view('Template/Footer');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function store()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			$hutang = $this->M_hutang;
			$validation = $this->form_validation;
			$validation->set_rules($hutang->rules());

			$id_member = $this->input->post();

			if ($validation->run()) {
				// if (!isset($id_member['id_member'])) {
				# code...
				$hutang->save();
				$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
				// }
				// else {
				// 	# code...
				// 	$hutang->saveUpdate();
				// }

			} else {
				$this->session->set_flashdata('failed', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
				<span class="badge badge-pill badge-danger">Gagal</span>
				Gagal disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			}
			redirect('user/hutang');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function show($id = null)
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			if (!isset($id)) {
				show_404();
			} else {
				if ($this->M_hutang->getById($id)) {
					$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
					$data['title'] = 'Argomart - Detail Data Hutang';
					$data['detail'] = $this->M_hutang->getById($id);
					$data['tmp'] = $this->M_hutang->gethtngById($id);
					$this->load->view('Template/Header', $data);
					$this->load->view('Hutang/Detail', $data);
					$this->load->view('Template/Footer');
				}
			}
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function bayarHutang($id = null)
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
			# code...
			$bayarhutang = $this->M_hutang;
			if (!isset($id)) {
				# code...
				show_404();
			} else {
				# code...
				if ($bayarhutang->bayar($id)) {
					# code...
					$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
						<span class="badge badge-pill badge-success">Berhasil</span>
						Berhasil disimpan.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
						</div>');
				}
			}

			redirect('user/hutang-detail/' . $id);
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	// public function histori($id = null)
	// {
	// 	# code...
	// 	if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
	// 		if (!isset($id)) {
	// 			show_404();
	// 		} else {
	// 			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
	// 			$data['title'] = 'Argomart - Detail Data Hutang';
	// 			$data['tmp'] = $this->M_hutang->gethtngById($id);
	// 			$this->load->view('Template/Header', $data);
	// 			$this->load->view('Hutang/Histori', $data);
	// 			$this->load->view('Template/Footer');
	// 		}
	// 	} else {
	// 		$data['title'] = 'Error 403 Access Denied';
	// 		$this->load->view('404_accessdenied', $data);
	// 	}
	// }

	function cetak($id = null)
	{
		$data['title'] = 'Cetak Hutang';
		$data['hutang'] = $this->M_hutang->cetak($id);
		$this->load->view('Hutang/Cetak', $data);
	}
}

/* End of file Hutang.php */

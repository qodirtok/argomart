<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lporanjul extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_lporanjul');
		$this->load->library('form_validation');

		is_login();
	}

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Laporan Penjualan";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('Template/Header', $data);
            $this->load->view('Laporan/Jual', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }
    function periode($date)
    {
        $str = explode('-', $date);
        $bulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Maret',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];
        return ' ' . $bulan[$str[0]] . ' ' . $str[1];
    }


	public function cetak() //untuk mencetak buku besar
	{
		if (isset($_POST['awal']) and isset($_POST['awal'])) {
			date_default_timezone_set('Asia/Jakarta');
			$awal   = $_POST['awal'];
			$akhir  = $_POST['akhir'];
			$penjualan = $this->M_lporanjul->getallpenjualan($awal, $akhir);



			$tgla = explode('-', $awal);
			$da = $tgla[2];
			$tglb = explode('-', $akhir);
			$db = $tglb[2];
			$periode = date('m-Y');
			$bln = $this->periode($periode);

			$pdf = new FPDF('P', 'mm', 'A4');
			$pdf->AddPage();
			$pdf->ln(5);
			$pdf->SetFont('Helvetica', 'B', 15);
			$pdf->Cell(0, 0.75, 'LAPORAN PENJUALAN', 0, 0, 'C');
			$pdf->ln(5);
			$pdf->SetFont('Helvetica', 'B', 11);
			$pdf->Cell(0, 0.75, 'Agromart', 0, 0, 'C');
			$pdf->ln(5);
			$pdf->Cell(0, 0.75, 'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 0, 0, 'C');
			$pdf->ln(5);
			$pdf->Cell(0, 0.75, 'Periode ' . $da . ' - ' . $db . $bln, 0, 0, 'C');
			$pdf->Line(11, 35, 200, 35);

			$pdf->Ln(15);
			// $pdf->Cell(1);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(20, 8, 'Tanggal', 1, 0, 'C');
			$pdf->Cell(30, 8, 'No Faktur', 1, 0, 'C');
			$pdf->Cell(30, 8, 'Nama Barang', 1, 0, 'C');
			$pdf->Cell(20, 8, 'Satuan', 1, 0, 'C');
			$pdf->Cell(30, 8, 'Harga Jual', 1, 0, 'C');
			$pdf->Cell(10, 8, 'Qty', 1, 0, 'C');
			$pdf->Cell(20, 8, 'Diskon (%)', 1, 0, 'C');
			$pdf->Cell(30, 8, 'Total', 1, 0, 'C');

			$total = 0;
			foreach ($penjualan as $data) {
				$pdf->Ln();
				$pdf->SetFont('Arial', '', 10);
				$pdf->Cell(20, 8, $data->tgl_penjualan, 'LR', 0, 'C');
				$pdf->Cell(30, 8, $data->no_faktur, 'LR', 0, 'C');
				$pdf->Cell(30, 8, $data->nama, 'LR', 0, 'C');
				$pdf->Cell(20, 8, $data->satuan, 'LR', 0, 'C');
				$pdf->Cell(30, 8, 'Rp. ' . number_format($data->harga, 0, ",", "."), 'LR', 0, 'R');
				$pdf->Cell(10, 8,  $data->jumlah, 'LR', 0, 'C');
				$pdf->Cell(20, 8,  $data->diskon . '%', 'LR', 0, 'C');
				$pdf->Cell(30, 8, 'Rp. ' . number_format($data->sub_total, 0, ",", "."), 'LR', 0, 'R');
				$total += $data->sub_total;
			}
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(160, 8, 'TOTAL', '1', 0, 'C');
			$pdf->Cell(30, 8, 'Rp. ' . number_format($total, 0, ",", ".") . ' ', '1', 0, 'R');


			$pdf->Output("Laporan Penjualan.pdf", "I");
		} else {
			$data['title'] = "Access Denied";
			$this->load->view('404_accessdenied', $data);
		}
	}
}

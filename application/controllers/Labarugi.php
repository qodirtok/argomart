<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Labarugi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_labarugi');
        $this->load->library('form_validation');

        is_login();
    }

    public function index()
    {
        if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '2' || $this->session->userdata('id_lvl') == '5') {
            $data['title'] = "Agromart - Laba Rugi";
            $data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('Template/Header', $data);
            $this->load->view('Laporan/Labarugi', $data);
            $this->load->view('Template/Footer', $data);
        } else {
            $data['title'] = 'Error 403 Access Denied';
            $this->load->view('404_accessdenied', $data);
        }
    }
    function periode($date)
    {
        $str = explode('-', $date);
        $bulan = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Maret',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];
        return ' ' . $bulan[$str[0]] . ' ' . $str[1];
    }

    public function cetak() //untuk mencetak buku besar
    {
        if (isset($_POST['awal']) and isset($_POST['awal'])) {
            date_default_timezone_set('Asia/Jakarta');
            $awal   = $_POST['awal'];
            $akhir  = $_POST['akhir'];
            $penjualan = $this->M_labarugi->getpenjualan($awal, $akhir);
            $hapok     = $this->M_labarugi->getHapok($awal, $akhir);
            $repo      = $this->M_labarugi->getRePo($awal, $akhir);
            $biaya     = $this->M_labarugi->getBiaya($awal, $akhir);
            $pendlain     = $this->M_labarugi->getPendLain($awal, $akhir);


            $tgla = explode('-', $awal);
            $da = $tgla[2];
            $tglb = explode('-', $akhir);
            $db = $tglb[2];
            $periode = date('m-Y');
            $bln = $this->periode($periode);

            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AddPage();
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 15);
            $pdf->Cell(0, 0.75, 'LAPORAN LABA RUGI', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->SetFont('Helvetica', 'B', 11);
            $pdf->Cell(0, 0.75, 'Agromart', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148', 0, 0, 'C');
            $pdf->ln(5);
            $pdf->Cell(0, 0.75, 'Periode '  . $db . $bln, 0, 0, 'C');
            $pdf->Line(11, 35, 200, 35);


            $pdf->Ln(10);
            $pdf->Cell(1);
            $pdf->Ln(5);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(60, 8, 'Pendapatan Penjualan', 0, 0, 'L');
            foreach ($penjualan as $data) {
                $no = 1;
                $saldo = $data->K - $data->debet;
                $total = number_format($saldo, 0, ',', '.');
                $pdf->Ln(7);
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 11);
                $pdf->Cell(12, 8, $data->kode_akun . ' - ', 0, 0, 'L');
                $pdf->Cell(60, 8, $data->nama_akun, 0, 0, 'L');
                $pdf->Cell(35, 8, '', 0, 0, 'R');
                $pdf->Cell(11, 8, 'Rp. ', 0, 0, 'L');
                $pdf->Cell(25, 8, ' ' . $total, 0, 0, 'R');
            }
            $retpot = 0;
            foreach ($repo as $data) {
                $pdf->Ln(5);
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 11);
                $pdf->Cell(12, 8, $data->kode_akun . ' - ', 0, 0, 'L');
                $pdf->Cell(60, 8, $data->nama_akun, 0, 0, 'L');
                $pdf->Cell(11, 8, 'Rp. ', 0, 0, 'L');
                if ($no == 1) {
                    $no++;
                    $retpot = $data->D - $data->K;
                    $jumlah = $data->D - $data->K;
                    $total = number_format($jumlah, 0, ',', '.');
                    $pdf->Cell(20, 8, ' ' . $total, 0, 0, 'R');
                    $pdf->Cell(20, 8, '', 0, 0, 'R');
                } else {
                    $retpot = $retpot + $data->D - $data->K;
                    $jumlah = $data->D - $data->K;
                    $total = number_format($jumlah, 0, ',', '.');
                    $pdf->Cell(20, 8, ' ' . $total, 0, 0, 'R');
                    $pdf->Cell(20, 8, '', 0, 0, 'R');
                }
            }
            foreach ($hapok as $data) {
                $jumlah = $data->D - $data->K;
                $total = number_format($jumlah, 0, ',', '.');
                $pdf->Ln(5);
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 11);
                $pdf->Cell(12, 8, $data->kode_akun . ' - ', 0, 0, 'L');
                $pdf->Cell(60, 8, $data->nama_akun, 0, 0, 'L');
                $pdf->Cell(11, 8, 'Rp. ', 0, 0, 'L');
                $pdf->Cell(20, 8, ' ' . $total, 0, 0, 'R');
                $pdf->Cell(35, 8, '', 0, 0, 'R');
            }
            $labakotor = $saldo - ($jumlah + $retpot);
            $lk = number_format($labakotor, 0, ',', '.');
            $pdf->Ln(5);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(60, 8, 'Total Pendapatan Penjualan', 0, 0, 'L');
            $pdf->Cell(12, 8, '', 0, 0, 'L');
            $pdf->Cell(35, 8, ' ', 0, 0, 'R');
            $pdf->Cell(11, 10, 'Rp. ', 0, 0, 'L');
            $pdf->Cell(25, 8, ' ' . $lk, 0, 0, 'R');

            $pdf->Ln(8);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(60, 8, 'Pendapatan Lain', 0, 0, 'L');
            $pdf->Ln(2);
            foreach ($pendlain as $data) {
                $pend = $data->K - $data->D;
                $total = number_format($pend, 0, ',', '.');
                $pdf->Ln(5);
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 11);
                $pdf->Cell(12, 8, $data->kode_akun . ' - ', 0, 0, 'L');
                $pdf->Cell(60, 8, $data->nama_akun, 0, 0, 'L');
                $pdf->Cell(11, 8, 'Rp. ', 0, 0, 'L');
                $pdf->Cell(20, 8, ' ' . $total, 0, 0, 'R');
                $pdf->Cell(35, 8, '', 0, 0, 'R');
            }

            $pdf->Ln(6);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(60, 8, 'Total Pendapatan Lain', 0, 0, 'L');
            $pdf->Cell(12, 8, '', 0, 0, 'L');
            $pdf->Cell(35, 8, ' ', 0, 0, 'R');
            $pdf->Cell(11, 8, 'Rp. ', 0, 0, 'L');
            $pdf->Cell(25, 8, ' ' . $total, 0, 0, 'R');
            $pdf->Ln(8);

            $beban = 0;
            $pdf->Ln(5);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(60, 8, 'Beban Penjualan', 0, 0, 'L');
            $pdf->Ln(2);
            foreach ($biaya as $data) {
                $pdf->Ln(6);
                $pdf->Cell(1);
                $pdf->SetFont('Arial', '', 11);
                $pdf->Cell(12, 8, $data->kode_akun . ' - ', 0, 0, 'L');
                $pdf->Cell(60, 8, $data->nama_akun, 0, 0, 'L');
                $pdf->Cell(11, 8, 'Rp. ', 0, 0, 'L');

                if ($no == 1) {
                    $no++;
                    $beban  = $data->D - $data->K;
                    $saldo = $data->D - $data->K;
                    $total = number_format($saldo, 0, ',', '.');
                    $pdf->Cell(20, 8, ' ' . $total, 0, 0, 'R');
                    $pdf->Cell(35, 8, '', 0, 0, 'R');
                } else {

                    $beban = $beban + $data->D - $data->K;
                    $jumlah = $data->D - $data->K;

                    $total = number_format($jumlah, 0, ',', '.');
                    $pdf->Cell(20, 8, ' ' . $total, 0, 0, 'R');
                    $pdf->Cell(35, 8, '', 0, 0, 'R');
                }
            }
            $biaya = number_format($beban, 0, ',', '.');
            $pdf->Ln(6);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(60, 8, 'Total Biaya/Beban Penjualan', 0, 0, 'L');
            $pdf->Cell(12, 8, '', 0, 0, 'L');
            $pdf->Cell(35, 8, ' ', 0, 0, 'R');
            $pdf->Cell(11, 8, 'Rp. ', 0, 0, 'L');
            $pdf->Cell(25, 8, ' ' . $biaya, 0, 0, 'R');


            $laba = ($labakotor + $pend) - $beban;
            $total = number_format($laba, 0, ',', '.');
            $pdf->Ln(16);
            $pdf->Cell(1);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Cell(60, 8, 'Pendapatan Bersih', 0, 0, 'L');
            $pdf->Cell(12, 8, '', 0, 0, 'L');
            $pdf->Cell(35, 8, ' ', 0, 0, 'R');
            $pdf->Cell(11, 8, 'Rp. ', 0, 0, 'L');
            $pdf->Cell(25, 8, ' ' . $total, 0, 0, 'R');

            $pdf->Output("Laporan Laba Rugi.pdf", "I");
        } else {
            $data['title'] = "Access Denied";
            $this->load->view('404_accessdenied', $data);
        }
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class kategori extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_kategori');
		$this->load->library('form_validation');
		is_login();
	}

	public function index()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$data['kategori'] = $this->M_kategori->getAll();
			$data['title'] = 'Agromart - Kategori';
			$this->load->view('Template/Header', $data);
			$this->load->view('Kategori/View.php', $data);
			$this->load->view('Template/Footer');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function Add() //untuk menampilkan halaman form tambah kategori
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$data['title'] = 'Agromart - Tambah Kategori';
			$this->load->view('Template/Header', $data);
			$this->load->view('Kategori/Tambah.php', $data);
			$this->load->view('Template/Footer');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function store() //untuk mengirim data inputan user ke database dan hanya mengatur validasi saja selebihnya ada di model
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$kategori = $this->M_kategori;
			$validation = $this->form_validation;
			$validation->set_rules($kategori->rules());

			if ($validation->run()) {
				$kategori->save();
				$data['title'] = 'Argomart - Kategori';
				// $this->session->set_flashdata('success', 'Berhasil disimpan');
				$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			} else {
				$this->session->set_flashdata('failed', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
				<span class="badge badge-pill badge-danger">Gagal</span>
				Gagal Disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
				// $this->session->set_flashdata('failed', 'gagal disimpan');
			}
			redirect('/kategori');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function show($id = null) //untuk menuju ke halaman view Detail berdasarkan kategori yang dipilih
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			if (!isset($id)) {
				show_404();
			} else {
				if ($this->M_kategori->getById($id)) {
					$data['kodekategori'] = $this->M_kategori->createKode();
					$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
					$data['title'] = 'Middos Market - view Data Kategori';
					$this->load->view('Template/Header', $data);
					$this->load->view('Kategori/Detail.php', $data);
					$this->load->view('Template/Footer');
				}
			}
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function edit($id = null)
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			if (!isset($id)) {
				show_404();
			} else {
				if ($this->M_kategori->getById($id)) {
					$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
					$data['title'] = 'Middos Market - Edit Data Kategori';
					$data['getById'] = $this->M_kategori->getById($id);
					$this->load->view('Template/Header', $data);
					$this->load->view('Kategori/Edit.php', $data);
					$this->load->view('Template/Footer');
				}
			}
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function update($id = null)
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();

			if (!isset($id)) redirect('kategori');

			$kategori = $this->M_kategori;
			$validation = $this->form_validation;
			$validation->set_rules($kategori->rules());

			if ($validation->run()) {
				$kategori->M_update();
				$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			}

			$data["tb_kategori"] = $kategori->getById($id);
			if (!$data["tb_kategori"]) show_404();

			redirect('kategori');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function destroy($id = null)
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			if (!isset($id)) show_404();

			if ($this->M_kategori->delete($id)) {
				# code...
				redirect('/kategori');
			}
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}
}

/* End of file kategori.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('M_barang');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$data['barang'] = $this->M_barang->getAll();
			$data['kategori'] = $this->db->get('tb_katagori')->result();
			$data['satuan'] = $this->db->get('tb_satuan')->result();
			$data['title'] = 'Agromart - Barang';
			$this->load->view('Template/Header', $data);
			$this->load->view('Barang/View.php', $data);
			$this->load->view('Template/Footer');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function add()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$data['barang'] = $this->M_barang->getAll();
			$data['kategori'] = $this->db->get('tb_katagori')->result();
			$data['satuan'] = $this->db->get('tb_satuan')->result();
			$data['kodebarang'] = $this->M_barang->createKode();
			$data['title'] = 'Agromart - tambah barang';
			$this->load->view('Template/Header', $data);
			$this->load->view('Barang/Tambah', $data);
			$this->load->view('Template/Footer', $data);
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function store()
	{
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			$barang = $this->M_barang;
			$validation = $this->form_validation;
			$validation->set_rules($barang->rules());

			if ($validation->run()) {
				$barang->save();
				$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			} else {
				$this->session->set_flashdata('failed', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
				<span class="badge badge-pill badge-danger">Gagal</span>
				Gagal disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			}

			// echo ("<pre>");
			// print_r($this);
			// print_r($this->upload->display_errors());
			redirect('/barang/add');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	// public function show($id = null)
	// {
	// 	# code...
	// 	if (!isset($id)) {
	// 		show_404();
	// 	} else {
	// 		if ($this->M_barang->getById($id)) {
	// 			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
	// 			$data['title'] = 'Agromart - view Data Member';
	// 			$this->load->view('template/header', $data);
	// 			$this->load->view('Barang/Detail.php', $data);
	// 			$this->load->view('template/footer');
	// 		}
	// 	}
	// }

	public function update($id = null)
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();
			if (!isset($id)) redirect('barang');
			$Barang = $this->M_barang;
			$validation = $this->form_validation;
			$validation->set_rules($Barang->rules());

			if ($validation->run()) {
				// $tmpl = $this->input->post('image');
				// var_dump($tmpl);
				// die;

				$Barang->update();
				$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
				<span class="badge badge-pill badge-success">Berhasil</span>
				Berhasil diubah dan di simpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			} else {
				$this->session->set_flashdata('failed', '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
				<span class="badge badge-pill badge-danger">Gagal</span>
				Gagal disimpan.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				</div>');
			}

			$data["tb_barang"] = $Barang->getById($id);
			if (!$data["tb_barang"]) show_404();

			redirect('barang/');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function updateStok($id = null)
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			$data['user'] = $this->db->get_where('tb_user', ['username' => $this->session->userdata('username')])->row_array();

			if (!isset($id)) redirect('barang');

			$Barang = $this->M_barang;

			$Barang->tambahStock();
			$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
			<span class="badge badge-pill badge-success">Berhasil</span>
			Berhasil ditambah.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			</div>');


			$data["tb_barang"] = $Barang->getById($id);
			if (!$data["tb_barang"]) show_404();


			redirect('barang/');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function kurangiStok($id = null)
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			if (!isset($id)) redirect('barang');

			$barang = $this->M_barang;

			$barang->kurangStok();
			$this->session->set_flashdata('success', '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
			<span class="badge badge-pill badge-success">Berhasil</span>
			Berhasil ditambah.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			</div>');

			$data["tb_barang"] = $barang->getById($id);
			if (!$data['tb_barang']) show_404();

			redirect('barang/');
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}

	public function destroy($id = null)
	{
		# code...
		if ($this->session->userdata('id_lvl') == '1' || $this->session->userdata('id_lvl') == '5') {
			if (!isset($id)) show_404();
			if ($this->M_barang->delete($id)) {
				# code...
				redirect('/barang');
			}
		} else {
			$data['title'] = 'Error 403 Access Denied';
			$this->load->view('404_accessdenied', $data);
		}
	}
}

/* End of file Barang.php */

-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2019 at 08:32 AM
-- Server version: 10.0.38-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_agro`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_akun`
--

CREATE TABLE `tb_akun` (
  `kode_akun` int(5) NOT NULL,
  `nama_akun` varchar(200) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `kelompok` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_akun`
--

INSERT INTO `tb_akun` (`kode_akun`, `nama_akun`, `jenis`, `kelompok`) VALUES
(1111, 'Kas', 'DEBET', 'NRC'),
(1112, 'Bank', 'DEBET', 'NRC'),
(1121, 'Deposito di BPR', 'DEBET', 'NRC'),
(1131, 'Piutang Dagang', 'DEBET', 'NRC'),
(1132, 'Cadangan Kerugian Piutang', 'DEBET', 'NRC'),
(1133, 'Piutang Karyawan', 'DEBET', 'NRC'),
(1134, 'Piutang Lain-lain', 'DEBET', 'NRC'),
(1141, 'Persediaan Barang', 'DEBET', 'NRC'),
(1151, 'Persekot Biaya Perjalanan', 'DEBET', 'NRC'),
(1152, 'Persekot Biaya Asuransi', 'DEBET', 'NRC'),
(1153, 'Sewa Dibayar Dimuka', 'DEBET', 'NRC'),
(1201, 'Investasi Jangka Panjang', 'DEBET', 'NRC'),
(1311, 'Tanah', 'DEBET', 'NRC'),
(1312, 'Peralatan Toko', 'DEBET', 'NRC'),
(1313, 'Kendaraan', 'DEBET', 'NRC'),
(1322, 'Ak. Penyusutan Peralatan Toko', 'DEBET', 'NRC'),
(1323, 'Ak. Penyusutan Kendaraan', 'DEBET', 'NRC'),
(2101, 'Utang Dagang', 'KREDIT', 'NRC'),
(2102, 'Utang Gaji', 'KREDIT', 'NRC'),
(2103, 'Utang Jangka Pendek Bank', 'KREDIT', 'NRC'),
(2104, 'Utang Jangka Pendek Lain-lain', 'KREDIT', 'NRC'),
(2201, 'Utang Jangka Panjang dari Bank', 'KREDIT', 'NRC'),
(3001, 'Modal Pemilik', 'KREDIT', 'NRC'),
(3002, 'Prive Pemilik', 'DEBET', 'NRC'),
(3003, 'Laba Periode Berjalan', 'KREDIT', 'NRC'),
(4101, 'Penjualan', 'KREDIT', 'LR '),
(4201, 'Retur penjualan', 'DEBET', 'LR'),
(4202, 'Potongan Penjualan', 'DEBET', 'LR'),
(4203, 'Retur Pembelian', 'KREDIT', 'LR'),
(4204, 'Potongan Pembelian', 'KREDIT', 'LR'),
(5101, 'Harga Pokok Penjualan ', 'DEBET', 'LR'),
(5211, 'Biaya Gaji ', 'DEBET', 'LR '),
(5212, 'Biaya Pemasaran', 'DEBET', 'LR '),
(5213, 'Biaya Penyusutan Peralatan Toko', 'DEBET', 'LR '),
(5214, 'Biaya Pengiriman', 'DEBET', 'LR'),
(5215, 'Biaya Penjualan Rupa-rupa', 'DEBET', 'LR '),
(5216, 'Biaya Sewa', 'DEBET', 'LR '),
(5217, 'Biaya Penyusutan Kendaraan', 'DEBET', 'LR '),
(5218, 'Biaya Perjalanan', 'DEBET', 'LR '),
(5219, 'Biaya Asuransi', 'DEBET', 'LR '),
(5220, 'Biaya Perlengkapan Toko', 'DEBET', 'LR '),
(5221, 'Biaya Administrasi Rupa-rupa', 'DEBET', '  LR '),
(5222, 'Biaya Listrik, Telpon & Air', 'DEBET', 'LR'),
(5223, 'Biaya Lain-lain', 'DEBET', '  LR '),
(6101, 'Pendapatan Lain-lain', 'KREDIT', '  LR ');

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id_barang` int(10) NOT NULL,
  `Kode_barang` varchar(50) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `stok` int(5) NOT NULL,
  `hrg_beli` double NOT NULL,
  `persenan` double DEFAULT NULL,
  `persenangrosir` double DEFAULT NULL,
  `hrg_jual` int(100) NOT NULL,
  `hrg_jual_grosir` int(100) DEFAULT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `id_satuan` int(10) NOT NULL,
  `id_katagori` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_barang`
--

INSERT INTO `tb_barang` (`id_barang`, `Kode_barang`, `nama`, `stok`, `hrg_beli`, `persenan`, `persenangrosir`, `hrg_jual`, `hrg_jual_grosir`, `image`, `id_satuan`, `id_katagori`) VALUES
(30, '8993007005975', 'indomilk pisang', 11, 4000, 0, NULL, 5000, 4000, 'default.jpg', 5, 3),
(31, '711844160019', 'abc jeruk', 4, 3000, 0, NULL, 4000, 3000, 'default.jpg', 5, 3),
(32, 'ARGMRT-TTP-5976', 'kaoskakianakpolos', 10, 18000, 0, NULL, 20000, 18000, 'default.jpg', 8, 9),
(34, 'ARGMRT-TTP-5978', 'bandana warna ', 4, 12000, 0, NULL, 14000, 12000, 'default.jpg', 8, 9),
(35, 'ARGMRT-TTP-5979', 'Longdress M L XL s', 14, 125000, 0, NULL, 140000, 125000, 'default.jpg', 8, 9),
(36, 'ARGMRT-TTP-5980', 'daster allsize', 9, 110000, 0, NULL, 125000, 110000, 'default.jpg', 8, 9),
(37, 'ARGMRT-TTP-5981', 'longdressTpendek', 3, 120000, 0, NULL, 135000, 120000, 'default.jpg', 8, 9),
(38, 'ARGMRT-TTP-5982', 'babydoll', 2, 120000, 0, NULL, 135000, 120000, 'default.jpg', 8, 9),
(39, 'ARGMRT-TTP-5983', 'blusbatik', 8, 75000, 0, NULL, 90000, 75000, 'default.jpg', 8, 9),
(40, 'ARGMRT-TTP-5984', 'bahanbatik', 5, 75000, 0, NULL, 85000, 75000, 'default.jpg', 8, 9),
(41, 'ARGMRT-TTP-5985', 'dasterbatik', 2, 65000, 0, NULL, 75000, 65000, 'default.jpg', 8, 9),
(42, 'ARGMRT-TTP-5986', 'hemjeans', 6, 100000, 0, NULL, 120000, 100000, 'default.jpg', 8, 9),
(43, 'ARGMRT-TTP-5987', 'makaroniallvariant', 20, 8000, 0, NULL, 10000, 8000, 'default.jpg', 8, 7),
(44, 'ARGMRT-TTP-5988', 'kripikusus', 14, 10000, 0, NULL, 13500, 10000, 'default.jpg', 8, 11),
(45, 'ARGMRT-TTP-5989', 'suscoklat', 7, 12500, 0, NULL, 15000, 12500, 'default.jpg', 8, 10),
(46, 'ARGMRT-TTP-5990', 'donat', 7, 14500, 0, NULL, 17000, 14500, 'default.jpg', 8, 10),
(47, 'ARGMRT-TTP-5991', 'otakotak', 4, 17000, 0, NULL, 21000, 18000, 'default.jpg', 14, 10),
(48, 'ARGMRT-TTP-5992', 'sosisjumbo2', 5, 12000, 0, NULL, 16000, 13000, 'default.jpg', 14, 9),
(49, 'ARGMRT-TTP-5993', 'krupuk', 7, 5000, 0, NULL, 7000, 5000, 'default.jpg', 14, 9),
(50, 'ARGMRT-TTP-5994', 'tahutuna', 7, 11500, 0, NULL, 14000, 11500, 'default.jpg', 14, 10),
(51, 'ARGMRT-TTP-5995', 'pempek', 1, 25000, 0, NULL, 27500, 25000, 'default.jpg', 8, 10),
(55, 'ARGMRT-TTP-5998', 'kaoskakimotif', 7, 20000, 0, NULL, 22000, 20000, 'default.jpg', 8, 9),
(58, 'ARGMRT-TTP-6000', 'stiksusu', 7, 15000, 0, NULL, 17000, 15000, 'default.jpg', 5, 7),
(59, 'ARGMRT-TTP-6001', 'otakisi2', 1, 12000, 0, NULL, 14000, 12000, 'default.jpg', 8, 9),
(60, 'ARGMRT-TTP-6002', 'bakso', 7, 13000, 0, NULL, 15000, 13000, 'default.jpg', 5, 9),
(61, 'ARGMRT-TTP-6003', 'siomay', 8, 12000, 0, NULL, 14000, 12000, 'default.jpg', 5, 9),
(62, 'ARGMRT-TTP-6004', 'rolade', 5, 18000, 0, NULL, 20000, 18000, 'default.jpg', 5, 9),
(63, 'ARGMRT-TTP-6005', 'sososisi3', 1, 16000, 0, NULL, 24000, 16000, 'default.jpg', 8, 9),
(64, 'ARGMRT-TTP-6006', 'telurasinisi3', 8, 9000, 0, NULL, 10500, 9000, 'default.jpg', 8, 9),
(65, '8996006858269', 'fruittea', 0, 4000, 0, NULL, 5000, 4000, 'default.jpg', 8, 3),
(67, '8996006142511', 'tehsosro', 5, 3000, 0, NULL, 3500, 3000, 'default.jpg', 5, 3),
(68, 'ARGMRT-TTP-6007', 'herbaloloevera', 16, 7000, 0, NULL, 8000, 7000, 'default.jpg', 17, 3),
(69, 'ARGMRT-TTP-6008', 'kripik singkong', 60, 4000, 0, NULL, 5000, 4000, 'default.jpg', 5, 9),
(70, 'ARGMRT-TTP-6009', 'indomilk vanila', 3, 4000, 0, NULL, 5000, 4000, 'default.jpg', 14, 3),
(71, 'ARGMRT-TTP-6010', 'Dorokdok', 22, 7500, 0, NULL, 10000, 7500, 'default.jpg', 5, 7),
(72, 'ARGMRT-TTP-6011', 'kripikmerahputih', 7, 5000, 0, NULL, 6000, 5000, 'default.jpg', 8, 7),
(73, 'ARGMRT-TTP-6012', 'kerupukkecil', 13, 5000, 0, NULL, 7000, 5000, 'default.jpg', 5, 7),
(74, 'ARGMRT-TTP-6013', 'dorodokbesar', 7, 15000, 0, NULL, 18000, 15000, 'default.jpg', 5, 11),
(75, 'ARGMRT-TTP-6014', 'krupukplipitmerah', 4, 5000, 0, NULL, 7000, 5000, 'default.jpg', 8, 11),
(76, '8991102300421', 'tango kotak ', 6, 10000, 0, NULL, 12500, 10000, 'default.jpg', 14, 10),
(77, '8993175537346', 'Nabati Besar ', 11, 5500, 0, NULL, 6500, 5500, 'default.jpg', 14, 10),
(78, '8991102302609', 'tango kecil', 13, 4000, 0, NULL, 5000, 4000, 'default.jpg', 14, 10),
(79, 'ARGMRT-TTP-6015', 'salad buah 2', 14, 7000, 0, NULL, 8000, 7000, 'default.jpg', 5, 10),
(80, 'ARGMRT-TTP-6016', 'singkong balado ', 11, 8500, 0, NULL, 10000, 8500, 'default.jpg', 5, 10),
(82, '8994116101190', 'jus jambu', 10, 4000, 0, NULL, 5000, 4000, 'default.jpg', 8, 3),
(83, '8992696520400', 'nescafe', 4, 3500, 0, NULL, 4500, 3500, 'default.jpg', 8, 3),
(84, 'ARGMRT-TTP-6018', 'rengginang', 18, 12000, 0, NULL, 13000, 12000, 'default.jpg', 5, 7),
(85, 'ARGMRT-TTP-6019', 'pedas makaroni', 11, 8000, 0, NULL, 12000, 8000, 'default.jpg', 5, 10),
(86, 'ARGMRT-TTP-6020', 'nasgon besar', 10, 8500, 0, NULL, 10000, 8500, 'default.jpg', 5, 7),
(87, 'ARGMRT-TTP-6021', 'nasgon kecil', 3, 4500, 0, NULL, 6000, 4500, 'default.jpg', 5, 7),
(89, 'ARGMRT-TTP-6023', 'kacang thailand', 17, 18000, 0, NULL, 20000, 18000, 'default.jpg', 5, 7),
(90, 'ARGMRT-TTP-6024', 'Rambak Lele', 7, 15000, 0, NULL, 17500, 15000, 'default.jpg', 5, 9),
(91, 'ARGMRT-TTP-6025', 'abon', 2, 72000, 0, NULL, 75000, 72000, 'default.jpg', 5, 10),
(92, 'ARGMRT-TTP-6026', 'srundeng', 19, 21000, 0, NULL, 23000, 21000, 'default.jpg', 5, 10),
(93, 'ARGMRT-TTP-6027', 'batik kotak', 6, 120000, 0, NULL, 125000, 120000, 'default.jpg', 5, 10),
(94, 'ARGMRT-TTP-6028', 'daster bunga non bordir', 4, 50000, 0, NULL, 65000, 50000, 'default.jpg', 8, 9),
(95, 'ARGMRT-TTP-6029', 'speaker alquran', 2, 200000, 0, NULL, 225000, 200000, 'default.jpg', 4, 9),
(97, 'ARGMRT-TTP-6031', 'baladokecil', 4, 6000, 0, NULL, 8000, 6000, 'default.jpg', 5, 7),
(98, 'ARGMRT-TTP-6032', 'stikbalado', 0, 6000, NULL, NULL, 8000, 6000, 'default.jpg', 5, 7),
(100, 'ARGMRT-TTP-6034', 'aqua 600ml', 22, 2000, 0, NULL, 3000, 2000, 'default.jpg', 17, 3),
(103, 'ARGMRT-TTP-6037', 'Semprit', 5, 45000, 0, NULL, 50000, 45000, 'default.jpg', 5, 9),
(104, 'ARGMRT-TTP-6038', 'castangle 500 gr', 4, 70000, 0, NULL, 75000, 70000, 'default.jpg', 5, 9),
(105, 'ARGMRT-TTP-6039', 'pie Nanas', 3, 15000, 0, NULL, 17500, 15000, 'default.jpg', 5, 9),
(107, 'ARGMRT-TTP-6041', 'salju blinjo', 11, 60000, 0, NULL, 65000, 60000, 'default.jpg', 5, 9),
(108, 'ARGMRT-TTP-6042', 'nastar Bintangbulat', 10, 65000, 0, NULL, 70000, 65000, 'default.jpg', 5, 9),
(109, 'ARGMRT-TTP-6043', 'castangle 250', 4, 35000, 0, NULL, 37500, 35000, 'default.jpg', 5, 9),
(110, 'ARGMRT-TTP-6044', 'Nugke besar', 18, 40000, 0, NULL, 45000, 40000, 'default.jpg', 8, 9),
(111, 'ARGMRT-TTP-6045', 'nugke kecil', 10, 30000, 0, NULL, 35000, 30000, 'default.jpg', 8, 9),
(112, 'ARGMRT-TTP-6046', 'Tas Nugkebesar', 16, 50000, 0, NULL, 55000, 50000, 'default.jpg', 8, 9),
(113, 'ARGMRT-TTP-6047', 'Nugke dompet tipis', 12, 20000, 0, NULL, 25000, 20000, 'default.jpg', 8, 9),
(115, 'ARGMRT-TTP-6048', 'beras merah', 5, 15000, 0, NULL, 17500, 15000, 'default.jpg', 5, 9),
(116, 'ARGMRT-TTP-6049', 'beras salmon', 5, 52000, 0, NULL, 57000, 52000, 'default.jpg', 5, 9),
(117, 'ARGMRT-TTP-6050', 'ayam ungkep', 3, 37500, 0, NULL, 40000, 37500, 'default.jpg', 5, 9),
(118, 'ARGMRT-TTP-6051', 'okey stik bulat', 3, 17500, 0, NULL, 20000, 17500, 'default.jpg', 5, 9),
(119, 'ARGMRT-TTP-6052', 'stikmendol', 18, 10000, 0, NULL, 12000, 10000, 'default.jpg', 5, 7),
(120, 'ARGMRT-TTP-6053', 'kripikmenjes', 19, 13000, 0, NULL, 15000, 13000, 'default.jpg', 5, 9),
(121, 'ARGMRT-TTP-6054', 'Goplem', 8, 13000, 0, NULL, 15000, 13000, 'default.jpg', 5, 9),
(122, 'ARGMRT-TTP-6055', 'sambalgoreng', 5, 20000, 0, NULL, 22000, 20000, 'default.jpg', 5, 7),
(123, 'ARGMRT-TTP-6056', 'teri krispi', 9, 20000, 0, NULL, 22000, 20000, 'default.jpg', 5, 9),
(124, 'ARGMRT-TTP-6057', 'oreocheescake', 9, 9000, 0, NULL, 10000, 9000, 'default.jpg', 5, 9),
(125, 'ARGMRT-TTP-6058', 'sate', 7, 35000, 0, NULL, 37000, 35000, 'default.jpg', 8, 9),
(126, 'ARGMRT-TTP-6059', 'champchik nug', 4, 14500, 0, NULL, 17000, 14500, 'default.jpg', 14, 9),
(127, 'ARGMRT-TTP-6060', 'champ soziz 375', 1, 27000, 0, NULL, 29000, 27000, 'default.jpg', 14, 9),
(128, 'ARGMRT-TTP-6061', 'Indahsalad', 0, 15000, 0, NULL, 16000, 15000, 'default.jpg', 5, 9),
(129, 'ARGMRT-TTP-6062', 'susu', 0, 7000, NULL, NULL, 8000, 7000, 'default.jpg', 5, 9),
(130, 'ARGMRT-TTP-6063', 'dessert', 0, 10000, NULL, NULL, 12000, 10000, 'default.jpg', 5, 9),
(131, 'ARGMRT-TTP-6064', 'icecream', 4, 5000, 0, NULL, 6000, 5000, 'default.jpg', 5, 9),
(132, 'ARGMRT-TTP-6065', 'susu', 0, 7000, NULL, NULL, 8000, 7000, 'default.jpg', 5, 9),
(133, 'ARGMRT-TTP-6066', 'PasteriusasiSusu', 0, 7000, 0, NULL, 8000, 7000, 'default.jpg', 5, 4),
(134, 'ARGMRT-TTP-6067', 'icecreambesar', 5, 25000, 0, NULL, 26000, 25000, 'default.jpg', 5, 9),
(135, 'ARGMRT-TTP-6068', 'ayamkampung', 2, 40000, 0, NULL, 42000, 40000, 'default.jpg', 5, 9),
(136, 'ARGMRT-TTP-6069', 'yakult', 8, 7600, 0, NULL, 9000, 7600, 'default.jpg', 5, 3),
(137, 'ARGMRT-TTP-6070', 'big salad', 0, 25000, 0, NULL, 27000, 25000, 'default.jpg', 5, 9),
(138, 'ARGMRT-TTP-6071', 'kia puding', 10, 19000, 0, NULL, 21500, 19000, 'default.jpg', 5, 9);

-- --------------------------------------------------------

--
-- Table structure for table `tb_barangtitip`
--

CREATE TABLE `tb_barangtitip` (
  `id_barangtitip` int(10) NOT NULL,
  `Kode_barangtitip` varchar(20) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `hrg_perbiji` int(25) NOT NULL,
  `total_jual` int(25) NOT NULL,
  `stok_barangtitip` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_pembelian`
--

CREATE TABLE `tb_detail_pembelian` (
  `id` double NOT NULL,
  `id_pembelian` varchar(15) NOT NULL,
  `Kode_barang` varchar(50) NOT NULL,
  `harga_beli` double NOT NULL,
  `harga_jual` double NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` double NOT NULL,
  `persenan` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detail_pembelian`
--

INSERT INTO `tb_detail_pembelian` (`id`, `id_pembelian`, `Kode_barang`, `harga_beli`, `harga_jual`, `jumlah`, `total`, `persenan`) VALUES
(53, 'BL041219000001', 'ARGMRT-TTP-5976', 18000, 20000, 11, 198000, NULL),
(54, 'BL041219000001', 'ARGMRT-TTP-5998', 20000, 22000, 8, 160000, NULL),
(55, 'BL041219000001', 'ARGMRT-TTP-5978', 12000, 14000, 4, 48000, NULL),
(56, 'BL041219000002', 'ARGMRT-TTP-5986', 100000, 120000, 6, 600000, NULL),
(57, 'BL041219000002', 'ARGMRT-TTP-5987', 8000, 10000, 20, 160000, NULL),
(58, 'BL041219000003', 'ARGMRT-TTP-5980', 110000, 125000, 10, 1100000, NULL),
(59, 'BL041219000003', 'ARGMRT-TTP-5979', 125000, 140000, 14, 1750000, NULL),
(60, 'BL041219000003', 'ARGMRT-TTP-5981', 120000, 135000, 3, 360000, NULL),
(61, 'BL041219000003', 'ARGMRT-TTP-5983', 75000, 90000, 10, 750000, NULL),
(62, 'BL041219000003', 'ARGMRT-TTP-5982', 120000, 135000, 3, 360000, NULL),
(63, 'BL041219000003', 'ARGMRT-TTP-5984', 75000, 85000, 5, 375000, NULL),
(64, 'BL041219000003', 'ARGMRT-TTP-5985', 65000, 75000, 3, 195000, NULL),
(65, 'BL041219000003', 'ARGMRT-TTP-5996', 6000, 8000, 35, 210000, NULL),
(66, 'BL041219000003', '8993007005975', 4000, 5000, 6, 24000, NULL),
(67, 'BL041219000004', 'ARGMRT-TTP-5988', 10000, 13500, 18, 180000, NULL),
(68, 'BL041219000004', 'ARGMRT-TTP-5990', 14500, 17000, 13, 188500, NULL),
(69, 'BL041219000004', 'ARGMRT-TTP-5992', 12000, 16000, 5, 60000, NULL),
(70, 'BL041219000004', 'ARGMRT-TTP-6001', 12000, 14000, 1, 12000, NULL),
(71, 'BL041219000004', 'ARGMRT-TTP-6005', 16000, 24000, 1, 16000, NULL),
(72, 'BL041219000004', 'ARGMRT-TTP-5993', 5000, 7000, 8, 40000, NULL),
(73, 'BL041219000004', 'ARGMRT-TTP-5994', 11500, 14000, 10, 115000, NULL),
(74, 'BL041219000004', 'ARGMRT-TTP-5995', 25000, 27500, 1, 25000, NULL),
(75, 'BL041219000004', 'ARGMRT-TTP-6006', 9000, 10500, 10, 90000, NULL),
(76, 'BL041219000004', 'ARGMRT-TTP-5989', 12500, 15000, 8, 100000, NULL),
(77, 'BL041219000004', 'ARGMRT-TTP-5991', 17000, 21000, 6, 102000, NULL),
(78, 'BL041219000005', '711844160019', 3000, 4000, 4, 12000, NULL),
(79, 'BL041219000006', 'ARGMRT-TTP-6003', 12000, 14000, 11, 132000, NULL),
(80, 'BL041219000006', 'ARGMRT-TTP-6002', 13000, 15000, 8, 104000, NULL),
(81, 'BL041219000006', 'ARGMRT-TTP-6004', 18000, 20000, 6, 108000, NULL),
(82, 'BL041219000007', 'ARGMRT-TTP-6000', 15000, 17000, 8, 120000, NULL),
(83, 'BL041219000008', '8996006858269', 4000, 5000, 3, 12000, NULL),
(84, 'BL051219000001', '8993007005975', 4000, 5000, 6, 24000, NULL),
(85, 'BL051219000002', '8996006142511', 3000, 3500, 10, 30000, NULL),
(86, 'BL051219000003', 'ARGMRT-TTP-6007', 7000, 8000, 20, 140000, NULL),
(87, 'BL051219000004', 'ARGMRT-TTP-6008', 4000, 5000, 60, 240000, NULL),
(88, 'BL061219000001', 'ARGMRT-TTP-6009', 4000, 5000, 6, 24000, NULL),
(89, 'BL061219000002', 'ARGMRT-TTP-6010', 7500, 10000, 24, 180000, NULL),
(90, 'BL061219000003', 'ARGMRT-TTP-6013', 15000, 18000, 10, 150000, NULL),
(91, 'BL061219000003', 'ARGMRT-TTP-6011', 5000, 6000, 8, 40000, NULL),
(92, 'BL061219000003', 'ARGMRT-TTP-6012', 5000, 7000, 16, 80000, NULL),
(93, 'BL061219000004', 'ARGMRT-TTP-6014', 5000, 7000, 6, 30000, NULL),
(94, 'BL091219000001', 'ARGMRT-TTP-6015', 7000, 8000, 34, 238000, NULL),
(95, 'BL091219000002', '8994116101190', 4000, 5000, 10, 40000, NULL),
(96, 'BL091219000002', '8992696520400', 3500, 4500, 4, 14000, NULL),
(97, 'BL091219000002', '8991102300421', 10000, 12500, 6, 60000, NULL),
(98, 'BL091219000002', '8991102302609', 4000, 5000, 13, 52000, NULL),
(99, 'BL091219000002', '8993175537346', 5500, 6500, 12, 66000, NULL),
(100, 'BL091219000002', 'ARGMRT-TTP-6016', 8500, 10000, 12, 102000, NULL),
(101, 'BL091219000003', 'ARGMRT-TTP-6018', 12000, 13000, 22, 264000, NULL),
(102, 'BL091219000004', 'ARGMRT-TTP-6020', 8500, 10000, 11, 93500, NULL),
(103, 'BL091219000004', 'ARGMRT-TTP-6021', 4500, 6000, 3, 13500, NULL),
(104, 'BL091219000005', 'ARGMRT-TTP-6019', 8000, 12000, 12, 96000, NULL),
(105, 'BL091219000006', 'ARGMRT-TTP-6023', 18000, 20000, 20, 360000, NULL),
(106, 'BL101219000001', 'ARGMRT-TTP-6027', 120000, 125000, 6, 720000, NULL),
(107, 'BL101219000001', 'ARGMRT-TTP-6026', 21000, 23000, 20, 420000, NULL),
(108, 'BL101219000001', 'ARGMRT-TTP-6025', 72000, 75000, 3, 216000, NULL),
(109, 'BL101219000002', 'ARGMRT-TTP-6024', 15000, 17500, 10, 150000, NULL),
(110, 'BL101219000003', 'ARGMRT-TTP-6028', 50000, 65000, 4, 200000, NULL),
(111, 'BL101219000004', 'ARGMRT-TTP-6031', 6000, 8000, 4, 24000, NULL),
(112, 'BL101219000005', 'ARGMRT-TTP-6034', 2000, 3000, 24, 48000, NULL),
(113, 'BL101219000006', 'ARGMRT-TTP-6029', 200000, 225000, 2, 400000, NULL),
(114, 'BL101219000007', 'ARGMRT-TTP-6042', 65000, 70000, 11, 715000, NULL),
(115, 'BL101219000007', 'ARGMRT-TTP-6041', 60000, 65000, 11, 660000, NULL),
(116, 'BL101219000007', 'ARGMRT-TTP-6037', 45000, 50000, 5, 225000, NULL),
(117, 'BL101219000007', 'ARGMRT-TTP-6039', 15000, 17500, 7, 105000, NULL),
(118, 'BL101219000007', 'ARGMRT-TTP-6040', 35000, 37500, 5, 175000, NULL),
(119, 'BL101219000008', 'ARGMRT-TTP-6038', 70000, 75000, 4, 280000, NULL),
(120, 'BL101219000009', 'ARGMRT-TTP-6043', 35000, 37500, 4, 140000, NULL),
(121, 'BL101219000010', 'ARGMRT-TTP-6044', 40000, 45000, 18, 720000, NULL),
(122, 'BL101219000010', 'ARGMRT-TTP-6045', 30000, 35000, 10, 300000, NULL),
(123, 'BL101219000010', 'ARGMRT-TTP-6046', 50000, 55000, 16, 800000, NULL),
(124, 'BL101219000011', 'ARGMRT-TTP-6047', 20000, 25000, 12, 240000, NULL),
(125, 'BL101219000012', 'ARGMRT-TTP-6049', 52000, 57000, 5, 260000, NULL),
(126, 'BL101219000012', 'ARGMRT-TTP-6048', 15000, 17500, 5, 75000, NULL),
(127, 'BL111219000001', 'ARGMRT-TTP-6050', 37500, 40000, 6, 225000, NULL),
(128, 'BL111219000002', 'ARGMRT-TTP-6051', 17500, 20000, 3, 52500, NULL),
(129, 'BL111219000003', 'ARGMRT-TTP-6052', 10000, 12000, 20, 200000, NULL),
(130, 'BL111219000003', 'ARGMRT-TTP-6053', 13000, 15000, 20, 260000, NULL),
(131, 'BL111219000003', 'ARGMRT-TTP-6054', 13000, 15000, 9, 117000, NULL),
(132, 'BL111219000003', 'ARGMRT-TTP-6055', 20000, 22000, 7, 140000, NULL),
(133, 'BL111219000004', 'ARGMRT-TTP-6056', 20000, 22000, 10, 200000, NULL),
(134, 'BL111219000005', 'ARGMRT-TTP-6057', 9000, 10000, 12, 108000, NULL),
(135, 'BL111219000006', 'ARGMRT-TTP-6058', 35000, 37000, 8, 280000, NULL),
(136, 'BL111219000007', 'ARGMRT-TTP-6059', 14500, 17000, 4, 58000, NULL),
(137, 'BL111219000008', 'ARGMRT-TTP-6060', 27000, 29000, 1, 27000, NULL),
(138, 'BL111219000009', 'ARGMRT-TTP-6061', 15000, 16000, 3, 45000, NULL),
(139, 'BL111219000009', 'ARGMRT-TTP-6064', 5000, 6000, 8, 40000, NULL),
(140, 'BL111219000010', 'ARGMRT-TTP-6066', 7000, 8000, 11, 77000, NULL),
(141, 'BL111219000011', 'ARGMRT-TTP-6067', 25000, 26000, 6, 150000, NULL),
(142, 'BL111219000011', 'ARGMRT-TTP-6068', 40000, 42000, 4, 160000, NULL),
(143, 'BL111219000012', 'ARGMRT-TTP-6069', 7600, 9000, 10, 76000, NULL),
(144, 'BL111219000013', 'ARGMRT-TTP-6070', 25000, 27000, 1, 25000, NULL),
(145, 'BL111219000014', 'ARGMRT-TTP-6071', 19000, 21500, 10, 190000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_penjualan`
--

CREATE TABLE `tb_detail_penjualan` (
  `id` double NOT NULL,
  `no_faktur` varchar(25) NOT NULL,
  `tgl_jual` date NOT NULL,
  `Kode_barang` varchar(50) NOT NULL,
  `nama` varchar(10) NOT NULL,
  `jumlah` int(15) NOT NULL,
  `satuan` varchar(255) NOT NULL,
  `harga` int(15) NOT NULL,
  `diskon` double NOT NULL,
  `sub_total` int(15) NOT NULL,
  `hrg_pokok` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detail_penjualan`
--

INSERT INTO `tb_detail_penjualan` (`id`, `no_faktur`, `tgl_jual`, `Kode_barang`, `nama`, `jumlah`, `satuan`, `harga`, `diskon`, `sub_total`, `hrg_pokok`) VALUES
(7, '0412190001', '0000-00-00', 'ARGMRT-TTP-5996', 'salad ', 11, 'Bks', 8000, 0, 88000, 6000),
(8, '0412190001', '0000-00-00', 'ARGMRT-TTP-5988', 'kripikusus', 1, 'Pcs', 13500, 0, 13500, 10000),
(9, '0412190001', '0000-00-00', 'ARGMRT-TTP-5991', 'otakotak', 1, 'Pak', 21000, 0, 21000, 17000),
(10, '0412190002', '0000-00-00', '8996006858269', 'fruittea', 1, 'Pcs', 5000, 0, 5000, 4000),
(11, '0412190003', '0000-00-00', 'ARGMRT-TTP-5996', 'salad ', 3, 'Bks', 8000, 0, 24000, 6000),
(12, '0412190004', '0000-00-00', 'ARGMRT-TTP-6000', 'stiksusu', 1, 'Bks', 17000, 0, 17000, 15000),
(13, '0512190001', '0000-00-00', 'ARGMRT-TTP-5993', 'krupuk', 1, 'Pak', 7000, 0, 7000, 5000),
(14, '0512190002', '0000-00-00', 'ARGMRT-TTP-6002', 'bakso', 1, 'Bks', 15000, 0, 15000, 13000),
(15, '0512190003', '0000-00-00', 'ARGMRT-TTP-5996', 'salad ', 2, 'Bks', 8000, 0, 16000, 6000),
(16, '0512190003', '0000-00-00', 'ARGMRT-TTP-5988', 'kripikusus', 1, 'Pcs', 13500, 0, 13500, 10000),
(17, '0512190004', '0000-00-00', 'ARGMRT-TTP-5996', 'salad ', 1, 'Bks', 8000, 0, 8000, 6000),
(18, '0512190005', '0000-00-00', 'ARGMRT-TTP-5996', 'salad ', 2, 'Bks', 8000, 0, 16000, 6000),
(19, '0512190006', '0000-00-00', 'ARGMRT-TTP-5996', 'salad ', 1, 'Bks', 8000, 0, 8000, 6000),
(20, '0512190008', '0000-00-00', 'ARGMRT-TTP-5996', 'salad ', 1, 'Bks', 8000, 0, 8000, 6000),
(21, '0512190009', '0000-00-00', 'ARGMRT-TTP-5996', 'salad ', 2, 'Bks', 8000, 0, 16000, 6000),
(22, '0612190001', '0000-00-00', 'ARGMRT-TTP-6009', 'indomilk v', 1, 'Pak', 5000, 0, 5000, 4000),
(23, '0612190002', '0000-00-00', '8996006142511', 'tehsosro', 3, 'Bks', 3500, 0, 10500, 3000),
(24, '0612190003', '0000-00-00', 'ARGMRT-TTP-6013', 'dorodokbes', 1, 'Bks', 18000, 0, 18000, 15000),
(25, '0612190004', '0000-00-00', 'ARGMRT-TTP-5996', 'salad ', 1, 'Bks', 8000, 0, 8000, 6000),
(26, '0612190005', '0000-00-00', 'ARGMRT-TTP-5994', 'tahutuna', 1, 'Pak', 14000, 0, 14000, 11500),
(27, '0612190006', '0000-00-00', 'ARGMRT-TTP-5990', 'donat', 1, 'Pcs', 17000, 0, 17000, 14500),
(28, '0912190001', '0000-00-00', 'ARGMRT-TTP-6009', 'indomilk v', 1, 'Pak', 5000, 0, 5000, 4000),
(29, '0912190001', '0000-00-00', '8993175537346', 'Nabati Bes', 1, 'Pak', 6500, 0, 6500, 5500),
(30, '0912190002', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 1, 'Bks', 8000, 0, 8000, 7000),
(31, '0912190003', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 1, 'Bks', 8000, 0, 8000, 7000),
(32, '0912190004', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 1, 'Bks', 8000, 0, 8000, 7000),
(33, '0912190005', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 1, 'Bks', 8000, 0, 8000, 7000),
(34, '1012190001', '0000-00-00', 'ARGMRT-TTP-6009', 'indomilk v', 1, 'Pak', 5000, 0, 5000, 4000),
(35, '1012190001', '0000-00-00', 'ARGMRT-TTP-6019', 'pedas maka', 1, 'Bks', 12000, 0, 12000, 8000),
(36, '1012190002', '0000-00-00', '8993007005975', 'indomilk p', 1, 'Bks', 5000, 0, 5000, 4000),
(37, '1012190002', '0000-00-00', '8996006858269', 'fruittea', 1, 'Pcs', 5000, 0, 5000, 4000),
(38, '1012190003', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 9, 'Bks', 8000, 0, 72000, 7000),
(39, '1012190004', '0000-00-00', 'ARGMRT-TTP-6003', 'siomay', 1, 'Bks', 14000, 0, 14000, 12000),
(40, '1012190005', '0000-00-00', 'ARGMRT-TTP-6003', 'siomay', 1, 'Bks', 14000, 0, 14000, 12000),
(41, '1012190006', '0000-00-00', 'ARGMRT-TTP-6016', 'singkong b', 1, 'Bks', 10000, 0, 10000, 8500),
(42, '1112190001', '0000-00-00', 'ARGMRT-TTP-5990', 'donat', 1, 'Pcs', 17000, 0, 17000, 14500),
(43, '1112190001', '0000-00-00', 'ARGMRT-TTP-6042', 'nastar Bin', 1, 'Bks', 70000, 0, 70000, 65000),
(44, '1112190001', '0000-00-00', 'ARGMRT-TTP-6024', 'Rambak Lel', 2, 'Bks', 17500, 0, 35000, 15000),
(45, '1112190001', '0000-00-00', 'ARGMRT-TTP-6012', 'kerupukkec', 2, 'Bks', 7000, 0, 14000, 5000),
(46, '1112190001', '0000-00-00', 'ARGMRT-TTP-6018', 'rengginang', 4, 'Bks', 13000, 0, 52000, 12000),
(47, '1112190001', '0000-00-00', 'ARGMRT-TTP-6006', 'telurasini', 1, 'Pcs', 10500, 0, 10500, 9000),
(48, '1112190001', '0000-00-00', 'ARGMRT-TTP-6010', 'Dorokdok', 2, 'Bks', 10000, 0, 20000, 7500),
(49, '1112190002', '0000-00-00', 'ARGMRT-TTP-6024', 'Rambak Lel', 1, 'Bks', 17500, 0, 17500, 15000),
(50, '1112190002', '0000-00-00', 'ARGMRT-TTP-6055', 'sambalgore', 1, 'Bks', 22000, 0, 22000, 20000),
(51, '1112190003', '0000-00-00', 'ARGMRT-TTP-6057', 'oreocheesc', 3, 'Bks', 10000, 0, 30000, 9000),
(52, '1112190003', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 2, 'Bks', 8000, 0, 16000, 7000),
(53, '1112190003', '0000-00-00', 'ARGMRT-TTP-6061', 'Indahsalad', 1, 'Bks', 16000, 0, 16000, 15000),
(54, '1112190003', '0000-00-00', 'ARGMRT-TTP-6064', 'icecream', 1, 'Bks', 6000, 0, 6000, 5000),
(55, '1112190003', '0000-00-00', 'ARGMRT-TTP-6007', 'herbaloloe', 1, 'Btl', 8000, 0, 8000, 7000),
(56, '1112190003', '0000-00-00', 'ARGMRT-TTP-6053', 'kripikmenj', 1, 'Bks', 15000, 0, 15000, 13000),
(57, '1112190003', '0000-00-00', 'ARGMRT-TTP-5988', 'kripikusus', 1, 'Pcs', 13500, 0, 13500, 10000),
(58, '1112190004', '0000-00-00', 'ARGMRT-TTP-6067', 'icecreambe', 1, 'Bks', 26000, 0, 26000, 25000),
(59, '1112190004', '0000-00-00', 'ARGMRT-TTP-5990', 'donat', 1, 'Pcs', 17000, 0, 17000, 14500),
(60, '1112190004', '0000-00-00', 'ARGMRT-TTP-6068', 'ayamkampun', 1, 'Bks', 42000, 0, 42000, 40000),
(61, '1112190005', '0000-00-00', 'ARGMRT-TTP-6020', 'nasgon bes', 1, 'Bks', 10000, 0, 10000, 8500),
(62, '1112190006', '0000-00-00', 'ARGMRT-TTP-6050', 'ayam ungke', 1, 'Bks', 40000, 0, 40000, 37500),
(63, '1112190007', '0000-00-00', 'ARGMRT-TTP-6066', 'Pasteriusa', 1, 'Bks', 8000, 0, 8000, 7000),
(64, '1112190007', '0000-00-00', 'ARGMRT-TTP-6007', 'herbaloloe', 1, 'Btl', 8000, 0, 8000, 7000),
(65, '1112190007', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 1, 'Bks', 8000, 0, 8000, 7000),
(66, '1112190008', '0000-00-00', 'ARGMRT-TTP-6052', 'stikmendol', 2, 'Bks', 12000, 0, 24000, 10000),
(67, '1112190008', '0000-00-00', 'ARGMRT-TTP-6066', 'Pasteriusa', 1, 'Bks', 8000, 0, 8000, 7000),
(68, '1112190008', '0000-00-00', 'ARGMRT-TTP-6068', 'ayamkampun', 1, 'Bks', 42000, 0, 42000, 40000),
(69, '1112190008', '0000-00-00', 'ARGMRT-TTP-5990', 'donat', 1, 'Pcs', 17000, 0, 17000, 14500),
(70, '1112190008', '0000-00-00', 'ARGMRT-TTP-6025', 'abon', 1, 'Bks', 75000, 0, 75000, 72000),
(71, '1112190009', '0000-00-00', 'ARGMRT-TTP-6050', 'ayam ungke', 1, 'Bks', 40000, 0, 40000, 37500),
(72, '1112190010', '0000-00-00', 'ARGMRT-TTP-5985', 'dasterbati', 1, 'Pcs', 75000, 0, 75000, 65000),
(73, '1112190011', '0000-00-00', 'ARGMRT-TTP-6055', 'sambalgore', 1, 'Bks', 22000, 0, 22000, 20000),
(74, '1112190011', '0000-00-00', 'ARGMRT-TTP-6023', 'kacang tha', 2, 'Bks', 20000, 0, 40000, 18000),
(75, '1112190011', '0000-00-00', 'ARGMRT-TTP-6026', 'srundeng', 1, 'Bks', 23000, 0, 23000, 21000),
(76, '1112190011', '0000-00-00', 'ARGMRT-TTP-5990', 'donat', 2, 'Pcs', 17000, 0, 34000, 14500),
(77, '1112190011', '0000-00-00', 'ARGMRT-TTP-5994', 'tahutuna', 1, 'Pak', 14000, 0, 14000, 11500),
(78, '1112190012', '0000-00-00', 'ARGMRT-TTP-6061', 'Indahsalad', 2, 'Bks', 16000, 0, 32000, 15000),
(79, '1112190012', '0000-00-00', 'ARGMRT-TTP-6066', 'Pasteriusa', 8, 'Bks', 8000, 0, 64000, 7000),
(80, '1112190012', '0000-00-00', 'ARGMRT-TTP-6054', 'Goplem', 1, 'Bks', 15000, 0, 15000, 13000),
(81, '1112190012', '0000-00-00', 'ARGMRT-TTP-6004', 'rolade', 1, 'Bks', 20000, 0, 20000, 18000),
(82, '1112190012', '0000-00-00', 'ARGMRT-TTP-5991', 'otakotak', 1, 'Pak', 21000, 0, 21000, 17000),
(83, '1112190013', '0000-00-00', 'ARGMRT-TTP-6058', 'sate', 1, 'Pcs', 37000, 0, 37000, 35000),
(84, '1112190013', '0000-00-00', 'ARGMRT-TTP-5982', 'babydoll', 1, 'Pcs', 135000, 0, 135000, 120000),
(85, '1112190013', '0000-00-00', 'ARGMRT-TTP-6066', 'Pasteriusa', 1, 'Bks', 8000, 0, 8000, 7000),
(86, '1112190014', '0000-00-00', 'ARGMRT-TTP-6003', 'siomay', 1, 'Bks', 14000, 0, 14000, 12000),
(87, '1112190014', '0000-00-00', 'ARGMRT-TTP-5994', 'tahutuna', 1, 'Pak', 14000, 0, 14000, 11500),
(88, '1112190015', '0000-00-00', 'ARGMRT-TTP-6013', 'dorodokbes', 1, 'Bks', 18000, 0, 18000, 15000),
(89, '1112190015', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 1, 'Bks', 8000, 0, 8000, 7000),
(90, '1112190015', '0000-00-00', 'ARGMRT-TTP-6064', 'icecream', 1, 'Bks', 6000, 0, 6000, 5000),
(91, '1112190015', '0000-00-00', 'ARGMRT-TTP-6069', 'yakult', 1, 'Bks', 9000, 0, 9000, 7600),
(92, '1112190015', '0000-00-00', 'ARGMRT-TTP-5976', 'kaoskakian', 1, 'Pcs', 20000, 0, 20000, 18000),
(93, '1112190016', '0000-00-00', 'ARGMRT-TTP-6064', 'icecream', 2, 'Bks', 6000, 0, 12000, 5000),
(94, '1112190016', '0000-00-00', '8996006858269', 'fruittea', 1, 'Pcs', 5000, 0, 5000, 4000),
(95, '1112190016', '0000-00-00', '8996006142511', 'tehsosro', 2, 'Bks', 3500, 0, 7000, 3000),
(96, '1112190016', '0000-00-00', 'ARGMRT-TTP-6039', 'pie Nanas', 1, 'Bks', 17500, 0, 17500, 15000),
(97, '1112190017', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 1, 'Bks', 8000, 0, 8000, 7000),
(98, '1112190017', '0000-00-00', 'ARGMRT-TTP-6034', 'aqua 600ml', 1, 'Btl', 3000, 0, 3000, 2000),
(99, '1112190018', '0000-00-00', 'ARGMRT-TTP-6069', 'yakult', 1, 'Bks', 9000, 0, 9000, 7600),
(100, '1112190019', '0000-00-00', 'ARGMRT-TTP-6015', 'salad buah', 2, 'Bks', 8000, 0, 16000, 7000),
(101, '1112190020', '0000-00-00', 'ARGMRT-TTP-6070', 'big salad', 1, 'Bks', 27000, 0, 27000, 25000),
(102, '1112190020', '0000-00-00', 'ARGMRT-TTP-6006', 'telurasini', 1, 'Pcs', 10500, 0, 10500, 9000),
(103, '1112190021', '0000-00-00', 'ARGMRT-TTP-6034', 'aqua 600ml', 1, 'Btl', 3000, 0, 3000, 2000),
(104, '1112190021', '0000-00-00', 'ARGMRT-TTP-6039', 'pie Nanas', 1, 'Bks', 17500, 0, 17500, 15000),
(105, '1112190022', '0000-00-00', 'ARGMRT-TTP-6007', 'herbaloloe', 2, 'Btl', 8000, 0, 16000, 7000),
(106, '1112190022', '0000-00-00', 'ARGMRT-TTP-6056', 'teri krisp', 1, 'Bks', 22000, 0, 22000, 20000),
(107, '1112190023', '0000-00-00', 'ARGMRT-TTP-6039', 'pie Nanas', 2, 'Bks', 17500, 0, 35000, 15000),
(108, '1112190023', '0000-00-00', 'ARGMRT-TTP-6013', 'dorodokbes', 1, 'Bks', 18000, 0, 18000, 15000),
(109, '1112190023', '0000-00-00', 'ARGMRT-TTP-6023', 'kacang tha', 1, 'Bks', 20000, 0, 20000, 18000),
(110, '1112190023', '0000-00-00', 'ARGMRT-TTP-5988', 'kripikusus', 1, 'Pcs', 13500, 0, 13500, 10000),
(111, '1112190023', '0000-00-00', 'ARGMRT-TTP-5989', 'suscoklat', 1, 'Pcs', 15000, 0, 15000, 12500),
(112, '1112190024', '0000-00-00', 'ARGMRT-TTP-5983', 'blusbatik', 1, 'Pcs', 90000, 0, 90000, 75000),
(113, '1112190024', '0000-00-00', 'ARGMRT-TTP-5998', 'kaoskakimo', 1, 'Pcs', 22000, 0, 22000, 20000),
(114, '1112190025', '0000-00-00', 'ARGMRT-TTP-6050', 'ayam ungke', 1, 'Bks', 40000, 0, 40000, 37500),
(115, '1112190025', '0000-00-00', 'ARGMRT-TTP-5983', 'blusbatik', 1, 'Pcs', 90000, 0, 90000, 75000),
(116, '1112190025', '0000-00-00', 'ARGMRT-TTP-5980', 'daster all', 1, 'Pcs', 125000, 0, 125000, 110000),
(117, '1112190025', '0000-00-00', 'ARGMRT-TTP-6014', 'krupukplip', 1, 'Pcs', 7000, 0, 7000, 5000),
(118, '1112190026', '0000-00-00', 'ARGMRT-TTP-6014', 'krupukplip', 1, 'Pcs', 7000, 0, 7000, 5000),
(119, '1112190026', '0000-00-00', 'ARGMRT-TTP-6012', 'kerupukkec', 1, 'Bks', 7000, 0, 7000, 5000),
(120, '1112190026', '0000-00-00', 'ARGMRT-TTP-6011', 'kripikmera', 1, 'Pcs', 6000, 0, 6000, 5000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_diskon`
--

CREATE TABLE `tb_diskon` (
  `id` int(20) NOT NULL,
  `id_barang` int(10) NOT NULL,
  `diskon` int(10) NOT NULL,
  `mulai` date DEFAULT NULL,
  `berakhir` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_hutang`
--

CREATE TABLE `tb_hutang` (
  `id_member` int(11) NOT NULL,
  `id_hutang` int(11) NOT NULL,
  `dibayar` int(15) NOT NULL,
  `sisa_bayar` int(15) NOT NULL,
  `no_faktur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_hutang`
--

INSERT INTO `tb_hutang` (`id_member`, `id_hutang`, `dibayar`, `sisa_bayar`, `no_faktur`) VALUES
(1, 55, 0, 717000, '023'),
(2, 22, 0, 12500500, '003'),
(3, 20, 0, 406000, '001'),
(4, 21, 0, 760000, '002'),
(5, 25, 0, 344000, '005'),
(6, 30, 0, 2200000, '010'),
(7, 31, 0, 1018000, '011'),
(8, 38, 0, 469500, '015'),
(9, 52, 0, 335000, '021'),
(10, 61, 122000, 40000, '025');

-- --------------------------------------------------------

--
-- Table structure for table `tb_hutang_tmp`
--

CREATE TABLE `tb_hutang_tmp` (
  `id_hutang` int(11) NOT NULL,
  `id_member` int(11) DEFAULT NULL,
  `sisa_bayar` int(11) DEFAULT NULL,
  `dibayar` int(11) NOT NULL DEFAULT '0',
  `no_faktur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_hutang_tmp`
--

INSERT INTO `tb_hutang_tmp` (`id_hutang`, `id_member`, `sisa_bayar`, `dibayar`, `no_faktur`) VALUES
(20, 3, 406000, 0, '001'),
(21, 4, 760000, 0, '002'),
(22, 2, 5124000, 0, '003'),
(23, 2, 928500, 0, '003'),
(24, 2, 12000, 0, '004'),
(25, 5, 344000, 0, '005'),
(26, 2, 120000, 0, '006'),
(27, 2, 12000, 0, '006'),
(28, 2, 24000, 0, '006'),
(29, 2, 30000, 0, '009'),
(30, 6, 140000, 0, '010'),
(31, 7, 240000, 0, '011'),
(32, 2, 24000, 0, '013'),
(33, 2, 180000, 0, '012'),
(34, 2, 270000, 0, '012'),
(35, 2, 30000, 0, '012'),
(36, 2, 238000, 0, '014'),
(37, 2, 264000, 0, '014'),
(38, 8, 107000, 0, '015'),
(39, 2, 96000, 0, '014'),
(40, 2, 360000, 0, '014'),
(41, 2, 1356000, 0, '016'),
(42, 2, 150000, 0, '017'),
(43, 2, 200000, 0, '014'),
(44, 2, 24000, 0, '014'),
(45, 2, 48000, 0, '0108'),
(46, 2, 400000, 0, '019'),
(47, 2, 1880000, 0, '019'),
(48, 2, 280000, 0, '019'),
(49, 2, 140000, 0, '019'),
(50, 6, 1820000, 0, '020'),
(51, 6, 240000, 0, '020'),
(52, 9, 335000, 0, '021'),
(53, 8, 225000, 0, '022'),
(54, 8, 52500, 0, '022'),
(55, 1, 717000, 0, '023'),
(56, 7, 200000, 0, '024'),
(57, 7, 108000, 0, '024'),
(58, 7, 280000, 0, '024'),
(59, 8, 58000, 0, '022'),
(60, 8, 27000, 0, '022'),
(61, 10, 85000, 0, '025'),
(62, 10, 77000, 0, '025'),
(63, 2, 310000, 0, '027'),
(64, 7, 190000, 0, '027');

--
-- Triggers `tb_hutang_tmp`
--
DELIMITER $$
CREATE TRIGGER `insert` AFTER INSERT ON `tb_hutang_tmp` FOR EACH ROW BEGIN
INSERT INTO tb_hutang SET 
id_member= NEW.id_member,dibayar = NEW.dibayar,
sisa_bayar = NEW.sisa_bayar, no_faktur = NEW.no_faktur,id_hutang=NEW.id_hutang
ON DUPLICATE KEY UPDATE sisa_bayar = sisa_bayar + NEW.sisa_bayar;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_jurnal`
--

CREATE TABLE `tb_jurnal` (
  `idjurnal` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `bukti` varchar(20) NOT NULL,
  `kode_akun` int(5) NOT NULL,
  `debet` int(11) NOT NULL,
  `kredit` int(11) NOT NULL,
  `ket` text NOT NULL,
  `id_user` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_jurnal`
--

INSERT INTO `tb_jurnal` (`idjurnal`, `tanggal`, `bukti`, `kode_akun`, `debet`, `kredit`, `ket`, `id_user`) VALUES
(38, '2019-12-04', 'BL041219000001', 1141, 198000, 0, 'Pembelian Dengan No.Faktur : BL041219000001', 6),
(39, '2019-12-04', 'BL041219000001', 1141, 160000, 0, 'Pembelian Dengan No.Faktur : BL041219000001', 6),
(40, '2019-12-04', 'BL041219000001', 1141, 48000, 0, 'Pembelian Dengan No.Faktur : BL041219000001', 6),
(42, '2019-12-04', 'UT-BL041219000001', 2101, 0, 406000, 'Hutang Dengan No.Faktur : BL041219000001', 6),
(43, '2019-12-04', 'BL041219000002', 1141, 600000, 0, 'Pembelian Dengan No.Faktur : BL041219000002', 6),
(44, '2019-12-04', 'BL041219000002', 1141, 160000, 0, 'Pembelian Dengan No.Faktur : BL041219000002', 6),
(46, '2019-12-04', 'UT-BL041219000002', 2101, 0, 760000, 'Hutang Dengan No.Faktur : BL041219000002', 6),
(47, '2019-12-04', 'BL041219000003', 1141, 1100000, 0, 'Pembelian Dengan No.Faktur : BL041219000003', 6),
(48, '2019-12-04', 'BL041219000003', 1141, 1750000, 0, 'Pembelian Dengan No.Faktur : BL041219000003', 6),
(49, '2019-12-04', 'BL041219000003', 1141, 360000, 0, 'Pembelian Dengan No.Faktur : BL041219000003', 6),
(50, '2019-12-04', 'BL041219000003', 1141, 750000, 0, 'Pembelian Dengan No.Faktur : BL041219000003', 6),
(51, '2019-12-04', 'BL041219000003', 1141, 360000, 0, 'Pembelian Dengan No.Faktur : BL041219000003', 6),
(52, '2019-12-04', 'BL041219000003', 1141, 375000, 0, 'Pembelian Dengan No.Faktur : BL041219000003', 6),
(53, '2019-12-04', 'BL041219000003', 1141, 195000, 0, 'Pembelian Dengan No.Faktur : BL041219000003', 6),
(54, '2019-12-04', 'BL041219000003', 1141, 210000, 0, 'Pembelian Dengan No.Faktur : BL041219000003', 6),
(55, '2019-12-04', 'BL041219000003', 1141, 24000, 0, 'Pembelian Dengan No.Faktur : BL041219000003', 6),
(57, '2019-12-04', 'UT-BL041219000003', 2101, 0, 5124000, 'Hutang Dengan No.Faktur : BL041219000003', 6),
(58, '2019-12-04', 'BL041219000004', 1141, 180000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(59, '2019-12-04', 'BL041219000004', 1141, 188500, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(60, '2019-12-04', 'BL041219000004', 1141, 60000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(61, '2019-12-04', 'BL041219000004', 1141, 12000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(62, '2019-12-04', 'BL041219000004', 1141, 16000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(63, '2019-12-04', 'BL041219000004', 1141, 40000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(64, '2019-12-04', 'BL041219000004', 1141, 115000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(65, '2019-12-04', 'BL041219000004', 1141, 25000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(66, '2019-12-04', 'BL041219000004', 1141, 90000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(67, '2019-12-04', 'BL041219000004', 1141, 100000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(68, '2019-12-04', 'BL041219000004', 1141, 102000, 0, 'Pembelian Dengan No.Faktur : BL041219000004', 6),
(70, '2019-12-04', 'UT-BL041219000004', 2101, 0, 928500, 'Hutang Dengan No.Faktur : BL041219000004', 6),
(71, '2019-12-04', 'JL0412190001', 1111, 88000, 0, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(72, '2019-12-04', 'JL0412190001', 4101, 0, 88000, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(73, '2019-12-04', 'JL0412190001', 5101, 66000, 0, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(74, '2019-12-04', 'JL0412190001', 1141, 0, 66000, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(75, '2019-12-04', 'JL0412190001', 1111, 13500, 0, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(76, '2019-12-04', 'JL0412190001', 4101, 0, 13500, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(77, '2019-12-04', 'JL0412190001', 5101, 10000, 0, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(78, '2019-12-04', 'JL0412190001', 1141, 0, 10000, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(79, '2019-12-04', 'JL0412190001', 1111, 21000, 0, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(80, '2019-12-04', 'JL0412190001', 4101, 0, 21000, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(81, '2019-12-04', 'JL0412190001', 5101, 17000, 0, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(82, '2019-12-04', 'JL0412190001', 1141, 0, 17000, 'Penjualan Dengan No.Faktur : 0412190001', 6),
(83, '2019-12-04', 'BL041219000005', 1141, 12000, 0, 'Pembelian Dengan No.Faktur : BL041219000005', 6),
(84, '2019-12-04', 'UT-BL041219000005', 2101, 0, 12000, 'Hutang Dengan No.Faktur : BL041219000005', 6),
(85, '2019-12-04', 'BL041219000006', 1141, 132000, 0, 'Pembelian Dengan No.Faktur : BL041219000006', 6),
(86, '2019-12-04', 'BL041219000006', 1141, 104000, 0, 'Pembelian Dengan No.Faktur : BL041219000006', 6),
(87, '2019-12-04', 'BL041219000006', 1141, 108000, 0, 'Pembelian Dengan No.Faktur : BL041219000006', 6),
(88, '2019-12-04', 'UT-BL041219000006', 2101, 0, 344000, 'Hutang Dengan No.Faktur : BL041219000006', 6),
(89, '2019-12-04', 'BL041219000007', 1141, 120000, 0, 'Pembelian Dengan No.Faktur : BL041219000007', 6),
(90, '2019-12-04', 'UT-BL041219000007', 2101, 0, 120000, 'Hutang Dengan No.Faktur : BL041219000007', 6),
(91, '2019-12-04', 'BL041219000008', 1141, 12000, 0, 'Pembelian Dengan No.Faktur : BL041219000008', 6),
(92, '2019-12-04', 'UT-BL041219000008', 2101, 0, 12000, 'Hutang Dengan No.Faktur : BL041219000008', 6),
(93, '2019-12-04', 'JL0412190002', 1111, 5000, 0, 'Penjualan Dengan No.Faktur : 0412190002', 6),
(94, '2019-12-04', 'JL0412190002', 4101, 0, 5000, 'Penjualan Dengan No.Faktur : 0412190002', 6),
(95, '2019-12-04', 'JL0412190002', 5101, 4000, 0, 'Penjualan Dengan No.Faktur : 0412190002', 6),
(96, '2019-12-04', 'JL0412190002', 1141, 0, 4000, 'Penjualan Dengan No.Faktur : 0412190002', 6),
(97, '2019-12-04', 'JL0412190003', 1111, 24000, 0, 'Penjualan Dengan No.Faktur : 0412190003', 6),
(98, '2019-12-04', 'JL0412190003', 4101, 0, 24000, 'Penjualan Dengan No.Faktur : 0412190003', 6),
(99, '2019-12-04', 'JL0412190003', 5101, 18000, 0, 'Penjualan Dengan No.Faktur : 0412190003', 6),
(100, '2019-12-04', 'JL0412190003', 1141, 0, 18000, 'Penjualan Dengan No.Faktur : 0412190003', 6),
(101, '2019-12-04', 'JL0412190004', 1111, 17000, 0, 'Penjualan Dengan No.Faktur : 0412190004', 6),
(102, '2019-12-04', 'JL0412190004', 4101, 0, 17000, 'Penjualan Dengan No.Faktur : 0412190004', 6),
(103, '2019-12-04', 'JL0412190004', 5101, 15000, 0, 'Penjualan Dengan No.Faktur : 0412190004', 6),
(104, '2019-12-04', 'JL0412190004', 1141, 0, 15000, 'Penjualan Dengan No.Faktur : 0412190004', 6),
(105, '2019-12-05', 'JL0512190001', 1111, 7000, 0, 'Penjualan Dengan No.Faktur : 0512190001', 6),
(106, '2019-12-05', 'JL0512190001', 4101, 0, 7000, 'Penjualan Dengan No.Faktur : 0512190001', 6),
(107, '2019-12-05', 'JL0512190001', 5101, 5000, 0, 'Penjualan Dengan No.Faktur : 0512190001', 6),
(108, '2019-12-05', 'JL0512190001', 1141, 0, 5000, 'Penjualan Dengan No.Faktur : 0512190001', 6),
(109, '2019-12-05', 'JL0512190002', 1111, 15000, 0, 'Penjualan Dengan No.Faktur : 0512190002', 6),
(110, '2019-12-05', 'JL0512190002', 4101, 0, 15000, 'Penjualan Dengan No.Faktur : 0512190002', 6),
(111, '2019-12-05', 'JL0512190002', 5101, 13000, 0, 'Penjualan Dengan No.Faktur : 0512190002', 6),
(112, '2019-12-05', 'JL0512190002', 1141, 0, 13000, 'Penjualan Dengan No.Faktur : 0512190002', 6),
(113, '2019-12-05', 'BL051219000001', 1141, 24000, 0, 'Pembelian Dengan No.Faktur : BL051219000001', 6),
(114, '2019-12-05', 'UT-BL051219000001', 2101, 0, 24000, 'Hutang Pembelian Dengan No.Faktur : BL051219000001', 6),
(115, '2019-12-05', 'JL0512190003', 1111, 16000, 0, 'Penjualan Dengan No.Faktur : 0512190003', 6),
(116, '2019-12-05', 'JL0512190003', 4101, 0, 16000, 'Penjualan Dengan No.Faktur : 0512190003', 6),
(117, '2019-12-05', 'JL0512190003', 5101, 12000, 0, 'Penjualan Dengan No.Faktur : 0512190003', 6),
(118, '2019-12-05', 'JL0512190003', 1141, 0, 12000, 'Penjualan Dengan No.Faktur : 0512190003', 6),
(119, '2019-12-05', 'JL0512190003', 1111, 13500, 0, 'Penjualan Dengan No.Faktur : 0512190003', 6),
(120, '2019-12-05', 'JL0512190003', 4101, 0, 13500, 'Penjualan Dengan No.Faktur : 0512190003', 6),
(121, '2019-12-05', 'JL0512190003', 5101, 10000, 0, 'Penjualan Dengan No.Faktur : 0512190003', 6),
(122, '2019-12-05', 'JL0512190003', 1141, 0, 10000, 'Penjualan Dengan No.Faktur : 0512190003', 6),
(123, '2019-12-05', 'JL0512190004', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 0512190004', 6),
(124, '2019-12-05', 'JL0512190004', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 0512190004', 6),
(125, '2019-12-05', 'JL0512190004', 5101, 6000, 0, 'Penjualan Dengan No.Faktur : 0512190004', 6),
(126, '2019-12-05', 'JL0512190004', 1141, 0, 6000, 'Penjualan Dengan No.Faktur : 0512190004', 6),
(127, '2019-12-05', 'JL0512190005', 1111, 16000, 0, 'Penjualan Dengan No.Faktur : 0512190005', 6),
(128, '2019-12-05', 'JL0512190005', 4101, 0, 16000, 'Penjualan Dengan No.Faktur : 0512190005', 6),
(129, '2019-12-05', 'JL0512190005', 5101, 12000, 0, 'Penjualan Dengan No.Faktur : 0512190005', 6),
(130, '2019-12-05', 'JL0512190005', 1141, 0, 12000, 'Penjualan Dengan No.Faktur : 0512190005', 6),
(131, '2019-12-05', 'JL0512190006', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 0512190006', 6),
(132, '2019-12-05', 'JL0512190006', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 0512190006', 6),
(133, '2019-12-05', 'JL0512190006', 5101, 6000, 0, 'Penjualan Dengan No.Faktur : 0512190006', 6),
(134, '2019-12-05', 'JL0512190006', 1141, 0, 6000, 'Penjualan Dengan No.Faktur : 0512190006', 6),
(135, '2019-12-05', 'JL0512190008', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 0512190008', 6),
(136, '2019-12-05', 'JL0512190008', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 0512190008', 6),
(137, '2019-12-05', 'JL0512190008', 5101, 6000, 0, 'Penjualan Dengan No.Faktur : 0512190008', 6),
(138, '2019-12-05', 'JL0512190008', 1141, 0, 6000, 'Penjualan Dengan No.Faktur : 0512190008', 6),
(139, '2019-12-05', 'BL051219000002', 1141, 30000, 0, 'Pembelian Dengan No.Faktur : BL051219000002', 6),
(140, '2019-12-05', 'UT-BL051219000002', 2101, 0, 30000, 'Hutang Pembelian Dengan No.Faktur : BL051219000002', 6),
(141, '2019-12-05', 'JL0512190009', 1111, 16000, 0, 'Penjualan Dengan No.Faktur : 0512190009', 6),
(142, '2019-12-05', 'JL0512190009', 4101, 0, 16000, 'Penjualan Dengan No.Faktur : 0512190009', 6),
(143, '2019-12-05', 'JL0512190009', 5101, 12000, 0, 'Penjualan Dengan No.Faktur : 0512190009', 6),
(144, '2019-12-05', 'JL0512190009', 1141, 0, 12000, 'Penjualan Dengan No.Faktur : 0512190009', 6),
(145, '2019-12-05', 'BL051219000003', 1141, 140000, 0, 'Pembelian Dengan No.Faktur : BL051219000003', 6),
(146, '2019-12-05', 'UT-BL051219000003', 2101, 0, 140000, 'Hutang Pembelian Dengan No.Faktur : BL051219000003', 6),
(147, '2019-12-05', 'BL051219000004', 1141, 240000, 0, 'Pembelian Dengan No.Faktur : BL051219000004', 6),
(148, '2019-12-05', 'UT-BL051219000004', 2101, 0, 240000, 'Hutang Pembelian Dengan No.Faktur : BL051219000004', 6),
(149, '2019-12-06', 'BL061219000001', 1141, 24000, 0, 'Pembelian Dengan No.Faktur : BL061219000001', 6),
(150, '2019-12-06', 'UT-BL061219000001', 2101, 0, 24000, 'Hutang Pembelian Dengan No.Faktur : BL061219000001', 6),
(151, '2019-12-06', 'JL0612190001', 1111, 5000, 0, 'Penjualan Dengan No.Faktur : 0612190001', 6),
(152, '2019-12-06', 'JL0612190001', 4101, 0, 5000, 'Penjualan Dengan No.Faktur : 0612190001', 6),
(153, '2019-12-06', 'JL0612190001', 5101, 4000, 0, 'Penjualan Dengan No.Faktur : 0612190001', 6),
(154, '2019-12-06', 'JL0612190001', 1141, 0, 4000, 'Penjualan Dengan No.Faktur : 0612190001', 6),
(155, '2019-12-06', 'BL061219000002', 1141, 180000, 0, 'Pembelian Dengan No.Faktur : BL061219000002', 6),
(156, '2019-12-06', 'UT-BL061219000002', 2101, 0, 180000, 'Hutang Pembelian Dengan No.Faktur : BL061219000002', 6),
(157, '2019-12-06', 'BL061219000003', 1141, 150000, 0, 'Pembelian Dengan No.Faktur : BL061219000003', 6),
(158, '2019-12-06', 'BL061219000003', 1141, 40000, 0, 'Pembelian Dengan No.Faktur : BL061219000003', 6),
(159, '2019-12-06', 'BL061219000003', 1141, 80000, 0, 'Pembelian Dengan No.Faktur : BL061219000003', 6),
(160, '2019-12-06', 'UT-BL061219000003', 2101, 0, 270000, 'Hutang Pembelian Dengan No.Faktur : BL061219000003', 6),
(161, '2019-12-06', 'BL061219000004', 1141, 30000, 0, 'Pembelian Dengan No.Faktur : BL061219000004', 6),
(162, '2019-12-06', 'UT-BL061219000004', 2101, 0, 30000, 'Hutang Pembelian Dengan No.Faktur : BL061219000004', 6),
(163, '2019-12-06', 'JL0612190002', 1111, 10500, 0, 'Penjualan Dengan No.Faktur : 0612190002', 6),
(164, '2019-12-06', 'JL0612190002', 4101, 0, 10500, 'Penjualan Dengan No.Faktur : 0612190002', 6),
(165, '2019-12-06', 'JL0612190002', 5101, 9000, 0, 'Penjualan Dengan No.Faktur : 0612190002', 6),
(166, '2019-12-06', 'JL0612190002', 1141, 0, 9000, 'Penjualan Dengan No.Faktur : 0612190002', 6),
(167, '2019-12-06', 'JL0612190003', 1111, 18000, 0, 'Penjualan Dengan No.Faktur : 0612190003', 6),
(168, '2019-12-06', 'JL0612190003', 4101, 0, 18000, 'Penjualan Dengan No.Faktur : 0612190003', 6),
(169, '2019-12-06', 'JL0612190003', 5101, 15000, 0, 'Penjualan Dengan No.Faktur : 0612190003', 6),
(170, '2019-12-06', 'JL0612190003', 1141, 0, 15000, 'Penjualan Dengan No.Faktur : 0612190003', 6),
(171, '2019-12-06', 'JL0612190004', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 0612190004', 6),
(172, '2019-12-06', 'JL0612190004', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 0612190004', 6),
(173, '2019-12-06', 'JL0612190004', 5101, 6000, 0, 'Penjualan Dengan No.Faktur : 0612190004', 6),
(174, '2019-12-06', 'JL0612190004', 1141, 0, 6000, 'Penjualan Dengan No.Faktur : 0612190004', 6),
(175, '2019-12-06', 'JL0612190005', 1111, 14000, 0, 'Penjualan Dengan No.Faktur : 0612190005', 6),
(176, '2019-12-06', 'JL0612190005', 4101, 0, 14000, 'Penjualan Dengan No.Faktur : 0612190005', 6),
(177, '2019-12-06', 'JL0612190005', 5101, 11500, 0, 'Penjualan Dengan No.Faktur : 0612190005', 6),
(178, '2019-12-06', 'JL0612190005', 1141, 0, 11500, 'Penjualan Dengan No.Faktur : 0612190005', 6),
(179, '2019-12-06', 'JL0612190006', 1111, 17000, 0, 'Penjualan Dengan No.Faktur : 0612190006', 6),
(180, '2019-12-06', 'JL0612190006', 4101, 0, 17000, 'Penjualan Dengan No.Faktur : 0612190006', 6),
(181, '2019-12-06', 'JL0612190006', 5101, 14500, 0, 'Penjualan Dengan No.Faktur : 0612190006', 6),
(182, '2019-12-06', 'JL0612190006', 1141, 0, 14500, 'Penjualan Dengan No.Faktur : 0612190006', 6),
(183, '2019-12-09', 'BL091219000001', 1141, 238000, 0, 'Pembelian Dengan No.Faktur : BL091219000001', 6),
(184, '2019-12-09', 'UT-BL091219000001', 2101, 0, 238000, 'Hutang Pembelian Dengan No.Faktur : BL091219000001', 6),
(185, '2019-12-09', 'BL091219000002', 1141, 40000, 0, 'Pembelian Dengan No.Faktur : BL091219000002', 6),
(186, '2019-12-09', 'BL091219000002', 1141, 14000, 0, 'Pembelian Dengan No.Faktur : BL091219000002', 6),
(187, '2019-12-09', 'BL091219000002', 1141, 60000, 0, 'Pembelian Dengan No.Faktur : BL091219000002', 6),
(188, '2019-12-09', 'BL091219000002', 1141, 52000, 0, 'Pembelian Dengan No.Faktur : BL091219000002', 6),
(189, '2019-12-09', 'BL091219000002', 1141, 66000, 0, 'Pembelian Dengan No.Faktur : BL091219000002', 6),
(190, '2019-12-09', 'BL091219000002', 1141, 102000, 0, 'Pembelian Dengan No.Faktur : BL091219000002', 6),
(191, '2019-12-09', 'BL091219000002', 1111, 0, 0, 'Pembelian Dengan No.Faktur : BL091219000002', 6),
(192, '2019-12-09', 'BL091219000003', 1141, 264000, 0, 'Pembelian Dengan No.Faktur : BL091219000003', 6),
(193, '2019-12-09', 'UT-BL091219000003', 2101, 0, 264000, 'Hutang Pembelian Dengan No.Faktur : BL091219000003', 6),
(194, '2019-12-09', 'BL091219000004', 1141, 93500, 0, 'Pembelian Dengan No.Faktur : BL091219000004', 6),
(195, '2019-12-09', 'BL091219000004', 1141, 13500, 0, 'Pembelian Dengan No.Faktur : BL091219000004', 6),
(196, '2019-12-09', 'UT-BL091219000004', 2101, 0, 107000, 'Hutang Pembelian Dengan No.Faktur : BL091219000004', 6),
(197, '2019-12-09', 'BL091219000005', 1141, 96000, 0, 'Pembelian Dengan No.Faktur : BL091219000005', 6),
(198, '2019-12-09', 'UT-BL091219000005', 2101, 0, 96000, 'Hutang Pembelian Dengan No.Faktur : BL091219000005', 6),
(199, '2019-12-09', 'BL091219000006', 1141, 360000, 0, 'Pembelian Dengan No.Faktur : BL091219000006', 6),
(200, '2019-12-09', 'UT-BL091219000006', 2101, 0, 360000, 'Hutang Pembelian Dengan No.Faktur : BL091219000006', 6),
(201, '2019-12-09', 'JL0912190001', 1111, 5000, 0, 'Penjualan Dengan No.Faktur : 0912190001', 6),
(202, '2019-12-09', 'JL0912190001', 4101, 0, 5000, 'Penjualan Dengan No.Faktur : 0912190001', 6),
(203, '2019-12-09', 'JL0912190001', 5101, 4000, 0, 'Penjualan Dengan No.Faktur : 0912190001', 6),
(204, '2019-12-09', 'JL0912190001', 1141, 0, 4000, 'Penjualan Dengan No.Faktur : 0912190001', 6),
(205, '2019-12-09', 'JL0912190001', 1111, 6500, 0, 'Penjualan Dengan No.Faktur : 0912190001', 6),
(206, '2019-12-09', 'JL0912190001', 4101, 0, 6500, 'Penjualan Dengan No.Faktur : 0912190001', 6),
(207, '2019-12-09', 'JL0912190001', 5101, 5500, 0, 'Penjualan Dengan No.Faktur : 0912190001', 6),
(208, '2019-12-09', 'JL0912190001', 1141, 0, 5500, 'Penjualan Dengan No.Faktur : 0912190001', 6),
(209, '2019-12-09', 'JL0912190002', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 0912190002', 6),
(210, '2019-12-09', 'JL0912190002', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 0912190002', 6),
(211, '2019-12-09', 'JL0912190002', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 0912190002', 6),
(212, '2019-12-09', 'JL0912190002', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 0912190002', 6),
(213, '2019-12-09', 'JL0912190003', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 0912190003', 6),
(214, '2019-12-09', 'JL0912190003', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 0912190003', 6),
(215, '2019-12-09', 'JL0912190003', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 0912190003', 6),
(216, '2019-12-09', 'JL0912190003', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 0912190003', 6),
(217, '2019-12-09', 'JL0912190004', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 0912190004', 6),
(218, '2019-12-09', 'JL0912190004', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 0912190004', 6),
(219, '2019-12-09', 'JL0912190004', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 0912190004', 6),
(220, '2019-12-09', 'JL0912190004', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 0912190004', 6),
(221, '2019-12-09', 'JL0912190005', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 0912190005', 6),
(222, '2019-12-09', 'JL0912190005', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 0912190005', 6),
(223, '2019-12-09', 'JL0912190005', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 0912190005', 6),
(224, '2019-12-09', 'JL0912190005', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 0912190005', 6),
(225, '2019-12-10', 'JL1012190001', 1111, 5000, 0, 'Penjualan Dengan No.Faktur : 1012190001', 6),
(226, '2019-12-10', 'JL1012190001', 4101, 0, 5000, 'Penjualan Dengan No.Faktur : 1012190001', 6),
(227, '2019-12-10', 'JL1012190001', 5101, 4000, 0, 'Penjualan Dengan No.Faktur : 1012190001', 6),
(228, '2019-12-10', 'JL1012190001', 1141, 0, 4000, 'Penjualan Dengan No.Faktur : 1012190001', 6),
(229, '2019-12-10', 'JL1012190001', 1111, 12000, 0, 'Penjualan Dengan No.Faktur : 1012190001', 6),
(230, '2019-12-10', 'JL1012190001', 4101, 0, 12000, 'Penjualan Dengan No.Faktur : 1012190001', 6),
(231, '2019-12-10', 'JL1012190001', 5101, 8000, 0, 'Penjualan Dengan No.Faktur : 1012190001', 6),
(232, '2019-12-10', 'JL1012190001', 1141, 0, 8000, 'Penjualan Dengan No.Faktur : 1012190001', 6),
(233, '2019-12-10', 'BL101219000001', 1141, 720000, 0, 'Pembelian Dengan No.Faktur : BL101219000001', 6),
(234, '2019-12-10', 'BL101219000001', 1141, 420000, 0, 'Pembelian Dengan No.Faktur : BL101219000001', 6),
(235, '2019-12-10', 'BL101219000001', 1141, 216000, 0, 'Pembelian Dengan No.Faktur : BL101219000001', 6),
(236, '2019-12-10', 'UT-BL101219000001', 2101, 0, 1356000, 'Hutang Pembelian Dengan No.Faktur : BL101219000001', 6),
(237, '2019-12-10', 'BL101219000002', 1141, 150000, 0, 'Pembelian Dengan No.Faktur : BL101219000002', 6),
(238, '2019-12-10', 'UT-BL101219000002', 2101, 0, 150000, 'Hutang Pembelian Dengan No.Faktur : BL101219000002', 6),
(239, '2019-12-10', 'BL101219000003', 1141, 200000, 0, 'Pembelian Dengan No.Faktur : BL101219000003', 6),
(240, '2019-12-10', 'UT-BL101219000003', 2101, 0, 200000, 'Hutang Pembelian Dengan No.Faktur : BL101219000003', 6),
(241, '2019-12-10', 'BL101219000004', 1141, 24000, 0, 'Pembelian Dengan No.Faktur : BL101219000004', 6),
(242, '2019-12-10', 'UT-BL101219000004', 2101, 0, 24000, 'Hutang Pembelian Dengan No.Faktur : BL101219000004', 6),
(243, '2019-12-10', 'BL101219000005', 1141, 48000, 0, 'Pembelian Dengan No.Faktur : BL101219000005', 6),
(244, '2019-12-10', 'UT-BL101219000005', 2101, 0, 48000, 'Hutang Pembelian Dengan No.Faktur : BL101219000005', 6),
(245, '2019-12-10', 'JL1012190002', 1111, 5000, 0, 'Penjualan Dengan No.Faktur : 1012190002', 6),
(246, '2019-12-10', 'JL1012190002', 4101, 0, 5000, 'Penjualan Dengan No.Faktur : 1012190002', 6),
(247, '2019-12-10', 'JL1012190002', 5101, 4000, 0, 'Penjualan Dengan No.Faktur : 1012190002', 6),
(248, '2019-12-10', 'JL1012190002', 1141, 0, 4000, 'Penjualan Dengan No.Faktur : 1012190002', 6),
(249, '2019-12-10', 'JL1012190002', 1111, 5000, 0, 'Penjualan Dengan No.Faktur : 1012190002', 6),
(250, '2019-12-10', 'JL1012190002', 4101, 0, 5000, 'Penjualan Dengan No.Faktur : 1012190002', 6),
(251, '2019-12-10', 'JL1012190002', 5101, 4000, 0, 'Penjualan Dengan No.Faktur : 1012190002', 6),
(252, '2019-12-10', 'JL1012190002', 1141, 0, 4000, 'Penjualan Dengan No.Faktur : 1012190002', 6),
(253, '2019-12-10', 'JL1012190003', 1111, 72000, 0, 'Penjualan Dengan No.Faktur : 1012190003', 6),
(254, '2019-12-10', 'JL1012190003', 4101, 0, 72000, 'Penjualan Dengan No.Faktur : 1012190003', 6),
(255, '2019-12-10', 'JL1012190003', 5101, 63000, 0, 'Penjualan Dengan No.Faktur : 1012190003', 6),
(256, '2019-12-10', 'JL1012190003', 1141, 0, 63000, 'Penjualan Dengan No.Faktur : 1012190003', 6),
(257, '2019-12-10', 'BL101219000006', 1141, 400000, 0, 'Pembelian Dengan No.Faktur : BL101219000006', 6),
(258, '2019-12-10', 'UT-BL101219000006', 2101, 0, 400000, 'Hutang Pembelian Dengan No.Faktur : BL101219000006', 6),
(259, '2019-12-10', 'BL101219000007', 1141, 715000, 0, 'Pembelian Dengan No.Faktur : BL101219000007', 6),
(260, '2019-12-10', 'BL101219000007', 1141, 660000, 0, 'Pembelian Dengan No.Faktur : BL101219000007', 6),
(261, '2019-12-10', 'BL101219000007', 1141, 225000, 0, 'Pembelian Dengan No.Faktur : BL101219000007', 6),
(262, '2019-12-10', 'BL101219000007', 1141, 105000, 0, 'Pembelian Dengan No.Faktur : BL101219000007', 6),
(263, '2019-12-10', 'BL101219000007', 1141, 175000, 0, 'Pembelian Dengan No.Faktur : BL101219000007', 6),
(264, '2019-12-10', 'UT-BL101219000007', 2101, 0, 1880000, 'Hutang Pembelian Dengan No.Faktur : BL101219000007', 6),
(265, '2019-12-10', 'BL101219000008', 1141, 280000, 0, 'Pembelian Dengan No.Faktur : BL101219000008', 6),
(266, '2019-12-10', 'UT-BL101219000008', 2101, 0, 280000, 'Hutang Pembelian Dengan No.Faktur : BL101219000008', 6),
(267, '2019-12-10', 'BL101219000009', 1141, 140000, 0, 'Pembelian Dengan No.Faktur : BL101219000009', 6),
(268, '2019-12-10', 'UT-BL101219000009', 2101, 0, 140000, 'Hutang Pembelian Dengan No.Faktur : BL101219000009', 6),
(269, '2019-12-10', 'JL1012190004', 1111, 14000, 0, 'Penjualan Dengan No.Faktur : 1012190004', 6),
(270, '2019-12-10', 'JL1012190004', 4101, 0, 14000, 'Penjualan Dengan No.Faktur : 1012190004', 6),
(271, '2019-12-10', 'JL1012190004', 5101, 12000, 0, 'Penjualan Dengan No.Faktur : 1012190004', 6),
(272, '2019-12-10', 'JL1012190004', 1141, 0, 12000, 'Penjualan Dengan No.Faktur : 1012190004', 6),
(273, '2019-12-10', 'JL1012190005', 1111, 14000, 0, 'Penjualan Dengan No.Faktur : 1012190005', 6),
(274, '2019-12-10', 'JL1012190005', 4101, 0, 14000, 'Penjualan Dengan No.Faktur : 1012190005', 6),
(275, '2019-12-10', 'JL1012190005', 5101, 12000, 0, 'Penjualan Dengan No.Faktur : 1012190005', 6),
(276, '2019-12-10', 'JL1012190005', 1141, 0, 12000, 'Penjualan Dengan No.Faktur : 1012190005', 6),
(277, '2019-12-10', 'BL101219000010', 1141, 720000, 0, 'Pembelian Dengan No.Faktur : BL101219000010', 6),
(278, '2019-12-10', 'BL101219000010', 1141, 300000, 0, 'Pembelian Dengan No.Faktur : BL101219000010', 6),
(279, '2019-12-10', 'BL101219000010', 1141, 800000, 0, 'Pembelian Dengan No.Faktur : BL101219000010', 6),
(280, '2019-12-10', 'UT-BL101219000010', 2101, 0, 1820000, 'Hutang Pembelian Dengan No.Faktur : BL101219000010', 6),
(281, '2019-12-10', 'BL101219000011', 1141, 240000, 0, 'Pembelian Dengan No.Faktur : BL101219000011', 6),
(282, '2019-12-10', 'UT-BL101219000011', 2101, 0, 240000, 'Hutang Pembelian Dengan No.Faktur : BL101219000011', 6),
(283, '2019-12-10', 'BL101219000012', 1141, 260000, 0, 'Pembelian Dengan No.Faktur : BL101219000012', 6),
(284, '2019-12-10', 'BL101219000012', 1141, 75000, 0, 'Pembelian Dengan No.Faktur : BL101219000012', 6),
(285, '2019-12-10', 'UT-BL101219000012', 2101, 0, 335000, 'Hutang Pembelian Dengan No.Faktur : BL101219000012', 6),
(286, '2019-12-10', 'JL1012190006', 1111, 10000, 0, 'Penjualan Dengan No.Faktur : 1012190006', 6),
(287, '2019-12-10', 'JL1012190006', 4101, 0, 10000, 'Penjualan Dengan No.Faktur : 1012190006', 6),
(288, '2019-12-10', 'JL1012190006', 5101, 8500, 0, 'Penjualan Dengan No.Faktur : 1012190006', 6),
(289, '2019-12-10', 'JL1012190006', 1141, 0, 8500, 'Penjualan Dengan No.Faktur : 1012190006', 6),
(290, '2019-12-11', 'BL111219000001', 1141, 225000, 0, 'Pembelian Dengan No.Faktur : BL111219000001', 6),
(291, '2019-12-11', 'UT-BL111219000001', 2101, 0, 225000, 'Hutang Pembelian Dengan No.Faktur : BL111219000001', 6),
(292, '2019-12-11', 'BL111219000002', 1141, 52500, 0, 'Pembelian Dengan No.Faktur : BL111219000002', 6),
(293, '2019-12-11', 'UT-BL111219000002', 2101, 0, 52500, 'Hutang Pembelian Dengan No.Faktur : BL111219000002', 6),
(294, '2019-12-11', 'BL111219000003', 1141, 200000, 0, 'Pembelian Dengan No.Faktur : BL111219000003', 6),
(295, '2019-12-11', 'BL111219000003', 1141, 260000, 0, 'Pembelian Dengan No.Faktur : BL111219000003', 6),
(296, '2019-12-11', 'BL111219000003', 1141, 117000, 0, 'Pembelian Dengan No.Faktur : BL111219000003', 6),
(297, '2019-12-11', 'BL111219000003', 1141, 140000, 0, 'Pembelian Dengan No.Faktur : BL111219000003', 6),
(298, '2019-12-11', 'UT-BL111219000003', 2101, 0, 717000, 'Hutang Pembelian Dengan No.Faktur : BL111219000003', 6),
(299, '2019-12-11', 'BL111219000004', 1141, 200000, 0, 'Pembelian Dengan No.Faktur : BL111219000004', 6),
(300, '2019-12-11', 'UT-BL111219000004', 2101, 0, 200000, 'Hutang Pembelian Dengan No.Faktur : BL111219000004', 6),
(301, '2019-12-11', 'BL111219000005', 1141, 108000, 0, 'Pembelian Dengan No.Faktur : BL111219000005', 6),
(302, '2019-12-11', 'UT-BL111219000005', 2101, 0, 108000, 'Hutang Pembelian Dengan No.Faktur : BL111219000005', 6),
(303, '2019-12-11', 'BL111219000006', 1141, 280000, 0, 'Pembelian Dengan No.Faktur : BL111219000006', 6),
(304, '2019-12-11', 'UT-BL111219000006', 2101, 0, 280000, 'Hutang Pembelian Dengan No.Faktur : BL111219000006', 6),
(305, '2019-12-11', 'BL111219000007', 1141, 58000, 0, 'Pembelian Dengan No.Faktur : BL111219000007', 6),
(306, '2019-12-11', 'UT-BL111219000007', 2101, 0, 58000, 'Hutang Pembelian Dengan No.Faktur : BL111219000007', 6),
(307, '2019-12-11', 'BL111219000008', 1141, 27000, 0, 'Pembelian Dengan No.Faktur : BL111219000008', 6),
(308, '2019-12-11', 'UT-BL111219000008', 2101, 0, 27000, 'Hutang Pembelian Dengan No.Faktur : BL111219000008', 6),
(309, '2019-12-11', 'BL111219000009', 1141, 45000, 0, 'Pembelian Dengan No.Faktur : BL111219000009', 6),
(310, '2019-12-11', 'BL111219000009', 1141, 40000, 0, 'Pembelian Dengan No.Faktur : BL111219000009', 6),
(311, '2019-12-11', 'UT-BL111219000009', 2101, 0, 85000, 'Hutang Pembelian Dengan No.Faktur : BL111219000009', 6),
(312, '2019-12-11', 'BL111219000010', 1141, 77000, 0, 'Pembelian Dengan No.Faktur : BL111219000010', 6),
(313, '2019-12-11', 'UT-BL111219000010', 2101, 0, 77000, 'Hutang Pembelian Dengan No.Faktur : BL111219000010', 6),
(314, '2019-12-11', 'JL1112190001', 1111, 17000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(315, '2019-12-11', 'JL1112190001', 4101, 0, 17000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(316, '2019-12-11', 'JL1112190001', 5101, 14500, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(317, '2019-12-11', 'JL1112190001', 1141, 0, 14500, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(318, '2019-12-11', 'JL1112190001', 1111, 70000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(319, '2019-12-11', 'JL1112190001', 4101, 0, 70000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(320, '2019-12-11', 'JL1112190001', 5101, 65000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(321, '2019-12-11', 'JL1112190001', 1141, 0, 65000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(322, '2019-12-11', 'JL1112190001', 1111, 35000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(323, '2019-12-11', 'JL1112190001', 4101, 0, 35000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(324, '2019-12-11', 'JL1112190001', 5101, 30000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(325, '2019-12-11', 'JL1112190001', 1141, 0, 30000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(326, '2019-12-11', 'JL1112190001', 1111, 14000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(327, '2019-12-11', 'JL1112190001', 4101, 0, 14000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(328, '2019-12-11', 'JL1112190001', 5101, 10000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(329, '2019-12-11', 'JL1112190001', 1141, 0, 10000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(330, '2019-12-11', 'JL1112190001', 1111, 52000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(331, '2019-12-11', 'JL1112190001', 4101, 0, 52000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(332, '2019-12-11', 'JL1112190001', 5101, 48000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(333, '2019-12-11', 'JL1112190001', 1141, 0, 48000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(334, '2019-12-11', 'JL1112190001', 1111, 10500, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(335, '2019-12-11', 'JL1112190001', 4101, 0, 10500, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(336, '2019-12-11', 'JL1112190001', 5101, 9000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(337, '2019-12-11', 'JL1112190001', 1141, 0, 9000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(338, '2019-12-11', 'JL1112190001', 1111, 20000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(339, '2019-12-11', 'JL1112190001', 4101, 0, 20000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(340, '2019-12-11', 'JL1112190001', 5101, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(341, '2019-12-11', 'JL1112190001', 1141, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190001', 6),
(342, '2019-12-11', 'JL1112190002', 1111, 17500, 0, 'Penjualan Dengan No.Faktur : 1112190002', 6),
(343, '2019-12-11', 'JL1112190002', 4101, 0, 17500, 'Penjualan Dengan No.Faktur : 1112190002', 6),
(344, '2019-12-11', 'JL1112190002', 5101, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190002', 6),
(345, '2019-12-11', 'JL1112190002', 1141, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190002', 6),
(346, '2019-12-11', 'JL1112190002', 1111, 22000, 0, 'Penjualan Dengan No.Faktur : 1112190002', 6),
(347, '2019-12-11', 'JL1112190002', 4101, 0, 22000, 'Penjualan Dengan No.Faktur : 1112190002', 6),
(348, '2019-12-11', 'JL1112190002', 5101, 20000, 0, 'Penjualan Dengan No.Faktur : 1112190002', 6),
(349, '2019-12-11', 'JL1112190002', 1141, 0, 20000, 'Penjualan Dengan No.Faktur : 1112190002', 6),
(350, '2019-12-11', 'JL1112190003', 1111, 30000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(351, '2019-12-11', 'JL1112190003', 4101, 0, 30000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(352, '2019-12-11', 'JL1112190003', 5101, 27000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(353, '2019-12-11', 'JL1112190003', 1141, 0, 27000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(354, '2019-12-11', 'JL1112190003', 1111, 16000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(355, '2019-12-11', 'JL1112190003', 4101, 0, 16000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(356, '2019-12-11', 'JL1112190003', 5101, 14000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(357, '2019-12-11', 'JL1112190003', 1141, 0, 14000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(358, '2019-12-11', 'JL1112190003', 1111, 16000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(359, '2019-12-11', 'JL1112190003', 4101, 0, 16000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(360, '2019-12-11', 'JL1112190003', 5101, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(361, '2019-12-11', 'JL1112190003', 1141, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(362, '2019-12-11', 'JL1112190003', 1111, 6000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(363, '2019-12-11', 'JL1112190003', 4101, 0, 6000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(364, '2019-12-11', 'JL1112190003', 5101, 5000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(365, '2019-12-11', 'JL1112190003', 1141, 0, 5000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(366, '2019-12-11', 'JL1112190003', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(367, '2019-12-11', 'JL1112190003', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(368, '2019-12-11', 'JL1112190003', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(369, '2019-12-11', 'JL1112190003', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(370, '2019-12-11', 'JL1112190003', 1111, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(371, '2019-12-11', 'JL1112190003', 4101, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(372, '2019-12-11', 'JL1112190003', 5101, 13000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(373, '2019-12-11', 'JL1112190003', 1141, 0, 13000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(374, '2019-12-11', 'JL1112190003', 1111, 13500, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(375, '2019-12-11', 'JL1112190003', 4101, 0, 13500, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(376, '2019-12-11', 'JL1112190003', 5101, 10000, 0, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(377, '2019-12-11', 'JL1112190003', 1141, 0, 10000, 'Penjualan Dengan No.Faktur : 1112190003', 6),
(378, '2019-12-11', 'BL111219000011', 1141, 150000, 0, 'Pembelian Dengan No.Faktur : BL111219000011', 6),
(379, '2019-12-11', 'BL111219000011', 1141, 160000, 0, 'Pembelian Dengan No.Faktur : BL111219000011', 6),
(380, '2019-12-11', 'UT-BL111219000011', 2101, 0, 310000, 'Hutang Pembelian Dengan No.Faktur : BL111219000011', 6),
(381, '2019-12-11', 'JL1112190004', 1111, 26000, 0, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(382, '2019-12-11', 'JL1112190004', 4101, 0, 26000, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(383, '2019-12-11', 'JL1112190004', 5101, 25000, 0, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(384, '2019-12-11', 'JL1112190004', 1141, 0, 25000, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(385, '2019-12-11', 'JL1112190004', 1111, 17000, 0, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(386, '2019-12-11', 'JL1112190004', 4101, 0, 17000, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(387, '2019-12-11', 'JL1112190004', 5101, 14500, 0, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(388, '2019-12-11', 'JL1112190004', 1141, 0, 14500, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(389, '2019-12-11', 'JL1112190004', 1111, 42000, 0, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(390, '2019-12-11', 'JL1112190004', 4101, 0, 42000, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(391, '2019-12-11', 'JL1112190004', 5101, 40000, 0, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(392, '2019-12-11', 'JL1112190004', 1141, 0, 40000, 'Penjualan Dengan No.Faktur : 1112190004', 6),
(393, '2019-12-11', 'JL1112190005', 1111, 10000, 0, 'Penjualan Dengan No.Faktur : 1112190005', 6),
(394, '2019-12-11', 'JL1112190005', 4101, 0, 10000, 'Penjualan Dengan No.Faktur : 1112190005', 6),
(395, '2019-12-11', 'JL1112190005', 5101, 8500, 0, 'Penjualan Dengan No.Faktur : 1112190005', 6),
(396, '2019-12-11', 'JL1112190005', 1141, 0, 8500, 'Penjualan Dengan No.Faktur : 1112190005', 6),
(397, '2019-12-11', 'JL1112190006', 1111, 40000, 0, 'Penjualan Dengan No.Faktur : 1112190006', 6),
(398, '2019-12-11', 'JL1112190006', 4101, 0, 40000, 'Penjualan Dengan No.Faktur : 1112190006', 6),
(399, '2019-12-11', 'JL1112190006', 5101, 37500, 0, 'Penjualan Dengan No.Faktur : 1112190006', 6),
(400, '2019-12-11', 'JL1112190006', 1141, 0, 37500, 'Penjualan Dengan No.Faktur : 1112190006', 6),
(401, '2019-12-11', 'JL1112190007', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(402, '2019-12-11', 'JL1112190007', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(403, '2019-12-11', 'JL1112190007', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(404, '2019-12-11', 'JL1112190007', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(405, '2019-12-11', 'JL1112190007', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(406, '2019-12-11', 'JL1112190007', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(407, '2019-12-11', 'JL1112190007', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(408, '2019-12-11', 'JL1112190007', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(409, '2019-12-11', 'JL1112190007', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(410, '2019-12-11', 'JL1112190007', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(411, '2019-12-11', 'JL1112190007', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(412, '2019-12-11', 'JL1112190007', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190007', 6),
(413, '2019-12-11', 'JL1112190008', 1111, 24000, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(414, '2019-12-11', 'JL1112190008', 4101, 0, 24000, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(415, '2019-12-11', 'JL1112190008', 5101, 20000, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(416, '2019-12-11', 'JL1112190008', 1141, 0, 20000, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(417, '2019-12-11', 'JL1112190008', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(418, '2019-12-11', 'JL1112190008', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(419, '2019-12-11', 'JL1112190008', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(420, '2019-12-11', 'JL1112190008', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(421, '2019-12-11', 'JL1112190008', 1111, 42000, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(422, '2019-12-11', 'JL1112190008', 4101, 0, 42000, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(423, '2019-12-11', 'JL1112190008', 5101, 40000, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(424, '2019-12-11', 'JL1112190008', 1141, 0, 40000, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(425, '2019-12-11', 'JL1112190008', 1111, 17000, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(426, '2019-12-11', 'JL1112190008', 4101, 0, 17000, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(427, '2019-12-11', 'JL1112190008', 5101, 14500, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(428, '2019-12-11', 'JL1112190008', 1141, 0, 14500, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(429, '2019-12-11', 'JL1112190008', 1111, 75000, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(430, '2019-12-11', 'JL1112190008', 4101, 0, 75000, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(431, '2019-12-11', 'JL1112190008', 5101, 72000, 0, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(432, '2019-12-11', 'JL1112190008', 1141, 0, 72000, 'Penjualan Dengan No.Faktur : 1112190008', 6),
(433, '2019-12-11', 'JL1112190009', 1111, 40000, 0, 'Penjualan Dengan No.Faktur : 1112190009', 6),
(434, '2019-12-11', 'JL1112190009', 4101, 0, 40000, 'Penjualan Dengan No.Faktur : 1112190009', 6),
(435, '2019-12-11', 'JL1112190009', 5101, 37500, 0, 'Penjualan Dengan No.Faktur : 1112190009', 6),
(436, '2019-12-11', 'JL1112190009', 1141, 0, 37500, 'Penjualan Dengan No.Faktur : 1112190009', 6),
(437, '2019-12-11', 'JL1112190010', 1111, 75000, 0, 'Penjualan Dengan No.Faktur : 1112190010', 6),
(438, '2019-12-11', 'JL1112190010', 4101, 0, 75000, 'Penjualan Dengan No.Faktur : 1112190010', 6),
(439, '2019-12-11', 'JL1112190010', 5101, 65000, 0, 'Penjualan Dengan No.Faktur : 1112190010', 6),
(440, '2019-12-11', 'JL1112190010', 1141, 0, 65000, 'Penjualan Dengan No.Faktur : 1112190010', 6),
(441, '2019-12-11', 'JL1112190011', 1111, 22000, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(442, '2019-12-11', 'JL1112190011', 4101, 0, 22000, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(443, '2019-12-11', 'JL1112190011', 5101, 20000, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(444, '2019-12-11', 'JL1112190011', 1141, 0, 20000, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(445, '2019-12-11', 'JL1112190011', 1111, 40000, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(446, '2019-12-11', 'JL1112190011', 4101, 0, 40000, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(447, '2019-12-11', 'JL1112190011', 5101, 36000, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(448, '2019-12-11', 'JL1112190011', 1141, 0, 36000, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(449, '2019-12-11', 'JL1112190011', 1111, 23000, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(450, '2019-12-11', 'JL1112190011', 4101, 0, 23000, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(451, '2019-12-11', 'JL1112190011', 5101, 21000, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(452, '2019-12-11', 'JL1112190011', 1141, 0, 21000, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(453, '2019-12-11', 'JL1112190011', 1111, 34000, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(454, '2019-12-11', 'JL1112190011', 4101, 0, 34000, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(455, '2019-12-11', 'JL1112190011', 5101, 29000, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(456, '2019-12-11', 'JL1112190011', 1141, 0, 29000, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(457, '2019-12-11', 'JL1112190011', 1111, 14000, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(458, '2019-12-11', 'JL1112190011', 4101, 0, 14000, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(459, '2019-12-11', 'JL1112190011', 5101, 11500, 0, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(460, '2019-12-11', 'JL1112190011', 1141, 0, 11500, 'Penjualan Dengan No.Faktur : 1112190011', 6),
(461, '2019-12-11', 'JL1112190012', 1111, 32000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(462, '2019-12-11', 'JL1112190012', 4101, 0, 32000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(463, '2019-12-11', 'JL1112190012', 5101, 30000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(464, '2019-12-11', 'JL1112190012', 1141, 0, 30000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(465, '2019-12-11', 'JL1112190012', 1111, 64000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(466, '2019-12-11', 'JL1112190012', 4101, 0, 64000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(467, '2019-12-11', 'JL1112190012', 5101, 56000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(468, '2019-12-11', 'JL1112190012', 1141, 0, 56000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(469, '2019-12-11', 'JL1112190012', 1111, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(470, '2019-12-11', 'JL1112190012', 4101, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(471, '2019-12-11', 'JL1112190012', 5101, 13000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(472, '2019-12-11', 'JL1112190012', 1141, 0, 13000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(473, '2019-12-11', 'JL1112190012', 1111, 20000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(474, '2019-12-11', 'JL1112190012', 4101, 0, 20000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(475, '2019-12-11', 'JL1112190012', 5101, 18000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(476, '2019-12-11', 'JL1112190012', 1141, 0, 18000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(477, '2019-12-11', 'JL1112190012', 1111, 21000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(478, '2019-12-11', 'JL1112190012', 4101, 0, 21000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(479, '2019-12-11', 'JL1112190012', 5101, 17000, 0, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(480, '2019-12-11', 'JL1112190012', 1141, 0, 17000, 'Penjualan Dengan No.Faktur : 1112190012', 6),
(481, '2019-12-11', 'JL1112190013', 1111, 37000, 0, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(482, '2019-12-11', 'JL1112190013', 4101, 0, 37000, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(483, '2019-12-11', 'JL1112190013', 5101, 35000, 0, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(484, '2019-12-11', 'JL1112190013', 1141, 0, 35000, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(485, '2019-12-11', 'JL1112190013', 1111, 135000, 0, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(486, '2019-12-11', 'JL1112190013', 4101, 0, 135000, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(487, '2019-12-11', 'JL1112190013', 5101, 120000, 0, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(488, '2019-12-11', 'JL1112190013', 1141, 0, 120000, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(489, '2019-12-11', 'JL1112190013', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(490, '2019-12-11', 'JL1112190013', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(491, '2019-12-11', 'JL1112190013', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(492, '2019-12-11', 'JL1112190013', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190013', 6),
(493, '2019-12-11', 'JL1112190014', 1111, 14000, 0, 'Penjualan Dengan No.Faktur : 1112190014', 6),
(494, '2019-12-11', 'JL1112190014', 4101, 0, 14000, 'Penjualan Dengan No.Faktur : 1112190014', 6),
(495, '2019-12-11', 'JL1112190014', 5101, 12000, 0, 'Penjualan Dengan No.Faktur : 1112190014', 6),
(496, '2019-12-11', 'JL1112190014', 1141, 0, 12000, 'Penjualan Dengan No.Faktur : 1112190014', 6),
(497, '2019-12-11', 'JL1112190014', 1111, 14000, 0, 'Penjualan Dengan No.Faktur : 1112190014', 6),
(498, '2019-12-11', 'JL1112190014', 4101, 0, 14000, 'Penjualan Dengan No.Faktur : 1112190014', 6),
(499, '2019-12-11', 'JL1112190014', 5101, 11500, 0, 'Penjualan Dengan No.Faktur : 1112190014', 6),
(500, '2019-12-11', 'JL1112190014', 1141, 0, 11500, 'Penjualan Dengan No.Faktur : 1112190014', 6),
(501, '2019-12-11', 'BL111219000012', 1141, 76000, 0, 'Pembelian Dengan No.Faktur : BL111219000012', 6),
(502, '2019-12-11', 'BL111219000012', 1111, 0, 76000, 'Pembelian Dengan No.Faktur : BL111219000012', 6),
(503, '2019-12-11', 'JL1112190015', 1111, 18000, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(504, '2019-12-11', 'JL1112190015', 4101, 0, 18000, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(505, '2019-12-11', 'JL1112190015', 5101, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(506, '2019-12-11', 'JL1112190015', 1141, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(507, '2019-12-11', 'JL1112190015', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(508, '2019-12-11', 'JL1112190015', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(509, '2019-12-11', 'JL1112190015', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(510, '2019-12-11', 'JL1112190015', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(511, '2019-12-11', 'JL1112190015', 1111, 6000, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(512, '2019-12-11', 'JL1112190015', 4101, 0, 6000, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(513, '2019-12-11', 'JL1112190015', 5101, 5000, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(514, '2019-12-11', 'JL1112190015', 1141, 0, 5000, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(515, '2019-12-11', 'JL1112190015', 1111, 9000, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(516, '2019-12-11', 'JL1112190015', 4101, 0, 9000, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(517, '2019-12-11', 'JL1112190015', 5101, 7600, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(518, '2019-12-11', 'JL1112190015', 1141, 0, 7600, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(519, '2019-12-11', 'JL1112190015', 1111, 20000, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(520, '2019-12-11', 'JL1112190015', 4101, 0, 20000, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(521, '2019-12-11', 'JL1112190015', 5101, 18000, 0, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(522, '2019-12-11', 'JL1112190015', 1141, 0, 18000, 'Penjualan Dengan No.Faktur : 1112190015', 6),
(523, '2019-12-11', 'JL1112190016', 1111, 12000, 0, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(524, '2019-12-11', 'JL1112190016', 4101, 0, 12000, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(525, '2019-12-11', 'JL1112190016', 5101, 10000, 0, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(526, '2019-12-11', 'JL1112190016', 1141, 0, 10000, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(527, '2019-12-11', 'JL1112190016', 1111, 5000, 0, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(528, '2019-12-11', 'JL1112190016', 4101, 0, 5000, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(529, '2019-12-11', 'JL1112190016', 5101, 4000, 0, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(530, '2019-12-11', 'JL1112190016', 1141, 0, 4000, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(531, '2019-12-11', 'JL1112190016', 1111, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(532, '2019-12-11', 'JL1112190016', 4101, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(533, '2019-12-11', 'JL1112190016', 5101, 6000, 0, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(534, '2019-12-11', 'JL1112190016', 1141, 0, 6000, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(535, '2019-12-11', 'JL1112190016', 1111, 17500, 0, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(536, '2019-12-11', 'JL1112190016', 4101, 0, 17500, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(537, '2019-12-11', 'JL1112190016', 5101, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(538, '2019-12-11', 'JL1112190016', 1141, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190016', 6),
(539, '2019-12-11', 'BU191211', 2101, 0, 122000, 'Pembayaran Utang Indah Nada', 6),
(540, '2019-12-11', 'BU191211', 1111, 122000, 0, 'Pembayaran Utang Indah Nada', 6),
(541, '2019-12-11', 'JL1112190017', 1111, 8000, 0, 'Penjualan Dengan No.Faktur : 1112190017', 6),
(542, '2019-12-11', 'JL1112190017', 4101, 0, 8000, 'Penjualan Dengan No.Faktur : 1112190017', 6),
(543, '2019-12-11', 'JL1112190017', 5101, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190017', 6),
(544, '2019-12-11', 'JL1112190017', 1141, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190017', 6);
INSERT INTO `tb_jurnal` (`idjurnal`, `tanggal`, `bukti`, `kode_akun`, `debet`, `kredit`, `ket`, `id_user`) VALUES
(545, '2019-12-11', 'JL1112190017', 1111, 3000, 0, 'Penjualan Dengan No.Faktur : 1112190017', 6),
(546, '2019-12-11', 'JL1112190017', 4101, 0, 3000, 'Penjualan Dengan No.Faktur : 1112190017', 6),
(547, '2019-12-11', 'JL1112190017', 5101, 2000, 0, 'Penjualan Dengan No.Faktur : 1112190017', 6),
(548, '2019-12-11', 'JL1112190017', 1141, 0, 2000, 'Penjualan Dengan No.Faktur : 1112190017', 6),
(549, '2019-12-11', 'JL1112190018', 1111, 9000, 0, 'Penjualan Dengan No.Faktur : 1112190018', 6),
(550, '2019-12-11', 'JL1112190018', 4101, 0, 9000, 'Penjualan Dengan No.Faktur : 1112190018', 6),
(551, '2019-12-11', 'JL1112190018', 5101, 7600, 0, 'Penjualan Dengan No.Faktur : 1112190018', 6),
(552, '2019-12-11', 'JL1112190018', 1141, 0, 7600, 'Penjualan Dengan No.Faktur : 1112190018', 6),
(553, '2019-12-11', 'JL1112190019', 1111, 16000, 0, 'Penjualan Dengan No.Faktur : 1112190019', 6),
(554, '2019-12-11', 'JL1112190019', 4101, 0, 16000, 'Penjualan Dengan No.Faktur : 1112190019', 6),
(555, '2019-12-11', 'JL1112190019', 5101, 14000, 0, 'Penjualan Dengan No.Faktur : 1112190019', 6),
(556, '2019-12-11', 'JL1112190019', 1141, 0, 14000, 'Penjualan Dengan No.Faktur : 1112190019', 6),
(557, '2019-12-11', 'BL111219000013', 1141, 25000, 0, 'Pembelian Dengan No.Faktur : BL111219000013', 6),
(558, '2019-12-11', 'BL111219000013', 1111, 0, 25000, 'Pembelian Dengan No.Faktur : BL111219000013', 6),
(559, '2019-12-11', 'JL1112190020', 1111, 27000, 0, 'Penjualan Dengan No.Faktur : 1112190020', 6),
(560, '2019-12-11', 'JL1112190020', 4101, 0, 27000, 'Penjualan Dengan No.Faktur : 1112190020', 6),
(561, '2019-12-11', 'JL1112190020', 5101, 25000, 0, 'Penjualan Dengan No.Faktur : 1112190020', 6),
(562, '2019-12-11', 'JL1112190020', 1141, 0, 25000, 'Penjualan Dengan No.Faktur : 1112190020', 6),
(563, '2019-12-11', 'JL1112190020', 1111, 10500, 0, 'Penjualan Dengan No.Faktur : 1112190020', 6),
(564, '2019-12-11', 'JL1112190020', 4101, 0, 10500, 'Penjualan Dengan No.Faktur : 1112190020', 6),
(565, '2019-12-11', 'JL1112190020', 5101, 9000, 0, 'Penjualan Dengan No.Faktur : 1112190020', 6),
(566, '2019-12-11', 'JL1112190020', 1141, 0, 9000, 'Penjualan Dengan No.Faktur : 1112190020', 6),
(567, '2019-12-11', 'JL1112190021', 1111, 3000, 0, 'Penjualan Dengan No.Faktur : 1112190021', 6),
(568, '2019-12-11', 'JL1112190021', 4101, 0, 3000, 'Penjualan Dengan No.Faktur : 1112190021', 6),
(569, '2019-12-11', 'JL1112190021', 5101, 2000, 0, 'Penjualan Dengan No.Faktur : 1112190021', 6),
(570, '2019-12-11', 'JL1112190021', 1141, 0, 2000, 'Penjualan Dengan No.Faktur : 1112190021', 6),
(571, '2019-12-11', 'JL1112190021', 1111, 17500, 0, 'Penjualan Dengan No.Faktur : 1112190021', 6),
(572, '2019-12-11', 'JL1112190021', 4101, 0, 17500, 'Penjualan Dengan No.Faktur : 1112190021', 6),
(573, '2019-12-11', 'JL1112190021', 5101, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190021', 6),
(574, '2019-12-11', 'JL1112190021', 1141, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190021', 6),
(575, '2019-12-11', 'JL1112190022', 1111, 16000, 0, 'Penjualan Dengan No.Faktur : 1112190022', 6),
(576, '2019-12-11', 'JL1112190022', 4101, 0, 16000, 'Penjualan Dengan No.Faktur : 1112190022', 6),
(577, '2019-12-11', 'JL1112190022', 5101, 14000, 0, 'Penjualan Dengan No.Faktur : 1112190022', 6),
(578, '2019-12-11', 'JL1112190022', 1141, 0, 14000, 'Penjualan Dengan No.Faktur : 1112190022', 6),
(579, '2019-12-11', 'JL1112190022', 1111, 22000, 0, 'Penjualan Dengan No.Faktur : 1112190022', 6),
(580, '2019-12-11', 'JL1112190022', 4101, 0, 22000, 'Penjualan Dengan No.Faktur : 1112190022', 6),
(581, '2019-12-11', 'JL1112190022', 5101, 20000, 0, 'Penjualan Dengan No.Faktur : 1112190022', 6),
(582, '2019-12-11', 'JL1112190022', 1141, 0, 20000, 'Penjualan Dengan No.Faktur : 1112190022', 6),
(583, '2019-12-11', 'JL1112190023', 1111, 35000, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(584, '2019-12-11', 'JL1112190023', 4101, 0, 35000, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(585, '2019-12-11', 'JL1112190023', 5101, 30000, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(586, '2019-12-11', 'JL1112190023', 1141, 0, 30000, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(587, '2019-12-11', 'JL1112190023', 1111, 18000, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(588, '2019-12-11', 'JL1112190023', 4101, 0, 18000, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(589, '2019-12-11', 'JL1112190023', 5101, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(590, '2019-12-11', 'JL1112190023', 1141, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(591, '2019-12-11', 'JL1112190023', 1111, 20000, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(592, '2019-12-11', 'JL1112190023', 4101, 0, 20000, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(593, '2019-12-11', 'JL1112190023', 5101, 18000, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(594, '2019-12-11', 'JL1112190023', 1141, 0, 18000, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(595, '2019-12-11', 'JL1112190023', 1111, 13500, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(596, '2019-12-11', 'JL1112190023', 4101, 0, 13500, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(597, '2019-12-11', 'JL1112190023', 5101, 10000, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(598, '2019-12-11', 'JL1112190023', 1141, 0, 10000, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(599, '2019-12-11', 'JL1112190023', 1111, 15000, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(600, '2019-12-11', 'JL1112190023', 4101, 0, 15000, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(601, '2019-12-11', 'JL1112190023', 5101, 12500, 0, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(602, '2019-12-11', 'JL1112190023', 1141, 0, 12500, 'Penjualan Dengan No.Faktur : 1112190023', 6),
(603, '2019-12-11', 'BL111219000014', 1141, 190000, 0, 'Pembelian Dengan No.Faktur : BL111219000014', 6),
(604, '2019-12-11', 'UT-BL111219000014', 2101, 0, 190000, 'Hutang Pembelian Dengan No.Faktur : BL111219000014', 6),
(605, '2019-12-11', 'JL1112190024', 1111, 90000, 0, 'Penjualan Dengan No.Faktur : 1112190024', 6),
(606, '2019-12-11', 'JL1112190024', 4101, 0, 90000, 'Penjualan Dengan No.Faktur : 1112190024', 6),
(607, '2019-12-11', 'JL1112190024', 5101, 75000, 0, 'Penjualan Dengan No.Faktur : 1112190024', 6),
(608, '2019-12-11', 'JL1112190024', 1141, 0, 75000, 'Penjualan Dengan No.Faktur : 1112190024', 6),
(609, '2019-12-11', 'JL1112190024', 1111, 22000, 0, 'Penjualan Dengan No.Faktur : 1112190024', 6),
(610, '2019-12-11', 'JL1112190024', 4101, 0, 22000, 'Penjualan Dengan No.Faktur : 1112190024', 6),
(611, '2019-12-11', 'JL1112190024', 5101, 20000, 0, 'Penjualan Dengan No.Faktur : 1112190024', 6),
(612, '2019-12-11', 'JL1112190024', 1141, 0, 20000, 'Penjualan Dengan No.Faktur : 1112190024', 6),
(613, '2019-12-11', 'JL1112190025', 1111, 40000, 0, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(614, '2019-12-11', 'JL1112190025', 4101, 0, 40000, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(615, '2019-12-11', 'JL1112190025', 5101, 37500, 0, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(616, '2019-12-11', 'JL1112190025', 1141, 0, 37500, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(617, '2019-12-11', 'JL1112190025', 1111, 90000, 0, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(618, '2019-12-11', 'JL1112190025', 4101, 0, 90000, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(619, '2019-12-11', 'JL1112190025', 5101, 75000, 0, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(620, '2019-12-11', 'JL1112190025', 1141, 0, 75000, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(621, '2019-12-11', 'JL1112190025', 1111, 125000, 0, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(622, '2019-12-11', 'JL1112190025', 4101, 0, 125000, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(623, '2019-12-11', 'JL1112190025', 5101, 110000, 0, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(624, '2019-12-11', 'JL1112190025', 1141, 0, 110000, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(625, '2019-12-11', 'JL1112190025', 1111, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(626, '2019-12-11', 'JL1112190025', 4101, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(627, '2019-12-11', 'JL1112190025', 5101, 5000, 0, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(628, '2019-12-11', 'JL1112190025', 1141, 0, 5000, 'Penjualan Dengan No.Faktur : 1112190025', 6),
(629, '2019-12-11', 'JL1112190026', 1111, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(630, '2019-12-11', 'JL1112190026', 4101, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(631, '2019-12-11', 'JL1112190026', 5101, 5000, 0, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(632, '2019-12-11', 'JL1112190026', 1141, 0, 5000, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(633, '2019-12-11', 'JL1112190026', 1111, 7000, 0, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(634, '2019-12-11', 'JL1112190026', 4101, 0, 7000, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(635, '2019-12-11', 'JL1112190026', 5101, 5000, 0, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(636, '2019-12-11', 'JL1112190026', 1141, 0, 5000, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(637, '2019-12-11', 'JL1112190026', 1111, 6000, 0, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(638, '2019-12-11', 'JL1112190026', 4101, 0, 6000, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(639, '2019-12-11', 'JL1112190026', 5101, 5000, 0, 'Penjualan Dengan No.Faktur : 1112190026', 6),
(640, '2019-12-11', 'JL1112190026', 1141, 0, 5000, 'Penjualan Dengan No.Faktur : 1112190026', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kaskeluar`
--

CREATE TABLE `tb_kaskeluar` (
  `id` int(11) NOT NULL,
  `kdbukti` varchar(15) NOT NULL,
  `kode_akun` int(11) NOT NULL,
  `ket` varchar(250) NOT NULL,
  `jumlah` int(25) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasmasuk`
--

CREATE TABLE `tb_kasmasuk` (
  `id` int(11) NOT NULL,
  `kdbukti` varchar(15) NOT NULL,
  `kode_akun` int(11) NOT NULL,
  `ket` varchar(250) NOT NULL,
  `jumlah` int(25) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_katagori`
--

CREATE TABLE `tb_katagori` (
  `id_katagori` int(10) NOT NULL,
  `nma_katagori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_katagori`
--

INSERT INTO `tb_katagori` (`id_katagori`, `nma_katagori`) VALUES
(3, 'Minuman'),
(4, 'Kesehatan'),
(7, 'Makanan Ringan'),
(9, 'Titip'),
(10, 'Makanan'),
(11, 'Kripik');

-- --------------------------------------------------------

--
-- Table structure for table `tb_lain`
--

CREATE TABLE `tb_lain` (
  `id` int(10) NOT NULL,
  `Tgl` date NOT NULL,
  `no_bukti` double NOT NULL,
  `uraian` text NOT NULL,
  `penerimaan` int(20) NOT NULL,
  `pengeluaran` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_lvl`
--

CREATE TABLE `tb_lvl` (
  `id_lvl` int(10) NOT NULL,
  `level` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lvl`
--

INSERT INTO `tb_lvl` (`id_lvl`, `level`) VALUES
(1, 'Superadmin'),
(2, 'Kasir'),
(3, 'Gudang'),
(5, 'Developer');

-- --------------------------------------------------------

--
-- Table structure for table `tb_member`
--

CREATE TABLE `tb_member` (
  `id_member` int(15) NOT NULL,
  `Kode_member` varchar(50) NOT NULL,
  `Nama` varchar(30) NOT NULL,
  `Nohp` varchar(15) NOT NULL,
  `Alamat` text NOT NULL,
  `id_kategori` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_modal`
--

CREATE TABLE `tb_modal` (
  `id_modal` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `modal` int(20) NOT NULL,
  `id_user` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_modal`
--

INSERT INTO `tb_modal` (`id_modal`, `tanggal`, `modal`, `id_user`) VALUES
(1, '2019-10-22', 500000, 9),
(2, '2019-10-22', 1000000, 6),
(3, '2019-10-24', 500000, 6),
(4, '2019-10-24', 100000, 10),
(5, '2019-11-28', 100000, 6),
(6, '2019-12-05', 150000, 6),
(7, '2019-12-06', 32000, 6),
(8, '2019-12-10', 11500, 6),
(9, '2019-12-11', 21000, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `id_pembayaran` int(10) NOT NULL,
  `nama_pembayaran` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`id_pembayaran`, `nama_pembayaran`) VALUES
(1, 'Lunas'),
(2, 'Hutang');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembelian`
--

CREATE TABLE `tb_pembelian` (
  `id_p` int(11) NOT NULL,
  `id_pembelian` varchar(15) NOT NULL,
  `no_faktur` varchar(25) NOT NULL,
  `id_supplier` int(10) NOT NULL,
  `tgl_beli` date NOT NULL,
  `jam_pembelian` time NOT NULL,
  `id_user` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pembelian`
--

INSERT INTO `tb_pembelian` (`id_p`, `id_pembelian`, `no_faktur`, `id_supplier`, `tgl_beli`, `jam_pembelian`, `id_user`) VALUES
(7, 'BL041219000001', '001', 3, '2019-12-04', '13:36:12', 6),
(8, 'BL041219000002', '002', 4, '2019-12-04', '13:40:45', 6),
(9, 'BL041219000003', '003', 2, '2019-12-04', '13:49:13', 6),
(10, 'BL041219000004', '003', 2, '2019-12-04', '13:58:19', 6),
(11, 'BL041219000005', '004', 2, '2019-12-04', '14:10:10', 6),
(12, 'BL041219000006', '005', 5, '2019-12-04', '14:11:42', 6),
(13, 'BL041219000007', '006', 2, '2019-12-04', '14:14:15', 6),
(14, 'BL041219000008', '006', 2, '2019-12-04', '14:41:39', 6),
(15, 'BL051219000001', '006', 2, '2019-12-05', '10:27:14', 6),
(16, 'BL051219000002', '009', 2, '2019-12-05', '14:20:07', 6),
(17, 'BL051219000003', '010', 6, '2019-12-05', '15:18:52', 6),
(18, 'BL051219000004', '011', 7, '2019-12-05', '15:36:46', 6),
(19, 'BL061219000001', '013', 2, '2019-12-06', '08:23:17', 6),
(20, 'BL061219000002', '012', 2, '2019-12-06', '09:53:10', 6),
(21, 'BL061219000003', '012', 2, '2019-12-06', '09:56:01', 6),
(22, 'BL061219000004', '012', 2, '2019-12-06', '09:57:52', 6),
(23, 'BL091219000001', '014', 2, '2019-12-09', '11:18:58', 6),
(24, 'BL091219000002', '014', 2, '2019-12-09', '11:24:17', 6),
(25, 'BL091219000003', '014', 2, '2019-12-09', '11:30:17', 6),
(26, 'BL091219000004', '015', 8, '2019-12-09', '12:53:03', 6),
(27, 'BL091219000005', '014', 2, '2019-12-09', '12:54:28', 6),
(28, 'BL091219000006', '014', 2, '2019-12-09', '12:57:39', 6),
(29, 'BL101219000001', '016', 2, '2019-12-10', '10:13:52', 6),
(30, 'BL101219000002', '017', 2, '2019-12-10', '10:22:30', 6),
(31, 'BL101219000003', '014', 2, '2019-12-10', '10:24:05', 6),
(32, 'BL101219000004', '014', 2, '2019-12-10', '12:03:26', 6),
(33, 'BL101219000005', '0108', 2, '2019-12-10', '12:42:45', 6),
(34, 'BL101219000006', '019', 2, '2019-12-10', '13:43:30', 6),
(35, 'BL101219000007', '019', 2, '2019-12-10', '13:59:26', 6),
(36, 'BL101219000008', '019', 2, '2019-12-10', '14:03:44', 6),
(37, 'BL101219000009', '019', 2, '2019-12-10', '14:06:17', 6),
(38, 'BL101219000010', '020', 6, '2019-12-10', '14:34:37', 6),
(39, 'BL101219000011', '020', 6, '2019-12-10', '14:36:24', 6),
(40, 'BL101219000012', '021', 9, '2019-12-10', '14:55:28', 6),
(41, 'BL111219000001', '022', 8, '2019-12-11', '07:56:37', 6),
(42, 'BL111219000002', '022', 8, '2019-12-11', '08:14:11', 6),
(43, 'BL111219000003', '023', 1, '2019-12-11', '08:39:34', 6),
(44, 'BL111219000004', '024', 7, '2019-12-11', '08:42:21', 6),
(45, 'BL111219000005', '024', 7, '2019-12-11', '08:44:05', 6),
(46, 'BL111219000006', '024', 7, '2019-12-11', '08:47:37', 6),
(47, 'BL111219000007', '022', 8, '2019-12-11', '08:51:04', 6),
(48, 'BL111219000008', '022', 8, '2019-12-11', '08:52:19', 6),
(49, 'BL111219000009', '025', 10, '2019-12-11', '08:58:35', 6),
(50, 'BL111219000010', '025', 10, '2019-12-11', '09:02:15', 6),
(51, 'BL111219000011', '027', 2, '2019-12-11', '10:00:40', 6),
(52, 'BL111219000012', '027', 1, '2019-12-11', '11:58:23', 6),
(53, 'BL111219000013', '029', 10, '2019-12-11', '13:12:23', 6),
(54, 'BL111219000014', '027', 7, '2019-12-11', '14:26:32', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengirim`
--

CREATE TABLE `tb_pengirim` (
  `id_pengirim` int(10) NOT NULL,
  `tgl_kirim` date NOT NULL,
  `no_faktur` varchar(25) NOT NULL,
  `id_member` int(15) DEFAULT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengirim`
--

INSERT INTO `tb_pengirim` (`id_pengirim`, `tgl_kirim`, `no_faktur`, `id_member`, `status`) VALUES
(1, '2019-09-07', '0709190006', 11, 'Proses'),
(2, '2019-09-07', '0709190008', 12, 'Proses');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengiriman`
--

CREATE TABLE `tb_pengiriman` (
  `id_pengiriman` int(5) NOT NULL,
  `nama_pengiriman` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengiriman`
--

INSERT INTO `tb_pengiriman` (`id_pengiriman`, `nama_pengiriman`) VALUES
(1, 'Ambil Sendiri'),
(2, 'Kirim');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penjualan`
--

CREATE TABLE `tb_penjualan` (
  `id_penjualan` double NOT NULL,
  `no_faktur` varchar(25) NOT NULL,
  `tgl_penjualan` date NOT NULL,
  `jam_penjualan` time NOT NULL,
  `jmlh_uang` int(20) NOT NULL,
  `total_penjualan` int(15) NOT NULL,
  `kembalian` int(20) NOT NULL,
  `keterangan_jual` varchar(30) NOT NULL,
  `id_member` int(15) NOT NULL,
  `id_user` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penjualan`
--

INSERT INTO `tb_penjualan` (`id_penjualan`, `no_faktur`, `tgl_penjualan`, `jam_penjualan`, `jmlh_uang`, `total_penjualan`, `kembalian`, `keterangan_jual`, `id_member`, `id_user`) VALUES
(7, '0412190001', '2019-12-04', '14:05:10', 122500, 122500, 0, 'Eceran', 0, 6),
(8, '0412190002', '2019-12-04', '14:42:43', 5000, 5000, 0, 'Eceran', 0, 6),
(9, '0412190003', '2019-12-04', '14:46:03', 24000, 24000, 0, 'Eceran', 0, 6),
(10, '0412190004', '2019-12-04', '14:47:14', 17000, 17000, 0, 'Eceran', 0, 6),
(11, '0512190001', '2019-12-05', '08:26:51', 7000, 7000, 0, 'Eceran', 0, 6),
(12, '0512190002', '2019-12-05', '08:32:34', 15000, 15000, 0, 'Eceran', 0, 6),
(13, '0512190003', '2019-12-05', '11:45:52', 50000, 29500, 20500, 'Eceran', 0, 6),
(14, '0512190004', '2019-12-05', '12:52:03', 8000, 8000, 0, 'Eceran', 0, 6),
(15, '0512190005', '2019-12-05', '12:52:37', 16000, 16000, 0, 'Eceran', 0, 6),
(16, '0512190006', '2019-12-05', '12:53:28', 8000, 8000, 0, 'Eceran', 0, 6),
(17, '0512190007', '2019-12-05', '12:53:32', 8000, 8000, 0, 'Eceran', 0, 6),
(18, '0512190008', '2019-12-05', '13:01:39', 8000, 8000, 0, 'Eceran', 0, 6),
(19, '0512190009', '2019-12-05', '15:07:01', 16000, 16000, 0, 'Eceran', 0, 6),
(20, '0612190001', '2019-12-06', '08:24:05', 5000, 5000, 0, 'Eceran', 0, 6),
(21, '0612190002', '2019-12-06', '10:32:46', 10500, 10500, 0, 'Eceran', 0, 6),
(22, '0612190003', '2019-12-06', '10:48:49', 20000, 18000, 2000, 'Eceran', 0, 6),
(23, '0612190004', '2019-12-06', '14:35:57', 8000, 8000, 0, 'Eceran', 0, 6),
(24, '0612190005', '2019-12-06', '15:27:56', 15000, 14000, 1000, 'Eceran', 0, 6),
(25, '0612190006', '2019-12-06', '15:30:56', 17000, 17000, 0, 'Eceran', 0, 6),
(26, '0912190001', '2019-12-09', '12:59:03', 11500, 11500, 0, 'Eceran', 0, 6),
(27, '0912190002', '2019-12-09', '13:30:16', 8000, 8000, 0, 'Eceran', 0, 6),
(28, '0912190003', '2019-12-09', '13:31:04', 8000, 8000, 0, 'Eceran', 0, 6),
(29, '0912190004', '2019-12-09', '13:32:10', 8000, 8000, 0, 'Eceran', 0, 6),
(30, '0912190005', '2019-12-09', '13:33:30', 8000, 8000, 0, 'Eceran', 0, 6),
(31, '1012190001', '2019-12-10', '08:55:10', 22000, 17000, 5000, 'Eceran', 0, 6),
(32, '1012190002', '2019-12-10', '13:11:00', 10000, 10000, 0, 'Eceran', 0, 6),
(33, '1012190003', '2019-12-10', '13:12:21', 100000, 72000, 28000, 'Eceran', 0, 6),
(34, '1012190004', '2019-12-10', '14:14:30', 20000, 14000, 6000, 'Eceran', 0, 6),
(35, '1012190005', '2019-12-10', '14:15:02', 14000, 14000, 0, 'Eceran', 0, 6),
(36, '1012190006', '2019-12-10', '15:16:34', 10000, 10000, 0, 'Eceran', 0, 6),
(37, '1112190001', '2019-12-11', '09:44:29', 220000, 218500, 1500, 'Eceran', 0, 6),
(38, '1112190002', '2019-12-11', '09:45:50', 50000, 39500, 10500, 'Eceran', 0, 6),
(39, '1112190003', '2019-12-11', '09:48:28', 104500, 104500, 0, 'Eceran', 0, 6),
(40, '1112190004', '2019-12-11', '10:03:40', 85000, 85000, 0, 'Eceran', 0, 6),
(41, '1112190005', '2019-12-11', '10:04:58', 10000, 10000, 0, 'Eceran', 0, 6),
(42, '1112190006', '2019-12-11', '10:06:53', 40000, 40000, 0, 'Eceran', 0, 6),
(43, '1112190007', '2019-12-11', '10:10:30', 50000, 24000, 26000, 'Eceran', 0, 6),
(44, '1112190008', '2019-12-11', '10:12:53', 170000, 166000, 4000, 'Eceran', 0, 6),
(45, '1112190009', '2019-12-11', '10:14:08', 50000, 40000, 10000, 'Eceran', 0, 6),
(46, '1112190010', '2019-12-11', '10:23:58', 100000, 75000, 25000, 'Eceran', 0, 6),
(47, '1112190011', '2019-12-11', '10:28:20', 140000, 133000, 7000, 'Eceran', 0, 6),
(48, '1112190012', '2019-12-11', '10:33:03', 155000, 152000, 3000, 'Eceran', 0, 6),
(49, '1112190013', '2019-12-11', '11:00:04', 200000, 180000, 20000, 'Eceran', 0, 6),
(50, '1112190014', '2019-12-11', '11:00:56', 100000, 28000, 72000, 'Eceran', 0, 6),
(51, '1112190015', '2019-12-11', '12:01:11', 100000, 61000, 39000, 'Eceran', 0, 6),
(52, '1112190016', '2019-12-11', '12:07:51', 50000, 41500, 8500, 'Eceran', 0, 6),
(53, '1112190017', '2019-12-11', '12:48:03', 12000, 11000, 1000, 'Eceran', 0, 6),
(54, '1112190018', '2019-12-11', '12:48:38', 10000, 9000, 1000, 'Eceran', 0, 6),
(55, '1112190019', '2019-12-11', '13:10:40', 100000, 16000, 84000, 'Eceran', 0, 6),
(56, '1112190020', '2019-12-11', '13:13:19', 37500, 37500, 0, 'Eceran', 0, 6),
(57, '1112190021', '2019-12-11', '13:22:04', 20500, 20500, 0, 'Eceran', 0, 6),
(58, '1112190022', '2019-12-11', '13:30:37', 50000, 38000, 12000, 'Eceran', 0, 6),
(59, '1112190023', '2019-12-11', '14:16:42', 101500, 101500, 0, 'Eceran', 0, 6),
(60, '1112190024', '2019-12-11', '14:32:44', 112000, 112000, 0, 'Eceran', 0, 6),
(61, '1112190025', '2019-12-11', '15:12:36', 262000, 262000, 0, 'Eceran', 0, 6),
(62, '1112190026', '2019-12-11', '15:20:28', 20000, 20000, 0, 'Eceran', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tb_retur_pembelian`
--

CREATE TABLE `tb_retur_pembelian` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(25) NOT NULL,
  `Kode_barang` varchar(50) NOT NULL,
  `nama` varchar(10) NOT NULL,
  `jumlah` int(15) NOT NULL,
  `harga` int(15) NOT NULL,
  `sub_total` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `id_supplier` int(10) NOT NULL,
  `id_user` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_retur_pembelian`
--
DELIMITER $$
CREATE TRIGGER `update_pembelian` AFTER INSERT ON `tb_retur_pembelian` FOR EACH ROW BEGIN UPDATE tb_detail_pembelian SET jumlah=jumlah-new.jumlah, total=total-new.sub_total WHERE id_pembelian=new.no_faktur and Kode_barang=new.Kode_barang; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_retur_penjualan`
--

CREATE TABLE `tb_retur_penjualan` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(25) NOT NULL,
  `Kode_barang` varchar(50) NOT NULL,
  `nama` varchar(10) NOT NULL,
  `jumlah` int(15) NOT NULL,
  `harga` int(15) NOT NULL,
  `sub_total` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `id_member` int(10) NOT NULL,
  `id_user` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `tb_retur_penjualan`
--
DELIMITER $$
CREATE TRIGGER `OnRepenInsert` BEFORE INSERT ON `tb_retur_penjualan` FOR EACH ROW BEGIN
 UPDATE tb_detail_penjualan SET jumlah=jumlah-new.jumlah, sub_total=sub_total-new.sub_total WHERE no_faktur=new.no_faktur and Kode_barang=new.Kode_barang;
UPDATE tb_penjualan SET total_penjualan=total_penjualan-new.sub_total WHERE no_faktur=new.no_faktur;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_satuan`
--

CREATE TABLE `tb_satuan` (
  `id_satuan` int(10) NOT NULL,
  `nma_satuan` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_satuan`
--

INSERT INTO `tb_satuan` (`id_satuan`, `nma_satuan`) VALUES
(4, 'Stuan'),
(5, 'Bks'),
(6, 'Roll'),
(7, 'Meter'),
(8, 'Pcs'),
(9, 'Karton'),
(10, 'Dus'),
(11, 'Kg'),
(12, 'Cm'),
(13, 'Liter'),
(14, 'Pak'),
(15, 'Biji'),
(16, 'Roll'),
(17, 'Btl');

-- --------------------------------------------------------

--
-- Table structure for table `tb_supplier`
--

CREATE TABLE `tb_supplier` (
  `id_supplier` int(10) NOT NULL,
  `kode_supplier` varchar(50) NOT NULL,
  `nma_supplier` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `notlpn` varchar(15) NOT NULL,
  `cperson` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_supplier`
--

INSERT INTO `tb_supplier` (`id_supplier`, `kode_supplier`, `nma_supplier`, `alamat`, `notlpn`, `cperson`) VALUES
(1, 'ARGMART-SPL-0001', 'Tes', 'Malang', '081123333112', 'Tes'),
(2, 'ARGMART-SPL-0002', 'B heny', 'malang', '0810000', '0815555'),
(3, 'ARGMART-SPL-0003', 'Puri ', 'kanjuruhan', '0101', '0202'),
(4, 'ARGMART-SPL-0004', 'B endang', 'Kanjuruhan ', '0202', '0202'),
(5, 'ARGMART-SPL-0005', 'p Yudi ', 'Malang', '002', '002'),
(6, 'ARGMART-SPL-0006', 'b eunike', 'Fapet', '888', '888'),
(7, 'ARGMART-SPL-0007', 'P ulum ', 'Malang', '081252521567', '081252521567'),
(8, 'ARGMART-SPL-0008', 'b tri ida', 'Fapet', '0303', '0404'),
(9, 'ARGMART-SPL-0009', 'b Dian', 'wagir', '081334094273', '081334094273'),
(10, 'ARGMART-SPL-0010', 'Indah Nada', 'Fapet', '085645793937', '000');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tmpbayar`
--

CREATE TABLE `tb_tmpbayar` (
  `id_tmp` int(11) NOT NULL,
  `id_hutang` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `nominal` int(15) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tmpbayar`
--

INSERT INTO `tb_tmpbayar` (`id_tmp`, `id_hutang`, `id_member`, `nominal`, `date`) VALUES
(9, 61, 10, 122000, '2019-12-11');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jk` enum('Laki - Laki','Perempuan') NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(128) NOT NULL,
  `id_lvl` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama`, `jk`, `nohp`, `alamat`, `username`, `password`, `image`, `id_lvl`) VALUES
(6, 'superadmin', 'Laki - Laki', '089648384838', 'jl Sasando', 'superadmin', '$2y$10$/t58Z1TOcg6jF/U0pS4GCeOSSswdjBFHklmG2x4Yk9YUpMj8DS2du', 'default.jpg', 1),
(9, 'I.Q.Z', 'Laki - Laki', '089680641487', 'Malang Dong', 'developer', '$2y$10$hSGed1IcivytFlk4ZUorU.l0GzyMnC1vm.s/Q1.wCUDgDa.72D54q', '21598.jpg', 5),
(10, 'Kaisr', 'Laki - Laki', '0', 'UNIKAMA', 'kasir', '$2y$10$0U/D7i0ponl03L1Dv2NWlueSxPbmyLEP7D0UwhhvdDs.8aYr1PswS', 'default.jpg', 2),
(11, 'Gudang', 'Laki - Laki', '0', 'UNIKAMA', 'gudang', '$2y$10$ML2Q/4/5Xgl.AHz0aO18/uxRSAlAFyq28Btzz1F5OmCUx9EUTXdU.', 'default.jpg', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_akun`
--
ALTER TABLE `tb_akun`
  ADD PRIMARY KEY (`kode_akun`);

--
-- Indexes for table `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_katagori` (`id_katagori`),
  ADD KEY `id_satuan` (`id_satuan`);

--
-- Indexes for table `tb_barangtitip`
--
ALTER TABLE `tb_barangtitip`
  ADD PRIMARY KEY (`id_barangtitip`);

--
-- Indexes for table `tb_detail_pembelian`
--
ALTER TABLE `tb_detail_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_detail_penjualan`
--
ALTER TABLE `tb_detail_penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_diskon`
--
ALTER TABLE `tb_diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_hutang`
--
ALTER TABLE `tb_hutang`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `tb_hutang_tmp`
--
ALTER TABLE `tb_hutang_tmp`
  ADD PRIMARY KEY (`id_hutang`);

--
-- Indexes for table `tb_jurnal`
--
ALTER TABLE `tb_jurnal`
  ADD PRIMARY KEY (`idjurnal`);

--
-- Indexes for table `tb_kaskeluar`
--
ALTER TABLE `tb_kaskeluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kasmasuk`
--
ALTER TABLE `tb_kasmasuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_katagori`
--
ALTER TABLE `tb_katagori`
  ADD PRIMARY KEY (`id_katagori`);

--
-- Indexes for table `tb_lain`
--
ALTER TABLE `tb_lain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_lvl`
--
ALTER TABLE `tb_lvl`
  ADD PRIMARY KEY (`id_lvl`);

--
-- Indexes for table `tb_member`
--
ALTER TABLE `tb_member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `tb_modal`
--
ALTER TABLE `tb_modal`
  ADD PRIMARY KEY (`id_modal`);

--
-- Indexes for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `tb_pembelian`
--
ALTER TABLE `tb_pembelian`
  ADD PRIMARY KEY (`id_p`);

--
-- Indexes for table `tb_pengirim`
--
ALTER TABLE `tb_pengirim`
  ADD PRIMARY KEY (`id_pengirim`);

--
-- Indexes for table `tb_pengiriman`
--
ALTER TABLE `tb_pengiriman`
  ADD PRIMARY KEY (`id_pengiriman`);

--
-- Indexes for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `tb_retur_pembelian`
--
ALTER TABLE `tb_retur_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_retur_penjualan`
--
ALTER TABLE `tb_retur_penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_satuan`
--
ALTER TABLE `tb_satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `tb_tmpbayar`
--
ALTER TABLE `tb_tmpbayar`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_lvl` (`id_lvl`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_barang`
--
ALTER TABLE `tb_barang`
  MODIFY `id_barang` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT for table `tb_barangtitip`
--
ALTER TABLE `tb_barangtitip`
  MODIFY `id_barangtitip` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_detail_pembelian`
--
ALTER TABLE `tb_detail_pembelian`
  MODIFY `id` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;
--
-- AUTO_INCREMENT for table `tb_detail_penjualan`
--
ALTER TABLE `tb_detail_penjualan`
  MODIFY `id` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `tb_diskon`
--
ALTER TABLE `tb_diskon`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_hutang_tmp`
--
ALTER TABLE `tb_hutang_tmp`
  MODIFY `id_hutang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `tb_jurnal`
--
ALTER TABLE `tb_jurnal`
  MODIFY `idjurnal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=641;
--
-- AUTO_INCREMENT for table `tb_kaskeluar`
--
ALTER TABLE `tb_kaskeluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_kasmasuk`
--
ALTER TABLE `tb_kasmasuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_katagori`
--
ALTER TABLE `tb_katagori`
  MODIFY `id_katagori` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_lain`
--
ALTER TABLE `tb_lain`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_lvl`
--
ALTER TABLE `tb_lvl`
  MODIFY `id_lvl` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_member`
--
ALTER TABLE `tb_member`
  MODIFY `id_member` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_modal`
--
ALTER TABLE `tb_modal`
  MODIFY `id_modal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id_pembayaran` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_pembelian`
--
ALTER TABLE `tb_pembelian`
  MODIFY `id_p` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `tb_pengirim`
--
ALTER TABLE `tb_pengirim`
  MODIFY `id_pengirim` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_pengiriman`
--
ALTER TABLE `tb_pengiriman`
  MODIFY `id_pengiriman` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  MODIFY `id_penjualan` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `tb_retur_pembelian`
--
ALTER TABLE `tb_retur_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_retur_penjualan`
--
ALTER TABLE `tb_retur_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_satuan`
--
ALTER TABLE `tb_satuan`
  MODIFY `id_satuan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  MODIFY `id_supplier` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_tmpbayar`
--
ALTER TABLE `tb_tmpbayar`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

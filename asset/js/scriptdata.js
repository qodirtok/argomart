$(document).ready(function () {
	$('#mydata').DataTable();
});


$(function () {
	$('#jml_uang').on("input", function () {
		var total = $('#total').val();
		var jumuang = $('#jml_uang').val();
		var hsl = jumuang.replace(/[^\d]/g, "");
		$('#jml_uang2').val(hsl);
		var kembali = hsl - total;
		$('#kembalian').val(kembali);
	})

});

$(function () {
	$('.jml_uang').priceFormat({
		prefix: '',
		//centsSeparator: '',
		centsLimit: 0,
		thousandsSeparator: ','
	});
	$('#jml_uang2').priceFormat({
		prefix: '',
		//centsSeparator: '',
		centsLimit: 0,
		thousandsSeparator: ''
	});
	$('#kembalian').priceFormat({
		prefix: '',
		//centsSeparator: '',
		centsLimit: 0,
		thousandsSeparator: ','
	});
	$('.harjul').priceFormat({
		prefix: '',
		//centsSeparator: '',
		centsLimit: 0,
		thousandsSeparator: ','
	});
});
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function () {
	var fileName = $(this).val().split("\\").pop();
	$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$(document).ready(function () {
	//Ajax kabupaten/kota insert
	$("#kode_brg").focus();
	$("#kode_brg").on("input", function () {
		var kobar = {
			kode_brg: $(this).val()
		};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() . 'transaksi/get_barang'; ?>",
			data: kobar,
			success: function (msg) {
				$('#detail_barang').html(msg);
			}
		});
	});
	$("#kode_brg").keypress(function (e) {
		if (e.which == 13) {
			$("#jumlah").focus();
		}
	});
});

$('#btnadd').on('click', function () {
	var lastRow = $('#table-row tbody tr:first').html();
	//alert(lastRow);
	$('#table-row tbody').append('<tr>' + lastRow + '</tr>');
	$('#table-row tbody tr:first input').val('');
	$("#kode").focus();
});
// Delete row on click in the table
$('#table-row').on('click', 'tr a', function (e) {
	var lenRow = $('#table-row tbody tr').length;
	e.preventDefault();
	if (lenRow == 1 || lenRow <= 1) {
		alert("Can't remove all row!");
	} else {
		$(this).parents('tr').remove();
	}
});

$(function () {

	$(document).on("click", "td", function () {
		$(this).find("span[class~='caption']").hide();
		$(this).find("input[class~='editor']").fadeIn().focus();
	});

	$(document).on("keydown", ".editor", function (e) {
		if (e.keyCode == 13) {
			var target = $(e.target);
			var value = target.val();
			var id = target.attr("data-id");
			var data = {
				id: id,
				value: value
			};
			if (target.is(".field-jumlah")) {
				data.modul = "jumlah";
			}

			$.ajax({
				type: "POST",
				url: "<?php echo base_url('transaksi/updatejmlh'); ?>",
				data: data,
				success: function (a) {
					target.hide();
					target.siblings("span[class~='caption']").html(value).fadeIn();
				}

			})

		}

	});

});


$(document).ready(function () {
	$('#kode').on('input', function () {

		var kode = $(this).val();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('kaskeluar/getakun') ?>",
			dataType: "JSON",
			data: {
				kode: kode
			},
			cache: false,
			success: function (data) {
				$.each(data, function (kode_akun, nama_akun) {
					$('[name="nama[]"]').val(data.nama_akun);
					// $('[name="harga"]').val(data.harga);
					// $('[name="satuan"]').val(data.satuan);

				});

			}
		});
		return false;
	});
});

$(document).ready(function () {
	$(".btn1").click(function () {
		$("p").hide();
	});

	$(".btn2").click(function () {

		$("p").show();

	});

});

function myFunction() {
	var x = document.getElementById("myDIV");
	if (x.style.display === "block") {
		x.style.display = "none";
	} else {
		x.style.display = "block";
	}
}
$(document).ready(function () {
	$('#id_karyawan').change(function () {
		var id_karyawan = $("#id_karyawan").val();
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: base_url + 'karyawan/data_karyawan_inactive/select_karyawan_ajax/' + id_karyawan,
			success: function (data) {
				alert(data);
			},
			error: function () {
				alert('error');
			}
		});
	});
});

